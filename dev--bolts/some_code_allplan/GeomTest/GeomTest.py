# ***************************************************************************
# *                                                                         *
# *   Copyright (c) 2015 - Bernd Hahnebach <bernd@bimstatik.org>            *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************


import NemAll_Python_Geometry as Geo
import NemAll_Python_Elements as Ele
import NemAll_Python_Utility as Util
import GeometryValidate as Validate


def check_allplan_version(build_ele, version):
    return True

def create_element(build_ele, doc):

    #----------------- CommonProperties
    com_prop = Ele.CommonProperties()
    com_prop.GetGlobalProperties()

    #------------------ objekt list
    model_ele_list = []

 
    # GeomNut
    params = {}
    params['s'] = 100  # width
    params['m_max'] = 10  # thickness
    params['d1'] = 80  # hole diameter
    # nut_solid = GeomNut(params).Shape
    # model_ele_list.append(Ele.ModelElement3D(com_prop, nut_solid))

    # GeomHexScrew
    params = {}
    params['e'] = 100  # head width
    params['k'] = 10  # head thickness
    params['l'] = 180  # length mit oder ohne kopf
    params['d1'] = 80  # screw diameter
    # hexscrew_solid = GeomHexScrew(params).Shape
    # model_ele_list.append(Ele.ModelElement3D(com_prop, hexscrew_solid))

    # GeomProfileRolledIBeam
    params = {}
    params['h'] = 800
    params['b' ] = 150
    params['tf'] = 20
    params['tw'] = 5
    params ['r'] = 10
    params['l'] = 1000
    # ibeam = GeomProfileRolledIBeam(params).Shape
    # model_ele_list.append(Ele.ModelElement3D(com_prop, ibeam))

    # GeomProfileRolledIBeam
    od = params['D'] = 100
    t = params['t'] = 10
    len = params['l']= 1000
    circle_hollow = GeomProfileCircleHollow(params).Shape
    model_ele_list.append(Ele.ModelElement3D(com_prop, circle_hollow))


    # model_ele_list.append(Ele.ModelElement3D(com_prop, make_extruded_solid()))

    return (model_ele_list, [])


############################################################################################################
# spielwiese
def make_extruded_solid():
    h = 300
    b = 150
    len = 1000

    pol = Geo.Polygon3D()
    pol += Geo.Point3D(0, 0, 0)
    pol += Geo.Point3D(b,  0, 0)
    pol += Geo.Point3D(b,  h,    0)
    pol += Geo.Point3D(0,  h,    0)
    pol += Geo.Point3D(0, 0, 0)

    area = Geo.PolygonalArea3D()
    area += pol
    solid = Geo.ExtrudedAreaSolid3D()
    solid.SetDirection(Geo.Vector3D(0, 0, len))
    #solid.SetRefPoint(Geo.Point3D(0, 0, 0))
    solid.SetExtrudedArea(area)

    err , polyhedron = Geo.CreatePolyhedron(solid)
    if err == Geo.eGeometryErrorCode.eOK:
        print('Extrusion without Error')

    return polyhedron


############################################################################################################
############################################################################################################
# Klassen und helper sind 1zu1 Kopien aus den BOLTS Allplan Modulen
# Ein neues Modul in BOLTS initialisieren, irgendeine shape erstellen, die noetigen params aufschreiben
# hier die neue Geometrie entwickeln, dann nur kopieren.

# classes
class GeomNut():
    def __init__(self, params):
        print('Init of Allplan nuts class GeomNut')

        # set params
        w = params['s']  # width
        t = params['m_max']  # thickness
        d = params['d1']  # hole diameter

        # build geometry
        self.Shape = make_nut_solid(w, t, d)


class GeomHexScrew():
    def __init__(self, params):
        print('Init of Allplan nuts class GeomHexScrew')

        # set params
        w = params['e']  # head width
        t = params['k']  # head thickness
        l = params['l']  # length mit oder ohne kopf
        d = params['d1']  # screw diameter

        # build geometry
        self.Shape = make_screw_solid(w, t, l, d)


class GeomProfileRolledIBeam():
    def __init__(self, params):
        print('Init of Allplan class: ' + self.__class__.__name__)

        # set params
        print(params)
        h = params['h']
        b = params['b']
        tf = params['tf']
        tw = params['tw']
        r = params ['r']
        len = params['l']

        # build geometry
        self.Shape = make_ibeam_solid(h, b, tf, tw, r, len)


class GeomProfileCircleHollow():
    def __init__(self, params):
        print('Init of Allplan class: ' + self.__class__.__name__)

        # set params
        print(params)
        od = params['D']  # outer diameter
        t = params['t']  # thickness
        len = params['l']  # profile length

        # build geometry
        self.Shape = make_circle_hollow_solid(od, t, len)


def make_circle_hollow_solid(od, t, len):
    id = od - t

    origin = Geo.AxisPlacement3D(Geo.Point3D(0, 0, 0), Geo.Vector3D(0, 1, 0), Geo.Vector3D(0, 0, 1))
    outer_cylinder = Geo.BRep3D.CreateCylinder(origin, 0.5 * od,  len)
    inner_cylinder = Geo.BRep3D.CreateCylinder(origin, 0.5 * id,  len)

    #------------------ Subtraction
    err, circle_hollow = Geo.MakeSubtraction(outer_cylinder, inner_cylinder)
    #if Validate.circle_hollow(err) and circle_hollow.IsValid():
    #    print('circle_hollow without Error')
    # siehe nut errorpruefung dort steht polyhedron ... 

    return circle_hollow


def make_ibeam_solid(h, b, tf, tw, r, len):
    pol = Geo.Polygon3D()
    # rectangular without fillet
    pol = Geo.Polygon3D()
    # starting at the left web fillet, off the lower flanch, going against clockwise
    pol += Geo.Point3D(-tw/2, -h/2+tf, 0)
    pol += Geo.Point3D(-b/2,  -h/2+tf, 0)
    pol += Geo.Point3D(-b/2,  -h/2,    0)
    pol += Geo.Point3D( b/2,  -h/2,    0)
    pol += Geo.Point3D( b/2,  -h/2+tf, 0)
    pol += Geo.Point3D( tw/2, -h/2+tf, 0)
    pol += Geo.Point3D( tw/2,  h/2-tf, 0)
    pol += Geo.Point3D( b/2,   h/2-tf ,0)
    pol += Geo.Point3D( b/2,   h/2 ,   0)
    pol += Geo.Point3D(-b/2,   h/2 ,   0)
    pol += Geo.Point3D(-b/2,   h/2-tf ,0)
    pol += Geo.Point3D(-tw/2,  h/2-tf ,0)
    pol += Geo.Point3D(-tw/2, -h/2+tf, 0)

    area = Geo.PolygonalArea3D()
    area += pol
    solid = Geo.ExtrudedAreaSolid3D()
    solid.SetDirection(Geo.Vector3D(0, 0, len))
    #solid.SetRefPoint(Geo.Point3D(0, 0, 0))
    solid.SetExtrudedArea(area)

    err , polyhedron = Geo.CreatePolyhedron(solid)
    if err == Geo.eGeometryErrorCode.eOK:
        print('IBeam without Error')

    return polyhedron

    
def make_screw_solid(w, t, l, d):
    head_width = w
    head_thickness = t
    bolt_shank_lenght = l
    screw_diameter = d

    #----------------- bolt_head
    bolt_head = make_hexagon_solid(head_width, head_thickness)

    #----------------- bolt_shank
    # origin_bolt_shank = Geo.AxisPlacement3D(Geo.Point3D(0, 0, -bolt_shank_lenght),
    origin_bolt_shank = Geo.AxisPlacement3D(Geo.Point3D(0, 0, 0),
                                                    Geo.Vector3D(0, 1, 0),
                                                    Geo.Vector3D(0, 0, 1))
    bolt_shank = Geo.BRep3D.CreateCylinder(origin_bolt_shank, 0.5 * screw_diameter,  bolt_shank_lenght)

    #------------------ union of bolt_head and bolt_shank
    err, screw = Geo.MakeUnion(bolt_head, bolt_shank)
    if Validate.polyhedron(err) and screw.IsValid():
        print('Screw without Error')
        
    return screw


def make_nut_solid(w, t, d):
    screw_diameter = d
    nut_width = w
    nut_thickness = t

    #----------------- nut_without_hole
    nut_without_hole = make_hexagon_solid(nut_width, nut_thickness)

    #----------------- hole
    origin = Geo.AxisPlacement3D(Geo.Point3D(0, 0, -nut_thickness),
                                                    Geo.Vector3D(0, 1, 0),
                                                    Geo.Vector3D(0, 0, 1))
    nut_hole = Geo.BRep3D.CreateCylinder(origin, 0.5 * screw_diameter,  3 * nut_thickness)

    #------------------ Subtraction
    err, nut = Geo.MakeSubtraction(nut_without_hole, nut_hole)
    if Validate.polyhedron(err) and nut.IsValid():
        print('Nut without Error')

    return nut


def make_hexagon_solid(a, t):
    import math
    origin = Geo.AxisPlacement3D(Geo.Point3D(0, 0, 0),
                                                    Geo.Vector3D(0, 1, 0),
                                                    Geo.Vector3D(0, 0, 1))

    d = a / math.sqrt(3)
    # a .. width
    # d .. inner hexagon diameter

    solid1 = Geo.BRep3D.CreateCuboid(origin, a, d, t)
    translation = Geo.Matrix3D()
    translation.Translate(Geo.Vector3D(0.5 * d, -0.5 * a, 0))
    solid1 = Geo.Transform(solid1, translation)

    rotation = Geo.Matrix3D()
    rotation_axis = Geo.Line3D(Geo.Point3D(0,0,0),Geo.Point3D(0,0,1))
    rotation_angle = Geo.Angle(math.pi / 3.0)
    rotation.Rotation(rotation_axis, rotation_angle)
    solid2 = Geo.Transform(solid1, rotation)

    rotation = Geo.Matrix3D()
    rotation_axis = Geo.Line3D(Geo.Point3D(0,0,0),Geo.Point3D(0,0,1))
    rotation_angle = Geo.Angle(math.pi * 2 / 3.0)
    rotation.Rotation(rotation_axis, rotation_angle)
    solid3 = Geo.Transform(solid1, rotation)

    # union of solids
    hexagon_solid = Geo.MakeUnion(solid1, solid2)[1]
    hexagon_solid = Geo.MakeUnion(hexagon_solid, solid3)[1]

    return hexagon_solid
