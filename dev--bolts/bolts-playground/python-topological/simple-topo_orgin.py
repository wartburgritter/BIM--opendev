from FreeCAD import Vector
import Part

V1 = Vector(0,10,0)
V2 = Vector(30,10,0)
V3 = Vector(30,-10,0)
V4 = Vector(0,-10,0)
VC1 = Vector(-10,0,0)
VC2 = Vector(40,0,0)

C1 = Part.Arc(V1,VC1,V4)
C2 = Part.Arc(V2,VC2,V3)
L1 = Part.Line(V1,V2)
L2 = Part.Line(V4,V3)

S1 = Part.Shape([C1,C2,L1,L2])
W = Part.Wire(S1.Edges)
F = Part.Face(W)
P = F.extrude(Vector(0,0,10))
Part.show(P)
