from FreeCAD import Vector
import Draft

V1 = Vector(0,10,0)
V2 = Vector(30,10,0)
V3 = Vector(30,-10,0)
V4 = Vector(0,-10,0)
VC3 = Vector(0,0,0)
VC4 = Vector(30,0,0)


pl=FreeCAD.Placement()
pl.Base = VC3
C1 = Draft.makeCircle(r,placement=pl,startangle=90,endangle=270)
pl.Base = VC4
C2 = Draft.makeCircle(r,placement=pl,startangle=270,endangle=90)

L1 = Draft.makeLine(V1,V2)
L2 = Draft.makeLine(V4,V3)


W = Draft.upgrade([C1,C2,L1,L2], delete=True)
#P = W.extrude(Vector(0,0,10))     # direct extrusion of the wire misses one circle

#F = Draft.upgrade(W, delete=True) # upgrade to a face does not work
#P = F.extrude(Vector(0,0,10))




