from FreeCAD import Vector
import Part

V1 = Vector(0,10,0)
V2 = Vector(30,10,0)
V3 = Vector(30,-10,0)
V4 = Vector(0,-10,0)
VC1 = Vector(0,0,0)
VC2 = Vector(30,0,0)

normal = Vector(0,0,1)

r = 10
C1 = Part.makeCircle(r,VC1,normal,90,270)
C2 = Part.makeCircle(r,VC2,normal,270,90)

L1 = Part.makeLine(V1,V2)
L2 = Part.makeLine(V3,V4)

W = Part.Wire([L1,C1,L2,C2])
F = Part.Face(W)
P = F.extrude(Vector(0,0,10))
Part.show(P)