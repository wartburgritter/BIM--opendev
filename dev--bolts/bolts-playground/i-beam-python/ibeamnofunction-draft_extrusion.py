from FreeCAD import Vector
import Draft



h=270
b=248
tf=32
tw=18
r=21
l=1000


PLF    =  [Vector((-tw/2-r),(-h/2+tf),0),
           Vector(-b/2,(-h/2+tf),0),
           Vector(-b/2,-h/2,0), 
           Vector(b/2,-h/2,0), 
           Vector(b/2,(-h/2+tf),0),
           Vector((tw/2+r),(-h/2+tf),0)]
PUF    =  [Vector(tw/2+r,(h/2-tf),0),
           Vector(b/2,(h/2-tf),0),
           Vector(b/2,h/2,0),
           Vector(-b/2,h/2,0),
           Vector(-b/2,(h/2-tf),0),
           Vector((-tw/2-r),(h/2-tf),0)]
LF = Draft.makeWire(PLF)
UF = Draft.makeWire(PUF)


WL  = Draft.makeLine(Vector(-tw/2,(-h/2+tf+r),0), Vector(-tw/2,(h/2-tf-r),0))
WR  = Draft.makeLine(Vector(tw/2,(-h/2+tf+r),0), Vector(tw/2,(h/2-tf-r),0))


pl=FreeCAD.Placement()
pl.Base = Vector((-tw/2-r),(h/2-tf-r))
R1 = Draft.makeCircle(r,placement=pl,startangle=0,endangle=90)
pl.Base = Vector((tw/2+r),(h/2-tf-r))
R2 = Draft.makeCircle(r,placement=pl,startangle=90,endangle=180)
pl.Base = Vector((-tw/2-r),(-h/2+tf+r),0)
R3 = Draft.makeCircle(r,placement=pl,startangle=270,endangle=0)
pl.Base = Vector((tw/2+r),(-h/2+tf+r),0)
R4 = Draft.makeCircle(r,placement=pl,startangle=180,endangle=270)


wbeam = Draft.upgrade([LF, UF, WL, WR, R1, R2, R3, R4], delete=True)

P = wbeam.extrude(Vector(0,0,l))
P = wbeam.extrude(Vector(0,0,l), solid = True)



# gui draft upgradetool um face zu erstellen
# extrudieren



#fbeam = Draft.upgrade([wbeam], delete=True)
#P = F.extrude(Vector(0,0,l))



#App.ActiveDocument.DWire.Shape.ShapeType
#Wire
#App.ActiveDocument.Line.Shape.ShapeType
#Wire
#App.ActiveDocument.Arc.Shape.ShapeType
#Edge







 

