from FreeCAD import Vector
import Part


h=270
b=248
tf=32
tw=18
r=21
l=1000



LF1 = Part.Line( Vector(-b/2,-h/2,0),           Vector(b/2,-h/2,0))
LF2 = Part.Line( Vector(b/2,-h/2,0),            Vector(b/2,(-h/2+tf),0))
LF3 = Part.Line( Vector(b/2,(-h/2+tf),0),       Vector((tw/2+r),(-h/2+tf),0))
LF4 = Part.Line( Vector((-tw/2-r),(-h/2+tf),0), Vector(-b/2,(-h/2+tf),0))
LF5 = Part.Line( Vector(-b/2,(-h/2+tf),0),      Vector(-b/2,-h/2,0))

UF1 = Part.Line( Vector(-b/2,h/2,0),            Vector(b/2,h/2,0))
UF2 = Part.Line( Vector(b/2,h/2,0),             Vector(b/2,(h/2-tf),0))
UF3 = Part.Line( Vector(b/2,(h/2-tf),0),        Vector((tw/2+r),(h/2-tf),0))
UF4 = Part.Line( Vector((-tw/2-r),(h/2-tf),0),  Vector(-b/2,h/2-tf,0))
UF5 = Part.Line( Vector(-b/2,(h/2-tf),0),       Vector(-b/2,h/2,0))

W1 =  Part.Line( Vector(-tw/2,(-h/2+tf+r),0),   Vector(-tw/2,(h/2-tf-r),0))
W2 =  Part.Line( Vector(tw/2,(-h/2+tf+r),0),    Vector(tw/2,(h/2-tf-r),0))

# circles mittels 
R1 = Part.makeCircle(r, Vector((-tw/2-r),(h/2-tf-r),0),  Vector(0,0,1), 0, 90)
R2 = Part.makeCircle(r, Vector((tw/2+r),(h/2-tf-r),0),   Vector(0,0,1), 90, 180)
R3 = Part.makeCircle(r, Vector((-tw/2-r),(-h/2+tf+r),0), Vector(0,0,1), 270, 0)
R4 = Part.makeCircle(r, Vector((tw/2+r),(-h/2+tf+r),0),  Vector(0,0,1), 180,270)


Part.show(R1)
Part.show(R2)
Part.show(R3)
Part.show(R4)


S = Part.Shape([LF1, LF2, LF3, LF4, LF5, UF1, UF2, UF3, UF4, UF5, W1, W2, R1, R2, R3, R4])
#S = Part.Shape([LF1, LF2, LF3, LF4, LF5, UF1, UF2, UF3, UF4, UF5, W1, W2])

Part.show(S)

# gui draft upgradetool um alle zu verknuepfen
# gui draft upgradetool um face zu erstellen
# extrudieren


#W = Part.Wire([S.Edges, R1])
#W = Part.Wire(S.Edges)
#F = Part.Face(W)
#P = F.extrude(Vector(0,0,l))

#Part.show(P)


