from FreeCAD import Vector
from Part import makeBox
import Part
import PartDesign



h=270
b=248
tf=32
tw=18
r=21
l=1000


box1 = makeBox(b,tf,l)
box1.translate(Vector(0,(h-tf),0))
box2 = makeBox(b,tf,l)
box3 = makeBox(tw,(h-2*tf),l)
box3.translate(Vector((b/2),tf,0))
beam =   box1.fuse(box2).fuse(box3)



Part.show(beam)



#part = document.addObject("Part::Feature","BOLTS_part")
#part.Label = name



beam1 = beam.makeFillet(r,[beam.Edges[6]])
beam = beam.makeFillet(r,[beam.Edges[17]])
beam = beam.makeFillet(r,[beam.Edges[23]])
beam = beam.makeFillet(r,[beam.Edges[33])

#gui edges
#6  17 23 33



# in pythenlist der edges nummerierung faengt bei null an!!!

