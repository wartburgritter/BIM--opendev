cd /home/hugo/Documents/dev/bolts/BOLTS/
python3 bolts.py export freecad
python3 bolts.py test freecad

grep -r --exclude='*.pyc' --exclude='*.po' --exclude='*.html'  Parameters

# ************************************************************************************************
# aktuelle entwicklung
import BOLTS as bolts
bolts.add_part_by_name('Profile8', {'type': '40x40', 'l': 5})
bolts.add_part_by_name('Profile8', {'type': '40x40light', 'l': 5})
bolts.add_part_by_name('Profile8', {'type': '40x40E', 'l': 5})

bolts.add_part_by_name('HEBProfile', {'type': 'HEB500'})


# ************************************************************************************************
# informations

# the repo, only one where all is in
# collection ... one blt-file is one collection
# class ... a collection nomally has various class, class is unique
# standard ... a class has one standard, a standard has one class

# a BOLTS part is characterised by three layer (instead of Class the standard could be taken too)
# CollectionID --> nut
# ClassID --> hexagonnut1
# Key --> M3


# but what happens if a standard is defined on two classes?
# an error on creating backend will be raised!


# ************************************************************************************************
import BOLTS as bolts

cl = bolts.repo.class_names.get_src(bolts.repo.names['HEBProfile'])

from BOLTS.bolttools import freecad
base = freecad.FreeCADData(bolts.repo).base_classes.get_src(cl)
coll = bolts.repo.collection_classes.get_src(cl)


name = bolts.repo.names['HEBProfile']
cl = bolts.repo.class_names.get_src(name)

name = bolts.repo.names['HEBProfile']
cl = bolts.repo.class_names.get_src(name)
bolts._get_free_in_params(cl)


cl = bolts.repo.class_names.get_src(name)


for cl in bolts.repo.iterclasses():
    print(cl[0].id)

for cl in bolts.repo.iterclasses():
    print('{} --> {}'.format(cl[0].id, bolts._get_free_in_params(cl[0])))
    # da muss in BOLTS daten ein error sein ... 


# aus name die id:
name = bolts.repo.names['HEBProfile']
cl = bolts.repo.class_names.get_src(name)
cl.id

for n in bolts.get_names():
    name = bolts.repo.names[n]
    cl = bolts.repo.class_names.get_src(name)
    print('{} --> {}'.format(cl.id, n))


# aus id den namen:
classid = 'tslot20x20'
for n in bolts.get_names():
    name = bolts.repo.names[n]
    cl = bolts.repo.class_names.get_src(name)
    if classid == cl.id:
        print(n)

bolts.get_name('tslot20x20')


# aus standard die id:
standard = bolts.repo.standards['DIN1025_2']
cl = bolts.repo.class_standards.get_src(standard)
cl.id


# aus id den standard:
for s in bolts.get_standards():
    standard = bolts.repo.standards[s]
    cl = bolts.repo.class_standards.get_src(standard)
    print('{} --> {}'.format(cl.id, s))

bolts.get_standard('ibeam_heb')


# get missing Params
import BOLTS as bolts
cl = bolts.repo.class_names.get_src(bolts.repo.names['HEBProfile'])
params = {'type': 'HEB500'} 
bolts._add_missing_inparams(cl, params)


# ************************************************************************************************
# add parts by Python
# make a new Document

import BOLTS as bolts

bolts.add_part_by_name('HEAProfile')  # will use the default parameter from blt file

bolts.add_part_by_classid('ibeam_heb', {'type': 'HEB500'})
bolts.add_part_by_name('HEBProfile', {'type': 'HEB500'})
bolts.add_part_by_standard('DIN1025_2', {'type': 'HEB500'})

bolts.add_part_by_classid('ibeam_heb', {'type': 'HEB500', 'name': 'my_profile'})
bolts.add_part_by_name('HEBProfile', {'type': 'HEB500', 'name': 'my_profile'})
bolts.add_part_by_standard('DIN1025_2', {'type': 'HEB500', 'name': 'my_profile'})

bolts.print_names()
bolts.print_standards()
bolts.get_default_params_by_name('HEBProfile')
bolts.get_default_params_by_name('V_slot20x80mm')
bolts.get_default_params_by_standard('DIN1025_2')
bolts.get_default_params_by_standard('DIN933')


bolts.add_part_by_name('HEBProfile', {'type': 'HEB500', 'l' : 50, 'arch' : False})
bolts.add_part_by_name('HEAProfile', {'type': 'HEA100', 'l' : 1000, 'arch' : False})
bolts.add_part_by_name('IPEProfile', {'type': 'IPE80', 'l' : 1000, 'arch' : False})

bolts.add_part_by_name('TSlotExtrusion20x20')

bolts.add_part_by_name('TSlotExtrusion20x20', {'l': 5})
bolts.add_part_by_name('TSlotExtrusionOneSlot20x20', {'l': 5})
bolts.add_part_by_name('TSlotExtrusionThreeSlots20x20', {'l': 5})
bolts.add_part_by_name('TSlotExtrusionTwoSlots20x20', {'l': 5})
bolts.add_part_by_name('TSlotExtrusionTwoSlotsopp20x20', {'l': 5})

bolts.add_part_by_name('V_slot20x20mm', {'l': 5, 'finish': 'Clear anodized'})
bolts.add_part_by_name('V_slot20x40mm', {'l': 5, 'finish': 'Clear anodized'})
bolts.add_part_by_name('V_slot20x60mm', {'l': 5, 'finish': 'Clear anodized'})
bolts.add_part_by_name('V_slot20x80mm', {'l': 5, 'finish': 'Black'})

bolts.add_part_by_standard('DIN1025_2')  # will use the default parameter from blt file
bolts.add_part_by_standard('DIN1025_2', {'type': 'HEB500', 'l' : 50, 'arch' : False})
bolts.add_part_by_standard('DIN933')
bolts.add_part_by_standard('DIN933', {'key': 'M10', 'l': 120, 'thread_type': 'coarse'})

# either add no params, or all params have to be added
# no error or warning is given if the parameter is not known
# TODO impolement this in the FreeCAD geometry methods
# print a warning if the params dict has some not known key


# ************************************************************************************************
# info zu FreeCAD Geometrieerzeugungsmethoden
# vorbei an BOLTS direkt die Geometrieerzeugungsmethoden aufrufen geht natuerlich auch
# ist aber nicht im Sinne von BOLTS
# https://forum.freecadweb.org/viewtopic.php?f=13&t=17720&start=10#p140124

from BOLTS.freecad.nut.nut import nut1
params = {
    'key': 'M3',
    'name': 'myM3nut',
    'e_min': 6.01,
    's': 5.5,
    'm_max': 2.4,
    'd1': 3.0
}
nut1(params, App.ActiveDocument)


from BOLTS.freecad.profile_i.profile_i import ibeam_parallel_flange as ibeam
params = {
    'type': 'HEM200',
    'name': 'myHEM200',
    'l': 300.0,
    'b': 206.0,
    'h': 220.0,
    'r': 18.0,
    'tw': 15.0,
    'tf': 25.0,
    'arch': False
}
ibeam(params, App.ActiveDocument)


from BOLTS.freecad.profile_hollow.profile_hollow import rectangle_hollow as rrw
params = {
    'name': 'my_test_hollow',
    'h': 100,
    'b': 100,
    't': 5,
    'l': 50,
    'arch': False
}
rrw(params, App.ActiveDocument)


# ************************************************************************************************
# ************************************************************************************************
# 2017 aber angepasst in 2019, aber nicht mehr noetig
# ************************************************************************************************
# https://forum.freecadweb.org/viewtopic.php?f=13&t=17720&start=20#p140248
def add_bolts_part(myboltsclassid, myboltskey):
    import BOLTS
    a_bolts_repo = BOLTS.repo

    # instance of class BOLTS.bolttools.blt.Class 
    a_bolts_class = None
    for c, in a_bolts_repo.iterclasses():
        # print c.id
        if c.id == myboltsclassid:
            a_bolts_class = c

    # instance class BOLTS.bolttools.freecad.BaseFunction
    abase = BOLTS.freecad_db.base_classes.get_src(a_bolts_class)

    # instance of class BOLTS.bolttools.blt.Collection 
    acoll = a_bolts_repo.collection_classes.get_src(a_bolts_class)

    # params
    params = {'key': myboltskey}
    params = a_bolts_class.parameters.collect(params)
    params['name'] = params['key']
    aparams = params
    print(params)

    # doc
    adoc = App.ActiveDocument
    if not adoc:
        adoc = App.newDocument('BOLTS_part')

    # add the part
    import importlib
    if isinstance(abase,BOLTS.bolttools.freecad.BaseFunction):
        module = importlib.import_module("BOLTS.freecad.%s.%s" %
            (acoll.id, abase.module_name))
        # obiges startes bei ersten hinzufuegen eines parts auch die gui
        print(module)
        module.__dict__[abase.name](aparams,adoc)
    else:
        raise  RuntimeError("Unknown base geometry type: %s" % type(base))


myboltsclassid = 'hexagonnut1'
myboltskey = 'M14'
add_bolts_part(myboltsclassid, myboltskey)

myboltsclassid = 'plainwasherchamfered'
myboltskey = 'M27'
add_bolts_part(myboltsclassid, myboltskey)

myboltsclassid = 'singlerowradialbearingimperial'
myboltskey = 'RLS26'
add_bolts_part(myboltsclassid, myboltskey)


# beams sind anders, die haben type, laenge ... 
myboltsclassid = 'ibeam_hem'
myboltskey = 'HEM200'
add_bolts_part(myboltsclassid, myboltskey)


# ************************************************************************************************
# ************************************************************************************************
# ************************************************************************************************
# 2017 und frueher, viel brainstorming und erlaeuterungen ...
# ************************************************************************************************
import BOLTS
for cl, in BOLTS.repo.iterclasses():
    print ''
    try:
        base = BOLTS.freecad_db.base_classes.get_src(cl)
    except:
        print 'problem at base --> ', cl.id

    try:
        coll = BOLTS.repo.collection_classes.get_src(cl)
        params = cl.parameters.union(base.parameters)
        free_params = params.free
        in_params = {}
        for p in free_params:
            p_type = params.types[p]
            default_value = params.defaults[p]
            in_params[p] = default_value

        newparams = cl.parameters.collect(in_params)
        newparams['name'] = 'boltspart'
        #print newparams
        #print vars(cl)
    except:
        print 'problem between base and adding --> ', cl.id

    try:
        BOLTS.boltsgui.add_part(coll,base,newparams,FreeCAD.ActiveDocument)
    except:
        print 'problem at adding --> ', cl.id


# ************************************************************************************************
# list namessafe and classids
import BOLTS
for coll, in BOLTS.repo.itercollections():
    for name,multiname in BOLTS.freecad_db.iternames(['name','multiname'],filter_collection=coll):
        clid = BOLTS.repo.class_names.get_src(name).id
        print(clid + ' --> ' + name.name.get_safe())
        #print(clid + ' --> ' + name.name.get_safe() + ' --> ' + name.name.get_nice())


# ************************************************************************************************
# error im FreeCAD python FIXME
import BOLTS
myname = 'V_slot20x20mm'
name = BOLTS.repo.names[myname]
cl = BOLTS.repo.class_names.get_src(name)
base = BOLTS.freecad_db.base_classes.get_src(cl)
coll = BOLTS.repo.collection_classes.get_src(cl)
params = cl.parameters.union(base.parameters)
free_params = params.free
in_params = {}
for p in free_params:
    p_type = params.types[p]
    default_value = params.defaults[p]
    in_params[p] = default_value

newparams = cl.parameters.collect(in_params)
newparams['name'] = name.labeling.get_nice(newparams)  # habe keinen namen  !!!
print newparams
BOLTS.boltsgui.add_part(coll,base,newparams,FreeCAD.ActiveDocument)


# ************************************************************************************************
import BOLTS
myname = 'HEAProfile'
name = BOLTS.repo.names[myname]
cl = BOLTS.repo.class_names.get_src(name)
base = BOLTS.freecad_db.base_classes.get_src(cl)
coll = BOLTS.repo.collection_classes.get_src(cl)
params = cl.parameters.union(base.parameters)
free_params = params.free
in_params = {}
for p in free_params:
    p_type = params.types[p]
    default_value = params.defaults[p]
    in_params[p] = default_value

newparams = cl.parameters.collect(in_params)
newparams['name'] = name.labeling.get_nice(newparams)  # habe keinen namen  !!!
print newparams
BOLTS.boltsgui.add_part(coll,base,newparams,FreeCAD.ActiveDocument)



# ************************************************************************************************
import sys
path = 'C:\\Users\\bhb\\AppData\\Roaming\\FreeCAD\\Mod\Macros\\BOLTS\\bolttools\\'
sys.path.append(path)
import blt
import freecad

# instance of class BOLTS.bolttools.blt.repo
repopath = 'C:\\Users\\bhb\\AppData\\Roaming\\FreeCAD\\Mod\Macros\\BOLTS'
repo = blt.Repository(repopath)

# instance of class BOLTS.bolttools.blt.Collection 
acoll = None
for coll, in repo.itercollections():
    if coll.id == 'nut':
        acoll = coll
    print coll.id, ' --> ', coll.name, ' --> ', coll.description

# myindentstop
print acoll
print acoll.id, ' --> ', coll.name, ' --> ', coll.description


# instance class BOLTS.bolttools.freecad.BaseFunction
function = {'name': 'nut1', 'classids': ['hexagonthinnut1', 'hexagonthinnut2', 'hexagonnut1', 'hexagonnut2', 'hexagonnut3']}
basefile = {'functions': [{'name': 'nut1', 'classids': ['hexagonthinnut1', 'hexagonthinnut2', 'hexagonnut1', 'hexagonnut2', 'hexagonnut3']}], 'filename': 'nut.py', 'type': 'function', 'license': 'LGPL 2.1+ <http://www.gnu.org/licenses/lgpl-2.1>', 'author': 'Johannes Reinhardt <jreinhardt@ist-dein-freund.de>'}
collname = 'nut'
backend_root = 'C:\Users\bhb\AppData\Roaming\FreeCAD\Mod\Macros\BOLTS\freecad'
abase = freecad.BaseFunction(function,basefile,collname,backend_root)
print abase
print abase.classids
print abase.module_name
print abase.parameters

# dictionary params
aparams = {'name': 'Metric hexagon nut M3', 'e_min': 6.01, 's': 5.5, 'key': 'M3', 'm_max': 2.4, 'd1': 3.0}

# doc
adoc = App.ActiveDocument

def add_bolts_part(collection, base,params,doc):
    if isinstance(base,freecad.BaseFunction):
        module = importlib.import_module("BOLTS.freecad.%s.%s" %
            (collection.id,base.module_name))
        # obiges startes bei ersten hinzufuegen eines parts auch die gui
        print module
        module.__dict__[base.name](params,doc)
    else:
        raise  RuntimeError("Unknown base geometry type: %s" % type(base))

# myindentstop

import importlib
add_bolts_part(acoll, abase, aparams, adoc)

# ok funktioniert, aber bei eingabe von import BOLTS in FreeCAD 
# werden alle classen Repository, Collection und BaseFunction angelegt
# Ich muss nur darauf zugreifen koennen ... 
# ein BOLTS part ist mit folgenden drei Werten charakterisiert
# CollectionID --> nut
# ClassID --> hexagonnut1
# Key --> M3
# Ich brauche eine Methode um mit diesen Werten part hinzuzufuegen
# Genau das waehle ich in GUI auch aus --> siehe module freecad_bolts.py in gui




# ************************************************************************************************
# ************************************************************************************************
myboltsclassid = 'hexagonnut1'
myboltskey = 'M3'

from BOLTS.bolttools import blt
from BOLTS.bolttools import freecad

# instance of class BOLTS.bolttools.blt.repo
repopath = 'C:\\Users\\bhb\\AppData\\Roaming\\FreeCAD\\Mod\Macros\\BOLTS'
a_bolts_repo = blt.Repository(repopath)


# instance of class BOLTS.bolttools.blt.Class 
a_bolts_class = None
for c, in a_bolts_repo.iterclasses():
    print c.id
    if c.id == myboltsclassid:
        a_bolts_class = c

# myindentstop
print a_bolts_class
print vars(a_bolts_class)

params = {'key': myboltskey}
params = a_bolts_class.parameters.collect(params)
params['name'] = params['key']
aparams = params
print params


# instance of BOLTS.bolttools.freecad.FreeCADData
a_freecad_class = freecad.FreeCADData(a_bolts_repo)

# instance class BOLTS.bolttools.freecad.BaseFunction
abase = a_freecad_class.base_classes.get_src(a_bolts_class)

# instance of class BOLTS.bolttools.blt.Collection 
acoll = a_bolts_repo.collection_classes.get_src(a_bolts_class)

# doc
adoc = App.ActiveDocument
if not adoc:
    adoc = App.newDocument('BOLTS_part')


def add_bolts_part(collection, base,params,doc):
    if isinstance(base,freecad.BaseFunction):
        module = importlib.import_module("BOLTS.freecad.%s.%s" %
            (collection.id,base.module_name))
        # obiges startes bei ersten hinzufuegen eines parts auch die gui
        print module
        module.__dict__[base.name](params,doc)
    else:
        raise  RuntimeError("Unknown base geometry type: %s" % type(base))

# myindentstop

import importlib
add_bolts_part(acoll, abase, aparams, adoc)
