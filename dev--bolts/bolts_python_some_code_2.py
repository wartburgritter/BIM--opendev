# ************************************************************************************************
# leeres widget
import FreeCADGui
from PySide import QtGui, QtCore
mw = FreeCADGui.getMainWindow()
example_widget = QtGui.QDockWidget("My Test", mw)
mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, example_widget)
QtGui.QApplication.restoreOverrideCursor()


# ************************************************************************************************
import FreeCADGui
from PySide import QtGui, QtCore  
path_to_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/bolts_widget.ui"

mw = FreeCADGui.getMainWindow()
example_widget = QtGui.QDockWidget("My Test", mw)
example_widget.ui = FreeCADGui.PySideUic.loadUi(path_to_ui)

mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, example_widget.ui)
QtGui.QApplication.restoreOverrideCursor()


# ************************************************************************************************
https://github.com/FreeCAD/FreeCAD/blob/master/src/Mod/TemplatePyMod/TaskPanel.py
https://wiki.freecadweb.org/index.php?title=Dialog_creation
https://wiki.freecadweb.org/Manual:Creating_interface_tools
https://forum.freecadweb.org/viewtopic.php?t=14930
https://forum.freecadweb.org/viewtopic.php?t=45635
https://forum.freecadweb.org/viewtopic.php?t=40903


# ************************************************************************************************
# use of loadUiType
path_to_bolts_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/bolts_widget.ui"
path_to_value_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/value_widget.ui"
from FreeCADGui import PySideUic as uic
Ui_BoltsWidget, QBoltsWidget = uic.loadUiType(path_to_bolts_ui)
Ui_ValueWidget, QValueWidget = uic.loadUiType(path_to_value_ui)
print(Ui_BoltsWidget)
print(type(Ui_BoltsWidget))
print(QBoltsWidget)
print(type(QBoltsWidget))



class LengthWidget(QValueWidget):
    def __init__(self, parent, label, default):
        QValueWidget.__init__(self, parent)
        self.ui = Ui_ValueWidget()
        self.ui.setupUi(self)
        self.ui.label.setText(label)
        self.ui.valueEdit.setText(default)

class BoltsWidget(QBoltsWidget):
    def __init__(self):
        QBoltsWidget.__init__(self)
        self.ui = Ui_BoltsWidget()
        self.ui.setupUi(self)
        len_widget = LengthWidget(self.ui.params, "MyLengthParam" + " (mm)", "999")
        self.ui.param_layout.addWidget(len_widget)


from PySide import QtGui, QtCore  
widget = BoltsWidget()
print(widget)
print(widget.ui)
print(type(widget))
print(type(widget.ui))
"""
<__main__.BoltsWidget object at 0x7ffb550fcbc8>
<Ui_BoltsWidget object at 0x7ffb550eef28>
<class '__main__.BoltsWidget'>
<class 'Ui_BoltsWidget'>
"""

import FreeCADGui
mw = FreeCADGui.getMainWindow()
mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, widget)
QtGui.QApplication.restoreOverrideCursor()


# ************************************************************************************************
# use of loadUi
path_to_bolts_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/bolts_widget.ui"
path_to_value_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/value_widget.ui"
from FreeCADGui import PySideUic as uic

class LengthWidget:
    def __init__(self, parent, label, default):
        self.ui = uic.loadUi(path_to_value_ui)
        self.ui.label.setText(label)
        self.ui.valueEdit.setText(default)


class BoltsWidget:
    def __init__(self):
        self.ui = uic.loadUi(path_to_bolts_ui)

        # this only adds the ui not any more methods of the class LengthWidget
        len_widget = LengthWidget(self.ui.params, "MyLengthParam" + " (mm)", "999").ui
        self.ui.param_layout.addWidget(len_widget)


from PySide import QtGui, QtCore  
widget = BoltsWidget()
print(widget)
print(widget.ui)
print(type(widget))
print(type(widget.ui))
"""
<__main__.BoltsWidget object at 0x7ffb95295f48>
<Ui_BoltsWidget object at 0x7ffbacfa08d0>
<class '__main__.BoltsWidget'>
<class 'Ui_BoltsWidget'>
"""

import FreeCADGui
mw = FreeCADGui.getMainWindow()
mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, widget.ui)
QtGui.QApplication.restoreOverrideCursor()


# ************************************************************************************************
# use of loadUi
path_to_bolts_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/bolts_widget.ui"
path_to_value_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/value_widget.ui"
path_to_bool_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/bool_widget.ui"
from FreeCADGui import PySideUic as uic


class BoltsWidget:
    def __init__(self):
        self.ui = uic.loadUi(path_to_bolts_ui)

        # lenwidget
        len_widget = uic.loadUi(path_to_value_ui)
        len_widget.label.setText("MyLengthParam" + " (mm)")
        len_widget.valueEdit.setText("999.99")
        self.ui.param_layout.addWidget(len_widget)

        # boolwidget
        bool_widget = uic.loadUi(path_to_bool_ui)
        bool_widget.checkBox.setText("MyBoolParam")
        bool_widget.checkBox.setChecked(True)
        self.ui.param_layout.addWidget(bool_widget)


from PySide import QtGui, QtCore  
widget = BoltsWidget()

import FreeCADGui
mw = FreeCADGui.getMainWindow()
mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, widget.ui)
QtGui.QApplication.restoreOverrideCursor()


# ************************************************************************************************
# use of loadUi
path_to_bolts_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/bolts_widget.ui"
path_to_value_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/value_widget.ui"
path_to_bool_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/bool_widget.ui"
from FreeCADGui import PySideUic as uic
from PySide import QtGui, QtCore  

class LengthWidget(QtGui.QWidget):
    def __init__(self, parent, label, default):
        super(LengthWidget, self).__init__()
        a_widget = uic.loadUi(path_to_value_ui)
        a_widget.label.setText(label)
        a_widget.valueEdit.setText(default)
        layout = QtGui.QHBoxLayout()
        layout.addWidget(a_widget)
        self.setLayout(layout)

class BoolWidget(QtGui.QWidget):
    def __init__(self, parent, label, default):
        super(BoolWidget, self).__init__()
        a_widget = uic.loadUi(path_to_bool_ui)
        a_widget.checkBox.setText(label)
        a_widget.checkBox.setChecked(default)
        layout = QtGui.QHBoxLayout()
        layout.addWidget(a_widget)
        self.setLayout(layout)

class BoltsWidget:
    def __init__(self):
        self.ui = uic.loadUi(path_to_bolts_ui)

        # this adds the ui and all methods of the class
        len_widget = LengthWidget(self.ui.params, "MyLengthParam" + " (mm)", "999")
        self.ui.param_layout.addWidget(len_widget)
        bool_widget = BoolWidget(self.ui.params, "MyBoolParam", True)
        self.ui.param_layout.addWidget(bool_widget)


widget = BoltsWidget()
import FreeCADGui
mw = FreeCADGui.getMainWindow()
mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, widget.ui)
QtGui.QApplication.restoreOverrideCursor()


# ************************************************************************************************
# use of loadUi
path_to_bolts_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/bolts_widget.ui"
path_to_value_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/value_widget.ui"
path_to_bool_ui = "/home/hugo/.FreeCAD/Mod/BOLTSFC/BOLTS/gui/bool_widget.ui"
from FreeCADGui import PySideUic as uic
from PySide import QtGui, QtCore  

class LengthWidget(QtGui.QWidget):
    def __init__(self, parent, label, default):
        super(LengthWidget, self).__init__()

        a_widget = uic.loadUi(path_to_value_ui)
        a_widget.label.setText(label)
        a_widget.valueEdit.setText(default)

        layout = QtGui.QHBoxLayout()
        layout.addWidget(a_widget)
        self.setLayout(layout)

    def output(self):
        print("LengthWidget")

class BoolWidget(QtGui.QWidget):
    def __init__(self, parent, label, default):
        super(BoolWidget, self).__init__()

        a_widget = uic.loadUi(path_to_bool_ui)
        a_widget.checkBox.setText(label)
        a_widget.checkBox.setChecked(default)

        layout = QtGui.QHBoxLayout()
        layout.addWidget(a_widget)
        self.setLayout(layout)

    def output(self):
        print("BoolWidget")

class BoltsWidget(QtGui.QWidget):
    def __init__(self):
        super(BoltsWidget, self).__init__()

        self.ui = uic.loadUi(path_to_bolts_ui)

        # this adds the ui and all methods of the class
        len_widget = LengthWidget(self.ui.params, "MyLengthParam" + " (mm)", "999")
        self.ui.param_layout.addWidget(len_widget)
        len_widget.output()
        bool_widget = BoolWidget(self.ui.params, "MyBoolParam", True)
        self.ui.param_layout.addWidget(bool_widget)
        bool_widget.output()

        layout = QtGui.QHBoxLayout()
        layout.addWidget(self.ui)
        self.setLayout(layout)

    def output(self):
        print("BoltsWidget")

# var1
import FreeCADGui
QtGui.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
mw = FreeCADGui.getMainWindow()
widget = QtGui.QDockWidget("Bolts Widget", mw)
widget.setWidget(BoltsWidget())
mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, widget)
QtGui.QApplication.restoreOverrideCursor()

# var2
mywidget = BoltsWidget()
mywidget.output()
mw.addDockWidget(QtCore.Qt.RightDockWidgetArea, mywidget)

# var3
mywidget = BoltsWidget()
mywidget.output()
mywidget.show()


# ************************************************************************************************
# ************************************************************************************************



