import BOLTS as bolts
repo = bolts.repo

cl=repo.collection_classes.get_src(repo.classes["hollow_square"])

bolts.repo.classes['hollow_square'].parameters.description
bolts.repo.classes['hollow_square'].parameters.tables[0].data
bolts.repo.classes['hollow_square'].parameters.tables[0].columns


params = {'type': '40-2.6'} 
bolts._add_missing_inparams(cl, params)



# get missing Params
import BOLTS as bolts
cl = bolts.repo.class_names.get_src(bolts.repo.names['HEBProfile'])


# test the method
from BOLTS.freecad.profile_hollow.profile_hollow import rectangle_hollow as rrw
params = {
    'name': 'my_test_hollow',
    'h': 100,
    'b': 100,
    't': 5,
    'l': 50,
    'arch': False
}
rrw(params, App.ActiveDocument)
