# for all BOLTS yaml files
# read yaml
# write back yaml
# yaml files are edited manually thus wild formating
# if read once and write back they are normalized

import yaml

with open('/home/hugo/Documents/dev/bolts/BOLTS/data/profile_hollow.blt', 'r') as f:
    yamlobj = yaml.load(f, Loader=yaml.SafeLoader)


#with open('/home/hugo/Documents/dev/bolts/BOLTS/data/profile_l_normalized.yaml', 'w') as f:
#    yaml.safe_dump(yamlobj, f)


with open('/home/hugo/Documents/dev/bolts/BOLTS/data/profile_hollow.yaml', 'w') as f:
    yaml.safe_dump(yamlobj, f, default_flow_style=False)


# mischung aus beiden ist gut
# 1 fuer daten und 2 fuer structurinformationen
