import os
import json

import yaml
from typing import Any, IO


class Loader(yaml.SafeLoader):
    """YAML Loader with `!include` constructor."""

    def __init__(self, stream: IO) -> None:
        """Initialise Loader."""

        try:
            self._root = os.path.split(stream.name)[0]
        except AttributeError:
            self._root = os.path.curdir

        super().__init__(stream)


def construct_include(loader: Loader, node: yaml.Node) -> Any:
    """Include file referenced at node."""

    filename = os.path.abspath(os.path.join(loader._root, loader.construct_scalar(node)))
    extension = os.path.splitext(filename)[1].lstrip('.')

    with open(filename, 'r') as f:
        if extension in ('yaml', 'yml'):
            return yaml.load(f, Loader)
        elif extension in ('json', ):
            return json.load(f)
        else:
            return ''.join(f.readlines())


yaml.add_constructor('!include', construct_include, Loader)


if __name__ == '__main__':

    #with open('foo.yaml', 'r') as f:
    #with open('/home/hugo/Documents/dev/bolts/BOLTS/data/vacuum.blt', 'r') as f:
    #    data1 = list(yaml.load(f, Loader))

    data1 = list(yaml.load_all(open('/home/hugo/Documents/dev/bolts/BOLTS/data/vacuum.blt'), Loader=Loader))

    data2 = list(yaml.load_all(open('/home/hugo/Documents/dev/bolts/BOLTS/data/vacuum.blt'), Loader=yaml.SafeLoader))

    import json
    print(json.dumps(data1, indent=4))
    print("new loader")
    print(json.dumps(data2, indent=4))
