# https://github.com/boltsparts/BOLTS_archive/issues/263
# https://azzamsa.com/n/gitpython-intro/

"""
# open shell in home
# python3

import sys
sys.path.append("/home/hugo/Documents/projekte--opendev_bim/dev--bolts/")
import importlib
import automate_module_creation as amc
amc.get_boltsparts_commit_data()
importlib.reload(amc)

amc.update_boltsfc()
amc.update_boltsws()
amc.update_boltspy()
amc.update_boltsos()
exit()

"""


"""
cd ~/Documents/dev/bolts/module_rolling_release_automation/boltsfc
cd ~/Documents/dev/bolts/module_rolling_release_automation/boltsws
cd ~/Documents/dev/bolts/module_rolling_release_automation/boltspy
cd ~/Documents/dev/bolts/module_rolling_release_automation/boltsos
git push origin main
berndhahnebach

"""

import os
import sys
from git import Repo
from os.path import join
from shutil import copy
from shutil import copytree  # dir1 to new created dir2
from shutil import rmtree


main_repo_path = join(os.sep, "home", "hugo", "Documents", "dev", "bolts", "module_rolling_release_automation")
local_boltsparts_path = join(main_repo_path, "boltsparts")
# print(main_repo_path)
# print(local_boltsfc_path)
sys.path.append(local_boltsparts_path)
import tools_bolts


# ************************************************************************************************
def get_boltsparts_commit_data():

    # local boltsparts repo, get the commit message

    # get local boltsparts repo
    lc_boltsparts_repo = Repo(local_boltsparts_path)

    # check if local boltsparts repo is clean an on the main branch, and nothing to commit
    clean_message_lc_boltsparts_repo = "On branch main\nYour branch is up to date with 'origin/main'.\n\nnothing to commit, working tree clean"
    present_message_lc_boltsparts_repo = lc_boltsparts_repo.git.status()
    if present_message_lc_boltsparts_repo != clean_message_lc_boltsparts_repo:
        print("Problem on lc_boltsparts_repo.")
        print(present_message_lc_boltsparts_repo)
        return False
    print("Clean lc_boltsparts_repo")

    # pull latest commits
    try:
        lc_boltsparts_repo.remotes.origin.pull()
    except Exception:
        print("Problem on pulling latest changes from boltsparts repo. Do you have internet connection activ?")
        return False

    # build commit message with data from local boltsparts repo
    lc_boltsparts_repo.git.status()
    last_commit = lc_boltsparts_repo.head.commit

    message_for_bolts_backend = "update to latest BOLTS, commit: {}, message: {}".format(last_commit.hexsha[0:12], last_commit.message[0:45])
    print("Commit message: {}".format(message_for_bolts_backend))
    from datetime import datetime
    commit_data = {
        "data": "backend was exported on {}\n\ninformation last commit of boltsparts repo:\n\n{}\n{}\n{}\n\n{}\n".format(
            str(datetime.now()),
            last_commit.hexsha[0:12],
            str(datetime.fromtimestamp(last_commit.authored_date)),
            last_commit.author.name,
            last_commit.message
        ),
        "supertext": message_for_bolts_backend
    }
    return commit_data


# ************************************************************************************************
def delete_repo_content(repo_path):

    repo_content = os.listdir(repo_path)
    for con in repo_content:
        if con != ".git":
            path_con = join(repo_path, con)
            if os.path.isfile(path_con):
                os.remove(path_con)
            elif os.path.isdir(path_con):
                rmtree(path_con)
    new_repo_content = os.listdir(repo_path)
    if new_repo_content != [".git"]:
        print(new_repo_content)
        print("Problem on deleting {} repo content.".format(os.path.basename(repo_path)))
        return False
    print("All contend in {} repo except .git directory has been deleted.".format(os.path.basename(repo_path)))
    return True


# ************************************************************************************************
def copy_dir_content(copy_source_path, copy_target_path):

    # copy_target_path might exist, thus copytree kann not be used
    source_content = os.listdir(copy_source_path)
    for asource in source_content:
        path_asource = join(copy_source_path, asource)
        path_atarget = join(copy_target_path, asource)
        if os.path.isfile(path_asource):
            copy(path_asource, path_atarget)
        elif os.path.isdir(path_asource):
            copytree(path_asource, path_atarget)
    print(sorted(os.listdir(copy_target_path)))
    return True


# ************************************************************************************************
def update_boltsfc():
    # freecad

    # *******************************************
    # get commit message for boltsos from local boltsparts repo
    boltsparts_commit_data = get_boltsparts_commit_data()
    message_for_boltsfc = boltsparts_commit_data["supertext"]
    if message_for_boltsfc is False:
        print("No commit message build from boltsparts repo.")
        return False

    # *******************************************
    # export freecad module
    tools_bolts.export("freecad", local_boltsparts_path)

    # *******************************************
    # local boltsfc repo
    # repopath boltsfc
    local_boltsfc_path = join(main_repo_path, "boltsfc")
    # print(local_boltsparts_path)

    # get local boltsfc repo
    lc_boltsfc_repo = Repo(local_boltsfc_path)

    # check if local boltfc repo is clean an on the main branch, and nothing to commit
    clean_message_lc_boltsfc_repo = "On branch main\nYour branch is up to date with 'origin/main'.\n\nnothing to commit, working tree clean"
    if lc_boltsfc_repo.git.status() != clean_message_lc_boltsfc_repo:
        print("Problem on lc_boltsfc_repo.")
        return False
    print("Clean lc_boltsfc_repo")

    # pull latest commits
    lc_boltsfc_repo.remotes.origin.pull()

    # check if BOLTS directory exists in output/freecad in local boltsparts repo and if it is not empty
    boltsfc_copy_source_path = join(local_boltsparts_path, "output", "freecad")
    if not os.path.isdir(boltsfc_copy_source_path):
        print("BOLTS freecad module might not have been created in boltsparts repo.")
        return False
    if len(os.listdir(boltsfc_copy_source_path)) == 0:
        print("No file inside freecad directory. Problem on created website backend.")
        return False
    print("freecad exists in output in local boltsparts repo and it is not empty.")

    # delete all content ecxept .git in local boltsfc repo
    if delete_repo_content(local_boltsfc_path) is False:
        return False

    # add backend export file
    backend_export_file = open(join(local_boltsfc_path, "export_version.txt"), "w")
    backend_export_file.write("{}".format(boltsparts_commit_data["data"]))
    backend_export_file.close()

    # copy content of output/freecad into local boltsfc repo
    if copy_dir_content(boltsfc_copy_source_path, local_boltsfc_path) is False:
        print("Something went wrong on copying boltsfc data into boltsfc repo.")
        return False

    # check if data has been copied
    # TODO, evtl. vergleich oslisdir oberste ebene, bedenke im ziel ist .git

    # check if there is something to commit
    if lc_boltsfc_repo.git.status() == clean_message_lc_boltsfc_repo:
        print("Nothing to commit local boltsfc repo. Has the BOLTS freecad module been exported for latest commit?")
        return False
    print("There seam to be changes to add and commit.")

    # add to index
    lc_boltsfc_repo.git.add('--all')

    # make a commit, will be made even if the index is empty
    # TODO: check if there really is something in the index
    lc_boltsfc_repo.index.commit(message_for_boltsfc)  # pendent get commit id and commit message from boltsparts repo

    # check if add and commit has been finished successful
    print("Commitmessage last commit: {}".format(lc_boltsfc_repo.head.commit.message))

    return True


# ************************************************************************************************
def update_boltsws():
    # website

    # *******************************************
    # get commit message for boltsos from local boltsparts repo
    boltsparts_commit_data = get_boltsparts_commit_data()
    message_for_boltsws = boltsparts_commit_data["supertext"]
    if message_for_boltsws is False:
        print("No commit message build from boltsparts repo.")
        return False

    # *******************************************
    # export website module
    tools_bolts.export("website", local_boltsparts_path)

    # *******************************************
    # local boltsws repo
    # repopath boltsws
    local_boltsws_path = join(main_repo_path, "boltsws")
    # print(local_boltsws_path)

    # get local boltsws repo
    lc_boltsws_repo = Repo(local_boltsws_path)

    # check if local boltsws repo is clean an on the main branch, and nothing to commit
    clean_message_lc_boltsws_repo = "On branch main\nYour branch is up to date with 'origin/main'.\n\nnothing to commit, working tree clean"
    if lc_boltsws_repo.git.status() != clean_message_lc_boltsws_repo:
        print("Problem on lc_boltsws_repo.")
        return False
    print("Clean lc_boltsws_repo")

    # pull latest commits
    lc_boltsws_repo.remotes.origin.pull()

    # check if website directory exists in output in local boltsparts repo and if it is not empty
    boltsws_copy_source_path = join(local_boltsparts_path, "output", "website")
    if not os.path.isdir(boltsws_copy_source_path):
        print("BOLTS website module might not have been created in boltsparts repo.")
        return False
    if len(os.listdir(boltsws_copy_source_path)) == 0:
        print("No file inside output/website directory. Problem on created website backend.")
        return False
    print("website exists in output in local boltsparts repo and it is not empty.")

    # delete all content ecxept .git in local boltsws repo
    if delete_repo_content(local_boltsws_path) is False:
        return False

    # add backend export file
    backend_export_file = open(join(local_boltsws_path, "export_version.txt"), "w")
    backend_export_file.write("{}".format(boltsparts_commit_data["data"]))
    backend_export_file.close()

    # copy content of output/website into local boltsws repo
    if copy_dir_content(boltsws_copy_source_path, local_boltsws_path) is False:
        print("Something went wrong on copying boltsws backend data into boltsws repo.")
        return False

    # check if data has been copied
    # TODO, evtl. vergleich oslisdir oberste ebene, bedenke im ziel ist .git

    # check if there is something to commit
    if lc_boltsws_repo.git.status() == clean_message_lc_boltsws_repo:
        print("Nothing to commit local boltsws repo. Has the BOLTS website module been exported for latest commit?")
        return False
    print("There seam to be changes to add and commit.")

    # add to index
    lc_boltsws_repo.git.add('--all')

    # make a commit, will be made even if the index is empty
    # TODO: check if there really is something in the index
    lc_boltsws_repo.index.commit(message_for_boltsws)  # pendent get commit id and commit message from boltsparts repo

    # check if add and commit has been finished successful
    print("Commitmessage last commit: {}".format(lc_boltsws_repo.head.commit.message))

    lc_boltsws_repo.git.cherry_pick("844db761d976")

    # check if cherry-pick has been finished successful
    print("Commitmessage last commit: {}".format(lc_boltsws_repo.head.commit.message))

    return True


# ************************************************************************************************
def update_boltspy():
    # pythonpackage

    # *******************************************
    # get commit message for boltsos from local boltsparts repo
    boltsparts_commit_data = get_boltsparts_commit_data()
    message_for_boltspy = boltsparts_commit_data["supertext"]
    if message_for_boltspy is False:
        print("No commit message build from boltsparts repo.")
        return False

    # *******************************************
    # export pythonpackage module
    tools_bolts.export("pythonpackage", local_boltsparts_path)

    # *******************************************
    # local boltspy repo
    # repopath boltspy
    local_boltspy_path = join(main_repo_path, "boltspy")
    # print(local_boltspy_path)

    # get local boltspy repo
    lc_boltspy_repo = Repo(local_boltspy_path)

    # check if local boltspy repo is clean an on the main branch, and nothing to commit
    clean_message_lc_boltspy_repo = "On branch main\nYour branch is up to date with 'origin/main'.\n\nnothing to commit, working tree clean"
    if lc_boltspy_repo.git.status() != clean_message_lc_boltspy_repo:
        print("Problem on lc_boltspy_repo.")
        return False
    print("Clean lc_boltspy_repo")

    # pull latest commits
    lc_boltspy_repo.remotes.origin.pull()

    # check if in pythonpackage directory in output the directory boltspy exits in local boltsparts repo and if it is not empty
    boltspy_copy_source_path = join(local_boltsparts_path, "output", "pythonpackage", "boltspy")
    if not os.path.isdir(boltspy_copy_source_path):
        print("BOLTS pythonpackage module might not have been created in boltsparts repo.")
        return False
    if len(os.listdir(boltspy_copy_source_path)) == 0:
        print("No file inside output/pythonpackage directory. Problem on created pythonpackage backend.")
        return False
    print("boltspy exists in output/pythonpackage in local boltsparts repo and it is not empty.")

    # the copy target path
    boltspy_copy_target_path = join(local_boltspy_path, "pythonpackage")

    # delete all content ecxept .git in local boltspy repo
    if delete_repo_content(local_boltspy_path) is False:
        return False

    # add backend export file
    backend_export_file = open(join(local_boltspy_path, "export_version.txt"), "w")
    backend_export_file.write("{}".format(boltsparts_commit_data["data"]))
    backend_export_file.close()

    # copy dir output/pythonpackage/boltspy as dir pythonpackage into local boltspy repo
    copytree(boltspy_copy_source_path, boltspy_copy_target_path)

    # check if dir has been copied
    if not os.path.isdir(boltspy_copy_target_path):
        print("BOLTS pythonpackage module might not have been copied in boltspy repo.")
        return False
    if len(os.listdir(boltspy_copy_target_path)) == 0:
        print("No file inside pythonpackage directory. Problem on created pythonpackage backend.")
        return False
    print("BOLTS pythonpackage module seams to have been copied into boltspy repo.")

    # check if there is something to commit
    if lc_boltspy_repo.git.status() == clean_message_lc_boltspy_repo:
        print("Nothing to commit local boltspy repo. Has the BOLTS pythonpackage module been exported for latest commit?")
        return False
    print("There seam to be changes to add and commit.")

    # add to index
    lc_boltspy_repo.git.add('--all')

    # make a commit, will be made even if the index is empty
    # TODO: check if there really is something in the index
    lc_boltspy_repo.index.commit(message_for_boltspy)  # pendent get commit id and commit message from boltsparts repo

    # check if add and commit has been finished successful
    print("Commitmessage last commit: {}".format(lc_boltspy_repo.head.commit.message))

    return True


# ************************************************************************************************
def update_boltsos():
    # openscad

    # *******************************************
    # get commit message for boltsos from local boltsparts repo
    boltsparts_commit_data = get_boltsparts_commit_data()
    message_for_boltsos = boltsparts_commit_data["supertext"]
    if message_for_boltsos is False:
        print("No commit message build from boltsparts repo.")
        return False

    # *******************************************
    # export openscad module
    tools_bolts.export("openscad", local_boltsparts_path)

    # *******************************************
    # local boltsos repo
    # repopath boltsos
    local_boltsos_path = join(main_repo_path, "boltsos")
    # print(local_boltsos_path)

    # get local boltsos repo
    lc_boltsos_repo = Repo(local_boltsos_path)

    # check if local boltsos repo is clean an on the main branch, and nothing to commit
    clean_message_lc_boltsos_repo = "On branch main\nYour branch is up to date with 'origin/main'.\n\nnothing to commit, working tree clean"
    if lc_boltsos_repo.git.status() != clean_message_lc_boltsos_repo:
        print("Problem on lc_boltsos_repo.")
        return False
    print("Clean lc_boltsos_repo")

    # pull latest commits
    lc_boltsos_repo.remotes.origin.pull()

    # check if in openscad directory in output the directory boltsos exits in local boltsparts repo and if it is not empty
    boltsos_copy_source_path = join(local_boltsparts_path, "output", "openscad")
    if not os.path.isdir(boltsos_copy_source_path):
        print("BOLTS openscad module might not have been created in boltsparts repo.")
        return False
    if len(os.listdir(boltsos_copy_source_path)) == 0:
        print("No file inside output/openscad directory. Problem on created openscad backend.")
        return False
    print("boltsos  exists in output/openscad in local boltsparts repo and it is not empty.")

    # the copy target path
    boltsos_copy_target_path = join(local_boltsos_path, "openscad")

    # delete all content ecxept .git in local boltsos repo
    if delete_repo_content(local_boltsos_path) is False:
        return False

    # add backend export file
    backend_export_file = open(join(local_boltsos_path, "export_version.txt"), "w")
    backend_export_file.write("{}".format(boltsparts_commit_data["data"]))
    backend_export_file.close()

    # copy dir output/openscad into local boltsos repo
    copytree(boltsos_copy_source_path, boltsos_copy_target_path)

    # check if dir has been copied
    if not os.path.isdir(boltsos_copy_target_path):
        print("BOLTS openscad module might not have been copied in boltsos repo.")
        return False
    if len(os.listdir(boltsos_copy_target_path)) == 0:
        print("No file inside openscad directory. Problem on created openscad backend.")
        return False
    print("BOLTS openscad module seams to have been copied into boltsos repo.")

    # check if there is something to commit
    if lc_boltsos_repo.git.status() == clean_message_lc_boltsos_repo:
        print("Nothing to commit local boltsos repo. Has the BOLTS openscad module been exported for latest commit?")
        return False
    print("There seam to be changes to add and commit.")

    # add to index
    lc_boltsos_repo.git.add('--all')

    # make a commit, will be made even if the index is empty
    # TODO: check if there really is something in the index
    lc_boltsos_repo.index.commit(message_for_boltsos)  # pendent get commit id and commit message from boltsparts repo

    # check if add and commit has been finished successful
    print("Commitmessage last commit: {}".format(lc_boltsos_repo.head.commit.message))

    return True


# get back the boltsfc, boltsws, boltsws, boltsos to origin
"""
git add .
git commit -m "tmp"
git checkout origin/main
git branch -D main
git checkout -b main
git branch --set-upstream-to=origin/main main
git pull
git log -1
git status

"""
