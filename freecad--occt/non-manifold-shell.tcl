pload ALL
# Create first face
circle c 0 1 0 1
trim c c 0.5*pi 1.5*pi
mkedge e1 c
line l 0 0 0 0 1 0
trim l l 0 2
mkedge e2 l
wire w e1 e2
mkplane plane1 w
# Create second face
circle c 0 1 0 1
trim c c 1.5*pi 2.5*pi
mkedge e1 c
line l 0 0 0 0 1 0
trim l l 0 2
mkedge e2 l
wire w e1 e2
mkplane plane2 w
# Create third face
circle c 0 1 0 1 0 0 1
trim c c 1.5*pi 2.5*pi
mkedge e1 c
line l 0 0 0 0 1 0
trim l l 0 2
mkedge e2 l
wire w e1 e2
mkplane plane3 w


#################################
# Sew faces
sewing sr1 plane1 plane2 +n
sewing sr2 plane1 plane2 plane3 +n


#################################
# view
vinit
vdisplay sr1
vfit
vdisplay sr2

#################################
# print info
nbshapes sr2
