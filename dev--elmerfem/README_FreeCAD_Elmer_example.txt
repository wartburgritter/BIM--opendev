##########################################################################
In FreeCAD:

* Open the standard 3D-FEM example on the start page under 'Example projects' called 'Load an FEM 3D example analysis.
* Select the mesh object called 'Box_Mesh' and delete it. The original Mesh was created with the integrated Netgen-Mesher (and has be recomputed with GMSH).
* Selet the object called 'Cube' and use 'FEM->FEM mesh from shape with GMSH' to generate a new mesh object.
* Select the new mesh object called 'Box_Mesh' and export it via 'File->Export' as 'mesh_with_BC_from_FC-FEM.unv'.
* Execute ElmerGrid in the folder where the exported .unv file is located with 'ElmerGrid 8 2 mesh_with_BC_from_FC-FEM.unv -autoclean'. This transvers the mesh including the boundary conditions stored in the .unv file into several Elmer-mesh files in an identically named folder.


##########################################################################
In ElmerGUI:

* Open the transfered mesh with 'File->Load mesh' by simply navigating into the prior generated folder called as the .unv file. The Mesh is now visible. 
* In 'Model->Setup->Simulation->Free_text' insert 'Coordinate scaling = 1'.
* 'Model->Equation->Add' and select in the column 'Linear elasticity' 'Acitve->on' and 'Apply to body-> Body1'. Select 'Edit Solver Settings' for in the current tab and there insert in the 'Solver specific options' in the 'Free text input' the string:'Element=p:2'. Than press 'Apply'.
Select in the 'Result Output' column the 'Active' field. 
Select 'Edit Solver Settings' for in the current tab and there insert in the 'Solver specific options' in the select in the 'General' tab under 'Execute solver' the point 'After simulation'.
Press 'Apply' and 'OK'.

* 'Model->Material-Add' and select 'Apply to body-> Body1' and select the 'Material library' -> Select 'Steel (alloy -generic)'. Adopt 'Youngs modulus' to '210.0e9' and 'Poisson ratio' to '0.3'.
 
* Under 'Model->Boundary condition->Add' in column 'Linear elasticity' set 'Displacement 1' and 'Displacement 2' and 'Displacement 3' to 0. Set 'Name:' to 'BoundaryConditionFixed'. Press 'OK'

* Under 'Model->Boundary condition->Add' in column 'Linear elasticity' set 'Force 2' to 9000000. Set 'Name:' to 'BoundaryConditionForce'. Press 'OK'

* Set 'Model->Set boundary properties' and select left boundary with a double-click. Choose 'BoundaryConditionFixed' from pull down-menu. Click 'Add'.

* Set 'Model->Set boundary properties' and select left boundary with a double-click. Choose 'BoundaryConditionForce' from pull down-menu. Click 'Add'.

* 'File->Save'
* 'File->Save Project'
* 'Sif->Generate'
* 'Run->Start solver'
* After the computation has finished: 'Run->Start paraview'.

##########################################################################
In Paraview:

Select the 'Properties' window press 'Apply' and select in the subgroup for 'Coloring' 'displacement' instead of 'Solid Color'. 



