analysis_dir = '/tmp/FEM_unittests/FEM_solverframework/'



solv = App.ActiveDocument.SolverCalculiX
machine_ccx = solv.Proxy.createMachine(solv, analysis_dir)
machine_ccx.testmode
import femsolver.run
machine_ccx.target = femsolver.run.PREPARE


machine_ccx.start()
machine_ccx.join()  # wait for the machine to finish.




solv = App.ActiveDocument.SolverElmer
machine_elmer = solv.Proxy.createMachine(solv, analysis_dir, True)
machine_elmer.testmode
import femsolver.run
machine_elmer.target = femsolver.run.PREPARE


machine_elmer.start()
machine_elmer.join()  # wait for the machine to finish.

