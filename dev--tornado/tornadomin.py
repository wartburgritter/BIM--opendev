'''
cd /home/hugo/Documents/projekte--BIM--opendev/dev--tornado/
python3 tornadomin.py

http://localhost:8888/
curl http://localhost:8888/
'''

import tornado.ioloop
import tornado.web

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('Greetings from Wartburgritter!\n')

if __name__ == "__main__":
    app = tornado.web.Application(handlers=[(r"/", IndexHandler)])
    app.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
