'''
python3 main.py

http://localhost:8888/
'''

import os

import tornado.httpserver
import tornado.ioloop
import tornado.web


def get_file_path():
    from tempfile import gettempdir
    return "SquareRootResult.txt", gettempdir()


class IndexHandler(tornado.web.RequestHandler):
 
    def get(self):
        self.render('template_index.html')


class SquareRootInputHandler(tornado.web.RequestHandler):

    def get(self):

        # get value
        input_value = float(self.get_argument('val_in', '25.0'))
 
        # calculate result
        from math import sqrt
        sqrt_value = round(sqrt(input_value), 2)

        # save file
        _file_name, _file_dir = get_file_path()
        _file_path = os.path.join(_file_dir, _file_name)
        wf = open(_file_path, 'w')
        wf.write("{}".format(sqrt_value))
        wf.close()

        # output
        self.render(
            'template_squareroot.html',
            val_out=input_value,
            val_sqrt=sqrt_value
        )

        # ?????????????
        # TODO 
        # to make sure every webuser downloads his result file
        # somehow return or save the file handle or path
        # to be able to transfer this to the download request handler
        # I know how to transfer data to a handler
        # ?????????????


class FileDownloadHandler(tornado.web.RequestHandler):

    def get(self):

        _file_name, _file_dir = get_file_path()
        _file_path = os.path.join(_file_dir, _file_name)
        if not _file_name or not os.path.exists(_file_path):
            raise HTTPError(404)

        self.set_header('Content-Type', 'application/force-download')
        self.set_header('Content-Disposition', 'attachment; filename={}'.format(_file_name))

        with open(_file_path, "rb") as f:
            try:
                while True:
                    _buffer = f.read(4096)
                    if _buffer:
                        self.write(_buffer)
                    else:
                        f.close()
                        self.finish()
                        return
            except:
                raise HTTPError(404)
        raise HTTPError(500)


if __name__ == "__main__":
    
    # create the app instance
    app = tornado.web.Application(
        handlers = [
            (r'/', IndexHandler),
            (r'/squareroot', SquareRootInputHandler),
            (r'/downloadresult', FileDownloadHandler),
        ],
        template_path = os.path.dirname(__file__)
    )

    # boilerplate code for tornado
    app.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
