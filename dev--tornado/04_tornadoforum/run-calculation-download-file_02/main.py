'''
cd /home/hugo/Documents/projekte--BIM--opendev/dev--tornado/04_tornadoforum/run-calculation-download-file_02
python3 main.py

http://localhost:8888/sqrt/150
'''


import os
import tornado.ioloop
import tornado.web
from math import sqrt


class Sqrt(tornado.web.RequestHandler):
    def get(self, sqrt_num):
        input_value = float(sqrt_num)
        sqrt_value = sqrt(input_value)

        # py3.6+ f string format
        self.write(f"<p>The square root of {sqrt_num} is {sqrt_value}</p>")

if __name__ == "__main__":
    app = tornado.web.Application(
        handlers = [
            (r"/sqrt/(?P<sqrt_num>[0-9]+\.[0-9]+)", Sqrt),
            (r"/sqrt/(?P<sqrt_num>[0-9]+)", Sqrt),
        ],
        # remove debug or change to False in production
        # one use of debug mode is it allows one to
        # change code without having to reboot HTTP server
        debug=True,
    )
    app.listen(8888)
    tornado.ioloop.IOLoop.instance().start()



