"""
python3 main.py

http://localhost:8888/
"""

import os

import tornado.httpserver
import tornado.ioloop
import tornado.web
from math import sqrt
from uuid import uuid4
from tempfile import gettempdir  # returns /tmp on my linux machine

# get temporary directory
global tmp_dir
tmp_dir = gettempdir()


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("template_index.html")


class SquareRootInputHandler(tornado.web.RequestHandler):
    def get(self):

        # get value
        input_value = float(self.get_argument("val_in", "25.0"))

        # calculate result
        sqrt_value = round(sqrt(input_value), 2)

        # save file
        _file_name = "SquareRootResult_" + str(uuid4())[-12:] + ".txt"
        _file_path = os.path.join(tmp_dir, _file_name)
        wf = open(_file_path, "w")
        wf.write("{}".format(sqrt_value))
        wf.close()

        # output
        self.render(
            "template_squareroot.html",
            val_out=input_value,
            val_sqrt=sqrt_value,
            val_filename=_file_name,
        )

        return


class FileDownloadHandler(tornado.web.RequestHandler):
    def get(self):

        # get _file_name
        _file_name = self.get_argument("filename")
        _download_file_name = "SquareRootResult.txt"

        # check if file exists
        _file_path = os.path.join(tmp_dir, _file_name)
        if not _file_name or not os.path.exists(_file_path):
            raise HTTPError(404)

        self.set_header("Content-Type", "application/force-download")
        self.set_header(
            "Content-Disposition", "attachment; filename={}".format(_download_file_name)
        )

        with open(_file_path, "rb") as f:
            try:
                while True:
                    _buffer = f.read(4096)
                    if _buffer:
                        self.write(_buffer)
                    else:
                        f.close()
                        self.finish()  
                        # request handling is completed and it's resources 
                        # (e.g. network connection) is released
                        # delete file
                        # os.remove(_file_path)  
                        # not good, user clicks download, file will be deleted, than cancel
                        # than download again, file is no more available
                        return
            except:
                raise HTTPError(404)
        raise HTTPError(500)

        # TODO delete the  files if they exeed a limit on disk space
        # separate python tool

        return


if __name__ == "__main__":

    # create the app instance
    app = tornado.web.Application(
        handlers=[
            (r"/", IndexHandler),
            (r"/squareroot", SquareRootInputHandler),
            (r"/downloadresult", FileDownloadHandler),
        ],
        template_path=os.path.dirname(__file__),
    )

    # boilerplate code for tornado
    app.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
