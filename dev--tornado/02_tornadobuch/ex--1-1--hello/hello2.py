'''
terminal:
cd /home/hugo/Documents/projekte--BIM--opendev/dev--tornado/02_tornadobuch/ex--1-1--hello/
python3 hello2.py

browser:
http://localhost:8888/

http://localhost:8888/?greeting=Berndi+sein+text

terminal:
curl http://localhost:8888/?greeting=Berndi+sein+text

'''

import tornado.ioloop
import tornado.web


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        greeting = self.get_argument('greeting', 'Hello')
        self.write(greeting + ', friendly user!')

if __name__ == "__main__":
    app = tornado.web.Application(handlers=[(r"/", IndexHandler)])
    app.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
