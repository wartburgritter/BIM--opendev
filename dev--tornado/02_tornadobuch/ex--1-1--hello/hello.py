'''
terminal:
cd /home/hugo/Documents/projekte--BIM--opendev/dev--tornado/buch_examples/ex--1-1--hello/
python3 hello.py

browser:
http://localhost:8888/

http://localhost:8888/?greeting=Berndi+sein+text

terminal:
curl http://localhost:8888/?greeting=Berndi+sein+text

'''

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options
define("port", default=8888, help="run on the given port", type=int)

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        greeting = self.get_argument('greeting', 'Hello')
        self.write(greeting + ', friendly user!')

if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = tornado.web.Application(handlers=[(r"/", IndexHandler)])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
