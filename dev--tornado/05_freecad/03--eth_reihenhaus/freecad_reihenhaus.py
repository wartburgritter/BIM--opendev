# **************************************************************************
# basedon: https://www.tornadoweb.org/en/stable/
''' run with: 
python3 freecad_reihenhaus.py
localhost:8888
'''
# **************************************************************************

import tornado.ioloop
import tornado.web

import os
import sys
sys.path.append('/home/hugo/Documents/dev/freecad/freecadbhb_dev/build/lib')
import FreeCAD


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        # on click load this starts once
        from Part import makeBox as box
        vol = round(box(1,1,1).Volume, 2)

        self.write(
            "Hello, FreeCAD user! This is a test. The volume of a box created with FreeCAD running on tornado webserver: {}\n"
            .format(vol)
        )

        modulename = 'reihenhaus'
        modulfullname = 'ethtools.reihenhaus'
        if modulename not in sys.modules and modulfullname not in sys.modules:
            FreeCAD.Console.PrintMessage('import {}\n'.format(modulename))
            from ethtools import reihenhaus
        else:
            FreeCAD.Console.PrintMessage('reload {}\n'.format(modulename))
            from ethtools import reihenhaus
            reload(reihenhaus)

        reihenaus_ifc = os.path.join(os.path.expanduser('~'), 'Desktop', 'reihenhaus_std.ifc')
        ifc_file = open(reihenaus_ifc, 'r')
        ifc_lines = ifc_file.readlines()
        ifc_file.close
        self.write('\n\n')
        for ln in ifc_lines:
            self.write(ln)

def make_app():
    return tornado.web.Application([
        (r"/", MainHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
