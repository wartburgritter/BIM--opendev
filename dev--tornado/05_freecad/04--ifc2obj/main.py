# ***************************************************************************
# *                                                                         *
# *   Copyright (c) 2019 - Bernd Hahnebach <bernd@bimstatik.org>            *
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU Lesser General Public License (LGPL)    *
# *   as published by the Free Software Foundation; either version 2 of     *
# *   the License, or (at your option) any later version.                   *
# *   for detail see the LICENCE text file.                                 *
# *                                                                         *
# *   This program is distributed in the hope that it will be useful,       *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
# *   GNU Library General Public License for more details.                  *
# *                                                                         *
# *   You should have received a copy of the GNU Library General Public     *
# *   License along with this program; if not, write to the Free Software   *
# *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
# *   USA                                                                   *
# *                                                                         *
# ***************************************************************************

"""
# got to the directory of this script and run
python3 main.py

http://localhost:8888/

# make code formating and check pep8 Python style guide
black main.py
flake8 main.py --max-line-length=100
python3 main.py

"""

import tornado.ioloop
import tornado.web

import subprocess
from os.path import join, exists, basename, splitext
from tempfile import gettempdir  # returns /tmp on my linux machine

global UPLOAD_FILE_PATH
UPLOAD_FILE_PATH = gettempdir()


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        args = dict(username="visitor")
        self.render("upload.html", **args)


class UploadHandler(tornado.web.RequestHandler):
    def post(self):
        if self.request.files.get("uploadfile", None):
            uploadFile = self.request.files["uploadfile"][0]
            filename = uploadFile["filename"]
            _filepath_ifc = join(UPLOAD_FILE_PATH, filename)
            fileObj = open(_filepath_ifc, "wb")
            fileObj.write(uploadFile["body"])
            # self.redirect("/")

        if not exists(_filepath_ifc):
            print("IFC file does not exist!")
            raise HTTPError(404)

        ifcconvert_binary = "/usr/local/bin/IfcConvert"
        # if no output is given to IfcConver inputpath.obj is taken :-)
        # but we would like to check for existence of the file thus we need the filepath
        _filepath_obj = splitext(_filepath_ifc)[0] + ".obj"

        try:
            p = subprocess.Popen(
                [ifcconvert_binary, "-y", _filepath_ifc, _filepath_obj],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            )
            ifcconvert_stdout, ifcconvert_stderr = p.communicate()
            # print(ifcconvert_stdout)
            print(ifcconvert_stderr)
        except Exception as e:
            print("Problems")
            print(str(e))

        """
        wie soll endseite aussehen
        per get die obj und mtl datei spaeter auch runterladen moeglich
        das heisst mit uuid arbeiten
        """

        """
        user_id = 500
        self.render("download.html", val_userid=user_id)
        # den button braucht es aber trotzdem um den download manuell anzustossen

        # or use self.redirect("/download")  # but than I can not pass the user_id
        # the above is what I seached to move from one handler to another
        """

        # im mat file sind die farben
        # texturen waeren dann nochmals in separaten files
        # hier wird es interresant, da ein automatisches zuweisen der texturen nach material im
        # ja moeglich waere !!!
        # aktuell nur mir obj umsetzen um zu zeigen es geht
        # aber text dazuschreiben

        # https://stackoverflow.com/questions/50949010/returning-response-of-tornado-post-request

        # **********************
        # ich glaube dies sollte in separaten handler ... !
        # dann kann man das file auch spaeter noch uploaden
        # mach doch vorerst ein download button
        # ein mini example fuer ohne download button und dann ab ins forum

        # ATM we only return the obj file and not the mtl files
        # there have to be two files
        # https://blenderartists.org/t/merge-mtl-file-to-obj-file/551954
        _file_path = _filepath_obj

        if not exists(_file_path):
            print("OBJ file does not exist!")
            raise HTTPError(404)
        else:
            print("YEAH")
            print(_file_path)
            print(basename(_file_path))

        self.set_header("Content-Type", "application/force-download")
        self.set_header(
            "Content-Disposition",
            "attachment; filename={}".format(basename(_file_path)),
        )

        with open(_file_path, "rb") as f:
            try:
                while True:
                    _buffer = f.read(4096)
                    if _buffer:
                        self.write(_buffer)
                    else:
                        f.close()
                        self.finish()
                        return
            except:
                raise HTTPError(404)
        raise HTTPError(500)

    """
    # https://stackoverflow.com/questions/13730945/how-do-i-return-http-error-code-without-default-template-in-tornado
    # https://www.programcreek.com/python/example/4068/tornado.web.HTTPError
    def write_error(self, status_code, **kwargs):
        self.set_status(status_code)
        if kwargs['reason']:
            self.finish(kwargs['reason'])
        else: 
            self.finish("<html><title>%(code)d: %(message)s</title>"
                "<body>%(code)d: %(message)s</body></html>" % {
                    "code": status_code,
                    "message": self._reason,
            })
    """


if __name__ == "__main__":

    app = tornado.web.Application(
        [(r"/", MainHandler), ("/upload", UploadHandler)],
        template_path="templates",
        debug=True,
    )

    # boilerplate code for tornado
    app.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
