'''
terminal:
cd /home/hugo/Documents/projekte--BIM--opendev/dev--tornado/freecad/01--hello_freecad/
python3 hello_freecad3.py


browser:
http://localhost:8888/
http://localhost:8888/?length=7&width=8&height=9
http://localhost:8888/?length=7.77&width=8.88&height=9.99


terminal:
curl http://localhost:8888/
curl "http://localhost:8888/?length=7&width=8&height=9"
curl "http://localhost:8888/?length=7.77&width=8.88&height=9.99"

'''

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web


from tornado.options import define, options
define(
    "port",
    default=8888,
    help="run on the given port",
    type=int
)


def get_box_volume(l, w, h):
    import sys
    sys.path.append('/home/hugo/Documents/dev/freecad/freecadbhb_dev/build/lib')
    import FreeCAD
    import Part
    return round(Part.makeBox(l, w, h).Volume, 2)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        l = float(self.get_argument('length', '1'))
        w = float(self.get_argument('width', '5'))
        h = float(self.get_argument('height', '3'))
        self.write(
            "FreeCAD box volume (l={},w={},h={}): {}\n"
            .format(l, w, h, get_box_volume(l, w, h))
        )


if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = tornado.web.Application(handlers=[(r"/", MainHandler)])

    # boilerplate code for tornado
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()


'''


        '''
