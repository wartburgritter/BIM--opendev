'''
terminal:
cd /home/hugo/Documents/projekte--BIM--opendev/dev--tornado/freecad/01--hello_freecad/
python3 hello_freecad6.py


browser:
http://localhost:8888/

oder direkt zur Volumenausgabe:
http://localhost:8888/volume?length=7&width=8&height=9

http://localhost:8888/download


terminal:

macht nicht unbedingt sinn, da die html daten zurueckgegeben werden ... 

curl http://localhost:8888/
curl "http://localhost:8888/?length=7&width=8&height=9"
curl "http://localhost:8888/volume?length=7&width=8&height=9"

'''

import os.path

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web


from tornado.options import define, options
define(
    "port",
    default=8888,
    help="run on the given port",
    type=int
)


def add_fc_to_path():
    import sys
    from platform import system
    if system() == "Linux":
        fc_pyd_path = '/home/hugo/Documents/dev/freecad/freecadbhb_dev/build/lib'
    elif system() == "Windows":
        fc_pyd_path = 'C:/0_BHA_privat/progr/FreeCAD_0.19.xxxxx_Py3Qt5/bin'
    else:
        print("Not supported operating system.\n")
        return
    if fc_pyd_path not in sys.path:
        sys.path.append(fc_pyd_path)


def get_fc_doc_data():

    # workaround
    # the reason behind is ATM I do not know how to trasfer data between RequestHandlers
    # thus use a hardcoded _file_path
    # whath could happen is if multiple browser windows are open a file will be overwritten
    # thus best would be to create the file on Request or transfer file handle

    # https://stackoverflow.com/questions/12240285/how-to-share-data-between-requests-in-tornado-web/12243529
    # https://stackoverflow.com/questions/25067916/python-tornado-updating-shared-data-between-requests

    return "OnlineBox", "/tmp"


def get_box_volume(l, w, h):

    # spaeter bei ifc evtl. probleme mit obj ViewProvider
    # da FreeCADGui nicht laeuft ... mal sehn ... ist auf jeden Fall loesbar

    _doc_name, _doc_dir = get_fc_doc_data()
    _doc_path = os.path.join(_doc_dir, _doc_name + ".FCStd")

    add_fc_to_path()
    import FreeCAD
    import Part
    doc_obj = FreeCAD.newDocument(_doc_name)
    box_obj = doc_obj.addObject("Part::Box", "TheOnlineBox")
    box_obj.Length = l
    box_obj.Width = w
    box_obj.Height = h
    doc_obj.recompute()
    doc_obj.saveAs(_doc_path)

    vol = round(box_obj.Shape.Volume, 2)
    FreeCAD.closeDocument(_doc_name)

    return vol


class IndexHandler(tornado.web.RequestHandler):
 
    def get(self):
        self.render('index.html')


class FreeCADVolumeHandler(tornado.web.RequestHandler):

    def get(self):
        l = float(self.get_argument('length', '1'))
        w = float(self.get_argument('width', '5'))
        h = float(self.get_argument('height', '3'))
        # TODO use float directly in the form
        self.render(
            'volume.html',
            l=l,
            w=w,
            h=h,
            v=get_box_volume(l, w, h)
        )


class FileDownloadHandler(tornado.web.RequestHandler):

    def get(self):

        # self.write("Download will start soon!\n")

        file_name, _file_dir = get_fc_doc_data()
        file_name += ".FCStd"
        _file_path = os.path.join(_file_dir, file_name)
        if not file_name or not os.path.exists(_file_path):
            raise HTTPError(404)

        self.set_header('Content-Type', 'application/force-download')
        self.set_header('Content-Disposition', 'attachment; filename={}'.format(file_name))

        with open(_file_path, "rb") as f:
            try:
                while True:
                    _buffer = f.read(4096)
                    if _buffer:
                        self.write(_buffer)
                    else:
                        f.close()
                        self.finish()
                        return
            except:
                raise HTTPError(404)
        raise HTTPError(500)


if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = tornado.web.Application(
        handlers = [
            (r'/', IndexHandler),
            (r'/volume', FreeCADVolumeHandler),
            (r'/download', FileDownloadHandler)
        ],
        template_path = os.path.join(os.path.dirname(__file__), "templates_hello_freecad6")
    )

    # boilerplate code for tornado
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
