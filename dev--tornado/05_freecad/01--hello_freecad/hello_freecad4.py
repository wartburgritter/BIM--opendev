'''
terminal:
cd /home/hugo/Documents/projekte--BIM--opendev/dev--tornado/freecad/01--hello_freecad/
python3 hello_freecad4.py


browser:
http://localhost:8888/

oder direkt zur Volumenausgabe:
http://localhost:8888/volume?length=7&width=8&height=9


terminal:

macht nicht unbedingt sinn, da die html daten zurueckgegeben werden ... 

curl http://localhost:8888/
curl "http://localhost:8888/?length=7&width=8&height=9"
curl "http://localhost:8888/volume?length=7&width=8&height=9"

'''

import os.path

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web


from tornado.options import define, options
define(
    "port",
    default=8888,
    help="run on the given port",
    type=int
)


def get_box_volume(l, w, h):
    import sys
    from platform import system
    if system() == "Linux":
        sys.path.append('/home/hugo/Documents/dev/freecad/freecadbhb_dev/build/lib')
    elif system() == "Windows":
        sys.path.append('C:/0_BHA_privat/progr/FreeCAD_0.19.xxxxx_Py3Qt5/bin')
    else:
        print("Not supported operating system.\n")
    import FreeCAD
    import Part
    return round(Part.makeBox(l, w, h).Volume, 2)


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('index.html')


class FreeCADVolumeHandler(tornado.web.RequestHandler):
    def get(self):
        l = float(self.get_argument('length', '1'))
        w = float(self.get_argument('width', '5'))
        h = float(self.get_argument('height', '3'))
        # TODO use float directly in the form
        self.render(
            'volume.html',
            l=l,
            w=w,
            h=h,
            v=get_box_volume(l, w, h)
        )


if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = tornado.web.Application(
        handlers = [
            (r'/', IndexHandler),
            (r'/volume', FreeCADVolumeHandler)
        ],
        template_path = os.path.join(os.path.dirname(__file__), "templates_hello_freecad4")
    )

    # boilerplate code for tornado
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
