'''
terminal:
cd /home/hugo/Documents/projekte--BIM--opendev/dev--tornado/freecad/01--hello_freecad/
python3 hello_freecad2.py

browser:
http://localhost:8888/

terminal:
curl http://localhost:8888/

'''

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web


from tornado.options import define, options
define(
    "port",
    default=8888,
    help="run on the given port",
    type=int
)


def get_box_volume():
    import sys
    sys.path.append('/home/hugo/Documents/dev/freecad/freecadbhb_dev/build/lib')
    import FreeCAD
    import Part
    return Part.makeBox(1, 1, 1).Volume


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write(
            "Hello, world and FreeCAD users. This is the volume of a FreeCAD internet box: {}\n"
            .format(get_box_volume())
        )


if __name__ == "__main__":
    app = tornado.web.Application(handlers=[(r"/", MainHandler)])

    # boilerplate code for tornado
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
