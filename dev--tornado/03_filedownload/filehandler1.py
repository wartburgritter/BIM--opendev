'''
terminal:
cd /home/hugo/Documents/projekte--BIM--opendev/dev--tornado/filedownload/
python3 filehandler1.py

browser:
http://localhost:8888/


terminal:
curl http://localhost:8888/

based on:
https://gist.github.com/alejandrobernardis/1790864
'''

import os

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options
define("port", default=8888, help="run on the given port", type=int)


class MyFileHandler(tornado.web.RequestHandler):

    def get(self):

        file_name = "datei1.txt"
        _file_dir = os.path.join(os.path.abspath(""), "downloads")
        _file_path = os.path.join(_file_dir, file_name)
        if not file_name or not os.path.exists(_file_path):
            raise HTTPError(404)

        self.set_header('Content-Type', 'application/force-download')
        self.set_header('Content-Disposition', 'attachment; filename={}'.format(file_name))

        with open(_file_path, "rb") as f:
            try:
                while True:
                    _buffer = f.read(4096)
                    if _buffer:
                        self.write(_buffer)
                    else:
                        f.close()
                        self.finish()
                        return
            except:
                raise HTTPError(404)
        raise HTTPError(500)


if __name__ == "__main__":
    app = tornado.web.Application(handlers=[(r"/", MyFileHandler)])

    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
