###################################
# inputparameter
l_x=200.0
l_y=330.0
l_cx=20.0
l_cy=80.0
l_cz=200.0
hBodenplatte=20.0
hFundament=50.0
h=hFundament+hBodenplatte

c_ue = 3.0     # Rebarueberdeckung bis Aussenkante Rebar 
c_hnom = 5.0   # seitliche ueberdeckung
A_s_d = 1.6   # Rebardurchmesser
A_s_s = 15.0  # Rebarabstand


# parameter bewehrung
# x --> Lage 1
c_xnom=c_ue+0.5*A_s_d
c_ynom=c_ue+1.5*A_s_d
aussenradius = 6*A_s_d                    # 6xDurchmesser
innenradius = 0.5*A_s_d



###################################
#freecad
import FreeCAD,Draft

uf = 10  # unitfaktor cm --> mm = 10
myDoc = App.newDocument("Einzelfundament")


fundBox = myDoc.addObject("Part::Box","Vertiefung")
fundBox.Height = hFundament*uf
fundBox.Length = l_x*uf
fundBox.Width = l_y*uf
fundBox.Placement.Base = (-0.5*l_x*uf, -0.5*l_y*uf, -h*uf)
fundBoxVO = fundBox.ViewObject
fundBoxVO.Transparency = 75

bplBox = myDoc.addObject("Part::Box","BPL")
bplBox.Height = hBodenplatte*uf
bplBox.Length = 1.5*l_x*uf
bplBox.Width = 1.5*l_y*uf
bplBox.Placement.Base = (-1.5*0.5*l_x*uf, -1.5*0.5*l_y*uf, -hBodenplatte*uf)
bplBoxVO = bplBox.ViewObject
bplBoxVO.Transparency = 75

stuetzBox = myDoc.addObject("Part::Box","Stuetze")
stuetzBox.Height = l_cz*uf
stuetzBox.Length = l_cx*uf
stuetzBox.Width = l_cy*uf

myDoc.recompute()
Gui.activeDocument().activeView().viewAxometric()
Gui.SendMsgToActiveView("ViewFit")
stuetzBox.Placement.Base = (-0.5*l_cx*uf, -0.5*l_cy*uf, 0)

#Gui.ActiveDocument.Vertiefung.hide()
#Gui.ActiveDocument.BPL.hide()
#Gui.ActiveDocument.Stuetze.hide()




#######################################
# reinforcement

# IN X-RICHTUNG
rebarX1 = myDoc.addObject("Part::Cylinder","Rebar_X_Part_1")
rebarX1.Radius = 0.5*A_s_d*uf
# laenge und verschiebung wenn eisen nur gerade
# rebarX1.Height = (l_x-2*c_hnom)*uf
#rebarX1.Placement.Base = (-(0.5*l_x-c_hnom)*10, 0 , -(h-c_xnom)*uf)
rebarX1.Height = (l_x-2*c_hnom-2*aussenradius)*uf
rebarX1.Placement.Base = (-(0.5*l_x-c_hnom-aussenradius)*uf, 0 , -(h-c_xnom)*uf)
rebarX1.Placement.Rotation = (0, 0.707, 0, 0.707)  # nicht exakt 90 grad!!!

rebarX2 = myDoc.addObject("Part::Cylinder","Rebar_X_Part_2")
rebarX2.Radius = 0.5*A_s_d*uf
rebarX2.Height = (h-2*c_hnom-2*aussenradius+1.5*A_s_d)*uf
rebarX2.Placement.Base = (-(0.5*l_x-c_hnom)*uf, 0 , -(h-c_xnom-aussenradius)*uf)
# laenge und verschiebung wenn eisen nur gerade
# rebarX2.Height = (h-2*c_hnom)*uf    
# rebarX2.Placement.Base = (-(0.5*l_x-c_hnom)*uf, 0 , -(h-c_xnom)*uf)   

rebarX3 = myDoc.addObject("Part::Cylinder","Rebar_X_Part_3")
rebarX3.Radius =0.5*A_s_d*uf
#rebarX3.Height = (h-2*c_hnom-aussenradius+A_s_d)*uf
rebarX3.Height = (h-2*c_hnom-2*aussenradius+1.5*A_s_d)*uf
rebarX3.Placement.Base = ((0.5*l_x-c_hnom)*uf, 0 , -(h-c_xnom-aussenradius)*uf)
# laenge und verschiebung wenn eisen nur gerade
# rebarX3.Height = (h-2*c_hnom)*uf     
# rebarX3.Placement.Base = ((0.5*l_x-c_hnom)*uf, 0 , -(h-c_xnom)*uf)

rebarX4 = myDoc.addObject("Part::Torus","Rebar_X_Part_4")
rebarX4.Radius1 = aussenradius*uf
rebarX4.Radius2 = innenradius*uf
rebarX4.Angle3 = 90
rebarX4.Placement.Base = ((-(0.5*l_x-c_hnom)+aussenradius)*uf, 0 , (-(h-c_xnom)+aussenradius)*uf)
rebarX4.Placement.Rotation = (-0.5,0.5,0.5,0.5)  
myDoc.recompute()

rebarX5 = myDoc.addObject("Part::Torus","Rebar_X_Part_5")
rebarX5.Radius1 = aussenradius*uf
rebarX5.Radius2 = innenradius*uf
rebarX5.Angle3 = 90
rebarX5.Placement.Base = (((0.5*l_x-c_hnom)-aussenradius)*uf, 0 , (-(h-c_xnom)+aussenradius)*uf)
rebarX5.Placement.Rotation = (-0.5,-0.5,0.5,-0.5)  
myDoc.recompute()

rebarX6 = myDoc.addObject("Part::Torus","Rebar_X_Part_6")
rebarX6.Radius1 = aussenradius*uf
rebarX6.Radius2 = innenradius*uf
rebarX6.Angle3 = 90
rebarX6.Placement.Base = ((-(0.5*l_x-c_hnom)-aussenradius)*uf, 0 , ((-c_xnom)-aussenradius)*uf)
rebarX6.Placement.Rotation = (0.707,0,0,0.707)  
myDoc.recompute()

rebarX7 = myDoc.addObject("Part::Torus","Rebar_X_Part_7")
rebarX7.Radius1 = aussenradius*uf
rebarX7.Radius2 = innenradius*uf
rebarX7.Angle3 = 90
rebarX7.Placement.Base = (((0.5*l_x-c_hnom)+aussenradius)*uf, 0 , ((-c_xnom)-aussenradius)*uf)
rebarX7.Placement.Rotation = (0,0.707,0.707,0)  

rebarX8 = myDoc.addObject("Part::Cylinder","Rebar_X_Part_8")
rebarX8.Radius = 0.5*A_s_d*uf
rebarX8.Height = 25*A_s_d*uf
rebarX8.Placement.Base = ((0.5*l_x-c_hnom+aussenradius)*uf, 0 , -(c_xnom)*uf)
rebarX8.Placement.Rotation = (0, 0.707, 0, 0.707)  # nicht exakt 90 grad!!!

rebarX9 = myDoc.addObject("Part::Cylinder","Rebar_X_Part_9")
rebarX9.Radius = 0.5*A_s_d*uf
rebarX9.Height = 25*A_s_d*uf
rebarX9.Placement.Base = (-(0.5*l_x-c_hnom+aussenradius+25*A_s_d)*uf, 0 , -(c_xnom)*uf)
rebarX9.Placement.Rotation = (0, 0.707, 0, 0.707)  # nicht exakt 90 grad!!!
myDoc.recompute()

# einzelne Rebarparts zusammenfuegen
rebarX = myDoc.addObject("Part::MultiFuse","Rebar_X")
rebarX.Shapes = [rebarX1, rebarX2, rebarX3, rebarX4, rebarX5, rebarX6, rebarX7, rebarX8, rebarX9]
myDoc.recompute()
rebarX_VO = rebarX.ViewObject
rebarX_VO.ShapeColor = (0.40,0.20,0.00)
myDoc.recompute()

# Asx in y-Richtung verlegen
nrOf_A_s_s_X = (l_y-2*c_hnom)//A_s_s                    # Verteilung in Y
real_c_hnom = 0.5*(l_y-nrOf_A_s_s_X*A_s_s)
nrOFRCBarsX = int (nrOf_A_s_s_X)+1
#rebarX.Placement.Base = (0,0,0)  # ist nicht global null, sondern des basiselementes also im Fundmittelpkt :-)
rebarX.Placement.Base = (0,(-0.5*l_y+real_c_hnom)*uf,0)  
myArrayX = Draft.makeArray(rebarX,FreeCAD.Vector(0,0,0),FreeCAD.Vector(0,A_s_s*uf,0),1,nrOFRCBarsX)
myDoc.recompute()
myArrayX_VO = myArrayX.ViewObject
myArrayX_VO.ShapeColor = (0.40,0.20,0.00)
myDoc.recompute()







# IN Y-RICHTUNG
rebarY1 = myDoc.addObject("Part::Cylinder","Rebar_Y_Part_1")
rebarY1.Radius = 0.5*A_s_d*uf
# laenge und verschiebung wenn eisen nur gerade
#rebarY1.Height = (l_y-2*c_hnom)*uf
#rebarY1.Placement.Base = (0, (0.5*l_y-c_hnom)*uf , -(h-c_ynom)*uf)
rebarY1.Height = (l_y-2*c_hnom-2*aussenradius)*uf
rebarY1.Placement.Base = (0, (0.5*l_y-c_hnom-aussenradius)*uf , -(h-c_ynom)*uf)
rebarY1.Placement.Rotation = (0.707, 0, 0, .707)  # nicht exakt 90 grad!!!

rebarY2 = myDoc.addObject("Part::Cylinder","Rebar_Y_Part_2")
rebarY2.Radius = 0.5*A_s_d*uf
# laenge und verschiebung wenn eisen nur gerade
#rebarY2.Height = (h-2*c_hnom-aussenradius+A_s_d)*uf
#rebarY2.Placement.Base = (0, -(0.5*l_y-c_hnom)*uf , -(h-c_ynom-aussenradius)*uf)
rebarY2.Height = (h-2*c_hnom-2*aussenradius-0.5*A_s_d)*uf
rebarY2.Placement.Base = (0, -(0.5*l_y-c_hnom)*uf , -(h-c_ynom-aussenradius)*uf)

rebarY3 = myDoc.addObject("Part::Cylinder","Rebar_Y_Part_3")
rebarY3.Radius = 0.5*A_s_d*uf
# laenge und verschiebung wenn eisen nur gerade
#rebarY3.Height = (h-2*c_hnom-aussenradius+A_s_d)*uf
#rebarY3.Placement.Base = (0,  (0.5*l_y-c_hnom)*uf, -(h-c_ynom-aussenradius)*uf)
rebarY3.Height = (h-2*c_hnom-2*aussenradius-0.5*A_s_d)*uf
#rebarY3.Height = (h-2*c_hnom-aussenradius-A_s_d)*uf
rebarY3.Placement.Base = (0,  (0.5*l_y-c_hnom)*uf, -(h-c_ynom-aussenradius)*uf)

rebarY4 = myDoc.addObject("Part::Torus","Rebar_Y_Part_4")
rebarY4.Radius1 = aussenradius*uf
rebarY4.Radius2 = innenradius*uf
rebarY4.Angle3 = 90
rebarY4.Placement.Base = (0, (-(0.5*l_y-c_hnom)+aussenradius)*uf , (-(h-c_ynom)+aussenradius)*uf)
rebarY4.Placement.Rotation = (0.5,-0.5,0.5,-0.5)
myDoc.recompute()

rebarY5 = myDoc.addObject("Part::Torus","Rebar_Y_Part_5")
rebarY5.Radius1 = aussenradius*uf
rebarY5.Radius2 = innenradius*uf
rebarY5.Angle3 = 90
rebarY5.Placement.Base = (0, ((0.5*l_y-c_hnom)-aussenradius)*uf , (-(h-c_ynom)+aussenradius)*uf)
rebarY5.Placement.Rotation = (-0.5,-0.5,0.5,0.5) 
myDoc.recompute()

rebarY6 = myDoc.addObject("Part::Torus","Rebar_Y_Part_6")
rebarY6.Radius1 = aussenradius*uf
rebarY6.Radius2 = innenradius*uf
rebarY6.Angle3 = 90
rebarY6.Placement.Base = (0, (-(0.5*l_y-c_hnom)-aussenradius)*uf ,((-c_ynom)-aussenradius)*uf)
rebarY6.Placement.Rotation = (0.5,0.5,0.5,0.5)
myDoc.recompute()

rebarY7 = myDoc.addObject("Part::Torus","Rebar_Y_Part_7")
rebarY7.Radius1 = aussenradius*uf
rebarY7.Radius2 = innenradius*uf
rebarY7.Angle3 = 90
rebarY7.Placement.Base = (0, ((0.5*l_y-c_hnom)+aussenradius)*uf , ((-c_ynom)-aussenradius)*uf)
rebarY7.Placement.Rotation = (-0.5,0.5,0.5,-0.5) 
myDoc.recompute()

rebarY8 = myDoc.addObject("Part::Cylinder","Rebar_Y_Part_8")
rebarY8.Radius = 0.5*A_s_d*uf
rebarY8.Height = 25*A_s_d*uf
rebarY8.Placement.Base = (0, -(0.5*l_y-c_hnom+aussenradius)*uf , (-c_ynom)*uf)
rebarY8.Placement.Rotation = (0.707, 0, 0, .707)  # nicht exakt 90 grad!!!

rebarY9 = myDoc.addObject("Part::Cylinder","Rebar_Y_Part_9")
rebarY9.Radius = 0.5*A_s_d*uf
rebarY9.Height = 25*A_s_d*uf
rebarY9.Placement.Base = (0, (0.5*l_y-c_hnom+aussenradius+25*A_s_d)*uf , (-c_ynom)*uf)
rebarY9.Placement.Rotation = (0.707, 0, 0, 0.707)  # nicht exakt 90 grad!!!

# einzelne Rebarparts zusammenfuegen
rebarY = myDoc.addObject("Part::MultiFuse","Rebar_Y")
rebarY.Shapes = [rebarY1, rebarY2, rebarY3, rebarY4, rebarY5, rebarY6, rebarY7, rebarY8, rebarY9]
myDoc.recompute()
rebarY_VO = rebarY.ViewObject
rebarY_VO.ShapeColor = (0.40,0.20,0.00)
myDoc.recompute()

# Asy in x-Richtung verlegen
nrOf_A_s_s_Y = (l_x-2*c_hnom)//A_s_s                    # Verteilung in Y
real_c_hnom = 0.5*(l_x-nrOf_A_s_s_Y*A_s_s)
nrOFRCBarsY = int (nrOf_A_s_s_Y)+1
#rebarY.Placement.Base = (0,0,0)  # ist nicht global null, sondern des basiselementes also im Fundmittelpkt :-)
rebarY.Placement.Base = ((-0.5*l_x+real_c_hnom)*uf,0,0)  
myArrayY = Draft.makeArray(rebarY,FreeCAD.Vector(A_s_s*uf,0,0),FreeCAD.Vector(0,0,0),nrOFRCBarsY,1)
myDoc.recompute()
myArrayY_VO = myArrayY.ViewObject
myArrayY_VO.ShapeColor = (0.40,0.20,0.00)
myDoc.recompute()
# END


####################################
#Drawing
import FreeCAD, Drawing


#Insert a Page object and assign a template
page1 = myDoc.addObject('Drawing::FeaturePage','Page')
App.ActiveDocument.Page.Template = App.getResourceDir()+'Mod/Drawing/Templates/A4_Simple.svg'
myDoc.recompute()    # wenn jetzt schon recompute dann erscheint weisses Blatt

#Create the views  and put them on the page
view1 = myDoc.addObject('Drawing::FeatureViewPart','View_1')
view1.Source = myDoc.Array        # Warum Verweis auf String
myDoc.Page.addObject(myDoc.View_1)
view2 = myDoc.addObject('Drawing::FeatureViewPart','View_2')
view2.Source = myDoc.Array001
myDoc.Page.addObject(myDoc.View_2)
view3 = myDoc.addObject('Drawing::FeatureViewPart','View_3')
view3.Source = myDoc.Vertiefung
myDoc.Page.addObject(myDoc.View_3)
view4 = myDoc.addObject('Drawing::FeatureViewPart','View_4')
view4.Source = myDoc.BPL
myDoc.Page.addObject(myDoc.View_4)
view5 = myDoc.addObject('Drawing::FeatureViewPart','View_5')
view5.Source = myDoc.Stuetze
myDoc.Page.addObject(myDoc.View_5)
myDoc.recompute()

# Change the properties of the views
scaleFactor = 0.035
viewVector = FreeCAD.Vector(1.0,1.4,0.75)
viewLocationX=150
viewLocationY=320
viewRotation=0

view1.Direction = (viewVector)
view1.X = viewLocationX
view1.Y = viewLocationY
view1.Scale = scaleFactor
view1.Rotation = viewRotation
view1.ShowHiddenLines = True
view1.LineWidth = 0.18
view2.Direction = (viewVector)
view2.X = viewLocationX
view2.Y = viewLocationY
view2.Scale =  scaleFactor
view2.Rotation = viewRotation
view2.LineWidth = 0.18
view2.ShowHiddenLines = True
view3.Direction = (viewVector)
view3.X = viewLocationX
view3.Y = viewLocationY
view3.Scale =  scaleFactor
view3.Rotation = viewRotation
view3.LineWidth = 0.35
view3.ShowHiddenLines = True
view4.Direction = (viewVector)
view4.X = viewLocationX
view4.Y = viewLocationY
view4.Scale =  scaleFactor
view4.Rotation = viewRotation
view4.LineWidth = 0.7
view4.ShowHiddenLines = True
view5.Direction = (viewVector)
view5.X = viewLocationX
view5.Y = viewLocationY
view5.Scale =  scaleFactor
view5.Rotation = viewRotation
view5.LineWidth = 0.7
view5.ShowHiddenLines = True
myDoc.recompute()



# create text
text1 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_1')
myDoc.Page.addObject(myDoc.Text_1)
text2 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_2')
myDoc.Page.addObject(myDoc.Text_2)
myDoc.recompute()


# change text
textScaleFactor = 5

text1.X = 30
text1.Y = 30
text1.Scale = 7.5  # Ueberschrift
text1.Text = [unicode('Calculation of a Single Column Foundation', 'utf-8'),]
text2.X = 30
text2.Y = 50
text2.Scale = textScaleFactor
myText = ['foundation length in x:', str(l_x)]
text2.Text = (myText)
myDoc.recompute()
















