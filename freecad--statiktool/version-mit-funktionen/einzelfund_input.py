# eingabe
# hier auch berechnung --> aufruf von funktionen keine formeln hier
# durchstanzen muss ein funktionsaufruf mit normenangabe sein
# im octave blick man gut durch in einem pythonprgramm auch, aber hier,
# blicht doch kein mensch mehr durch!!!!


import einzelfund_toolbox

# output is for debugging reason only
# 1 enables debugg mode, 0 disables debugg mode
debugg = 1

if debugg:
  print
  print "!!!debugging mode enabled!!!"
  print
else:
  print "no debugging"

########################################
## pendent get all materialvaluse from database using functions
#input material, normtyp and normversion  norm in englisch????
normtyp="SIA262"
normversion="2003"
concrete="C25/30"
#material mit teilsicherheitsfaktoren
f_cd=1.65
tau_cd=0.1
g_ck=25E-6               # 1 m3 = 1E-6 cm3
f_sd=43.5
#teilsicherheitsfaktoren last
gamma_g=1.35
gamma_p=1.5




#input geometrie
h=45.0
l_x=170.0
l_y=170.0
c_xnom=4.0
c_ynom=6.0
l_cx=40.0
l_cy=30.0
A_sx=18.0
A_sy=18.0

#input force
V_Ed=1170.0
#DLA=0.7
DLA=0.6666666667    # gamma_gp=1.4 :-)

if debugg:
  print "h = ", h
  print "l_x = ", l_x
  print "l_y = ", l_y
  print "c_xnom = ", c_xnom
  print "c_ynom = ", c_ynom
  print "l_cx = ", l_cx
  print "l_cy = ", l_cy
  print "A_sx = ", A_sx
  print "A_sy = ", A_sy
  print

#########################################
#Calculations
#eigentlich ne klasse querschnitt und dann wird eine instanz der klasse uebergeben

gamma_gp=gamma_g*DLA+gamma_p*(1-DLA)
if debugg:
  print "gamma_gp = ", gamma_gp
  print 


d_x = einzelfund_toolbox.calculate_CS_Values (h, c_xnom)[0]
d_y = einzelfund_toolbox.calculate_CS_Values (h, c_ynom)[0]
z_x = einzelfund_toolbox.calculate_CS_Values (h, c_xnom)[1]
z_y = einzelfund_toolbox.calculate_CS_Values (h, c_ynom)[1]
d_avg = einzelfund_toolbox.calculate_CS_Values (h, 0.5*(c_xnom+c_ynom))[0]
z_avg = einzelfund_toolbox.calculate_CS_Values (h, 0.5*(c_xnom+c_ynom))[1]

if debugg:
  print "d_x = ", d_x 
  print "d_y = ", d_y
  print "z_x = ", z_x
  print "z_y = ", z_y 
  print "d_avg = ", d_avg
  print "z_avg = ", z_avg
  print


M_Rdx = einzelfund_toolbox.calculate_Resistance_Beding_Moment (A_sx, f_sd, z_x)
M_Rdy = einzelfund_toolbox.calculate_Resistance_Beding_Moment (A_sy, f_sd, z_y)
m_Rdx=M_Rdx/l_y
m_Rdy=M_Rdy/l_x
a_sx=A_sx/l_y
a_sy=A_sy/l_x
roh_x=a_sx/d_x
roh_y=a_sy/d_y
if debugg:
  print "M_Rdx = ", M_Rdx*1E-2                #kNcm-->kNm 
  print "M_Rdy = ", M_Rdy*1E-2                #kNcm-->kNm 
  print "m_Rdx = ", m_Rdx                     #kNcm-->kNm 
  print "m_Rdy = ", m_Rdy                     #kNcm-->kNm 
  print "a_sx = ", a_sx*1E2                   #cm2/cm-->cm2/m
  print "a_sy = ", a_sy*1E2                   #cm2/cm-->cm2/m
  print "roh_x = ", roh_x*1E2                 #%
  print "roh_y = ", roh_y*1E2                 #%
  print
  

V_Ek=V_Ed/gamma_gp
sigma_Ek=(V_Ek/(l_x*l_y)+h*g_ck)      
sigma_Ed=V_Ed/(l_x*l_y)         
m_0dx=0.5*sigma_Ed*(0.5*(l_x-l_cx))**2
m_0dy=0.5*sigma_Ed*(0.5*(l_y-l_cy))**2
  
if debugg:
  print "sigma_Ek = ", sigma_Ek*1E4              # kN/cm2-->kN/m2
  print "sigma_Ed = ", sigma_Ed*1E4              # kN/cm2-->kN/m2
  print "m_0dx = ", m_0dx
  print "m_0dy = ", m_0dy
  

# durchstanzen
u = einzelfund_toolbox.calculate_Durchstanzen_Umfang (l_cx, l_cy, d_avg)
k_r = einzelfund_toolbox.calculate_Durchstanzen_Beiwert (l_x, l_y, m_0dx, m_0dy, m_Rdx, m_Rdy, d_avg)
V_Rd=u*d_avg*k_r*tau_cd


if debugg:
  print "u = ", u
  print "k_r = ", k_r
  print "V_Rd = ", V_Rd
  print 

  
# zuerst etwas, dass mir auf arbeit sofort nuetz
# berechnung der bewehrung!!!
  
  
######freecad###########
# neues document
# box fundament 
# stuetze 2 m hoch
# 
  
  
  
  
  
  
  
  
  

