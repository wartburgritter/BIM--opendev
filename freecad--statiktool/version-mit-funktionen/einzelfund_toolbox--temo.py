# output only for debugging --> in if abfragen gemaess parameter


def sum(a,b):
  return a + b
 

##########################################
# calculations sicherheiten auslagern zu sicherheiten
gamma_gp=gamma_g*DLA+gamma_p*(1-DLA)
#gamma_gp=1.4
print "gamma_gp", gamma_gp

#calculationsgeometrie auslagern zu querschnittswerte
d_x=h-c_xnom
d_y=h-c_ynom
d_avg=0.5*(d_x+d_y)
z_x=0.95*d_x
z_y=0.95*d_y



#########################################
#Biegung (Bewehrung und Biegewiderstand)
#Widerstand
M_Rdx=A_sx*f_sd*z_x
m_Rdx=M_Rdx/l_y
a_sx=A_sx/l_y
roh_x=a_sx/d_x
M_Rdy=A_sy*f_sd*z_y
m_Rdy=M_Rdy/l_x
a_sy=A_sy/l_x
roh_y=a_sy/d_y

print "M_Rdx*1E-2", M_Rdx*1E-2                #kNcm-->kNm 
print "a_sx*1E2", a_sx*1E2                    #cm2/cm-->cm2/m
print "roh_x*1E2", roh_x*1E2                  #%
print "a_sy*1E2", a_sy*1E2                    #cm2/cm-->cm2/m
print "roh_y*1E2", roh_y*1E2                  #%
print "M_Rdy*1E-2", M_Rdy*1E-2                #kNcm-->kNm 

#Einwirkung
M_Edx=0.125*V_Ed*l_x*(1-l_cx/l_x)
m_Edx=M_Edx/l_y
M_Edy=0.125*V_Ed*l_y*(1-l_cy/l_y)
m_Edy=M_Edy/l_x

print "M_Edx*1E-2", M_Edx*1E-2                #kNcm-->kNm
print "M_Edy*1E-2", M_Edy*1E-2                #kNcm-->kNm

#erforderlicher Widerstand
erfA_sx=M_Edx/(z_x*f_sd)
erfa_sx=erfA_sx/l_y
erfA_sy=M_Edy/(z_y*f_sd)
erfa_sy=erfA_sy/l_x

print "erfa_sx*1E2", erfa_sx*1E2              #cm2/cm-->cm2/m
print "erfa_sy*1E2", erfa_sy*1E2              #cm2/cm-->cm2/m



#########################################
#Boden
#Einwirkung
V_Ek=V_Ed/gamma_gp
sigma_Ek=(V_Ek/(l_x*l_y)+h*g_ck)      
sigma_Ed=V_Ed/(l_x*l_y)         
print sigma_Ek*1E4                           # kN/cm2-->kN/m2
print sigma_Ed*1E4                           # kN/cm2-->kN/m2



#########################################
#Durchstanzen (Schubwiderstand)
#Widerstand
D_k=(l_cx+l_cy)*0.6366
pi=3.14                                   # pi in python????????
u=(D_k+d_avg)*pi

m_0dx=0.5*sigma_Ed*(0.5*(l_x-l_cx))**2
m_0dy=0.5*sigma_Ed*(0.5*(l_y-l_cy))**2

r_yx=0.7*0.5*l_x*(m_0dx/m_Rdx)**1.5
r_yy=0.7*0.5*l_y*(m_0dy/m_Rdy)**1.5
r_y=max(r_yx,r_yy)
k_r=1/(0.45+0.9*r_y*1E-2)
limk_r=1/(1+2.2*d_avg*1E-2)
##if (k_r<limk_r) k_r=limk_r endif
V_Rd=u*d_avg*k_r*tau_cd

print k_r
print V_Rd

#Einwirkung
Redn=0.2
V_Edeff=V_Ed*(1-Redn)

print V_Edeff




