 

##########################################
def calculate_CS_Values (h, c_nom):
  d=h-c_nom
  z=0.95*d

  return d, z
  
  
##########################################
def calculate_Resistance_Beding_Moment (A_s, f, z):
  M=A_s*f*z
  
  return M 
 
 
##########################################
def calculate_Durchstanzen_Umfang (l_cx, l_cy, d_avg):
  D_k=(l_cx+l_cy)*0.6366
  pi=3.14                                   # pi in python????????
  u=(D_k+d_avg)*pi
 
  return u
  
  
##########################################
def calculate_Durchstanzen_Beiwert (l_x, l_y, m_0dx, m_0dy, m_Rdx, m_Rdy, d_avg):
  r_yx=0.7*0.5*l_x*(m_0dx/m_Rdx)**1.5
  r_yy=0.7*0.5*l_y*(m_0dy/m_Rdy)**1.5
  r_y=max(r_yx,r_yy)
  print "r_y = ", r_y
  k_r=1/(0.45+0.9*r_y*1E-2)
  limk_r=1/(1+2.2*d_avg*1E-2)
  ##if (k_r<limk_r) k_r=limk_r endif

  return k_r
  
  