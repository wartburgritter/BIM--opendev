# versionierung und versionshistorie einfuehren
# nach hochladen auf git und version 0.1

# Version without any functions
pi=3.14                                   # pi in python????????


# calculation of rebar
# 1 uses given erfA_sx and  to calculate the  mode, 0 disables debugg mode


########################################
#material mit teilsicherheitsfaktoren
f_cd=1.65
tau_cd=0.10
g_ck=25E-6               # 1 m3 = 1E-6 cm3
f_sd=43.5

#teilsicherheitsfaktoren last
gamma_g=1.35
gamma_p=1.5


#input geometrie
h=45.0
l_x=170.0
l_y=170.0
l_cx=40.0
l_cy=30.0

# rebar caliber currently x=y, ok for me
A_s_d= 1.6              # Rebardurchmesser gleich fuer X und Y
# Nr and Spaces X of Rebars in X means Spaces in Y
nrOfRCBarsX=9
nrOfRCBarsY=nrOfRCBarsX
nrOfGreaterRCSpacesX=2   # Rebarspaces on each side with bigger rebarspace
nrOfGreaterRCSpacesY=nrOfGreaterRCSpacesX

# more inputparameter
l_cz=200.0
hBodenplatte=20.0
hFundament=h-hBodenplatte
aussenradius = 6*A_s_d                    # 6xDurchmesser
innenradius = 0.5*A_s_d
c_ue=3.4     # cover of reinforcement   # 16er eisen ergibt bei 3.4 d_avg=5.0
# Reinforcement in x-direction is first layer
# cover of reinforcement to the axis of the rebars
c_xnom=c_ue+0.5*A_s_d   
c_ynom=c_ue+1.5*A_s_d  
c_hnom=c_ue+0.5*A_s_d   # at the side and at the top 
A_s_area=0.25*pi*A_s_d**2

#input force
V_Ed=1170.0
#DLA=0.7
DLA=0.6666666667    # gamma_gp=1.4 :-)


##########################################
# calculations sicherheiten
gamma_gp=gamma_g*DLA+gamma_p*(1-DLA)
#gamma_gp=1.4
print "gamma_gp", gamma_gp

#calculationsgeometrie
d_x=h-c_xnom
d_y=h-c_ynom
d_avg=0.5*(d_x+d_y)
z_x=0.95*d_x
z_y=0.95*d_y

A_sx=nrOfRCBarsX*A_s_area
A_sy=nrOfRCBarsY*A_s_area


#########################################
#Biegung (Bewehrung und Biegewiderstand)
#Widerstand
M_Rdx=A_sx*f_sd*z_x
m_Rdx=M_Rdx/l_y
a_sx=A_sx/l_y
roh_x=a_sx/d_x
M_Rdy=A_sy*f_sd*z_y
m_Rdy=M_Rdy/l_x
a_sy=A_sy/l_x
roh_y=a_sy/d_y

print "M_Rdx*1E-2", M_Rdx*1E-2                #kNcm-->kNm 
print "a_sx*1E2", a_sx*1E2                    #cm2/cm-->cm2/m
print "roh_x*1E2", roh_x*1E2                  #%
print "a_sy*1E2", a_sy*1E2                    #cm2/cm-->cm2/m
print "roh_y*1E2", roh_y*1E2                  #%
print "M_Rdy*1E-2", M_Rdy*1E-2                #kNcm-->kNm 

#Einwirkung
M_Edx=0.125*V_Ed*l_x*(1-l_cx/l_x)
m_Edx=M_Edx/l_y
M_Edy=0.125*V_Ed*l_y*(1-l_cy/l_y)
m_Edy=M_Edy/l_x
print "M_Edx*1E-2", M_Edx*1E-2                #kNcm-->kNm
print "M_Edy*1E-2", M_Edy*1E-2                #kNcm-->kNm

#erforderlicher Widerstand
erfA_sx=M_Edx/(z_x*f_sd)
erfa_sx=erfA_sx/l_y
erfA_sy=M_Edy/(z_y*f_sd)
erfa_sy=erfA_sy/l_x

print "erfa_sx*1E2", erfa_sx*1E2              #cm2/cm-->cm2/m
print "erfa_sy*1E2", erfa_sy*1E2              #cm2/cm-->cm2/m



#########################################
#Boden
#Einwirkung
V_Ek=V_Ed/gamma_gp
sigma_Ek=(V_Ek/(l_x*l_y)+h*g_ck)      
sigma_Ed=V_Ed/(l_x*l_y)         
print sigma_Ek*1E4                           # kN/cm2-->kN/m2
print sigma_Ed*1E4                           # kN/cm2-->kN/m2



#########################################
#Durchstanzen (Schubwiderstand)
#Widerstand
D_k=(l_cx+l_cy)*0.6366
u=(D_k+d_avg)*pi

m_0dx=0.5*sigma_Ed*(0.5*(l_x-l_cx))**2
m_0dy=0.5*sigma_Ed*(0.5*(l_y-l_cy))**2

r_yx=0.7*0.5*l_x*(m_0dx/m_Rdx)**1.5
r_yy=0.7*0.5*l_y*(m_0dy/m_Rdy)**1.5
r_y=max(r_yx,r_yy)
k_r=1/(0.45+0.9*r_y*1E-2)
limk_r=1/(1+2.2*d_avg*1E-2)
##if (k_r<limk_r) k_r=limk_r endif
V_Rd=u*d_avg*k_r*tau_cd

print k_r
print V_Rd

#Einwirkung
#Reduction area of force going direct to soil
A_Direct_1=pi*d_avg**2
A_Direct_2=2*d_avg*(l_cx+l_cy)
A_Direct_3=l_cx*l_cy
A_Direct=A_Direct_1+A_Direct_2+A_Direct_3

Redn=0.5*A_Direct/(l_x*l_y)
V_Edeff=V_Ed*(1-Redn)

print Redn
print V_Edeff





###################################
#calculagtions for freecad

# length of spaces by given rebar number and number of bigger spaces
# base rebars, extra rebars
#nrOfExtraRebarsX=nrOfRCBarsX-1-2*nrOfGreaterRCSpacesY
#nrOfExtraRebarsY=nrOfRCBarsY-1-2*nrOfGreaterRCSpacesX
#nrOfBaseRebarsX=nrOfRCBarsX-nrOfExtraRebarsX
#nrOfBaseRebarsY=nrOfRCBarsY-nrOfExtraRebarsY
#nrOfBaseRCSpacesX=nrOfBaseRebarsX-1
#nrOfBaseRCSpacesY=nrOfBaseRebarsY-1

#BaseRCSpaceX=(l_y-2*c_hnom)/nrOfBaseRCSpacesX
#BaseRCSpaceY=(l_x-2*c_hnom)/nrOfBaseRCSpacesY

# besser erst anzahl, dann mit anzahl den zwischenraum, folgende zeilen
nrOfVirtuelRCSpacesX=nrOfRCBarsX-1+2*nrOfGreaterRCSpacesX
nrOfVirtualRCSpacesY=nrOfRCBarsY-1+2*nrOfGreaterRCSpacesY
BaseRCSpaceX=2*(l_y-2*c_hnom)/(nrOfVirtuelRCSpacesX)
BaseRCSpaceY=2*(l_x-2*c_hnom)/(nrOfVirtualRCSpacesY)


#nrOfBaseRebarsX=
#nrOfBaseRebarsY=


###################################
#freecad
import FreeCAD, Draft, Part

uf = 1  # unitfaktor cm --> mm = 10
myDoc = App.newDocument("Einzelfundament")


fundBox = myDoc.addObject("Part::Box","Vertiefung")
fundBox.Height = hFundament*uf
fundBox.Length = l_x*uf
fundBox.Width = l_y*uf
fundBox.Placement.Base = (-0.5*l_x*uf, -0.5*l_y*uf, -h*uf)
fundBoxVO = fundBox.ViewObject
fundBoxVO.Transparency = 75

bplBox = myDoc.addObject("Part::Box","BPL")
bplBox.Height = hBodenplatte*uf
bplBox.Length = 1.5*l_x*uf
bplBox.Width = 1.5*l_y*uf
bplBox.Placement.Base = (-1.5*0.5*l_x*uf, -1.5*0.5*l_y*uf, -hBodenplatte*uf)
bplBoxVO = bplBox.ViewObject
bplBoxVO.Transparency = 75

stuetzBox = myDoc.addObject("Part::Box","Stuetze")
stuetzBox.Height = l_cz*uf
stuetzBox.Length = l_cx*uf
stuetzBox.Width = l_cy*uf

myDoc.recompute()
Gui.activeDocument().activeView().viewAxometric()
Gui.SendMsgToActiveView("ViewFit")
stuetzBox.Placement.Base = (-0.5*l_cx*uf, -0.5*l_cy*uf, 0)

#Gui.ActiveDocument.Vertiefung.hide()
#Gui.ActiveDocument.BPL.hide()
#Gui.ActiveDocument.Stuetze.hide()



#######################################
# reinforcement

# IN X-RICHTUNG
rebarX1 = myDoc.addObject("Part::Cylinder","Rebar_X_Part_1")
rebarX1.Radius = 0.5*A_s_d*uf
rebarX1.Height = (l_x-2*c_hnom-2*aussenradius)*uf
rebarX1.Placement.Base = (-(0.5*l_x-c_hnom-aussenradius)*uf, 0 , -(h-c_xnom)*uf)
rebarX1.Placement.Rotation = (0, 0.707, 0, 0.707)  # nicht exakt 90 grad!!!

rebarX2 = myDoc.addObject("Part::Cylinder","Rebar_X_Part_2")
rebarX2.Radius = 0.5*A_s_d*uf
rebarX2.Height = (h-c_hnom-c_xnom-2*aussenradius)*uf
rebarX2.Placement.Base = (-(0.5*l_x-c_hnom)*uf, 0 , -(h-c_xnom-aussenradius)*uf)

rebarX3 = myDoc.addObject("Part::Cylinder","Rebar_X_Part_3")
rebarX3.Radius =0.5*A_s_d*uf
rebarX3.Height = (h-c_hnom-c_xnom-2*aussenradius)*uf
rebarX3.Placement.Base = ((0.5*l_x-c_hnom)*uf, 0 , -(h-c_xnom-aussenradius)*uf)

rebarX4 = myDoc.addObject("Part::Torus","Rebar_X_Part_4")
rebarX4.Radius1 = aussenradius*uf
rebarX4.Radius2 = innenradius*uf
rebarX4.Angle3 = 90
rebarX4.Placement.Base = ((-(0.5*l_x-c_hnom)+aussenradius)*uf, 0 , (-(h-c_xnom)+aussenradius)*uf)
rebarX4.Placement.Rotation = (-0.5,0.5,0.5,0.5)  
myDoc.recompute()

rebarX5 = myDoc.addObject("Part::Torus","Rebar_X_Part_5")
rebarX5.Radius1 = aussenradius*uf
rebarX5.Radius2 = innenradius*uf
rebarX5.Angle3 = 90
rebarX5.Placement.Base = (((0.5*l_x-c_hnom)-aussenradius)*uf, 0 , (-(h-c_xnom)+aussenradius)*uf)
rebarX5.Placement.Rotation = (-0.5,-0.5,0.5,-0.5)  
myDoc.recompute()

rebarX6 = myDoc.addObject("Part::Torus","Rebar_X_Part_6")
rebarX6.Radius1 = aussenradius*uf
rebarX6.Radius2 = innenradius*uf
rebarX6.Angle3 = 90
rebarX6.Placement.Base = ((-(0.5*l_x-c_hnom)-aussenradius)*uf, 0 , ((-c_xnom)-aussenradius)*uf)
rebarX6.Placement.Rotation = (0.707,0,0,0.707)  
myDoc.recompute()

rebarX7 = myDoc.addObject("Part::Torus","Rebar_X_Part_7")
rebarX7.Radius1 = aussenradius*uf
rebarX7.Radius2 = innenradius*uf
rebarX7.Angle3 = 90
rebarX7.Placement.Base = (((0.5*l_x-c_hnom)+aussenradius)*uf, 0 , ((-c_xnom)-aussenradius)*uf)
rebarX7.Placement.Rotation = (0,0.707,0.707,0)  

rebarX8 = myDoc.addObject("Part::Cylinder","Rebar_X_Part_8")
rebarX8.Radius = 0.5*A_s_d*uf
rebarX8.Height = 25*A_s_d*uf
rebarX8.Placement.Base = ((0.5*l_x-c_hnom+aussenradius)*uf, 0 , -(c_xnom)*uf)
rebarX8.Placement.Rotation = (0, 0.707, 0, 0.707)  # nicht exakt 90 grad!!!

rebarX9 = myDoc.addObject("Part::Cylinder","Rebar_X_Part_9")
rebarX9.Radius = 0.5*A_s_d*uf
rebarX9.Height = 25*A_s_d*uf
rebarX9.Placement.Base = (-(0.5*l_x-c_hnom+aussenradius+25*A_s_d)*uf, 0 , -(c_xnom)*uf)
rebarX9.Placement.Rotation = (0, 0.707, 0, 0.707)  # nicht exakt 90 grad!!!
myDoc.recompute()

# einzelne Rebarparts zusammenfuegen
rebarX = myDoc.addObject("Part::MultiFuse","Rebar_X")
rebarX.Shapes = [rebarX1, rebarX2, rebarX3, rebarX4, rebarX5, rebarX6, rebarX7, rebarX8, rebarX9]
myDoc.recompute()
rebarX_VO = rebarX.ViewObject
rebarX_VO.ShapeColor = (0.40,0.20,0.00)
myDoc.recompute()

# Asx in y-Richtung verlegen
#rebarX.Placement.Base = (0,0,0)  # ist nicht global null, sondern des basiselementes also im Fundmittelpkt :-)
rebarX.Placement.Base = (0,(-0.5*l_y+c_hnom)*uf,0)  
myArrayX = Draft.makeArray(rebarX,FreeCAD.Vector(0,0,0),FreeCAD.Vector(0,BaseRCSpaceX*uf,0),1,nrOfRCBarsX)
# in entwicklerversion ist es moeglich namen zu uebergeben, aber dann kommt no object linked
#myArrayX = Draft.makeArray(rebarX,FreeCAD.Vector(0,0,0),FreeCAD.Vector(0,BaseRCSpaceX*uf,0),1,nrOfRCBarsX,'Verlegung_X')
myDoc.recompute()
myArrayX_VO = myArrayX.ViewObject
myArrayX_VO.ShapeColor = (0.40,0.20,0.00)
myDoc.recompute()



# IN Y-RICHTUNG
rebarY1 = myDoc.addObject("Part::Cylinder","Rebar_Y_Part_1")
rebarY1.Radius = 0.5*A_s_d*uf
rebarY1.Height = (l_y-2*c_hnom-2*aussenradius)*uf
rebarY1.Placement.Base = (0, (0.5*l_y-c_hnom-aussenradius)*uf , -(h-c_ynom)*uf)
rebarY1.Placement.Rotation = (0.707, 0, 0, .707)  # nicht exakt 90 grad!!!

rebarY2 = myDoc.addObject("Part::Cylinder","Rebar_Y_Part_2")
rebarY2.Radius = 0.5*A_s_d*uf
rebarY2.Height = (h-c_hnom-c_ynom-2*aussenradius-A_s_d)*uf
rebarY2.Placement.Base = (0, -(0.5*l_y-c_hnom)*uf , -(h-c_ynom-aussenradius)*uf)

rebarY3 = myDoc.addObject("Part::Cylinder","Rebar_Y_Part_3")
rebarY3.Radius = 0.5*A_s_d*uf
rebarY3.Height = (h-c_hnom-c_ynom-2*aussenradius-A_s_d)*uf
rebarY3.Placement.Base = (0,  (0.5*l_y-c_hnom)*uf, -(h-c_ynom-aussenradius)*uf)

rebarY4 = myDoc.addObject("Part::Torus","Rebar_Y_Part_4")
rebarY4.Radius1 = aussenradius*uf
rebarY4.Radius2 = innenradius*uf
rebarY4.Angle3 = 90
rebarY4.Placement.Base = (0, (-(0.5*l_y-c_hnom)+aussenradius)*uf , (-(h-c_ynom)+aussenradius)*uf)
rebarY4.Placement.Rotation = (0.5,-0.5,0.5,-0.5)
myDoc.recompute()

rebarY5 = myDoc.addObject("Part::Torus","Rebar_Y_Part_5")
rebarY5.Radius1 = aussenradius*uf
rebarY5.Radius2 = innenradius*uf
rebarY5.Angle3 = 90
rebarY5.Placement.Base = (0, ((0.5*l_y-c_hnom)-aussenradius)*uf , (-(h-c_ynom)+aussenradius)*uf)
rebarY5.Placement.Rotation = (-0.5,-0.5,0.5,0.5) 
myDoc.recompute()

rebarY6 = myDoc.addObject("Part::Torus","Rebar_Y_Part_6")
rebarY6.Radius1 = aussenradius*uf
rebarY6.Radius2 = innenradius*uf
rebarY6.Angle3 = 90
rebarY6.Placement.Base = (0, (-(0.5*l_y-c_hnom)-aussenradius)*uf ,((-c_ynom)-aussenradius)*uf)
rebarY6.Placement.Rotation = (0.5,0.5,0.5,0.5)
myDoc.recompute()

rebarY7 = myDoc.addObject("Part::Torus","Rebar_Y_Part_7")
rebarY7.Radius1 = aussenradius*uf
rebarY7.Radius2 = innenradius*uf
rebarY7.Angle3 = 90
rebarY7.Placement.Base = (0, ((0.5*l_y-c_hnom)+aussenradius)*uf , ((-c_ynom)-aussenradius)*uf)
rebarY7.Placement.Rotation = (-0.5,0.5,0.5,-0.5) 
myDoc.recompute()

rebarY8 = myDoc.addObject("Part::Cylinder","Rebar_Y_Part_8")
rebarY8.Radius = 0.5*A_s_d*uf
rebarY8.Height = 25*A_s_d*uf
rebarY8.Placement.Base = (0, -(0.5*l_y-c_hnom+aussenradius)*uf , (-c_ynom)*uf)
rebarY8.Placement.Rotation = (0.707, 0, 0, .707)  # nicht exakt 90 grad!!!

rebarY9 = myDoc.addObject("Part::Cylinder","Rebar_Y_Part_9")
rebarY9.Radius = 0.5*A_s_d*uf
rebarY9.Height = 25*A_s_d*uf
rebarY9.Placement.Base = (0, (0.5*l_y-c_hnom+aussenradius+25*A_s_d)*uf , (-c_ynom)*uf)
rebarY9.Placement.Rotation = (0.707, 0, 0, 0.707)  # nicht exakt 90 grad!!!

# einzelne Rebarparts zusammenfuegen
rebarY = myDoc.addObject("Part::MultiFuse","Rebar_Y")
rebarY.Shapes = [rebarY1, rebarY2, rebarY3, rebarY4, rebarY5, rebarY6, rebarY7, rebarY8, rebarY9]
myDoc.recompute()
rebarY_VO = rebarY.ViewObject
rebarY_VO.ShapeColor = (0.40,0.20,0.00)
myDoc.recompute()

# Asy in x-Richtung verlegen
#rebarY.Placement.Base = (0,0,0)  # ist nicht global null, sondern des basiselementes also im Fundmittelpkt :-)
rebarY.Placement.Base = ((-0.5*l_x+c_hnom)*uf,0,0)  
myArrayY = Draft.makeArray(rebarY,FreeCAD.Vector(BaseRCSpaceY*uf,0,0),FreeCAD.Vector(0,0,0),nrOfRCBarsY,1)
myDoc.recompute()
myArrayY_VO = myArrayY.ViewObject
myArrayY_VO.ShapeColor = (0.40,0.20,0.00)
myDoc.recompute()



####################################
#Drawing
import FreeCAD, Drawing


#Insert a Page object and assign a template
page1 = myDoc.addObject('Drawing::FeaturePage','Page')
App.ActiveDocument.Page.Template = App.getResourceDir()+'Mod/Drawing/Templates/A4_Portrait_plain.svg'
myDoc.recompute()    # wenn jetzt schon recompute dann erscheint weisses Blatt

#Create the views  and put them on the page
isoview1 = myDoc.addObject('Drawing::FeatureViewPart','View_1')
isoview1.Source = myDoc.Array        # Warum Verweis auf String
myDoc.Page.addObject(myDoc.View_1)
isoview2 = myDoc.addObject('Drawing::FeatureViewPart','View_2')
isoview2.Source = myDoc.Array001
myDoc.Page.addObject(myDoc.View_2)
isoview3 = myDoc.addObject('Drawing::FeatureViewPart','View_3')
isoview3.Source = myDoc.Vertiefung
myDoc.Page.addObject(myDoc.View_3)
isoview4 = myDoc.addObject('Drawing::FeatureViewPart','View_4')
isoview4.Source = myDoc.BPL
myDoc.Page.addObject(myDoc.View_4)
isoview5 = myDoc.addObject('Drawing::FeatureViewPart','View_5')
isoview5.Source = myDoc.Stuetze
myDoc.Page.addObject(myDoc.View_5)
myDoc.recompute()

# Change the properties of the views
scaleFactor = 0.4*uf
viewVector = FreeCAD.Vector(1.0,1.4,0.75)
viewLocationX=200
viewLocationY=360
viewRotation=0

isoview1.Direction = (viewVector)
isoview1.X = viewLocationX
isoview1.Y = viewLocationY
isoview1.Scale = scaleFactor
isoview1.Rotation = viewRotation
isoview1.ShowHiddenLines = True
isoview1.LineWidth = 0.18
isoview2.Direction = (viewVector)
isoview2.X = viewLocationX
isoview2.Y = viewLocationY
isoview2.Scale =  scaleFactor
isoview2.Rotation = viewRotation
isoview2.LineWidth = 0.18
isoview2.ShowHiddenLines = True
isoview3.Direction = (viewVector)
isoview3.X = viewLocationX
isoview3.Y = viewLocationY
isoview3.Scale =  scaleFactor
isoview3.Rotation = viewRotation
isoview3.LineWidth = 0.35
isoview3.ShowHiddenLines = True
isoview4.Direction = (viewVector)
isoview4.X = viewLocationX
isoview4.Y = viewLocationY
isoview4.Scale =  scaleFactor
isoview4.Rotation = viewRotation
isoview4.LineWidth = 0.7
isoview4.ShowHiddenLines = True
isoview5.Direction = (viewVector)
isoview5.X = viewLocationX
isoview5.Y = viewLocationY
isoview5.Scale =  scaleFactor
isoview5.Rotation = viewRotation
isoview5.LineWidth = 0.7
isoview5.ShowHiddenLines = True
myDoc.recompute()


# create text
text1 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_1')
myDoc.Page.addObject(myDoc.Text_1)
text2 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_2')
myDoc.Page.addObject(myDoc.Text_2)
text3 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_3')
myDoc.Page.addObject(myDoc.Text_3)
text4 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_4')
myDoc.Page.addObject(myDoc.Text_4)
text5 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_5')
myDoc.Page.addObject(myDoc.Text_5)
text6 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_6')
myDoc.Page.addObject(myDoc.Text_6)
text7 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_7')
myDoc.Page.addObject(myDoc.Text_7)
text8 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_8')
myDoc.Page.addObject(myDoc.Text_8)
text9 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_9')
myDoc.Page.addObject(myDoc.Text_9)
text10 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_10')
myDoc.Page.addObject(myDoc.Text_10)
text11 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_11')
myDoc.Page.addObject(myDoc.Text_11)
text12 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_12')
myDoc.Page.addObject(myDoc.Text_12)
text13 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_13')
myDoc.Page.addObject(myDoc.Text_13)
text14 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_14')
myDoc.Page.addObject(myDoc.Text_14)
text15 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_15')
myDoc.Page.addObject(myDoc.Text_15)
text16 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_16')
myDoc.Page.addObject(myDoc.Text_16)
text17 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_17')
myDoc.Page.addObject(myDoc.Text_17)
text18 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_18')
myDoc.Page.addObject(myDoc.Text_18)
text19 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_19')
myDoc.Page.addObject(myDoc.Text_19)
text20 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_20')
myDoc.Page.addObject(myDoc.Text_20)
text21 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_21')
myDoc.Page.addObject(myDoc.Text_21)
text22 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_22')
myDoc.Page.addObject(myDoc.Text_22)
text23 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_23')
myDoc.Page.addObject(myDoc.Text_23)
text24 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_24')
myDoc.Page.addObject(myDoc.Text_24)
text25 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_25')
myDoc.Page.addObject(myDoc.Text_25)
text26 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_26')
myDoc.Page.addObject(myDoc.Text_26)
text27 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_27')
myDoc.Page.addObject(myDoc.Text_27)
text28 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_28')
myDoc.Page.addObject(myDoc.Text_28)
text29 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_29')
myDoc.Page.addObject(myDoc.Text_29)
text30 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_30')
myDoc.Page.addObject(myDoc.Text_30)
text31 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_31')
myDoc.Page.addObject(myDoc.Text_31)
text32 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_32')
myDoc.Page.addObject(myDoc.Text_32)
text33 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_33')
myDoc.Page.addObject(myDoc.Text_33)
text34 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_34')
myDoc.Page.addObject(myDoc.Text_34)
text35 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_35')
myDoc.Page.addObject(myDoc.Text_35)

myDoc.recompute()


# change text
textScaleFactor = 5.0
textSpaceFactor = 1.65
textSpacing = 5

text1.X = 30
text1.Y = 30
text2.X = text1.X
text2.Y = text1.Y+textScaleFactor*textSpaceFactor+textSpacing*1.5
text3.X = text1.X
text3.Y = text2.Y+textScaleFactor*textSpaceFactor
text4.X = text1.X
text4.Y = text3.Y+textScaleFactor*textSpaceFactor
text5.X = text1.X
text5.Y = text4.Y+textScaleFactor*textSpaceFactor
text6.X = text1.X
text6.Y = text5.Y+textScaleFactor*textSpaceFactor
text7.X = text1.X
text7.Y = text6.Y+textScaleFactor*textSpaceFactor+textSpacing*1.5
text8.X = text1.X
text8.Y = text7.Y+textScaleFactor*textSpaceFactor
text9.X = text1.X
text9.Y = text8.Y+textScaleFactor*textSpaceFactor
text10.X = text1.X
text10.Y = text9.Y+textScaleFactor*textSpaceFactor+textSpacing*1.5
text11.X = text1.X
text11.Y = text10.Y+textScaleFactor*textSpaceFactor
text12.X = text1.X
text12.Y = text11.Y+textScaleFactor*textSpaceFactor
text13.X = text1.X
text13.Y = text12.Y+textScaleFactor*textSpaceFactor
text14.X = text1.X
text14.Y = text13.Y+textScaleFactor*textSpaceFactor
text15.X = text1.X
text15.Y = text14.Y+textScaleFactor*textSpaceFactor
text16.X = text1.X
text16.Y = text15.Y+textScaleFactor*textSpaceFactor
text17.X = text1.X
text17.Y = text16.Y+textScaleFactor*textSpaceFactor
text18.X = text1.X
text18.Y = text17.Y+textScaleFactor*textSpaceFactor
text19.X = text1.X
text19.Y = text18.Y+textScaleFactor*textSpaceFactor+textSpacing*1.5
text20.X = text1.X
text20.Y = text19.Y+textScaleFactor*textSpaceFactor
text21.X = text1.X
text21.Y = text20.Y+textScaleFactor*textSpaceFactor
text22.X = text1.X
text22.Y = text21.Y+textScaleFactor*textSpaceFactor
text23.X = text1.X
text23.Y = text22.Y+textScaleFactor*textSpaceFactor
text24.X = text1.X
text24.Y = text23.Y+textScaleFactor*textSpaceFactor
text25.X = text1.X
text25.Y = text24.Y+textScaleFactor*textSpaceFactor
text26.X = text1.X
text26.Y = text25.Y+textScaleFactor*textSpaceFactor
text27.X = text1.X
text27.Y = text26.Y+textScaleFactor*textSpaceFactor
text28.X = text1.X
text28.Y = text27.Y+textScaleFactor*textSpaceFactor
text29.X = text1.X
text29.Y = text28.Y+textScaleFactor*textSpaceFactor
text30.X = text1.X
text30.Y = text29.Y+textScaleFactor*textSpaceFactor+textSpacing*1.5
text31.X = text1.X
text31.Y = text30.Y+textScaleFactor*textSpaceFactor
text32.X = text1.X
text32.Y = text31.Y+textScaleFactor*textSpaceFactor
text33.X = text1.X
text33.Y = text32.Y+textScaleFactor*textSpaceFactor+textSpacing*1.5
text34.X = text1.X
text34.Y = text33.Y+textScaleFactor*textSpaceFactor
text35.X = text1.X
text35.Y = text34.Y+textScaleFactor*textSpaceFactor


text1.Scale = textScaleFactor+2  # Ueberschrift
text2.Scale = textScaleFactor+1  # Ueberschrift
text3.Scale = textScaleFactor
text4.Scale = textScaleFactor
text5.Scale = textScaleFactor
text6.Scale = textScaleFactor
text7.Scale =  textScaleFactor+1
text8.Scale = textScaleFactor
text9.Scale = textScaleFactor
text10.Scale = textScaleFactor+1
text11.Scale = textScaleFactor
text12.Scale = textScaleFactor
text13.Scale = textScaleFactor
text14.Scale = textScaleFactor
text15.Scale = textScaleFactor
text16.Scale = textScaleFactor
text17.Scale = textScaleFactor
text18.Scale = textScaleFactor
text19.Scale = textScaleFactor+1
text20.Scale = textScaleFactor
text21.Scale = textScaleFactor
text22.Scale = textScaleFactor
text23.Scale = textScaleFactor
text24.Scale = textScaleFactor
text25.Scale = textScaleFactor
text26.Scale = textScaleFactor
text27.Scale = textScaleFactor
text28.Scale = textScaleFactor
text29.Scale = textScaleFactor
text30.Scale = textScaleFactor+1
text31.Scale = textScaleFactor
text32.Scale = textScaleFactor
text33.Scale = textScaleFactor+1
text34.Scale = textScaleFactor
text35.Scale = textScaleFactor

# Version or unicode text string definition, special caracter should not be a problem anymore if used
#myText1 = [unicode('Calculation of a Single Column Foundation according SIA262(2003)', 'utf-8'),]
# book pyhton3 page 165 for diggits of output

myText1 = 'Calculation of a Single Column Foundation according SIA262(2003)'
myText2 = 'Input Values'
myText3 = 'Concrete:  f_cd = '+str(f_cd)+'kN/cm2 ; tau_cd = '+str(tau_cd)+'kN/cm3 ; g_ck = '+str(g_ck*1E6)+'kN/m2'
myText4 = 'Reinforcement:  f_sd = '+str(f_sd)+'kN/cm2'
myText5 = 'gamma_gp = '+str(gamma_gp)+'( dead load ratio = '+str("{0:0.3f}".format(DLA*100))+'% )'
myText6 = 'V_Ed = '+str(V_Ed)+' kN'
myText7 = 'Soil Pressure'
myText8 = 'sigma_Ek = '+str("{0:0.1f}".format(sigma_Ek*1E4))+'kN/m2'
myText9 = 'sigma_Ed = '+str("{0:0.1f}".format(sigma_Ed*1E4))+'kN/m2'
myText10 = 'Punching Shear (Durchstanzen)'
myText11 = 'D_k ='+str(D_k)+'m ; d_avg ='+str(d_avg)+'cm'
myText12 = 'u ='+str("{0:0.1f}".format(u))+'cm'
myText13 = 'r_yx ='+str("{0:0.1f}".format(r_yx))+'cm ; r_yy ='+str("{0:0.1f}".format(r_yy))+'cm'
myText14 = 'max r_y ='+str("{0:0.2f}".format(r_y))+'cm'
myText15 = 'k_r ='+str("{0:0.2f}".format(k_r))+'( lim k_r ='+str("{0:0.2f}".format(limk_r))+')'
myText16 = 'V_Rd ='+str("{0:0.1f}".format(V_Rd))+'kN'
myText17 = 'Redn = '+str("{0:0.2f}".format(Redn))
myText18 = 'V_Edeff = '+str("{0:0.1f}".format(V_Edeff))+'kN'
myText19 = 'Reinforcement'
myText20 = 'M_Edx ='+str("{0:0.1f}".format(M_Edx*1E-2))+'kNm ; m_Edx ='+str("{0:0.1f}".format(m_Edx))+'kNm/m'
myText21 = 'M_Edy ='+str("{0:0.1f}".format(M_Edy*1E-2))+'kNm ; m_Edy ='+str("{0:0.1f}".format(m_Edy))+'kNm/m'
myText22 = 'erf A_sx ='+str("{0:0.1f}".format(erfA_sx))+'cm2 ; erf a_sx ='+str("{0:0.1f}".format(erfa_sx*1E2))+'cm2/m'  
myText23 = 'erf A_sy ='+str("{0:0.1f}".format(erfA_sy))+'cm2 ; erf a_sy ='+str("{0:0.1f}".format(erfa_sy*1E2))+'cm2/m'
myText24 = 'vorh A_sx ='+str("{0:0.1f}".format(A_sx))+'cm2 ='+str(nrOfRCBarsX)+'x caliber '+str(int(A_s_d*1E1))+' mm'
myText25 = 'vorh A_sy ='+str("{0:0.1f}".format(A_sy))+'cm2 ='+str(nrOfRCBarsY)+'x caliber '+str(int(A_s_d*1E1))+' mm'
myText26 = 'vorh a_sx ='+str("{0:0.1f}".format(a_sx*1E2))+'cm2/m ; roh_x ='+str("{0:0.2f}".format(roh_x*1E2))+'%'
myText27 = 'vorh a_sy ='+str("{0:0.1f}".format(a_sy*1E2))+'cm2/m ; roh_y ='+str("{0:0.2f}".format(roh_y*1E2))+'%'
myText28 = 'M_Rdx ='+str("{0:0.1f}".format(M_Rdx*1E-2))+'kNm ; m_Rdx ='+str("{0:0.1f}".format(m_Rdx))+'kNm/m'
myText29 = 'M_Rdy ='+str("{0:0.1f}".format(M_Rdy*1E-2))+'kNm ; m_Rdy ='+str("{0:0.1f}".format(m_Rdy))+'kNm/m'
myText30 = 'Grundbewehrung'
myText31 = 'x caliber'+str(int(A_s_d*1E1))+'mm auf '+str(BaseRCSpaceX)+' cm'
myText32 = 'x caliber'+str(int(A_s_d*1E1))+'mm auf '+str(BaseRCSpaceY)+' cm'
myText33 = 'Zulagebewehrung'
myText34 = 'x caliber'+str(int(A_s_d*1E1))+'mm auf '+str(BaseRCSpaceX)+' cm'
myText35 = 'x caliber'+str(int(A_s_d*1E1))+'mm auf '+str(BaseRCSpaceY)+' cm'
#myText31 = 'vorh Base A_sx ='+str(nrOfBaseRebarsX)+'x caliber'+str(int(A_s_d*1E1))+'mm auf '+str(BaseRCSpaceX)+' cm'
#myText32 = 'vorh Base A_sy ='+str(nrOfBaseRebarsY)+'x caliber'+str(int(A_s_d*1E1))+'mm auf '+str(BaseRCSpaceY)+' cm'
#myText34 = 'vorh Zulagen A_sx ='+str(nrOfExtraRebarsX)+'x caliber'+str(int(A_s_d*1E1))+'mm auf '+str(BaseRCSpaceX)+' cm'
#myText35 = 'vorh Zulagen A_sy ='+str(nrOfExtraRebarsY)+'x caliber'+str(int(A_s_d*1E1))+'mm auf '+str(BaseRCSpaceY)+' cm'


text1.Text = (myText1)
text2.Text = (myText2)
text3.Text = (myText3)
text4.Text = (myText4)
text5.Text = (myText5)
text6.Text = (myText6)
text7.Text = (myText7)
text8.Text = (myText8)
text9.Text = (myText9)
text10.Text = (myText10)
text11.Text = (myText11)
text12.Text = (myText12)
text13.Text = (myText13)
text14.Text = (myText14)
text15.Text = (myText15)
text16.Text = (myText16)
text17.Text = (myText17)
text18.Text = (myText18)
text19.Text = (myText19)
text20.Text = (myText20)
text21.Text = (myText21)
text22.Text = (myText22)
text23.Text = (myText23)
text24.Text = (myText24)
text25.Text = (myText25)
text26.Text = (myText26)
text27.Text = (myText27)
text28.Text = (myText28)
text29.Text = (myText29)
text30.Text = (myText30)
text31.Text = (myText31)
text32.Text = (myText32)
text33.Text = (myText33)
text34.Text = (myText34)
text35.Text = (myText35)

myDoc.recompute()



# Draft massketten
dimensionFontsize=10*uf
# es waere natuerlich auch moeglich die Eckpunkte der box stutze zu holen und die zu vermassen
d1 = Draft.makeDimension(FreeCAD.Vector(-0.5*l_cx*uf,-0.5*l_cy*uf,l_cz*uf),FreeCAD.Vector(0.5*l_cx*uf,-0.5*l_cy*uf,l_cz*uf),FreeCAD.Vector(0,-(0.5*l_cy+20)*uf,l_cz*uf))
d1_VO = d1.ViewObject
d1_VO.FontSize = dimensionFontsize

d2 = Draft.makeDimension(FreeCAD.Vector(0.5*l_cx*uf,-0.5*l_cy*uf,l_cz*uf),FreeCAD.Vector(0.5*l_cx*uf,0.5*l_cy*uf,l_cz*uf),FreeCAD.Vector((0.5*l_cx+20)*uf,0,l_cz*uf))
d2_VO = d2.ViewObject
d2_VO.FontSize = dimensionFontsize

d3 = Draft.makeDimension(FreeCAD.Vector(0.5*l_x*uf,-0.5*l_y*uf,0),FreeCAD.Vector(0.5*l_x*uf,-0.5*l_y*uf,-h),FreeCAD.Vector((1.5*0.5*l_x+20)*uf,0,-h))
d3_VO = d3.ViewObject
d3_VO.FontSize = dimensionFontsize


# Top View
topview1 = myDoc.addObject('Drawing::FeatureViewPart','Top_View_1')
topview1.Source = myDoc.Vertiefung
myDoc.Page.addObject(myDoc.Top_View_1)
topview2 = myDoc.addObject('Drawing::FeatureViewPart','Top_View_2')
topview2.Source = myDoc.Stuetze
myDoc.Page.addObject(myDoc.Top_View_2)
topview5=Draft.makeDrawingView(d1, page1)
topview6=Draft.makeDrawingView(d2, page1)


# Change the properties of the views
topviewscaleFactor = 0.4*uf
topviewVector = FreeCAD.Vector(0,0,1)
topviewLocationX=200
topviewLocationY=240
topviewRotation=0

topview1.Direction = (topviewVector)
topview1.X = topviewLocationX
topview1.Y = topviewLocationY
topview1.Scale = topviewscaleFactor
topview1.Rotation = viewRotation
topview1.ShowHiddenLines = True
topview1.LineWidth = 0.35
topview2.Direction = (topviewVector)
topview2.X = topviewLocationX
topview2.Y = topviewLocationY
topview2.Scale = topviewscaleFactor
topview2.Rotation = viewRotation
topview2.LineWidth = 0.35
topview5.Direction = (topviewVector)
topview5.X = topviewLocationX
topview5.Y = topviewLocationY
topview5.Scale = topviewscaleFactor
topview5.LineWidth = 0.18
topview6.Direction = (topviewVector)
topview6.X = topviewLocationX
topview6.Y = topviewLocationY
topview6.Scale = topviewscaleFactor
topview6.LineWidth = 0.18



# Front View
frontview1 = myDoc.addObject('Drawing::FeatureViewPart','Front_View_1')
frontview1.Source = myDoc.Vertiefung
myDoc.Page.addObject(myDoc.Front_View_1)
frontview2 = myDoc.addObject('Drawing::FeatureViewPart','Front_View_2')
frontview2.Source = myDoc.BPL
myDoc.Page.addObject(myDoc.Front_View_2)
frontview3 = myDoc.addObject('Drawing::FeatureViewPart','Front_View_3')
frontview3.Source = myDoc.Stuetze
myDoc.Page.addObject(myDoc.Front_View_3)
frontview5=Draft.makeDrawingView(d3, page1)


# Change the properties of the views
frontviewscaleFactor = 0.4*uf
frontviewVector = FreeCAD.Vector(0,1,0)
frontviewLocationX=200
frontviewLocationY=160
frontviewRotation=270

frontview1.Direction = (frontviewVector)
frontview1.X = frontviewLocationX
frontview1.Y = frontviewLocationY
frontview1.Scale = frontviewscaleFactor
frontview1.Rotation = frontviewRotation
frontview1.ShowHiddenLines = True
frontview1.LineWidth = 0.35
frontview2.Direction = (frontviewVector)
frontview2.X = frontviewLocationX
frontview2.Y = frontviewLocationY
frontview2.Scale = frontviewscaleFactor
frontview2.Rotation = frontviewRotation
frontview2.LineWidth = 0.35
frontview3.Direction = (frontviewVector)
frontview3.X = frontviewLocationX
frontview3.Y = frontviewLocationY
frontview3.Scale = frontviewscaleFactor
frontview3.Rotation = frontviewRotation
frontview3.LineWidth = 0.35
frontview5.Direction = (frontviewVector)
frontview5.X = frontviewLocationX
frontview5.Y = frontviewLocationY
frontview5.Scale = frontviewscaleFactor
#frontview5.Rotation = frontviewRotation
frontview5.LineWidth = 0.18


myDoc.recompute()


# Da hats bugs bei isoview
#isoview6=Draft.makeDrawingView(d1, page1)
#isoview6.Direction = (viewVector)
#isoview6.X = viewLocationX
#isoview6.Y = viewLocationY
#isoview6.Scale = scaleFactor





