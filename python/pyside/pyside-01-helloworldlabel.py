#!/usr/bin/python
# -'''- coding: utf-8 -'''-
 
import sys
from PySide.QtCore import *
from PySide.QtGui import *
 
 
# Create the Qt Application
app = QApplication(sys.argv)
# Create a button, connect it and show it
label = QLabel()
label.setText('helloWorld, here I am')
label.show()
# Run the main Qt loop
app.exec_()
