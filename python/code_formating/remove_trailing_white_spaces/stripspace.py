#!/usr/bin/env python2
"""\
strip trailing whitespace from file
usage: stripspace.py <file> or comment sys.exit(__doc__) and provide a file in the line above !
Trailing whitespace --> are extra spaces (and tabs) at the end of line
http://stackoverflow.com/questions/21410075/what-is-trailing-whitespace-and-how-can-i-handle-this
sollte doch auch die whitespace in leeren lines finden !
"""

import sys

'''
if len(sys.argv[1:]) != 1:
  #inp = outp = '/home/hugo/Documents/dev/freecad/freecadbhb_master/src/Mod/Arch/importIFC.py'
  inp = outp = '/home/hugo/Documents/dev/freecad/freecadbhb_dev/src/Mod/Fem/Gui/ViewProviderFemMesh.cpp'

  sys.exit(__doc__)
else:
  inp = outp = sys.argv[1]
'''


inp = outp = '/home/hugo/Documents/dev/freecad/freecadbhb_dev/src/Mod/Fem/Gui/ViewProviderFemMesh.cpp'
content = ''
outsize = 0
with open(inp, 'rb') as infile:
  content = infile.read()
with open(outp, 'wb') as output:
  for line in content.splitlines():
    newline = line.rstrip(" \t")
    outsize += len(newline) + 1
    output.write(newline + '\n')

print("Done. Stripped %s bytes." % (len(content)-outsize))