#!/usr/bin/env python2
"""\
strip trailing whitespace from file
usage: stripspace.py <file> or comment sys.exit(__doc__) and provide a file in the line above !
Trailing whitespace --> are extra spaces (and tabs) at the end of line
http://stackoverflow.com/questions/21410075/what-is-trailing-whitespace-and-how-can-i-handle-this
sollte doch auch die whitespace in leeren lines finden !
"""

import sys

def strip_white_spaces(filenpath):
    inp = outp = filenpath
    content = ''
    outsize = 0
    with open(inp, 'rb') as infile:
        content = infile.read()
    with open(outp, 'wb') as output:
        for line in content.splitlines():
            newline = line.rstrip(" \t")
            outsize += len(newline) + 1
            output.write(newline + '\n')

    print("Done. Stripped %s bytes." % (len(content)-outsize))

'''
run in shell by
python stripspace_fem.py
!!! Aufpassen: zur sicherheit strip = False --> set to true !!!
'''

strip = True
import os
mydir = '/home/hugo/Documents/dev/freecad/freecadbhb_dev/freecad/src/Mod/Fem/App/'
print ''
print mydir
for myfile in sorted(os.listdir(mydir)):
    if myfile.endswith(".cpp") or myfile.endswith(".h") or myfile.endswith(".txt"):
        print(myfile)
        if strip:
            strip_white_spaces(mydir + myfile)
mydir = '/home/hugo/Documents/dev/freecad/freecadbhb_dev/freecad/src/Mod/Fem/Gui/'
print ''
print mydir
for myfile in sorted(os.listdir(mydir)):
    if myfile.endswith(".cpp") or myfile.endswith(".h") or myfile.endswith(".txt"):
        print(myfile)
        if strip:
            strip_white_spaces(mydir + myfile)
mydir = '/home/hugo/Documents/dev/freecad/freecadbhb_dev/freecad/src/Mod/Fem/'
print ''
print mydir
for myfile in sorted(os.listdir(mydir)):
    if myfile.endswith(".py") or myfile.endswith(".txt"):
        if myfile != 'convert2TetGen.py':
            print(myfile)
            if strip:
                strip_white_spaces(mydir + myfile)


'''
#or if you want to traverse directory, use os.walk:
import os
for root, dirs, files in os.walk("/mydir"):
    for file in files:
        if file.endswith(".txt"):
             print(os.path.join(root, file))
'''

