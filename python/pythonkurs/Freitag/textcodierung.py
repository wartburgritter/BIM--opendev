import base64

# um den string zu encodieren braucht python die binarydaten
# diese werden mit b'' einelesen heisst auch bytestring

a = base64.b64encode(b'hello world')

print(a)

b = base64.b64decode(a)

print(b)

