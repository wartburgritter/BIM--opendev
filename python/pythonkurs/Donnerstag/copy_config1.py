import shutil
import os

"""
folgendes ist viel besser zum testen meiner routinen
python3 starten

line = 'irgend eine zeile eingeben'
line.startswith('wort')
line.startswith('irgend')

line.find('eine')
line.find('ne zei')

'eingeb' in line
'heute' in line

list  = [1,2,3,5,7,9,11,13,17,19,23]
list.index(11)
11 in list
12 in list

"""


def version_mini(zeile, suchstring, newstring):
  """ist die miniversion, aber alles was hinter dem suchstring steht wird geloescht
  auch das was zwischen suchstring und = steht
  """
  if zeile.startswith(suchstring):
      zeile = (suchstring + newstring + '\n' )
  return zeile


def version_find(zeile, suchstring, newstring):
  """find gibt das erste auffinden zuruecknull
  Wenn du an der ersten Stelle (0) den string findes mache
  """
  if zeile.find(suchstring) == False:
    print(zeile)
    teile = zeile.split('=')
    print(teile)
    zeile = (teile[0] + '=' + newstring + '\n')
    print(zeile)
  return zeile
      
      
def version_startswith(zeile, suchstring, newstring):
  """startswith gibt True oder False zurueck
  """
  if zeile.startswith(suchstring):   
    teile = zeile.split('=')
    zeile = (teile[0] + '=' +  newstring + '\n')
  return zeile
      
      
def version_in(zeile, suchstring, newstring):
  """gibt True oder False zurueck wenn string egal wo in zeile
  findet den string auch wenn er in der mitte der zeile steht  
  """
  if suchstring in zeile:   
    print(zeile)
    teile = zeile.split('=')
    zeile = (teile[0] + '=' + newstring + '\n')
  return zeile
      
      
###############################################################################
if __name__ == '__main__':

  pathread = 'hugin'
  f1 = open(pathread, 'r')
   
  pathwrite = 'new1hugin'
  f2 = open(pathwrite, 'w')
  
  suchen = 'AutoPanoCount' 
  ersetzen = 'Mein neuer Text in der Configdatei!!!!!!!!!!'
  
  for line in f1:
    #line = version_find(line, suchen, ersetzen)
    #line = version_startswith(line, suchen, ersetzen)
    line = version_in(line, suchen, ersetzen)
    #line = version_mini(line, suchen, ersetzen)
    f2.write(line)

    
  f1.close()
  f2.close()
 
  
"""dateioperationen  
  # dateien kopieren
  shutil.copy2(pathread,'hugin_zcopie_fuer_loeschen')
 
  # datei umbenennen
  os.rename('hugin_zcopie_fuer_loeschen','testloeschen')
  
  # datei loeschen
  os.remove('testloeschen')
"""
  
  