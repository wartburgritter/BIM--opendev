import os

"""configdemo.py als unterprogramm
Datei öffnen, einlesen, in Datei schreiben
    Beispiel von Arno Keppke
"""


def change_config(filename, filename_out, pattern, new):
    # wegen funktion schreiben wir in temporaere datei
    datei_out = filename + '.tmp'
    f_in = open(filename, 'r')
    f_out = open(datei_out, 'w')

    for line in f_in:
        if line.startswith(pattern):
            line = new
        f_out.write(line)
    f_in.close()
    f_out.close()
    #os.remove(filename)
    #os.rename(datei_out, filename)
    return

    
    
if __name__ == '__main__':

    datei_in = 'hugin'
    datei_out = 'new3hugin'
    
    suchen = 'AutoPanoCount' 
    ersetzen = 'Mein neuer Text in der Configdatei!!!!!!!!!!'

    
    change_config(datei_in, datei_out, suchen, ersetzen)
    
    
