"""
#in shell pydoc3 sqlite
import sqlite3
help sqlite3
dir sqlite3

"""

import sqlite3

# die datenbank sqlite3 ist in python integriert
# die daten werden in einer datei gespeichert, 
# siehe verzeichnis von dbzeug.py datei testdb.sqlite

# connect erstellt eine neue datenbank falls keine existiert.
db = sqlite3.connect('testdb.sqlite')   


##################################################################
# sqlbefehl absetzen tabelle erstellen
mysqlbefehl = 'create table demo (id integer, name text)'
db.execute(mysqlbefehl)

# tabelle mit daten fuellen
db.execute('insert into demo values(1, "Bernd")')
db.execute('insert into demo values(2, "Harald")')
db.execute('insert into demo values(3, "Anne")')
db.execute('insert into demo values(4, "Thorsten")')
db.execute('insert into demo values(5, "Sabrina")')
db.execute('insert into demo values(5, "Sabrina")')  # ist bei unserer db moeglich, da nicht verhindert


# daten sind noch nicht in datenbank geschrieben 
# daten liegen im journal siehe datei datenbankname-journal
# mittels commit werden die daten in datenbank geschrieben
db.commit()




##################################################################
# Abfragen

# liefert ein handle auf die datenbank wie open auf datei
# dir(cursor)   # gibt infos zum handle aus

cursor = db.execute('select count(*) from demo')
cursor.fetchall()

cursor = db.execute('select * from demo')
cursor.fetchone()
cursor.fetchone()
cursor.fetchone()
cursor.fetchone()
cursor.fetchone()






