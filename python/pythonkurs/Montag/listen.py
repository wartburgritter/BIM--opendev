import copy
liste = [5,6,7,8,9,10]
kopie = liste
print("kopie ist die echte kopie", not liste is kopie)

# string und variablen uebergeben kopie alle anderen
# objekten uebergeben referenz, so auch aarays

kopie[0] = 0
print(liste)

print('-' * 40)

print(liste[1:3])
print(liste[1:-1])   # da faengt er hinten an zu zaehlen
print(liste[1:])     # alle bis auf das letzte element
print(liste[-2:])    # nur die letzten beiden elemente
print(liste[:-1])    # alle bis auf das letzte element


# erzeugt kopie des arrays
# es wird eine kopie erzeugt diese an print erzeugt und dann wieder geloescht
# nicht wirklich sinnvol fuer print, aber es ist eben eine kopie
print(liste[:])

print('-' * 80)

lang = [liste] * 3   # es werden nur referenzen kopiert
print(lang)
lang[0][0] = 100
print(lang)

print('-' * 80)

kopielang = lang[:]     # es werden nur referenzen kopiert
# es werden nur fuer variablen und string datnkopieen erzeugt
# sonst werden immer nur die referenzen kopiert
kopielang[0][1] = 42
print(kopielang)
print(lang)
print(type(lang[0][0]))

# deepcopy kopiert die Daten und behaelt die Referenzen in tieferen ebenen
neuekopie = copy.deepcopy(lang)
neuekopie[0][0] = 23
print(neuekopie)
print(lang)