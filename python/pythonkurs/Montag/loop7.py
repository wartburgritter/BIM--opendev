max = 5
for i in range(max):
    print("Wert ist", i)
    
# range ist eine funktion die indexe zur verfuegung stellt
# range zaehlt
# range in python 3 war xrange in python 2 
# in python 3 ist range eine richtige funktion in python 2 stellt es eine liste zur verfuegung

print('-' * 60)


# startwert, endwert
for i in range(5,15):
  print("Wert ist", i)
  
print('-' * 60)



# startwert, endwert, increment
for i in range(5,15,2):
  print("Wert ist", i)

print('-' * 60)



# liste rueckwaerts zaehlen
list=[1,2,3,4,5,6,7,8,9]
for i in range(len(list)-1,0,-1):
  print("Wert ist", i)
