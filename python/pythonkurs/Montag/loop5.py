tupel = (1, "Zeichen", 2)

print(tupel[0])

for elem in tupel:
    print(elem)
    

# Tupel sind unveraenderlich, folglich gibt das einen Fehler:
# tupel[1]=hallo

# siehe 
#help(())
# es sind fuer tupel nur zwei objektmethoden verfuegbar count und index