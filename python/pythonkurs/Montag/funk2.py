def plus(wert1, wert2):
    ergebnis = wert1 + wert2
    return ergebnis
    
def plus_mit_default(wert1=1, wert2=2):  # --> default fuer parameter
    ergebnis = wert1 + wert2
    return ergebnis
    
    
wert = plus(1,12)
print (wert)
print (plus)

print (print)

zahl1 = 1
zahl2 = 12
zahl3 = plus(zahl1, zahl2)
print(zahl3)


print (plus_mit_default())