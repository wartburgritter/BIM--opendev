text1 = 'Das ist eine Zeichenkette"'
text2 = "Das ist eine andere Kette'"

print(text1)
print(text2)
# der unterschied ist nur, dass die
# anfuehrungszeichen ineinande nicht
# escaped werden muessen

text3 = "1. Zeile\n2. Zeile\n3. Zeile"
print(text3)

text4 = "1. Zeile\n\
2. Zeile\n\
3. Zeile"
print(text4)

text5 = """1. Zeile
2. Zeile
3. Zeile"""
print(text5)    # koennen auch als kommentare "missbraucht" werden


rawstring = r'Backslash: \\weiterer text \ und\ '
print(rawstring)


 # joey
 # lange zeichenketten doppelte
 # leere strings einfache
 # weil er auch viel in bash und C programmiert




