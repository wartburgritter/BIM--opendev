# dictionayr besteht aus indexes und elementes
# keys liefert indexes

# items liefert die indexe und daten als tupel 

dictionary = {}
dictionary['Essen'] = 'E'
dictionary['Bochum'] = 'B'
dictionary['Bremen'] = 'HB'
#
print(dictionary)
#
print('-' * 40)

for item in dictionary.items():
  print(item)

# items ist eine objektmethode oder variable

for myindex, item in dictionary.items():
  print(myindex, ':', item)
#
print('-' * 40)
#
for myindex in dictionary.keys():
    print(myindex, ':', dictionary[myindex])
    
    
  