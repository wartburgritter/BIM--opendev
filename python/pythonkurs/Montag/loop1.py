max = 10
i = 0
#
# solange wie die bedingung logisch wahr ist wird die schleife ausgefuehrt
# % heisst modulo und bedeutet Rest einer Division
# 3 % 2 = 1, 4 % 2 = 0
while i < max:
    if i % 2 == 0:        # monoloperator gibt den rest einer division zurueck
         print("Aktueller Wert:",i)
    i = i + 1
# code wird IMMER nach beendigung der schleife ueber die bedingung ausgefuehrt
else:
    print("letzter Wert:", i)
    

