zahl1 = 10
zahl2 = 20
ergebnis = zahl1 * zahl2
#
print(zahl1, '*', zahl2, '=', ergebnis)
#
#Folgende Zeile produziert Fehler, dat text noch nicht definiert ist
# print(text)
#
wort = "Schulung"
# Repeatoperator
text = wort * 5
print('-' * 40)
print(text)
#
#
wort2 = "Schulung\n"
text2 = wort2 * 5
print('-' * 40)
#print(text2)
