def schmierkran(wert):
    # Achtung, hier wird die Datenstruktur ausserhalb der Funktion veraendert
    wert[2] = '2'
    return wert[2]

    
def schmierauto(wert):
    wert = wert[:]  # erzeugt Array-Kopie und weist sie Varable zu
    # die liste ausserhalb wird nicht veraendert
    wert[2] = '2'
    return wert[2]

    

liste = [1,2,3,4]
print(schmierauto(liste))   # aendert liste nicht
print(liste)
print(schmierkran(liste))   # aendert liste
print(liste)

# ein Array und ein Dictionayr wird als Referenz nicht als Kopie uebergeben.