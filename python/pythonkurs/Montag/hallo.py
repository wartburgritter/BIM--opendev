#! /usr/bin/python3

# obiges heisst auch shebangzeile
# gibt an mit welchen programm die datei ausgefuehrt wird.
# datei ausfuehrbar machen 

# Print ist eine Funktion, Zeilenende wird automatisch angehaengt
print("Hallo Welt")

# strings brauchen hochkomma
print ("Essen")

# zahlen brauchen kein hochkomma
print(1001)

print("Text mit Ümlläutön")

# print schreibt imme leerzeichen
print("Text", 3, "Abzweigungen")

# parameter sep uebergeben um keine leerzeichen zu schreiben
print("Wind", "rose", sep="")

print("Wind", "rose", sep="", end='')
print("nun kommt Text ohne Zeilenumbruch")

# innerhalb doppelter hochkommas einfache hochkommas moeglich
# innerhalt einfacher hochkammas doppelte hochkommas moeglich
# das ist der einzige unterschied zw. einfachen und doppelten

# joey
# lange zeichenketten doppelte
# leere strings einfache
# weil er auch viel in bash und C programmiert


