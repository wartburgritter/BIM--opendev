""" Bestimmte Dateien in Verzeichnissen ausgeben

Aufruf des Programms mit Argument fuer Dateiendung
Bei keinem Parameter Fehler"""

#  an has_suffix wird uebegeben:
#  der punkt plus der mit dem programm uebergebene parameter

import os
import sys

# funktionsgenerator
def has_suffix(suffix):
  return lambda name : name[-len(suffix):] == suffix
  

################################################################################
if __name__ == '__main__':

  
  meinverzeichnisname = '/home/hugo/Documents/projekte--ifc/freecad/'
  for dirpath, dirnames, filenames in os.walk(meinverzeichnisname):
    filenames = list(filter(has_suffix('.'+sys.argv[1]), filenames))
    if filenames:
      print("In", dirpath)
      for d in filenames:
        print("   ",d)


        
  print('-'*80)



