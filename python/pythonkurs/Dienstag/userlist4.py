import userlist2

if __name__ == '__main__':
  user = userlist2.userlist('/etc/passwd')

print(user)

# funktioniert, da kennt prinzip des blocks nicht
# variable ist im gesammten hauptprogramm definiert
# auch nach einer forschleife existiert die zaehlvariable noch
# sie wird nur in der naechsten forschleife wieder neu zugewiesen

# Achtung: Wenn Programm als Modul importiert wird, ist die Variable
# user nicht definiert, import userlist2 und pydoc3 userlist2 erzeugen
# daher einen fehler