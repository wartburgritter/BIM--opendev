filename = '/etc/passwd'

f = open(filename, 'r')   # r oeffnet nur zum lesen
# gibt ein opjekt zurueck welches lesbar ist
line = f.readline()
print(line,end='')     # end='' entfernt den Zeilenumbruch

print('-'*80)


while line:
  print(line,end='')
  line = f.readline()
f.close                # der dateiscrptor (pointer) ist am ende schliessen und wieder oeffnen
print('-'*80)
 
 
f = open(filename, 'r')
for line in f.readlines():   # erzeugt array mit allen zeilen 
  print(line,end='')
f.close
print('-'*80)
 
 
f = open(filename, 'r')    
for line in f:               # erzeugt kein array mit allen zeilen wenig speicher
    print(line,end='')


# datei schliesse, da os nur max geoeffnete dateien zulaesst
print(f)
f.close




