""" Bestimmte Dateien in Verzeichnissen ausgeben

Variante mit Filter und lamda-ausdruck """


import os


################################################################################
if __name__ == '__main__':

  meinverzeichnisname = '/home/hugo/Documents/projekte--ifc/freecad/'
  for dirpath, dirnames, filenames in os.walk(meinverzeichnisname):
    # Filter verwendet das PRAEDIKAT auf jedes Element an, und uebernimmt
    # diejenigen, fuer die True berechnet wurde. Hier also die .py-Dateien
    # Ein PRAEDIKAT  ist immer eine Funktion
    # Lamda-Ausdruck ist eine Mini-Funktion, hier mit 1 Parameter n
    filenames = list(filter(lambda n: n[-4:] == '.ifc', filenames))
    if filenames:
      print("In", dirpath)
      for d in filenames:
        print("   ",d)

      
  print('-'*80)

  
  
# inlinefunktion