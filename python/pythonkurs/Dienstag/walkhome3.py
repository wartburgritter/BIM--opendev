""" Bestimmte Dateien in Verzeichnissen ausgeben

Variante mit neuer liste in der dateinamen stehen"""


import os


def os_name_ausgeben():
  """Liefert den Typ des Betriebssystems """
  return os.name

  
#Funktion ist ein Praedikat, liefert nur True/False fuer Parameter
def is_python(name):
  """Liefert True, falls name auf .py endet, sonst False
  
  name muss ein String sein
  Falls nicht, wird ValueError ausgeloest
  """
  if type(name) != type('str'):
    raise ValueError
    
  return name[-3:] == '.py'    #gibt wahr zurueck, wenn .py 


################################################################################
if __name__ == '__main__':

  meinverzeichnisname = '/home/hugo/Documents/projekte--ifc/freecad/'
  for dirpath, dirnames, filenames in os.walk(meinverzeichnisname):
    if filenames:           # alternativ len(filenames)
      liste = []
      for name in filenames:
        if is_python(name):
          liste.append(name)
      if liste:
         print("In", dirpath)
         for dateiname in (liste):
           print(dateiname)

      
  print('-'*80)

  