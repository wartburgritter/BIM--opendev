def pl():
  """Druckt eine Zeile voll Bindestriche"""
  print('-'*80)

def userlist_bin_sh(path):
  """Liefert Userliste als Array zurueck"""
  try:
      f = open(path, 'r')
  except IOError as e:
    # leere Liste zurueckgeben, falls Datei nicht lesbar
    print("Datei", path, "kann nicht gelesen werden")
    text = str(e)
    if text[:9] == '[Errno 2]':          # [:9] greift auf die ersten 9 arrayelemete zu
      print("Bitte Datei anlegen")
    elif text[:10] == '[Errno 13]':
      print("Bitte Berechtigungen anpassen")
    else:
      raise   # aktuelle Exception weiterreichen
    return[]
  liste = []
  for line in f:
    teile = line[:-1].split(':')
    if teile[6] == '/bin/sh':
      liste.append(teile[0])
  f.close()
  return liste



#########hauptprogramm###############
# wie gehe ich mit exceptions um

if __name__ == '__main__':
#  user = userlist('/etc/shadow----')     # datei existiert nicht --> anlegen
  user = userlist_bin_sh('/etc/passwd')         # i.o.
#  user = userlist('/etc/shadow')          # Berechtigungen stimmen nicht

  print(user)