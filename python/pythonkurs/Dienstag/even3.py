# yield erzeugt generator-Object aka. (also known as) Iterator
# range ist genau so ein 
# Funkion ist ein Generator und der Rueckgabewert ist ein Iterator
# Es werden nicht die eine milliarde werde uebergeben

def geradezahl(n):
  """es werden alle geraden zahlen beginnend bei null zurueckgegeben"""
  l = []
  i = 0
  while i <= n:
    yield i      # i zurieckliefern und dort weiterarbeiten
    i += 2

# es werden dauernd werte geliefert, wenn n x milliarden gross ist
# wuerde sonst ein riesiges array angelegt werden.

for zahl in geradezahl(10):
  print(zahl, "ist gerade")

print('-'*80)
  
print(geradezahl(10000000000))
print(geradezahl)
print(type(geradezahl(10000000000)))

print('-'*80)


# falls doch die werte in einem array gebraucht werden
# list ist eine pythonfunktion die aus allem moeglichen 
# eine liste erzeugt
liste = list(geradezahl(20))
print(liste)


meingenerator = geradezahl(10)
print(meingenerator)
print(type(meingenerator))
print(meingenerator.__next__())
print(meingenerator.__next__())


print('-'*80)

if __name__ == 'main':
  neuergenerator = geradezahl(28)
  while True:
    print(neuergenerator.__next__(), "ist gerade Zahl")
    
  print("Ende des Hauptprogramm")
  
  
  
  
  
 