def geradezahl(n):
  """es werden alle geraden zahlen beginnend bei null zurueckgegeben"""
  l = []
  i = 0
  while i <= n:
    yield i      # i zurieckliefern und dort weiterarbeiten
    i += 2

    
if __name__ == '__main__':
  # spielerei, direkt auf das generatorobjekt zugreifen
  meingenerator = geradezahl(28)
  while True:
    try:
        print(meingenerator.__next__(), "ist gerade Zahl")
    # Am Ende der Iteration wird die Exception StopIteration ausgeloest
    except StopIteration:
        # Wenn sie erkannt wird, soll die while-Schleife beendet werden
        break
        
  print("Ende des Hauptprogramm")
  print('-'*80)
  
  
  # normal wird die 
  for zahl in geradezahl(28):
    print(zahl, "ist gerade Zahl")
  
  
 