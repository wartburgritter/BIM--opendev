""" Dateioperationen von Bernd

hier sind einige Methoden von mir mit denen 
Dateioperationen ausgefuehrt werden"""

# kommentare innerhalb der """ """ sind mit absicht an dem ort,
# mit pydoc3 dateinmae ohne .py koennen diese ausgelesen werdenq

def pl():
  """Druckt eine Zeile voll Bindestriche"""
  print('-'*80)


def userlist_bin_sh(path):
  """Liefert Userliste als Array zurueck"""
  try:
      f = open(path, 'r')
  except IOError as e:
    # leere Liste zurueckgeben, falls Datei nicht lesbar
    print("Datei", path, "kann nicht gelesen werden")
    text = str(e)
    if text[:9] == '[Errno 2]':          # [:9] greift auf die ersten 9 arrayelemete zu
      print("Bitte Datei anlegen")
    elif text[:10] == '[Errno 13]':
      print("Bitte Berechtigungen anpassen")
    else:
      raise   # aktuelle Exception weiterreichen
    return[]
  liste = []
  for line in f:
    try:
        line.index('/bin/sh')
        teile = line[:-1].split(':')
        liste.append(teile[0])
    except ValueError:
        pass
  f.close()
  return liste
 
 
def funktion_einfach_mal_definieren():
  """ Einfach mal eine Funktion definieren ohne das ein Fehler kommt"""
  pass

  
def write_array_to_file(path, array):
  """Schreibt ein Array in eine Datei"""
  try:
      f = open(path, 'w')
  except IOError:
    print("Array konte nicht in die Datei geschrieben werden")
    return
  for elem in array:
    f.write(elem + '\n')
  f.close()
  
  
# Variante mit Context Manager, das heisst das programm macht dateioperationen selbst
def write_array_to_file_cm(path, array):
  """Schreibt ein Aqrray in eine Datei und benutzt den Context Manager"""
  try:
      with open(path, 'w') as f:       # wird mit with eingeleitet
          for elem in array:
              f.write(elem + '\n')
  except IOError:
    print("Array konte nicht in die Datei geschrieben werden")

    
def zzhauptprogramm()
  """ """
  pass

################################################################################
if __name__ == '__main__':
  user = userlist_bin_sh('/etc/passwd')         # i.o.

  write_array_to_file('/tmp/user_bin_sh.txt', user)
  write_array_to_file_cm('/tmp/user_bin_sh_cm.txt', user)

#  for username in sorted(user):
#    print(username)
    

