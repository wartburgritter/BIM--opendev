""" Bestimmte Dateien in Verzeichnissen ausgeben

Aufruf des Programms mit Argument fuer Dateiendung"""




import os
import sys

# funktionsgenerator
def has_suffix(suffix):
    if len(suffix):
      return lambda name : name[-len(suffix):] == suffix
    else:
      pass

################################################################################
if __name__ == '__main__':

  meinverzeichnisname = '/home/hugo/Documents/projekte--ifc/freecad/'
  for dirpath, dirnames, filenames in os.walk(meinverzeichnisname):
    if (len(sys.argv) > 1):    
        filenames = list(filter(has_suffix('.'+sys.argv[1]), filenames))
    else:
        filenames =  list(filter(lambda name : name.count('.') == 0#+#, filenames))
    if filenames: 
      print("In", dirpath)
      for d in filenames:
        print("   ",d)


      
  print('-'*80)

# in der funktion has_suffix pruefen ob ein punkt vorhanden ist
# je nach dem die richtige 

