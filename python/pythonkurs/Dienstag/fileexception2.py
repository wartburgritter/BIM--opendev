def pl():
  """Druckt eine Zeile voll Bindestriche"""
  print('-'*80)

  
def userlist_bin_sh(path):
  """Liefert Userliste als Array zurueck"""
  try:
      f = open(path, 'r')
  except IOError as e:
    # leere Liste zurueckgeben, falls Datei nicht lesbar
    print("Datei", path, "kann nicht gelesen werden")
    text = str(e)
    if text[:9] == '[Errno 2]':          # [:9] greift auf die ersten 9 arrayelemete zu
      print("Bitte Datei anlegen")
    elif text[:10] == '[Errno 13]':
      print("Bitte Berechtigungen anpassen")
    else:
      raise   # aktuelle Exception weiterreichen
    return[]
  liste = []
  for line in f:
    try:
        line.index('/bin/sh')
        teile = line[:-1].split(':')
        liste.append(teile[0])
    except ValueError:
        pass
  f.close()
  return liste




if __name__ == '__main__':
  user = userlist_bin_sh('/etc/passwd')         # i.o.

  for username in sorted(user):
    print(username)
    
 