"""Testprogramm readfile Nr. 6

List die Passwortdatei aus, in Funkionen,
und gibt die Userliste als Array zurueck

"""

def pl():
  """Druckt eine Zeile voll Bindestriche"""
  print('-'*80)

def userlist(path):
  """Liefert Userliste als Array zurueck"""
  f = open(path, 'r')
  liste = []
  for line in f:
    teile = line[:-1].split(':')
    liste.append(teile[0])
  f.close()
  return liste

  
# funktionen dokumentieren
# pydoc3 dateiname_ohne_.py
# pydoc3 userlist
  
  
user = userlist('/etc/passwd')
groups = userlist('/etc/group')



pl()

print(user)
pl()

pl()
print(groups)

for username in sorted(user):
  print(username)
pl()
  
for groupname in sorted(groups, reverse=True):
  print(groupname)
pl()



  
