""" Bestimmte Dateien in Verzeichnissen ausgeben

Variante mit Filter, der Praedikat als Vergleich nutzt. 
Diesmal unabhaengig von der laenge des suffixes. 
aber filter kann keine zwei defs aufrufen, 
unsere funktion has_suffix ist ein funktionsgenerator
weil sie keinen wert zurueckgibt, sondern eine funktion zurueck gibt"""

# Funktion erzeugt mit Lambda-Ausdruck Return-Wert eine Inline-Funktion
# has_suffix ist ein Generator, denn es wird eine Funktion zurueckgegeben

import os

# funktionsgenerator
def has_suffix(suffix):
  return lambda name : name[-len(suffix):] == suffix
  

################################################################################
if __name__ == '__main__':

  meinverzeichnisname = '/home/hugo/Documents/projekte--ifc/freecad/'
  for dirpath, dirnames, filenames in os.walk(meinverzeichnisname):
    filenames = list(filter(has_suffix('.ifc'), filenames))
    if filenames:
      print("In", dirpath)
      for d in filenames:
        print("   ",d)


      
  print('-'*80)

  