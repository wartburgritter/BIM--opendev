""" Bestimmte Dateien in Verzeichnissen ausgeben

Variante mit Filter, der Praedikat als Vergleich nutzt. 
Diesmal unabhaengig von der laenge des suffixes. aber filter kann keine
zwei deffs aufrufen, aber lambda kann noch mal eine funktio aufrufen"""


import os


def has_suffix(name, suffix):
  return name[-len(suffix):] == suffix
  

################################################################################
if __name__ == '__main__':

  meinverzeichnisname = '/home/hugo/Documents/projekte--ifc/freecad/'
  for dirpath, dirnames, filenames in os.walk(meinverzeichnisname):
    filenames = list(filter(lambda name: has_suffix(name, '.ifc'), filenames))
    if filenames:
      print("In", dirpath)
      for d in filenames:
        print("   ",d)


      
  print('-'*80)

  