""" Inhalt von Verzeichnissen ausgeben

Es sollen alle Pythondateien eines Verzeichnisses auf dem
Bildschirm ausgegeben werden"""

#import glob # wuerde das gesamte Modul laden

from glob import glob   # laed nur die funktion glob des moduls glob



def list_files(pattern):
  return glob(pattern)



################################################################################
if __name__ == '__main__':
  liste = list_files('*.py')
  liste2 = list_files('../*/*.py')
  
  for dateiname in sorted(liste2):
    print(dateiname)
  
  