""" Bestimmte Dateien in Verzeichnissen ausgeben

Variante mit Varianble in der Pfad steht"""


import os


def os_name_ausgeben():
  """Liefert den Typ des Betriebssystems """
  return os.name

  
#Funktion ist ein Praedikat, liefert nur True/False fuer Parameter
def is_python(name):
  """Liefert True, falls name auf .py endet, sonst False
  
  name muss ein String sein
  Falls nicht, wird ValueError ausgeloest
  """
  if type(name) != type('str'):
    raise ValueError
    
  return name[-3:] == '.py'    #gibt wahr zurueck, wenn .py 


################################################################################
if __name__ == '__main__':

  olddirpath = None
  meinverzeichnisname = '/home/hugo/Documents/projekte--ifc/freecad/'
  for dirpath, dirnames, filenames in os.walk(meinverzeichnisname):
    if filenames:           # alternativ len(filenames)
      for name in filenames:
        if is_python(name):
         if olddirpath != dirpath:
           print("In", dirpath)
           olddirpath = dirpath
         print("   ",name)

      
  print('-'*80)

  