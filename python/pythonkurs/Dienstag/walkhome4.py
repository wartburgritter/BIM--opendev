""" Bestimmte Dateien in Verzeichnissen ausgeben

Variante mit Filter, der Praedikat als Vergleich nutzt. """


import os


def os_name_ausgeben():
  """Liefert den Typ des Betriebssystems """
  return os.name

  
#Funktion ist ein Praedikat, liefert nur True/False fuer Parameter
def is_python(name):
  """Liefert True, falls name auf .py endet, sonst False
  
  name muss ein String sein
  Falls nicht, wird ValueError ausgeloest
  """
  if type(name) != type('str'):
    raise ValueError
    
  return name[-3:] == '.py'    #gibt wahr zurueck, wenn .py 


################################################################################
if __name__ == '__main__':

  meinverzeichnisname = '/home/hugo/Documents/projekte--ifc/freecad/'
  for dirpath, dirnames, filenames in os.walk(meinverzeichnisname):
    # Filter verwendet das Praedikat auf jedes Element an, und uebernimmt
    # diejenigen, fuer die True berechnet wurde. Hier also die .py-Dateien
    filenames = list(filter(is_python, filenames))
    if filenames:
      print("In", dirpath)
      for d in filenames:
        print("   ",d)

      
  print('-'*80)

  