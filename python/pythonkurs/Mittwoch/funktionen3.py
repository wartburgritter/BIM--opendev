def inner(value):
  # besser isinstance als type verwenden
  if not isinstance(value, int):
    raise TypeError
  print("inner({0})".format(format(value)))
  
def outer(value):
  print("outer({0})".format(value))
  inner(value * 2)
  
  
# Abfrage des Klassentypes
"""
hugo@weide:~/pythonkurs/Mittwoch$ python3
Python 3.2.3 (default, Feb 20 2013, 14:44:27) 
[GCC 4.7.2] on linux2
Type "help", "copyright", "credits" or "license" for more information.                
>>> from objekte07 import Auto, Volvo
>>> mein_auto = Auto()                                                                
>>> mein_volvo = Volvo()                                                              
>>> isinstance(mein_auto, Auto)
True                                                                                  
>>> isinstance(mein_auto, Volvo)                                                  
False                                                                                 
>>> isinstance(mein_volvo, Volvo)                                                      
True                                                                                  
>>> isinstance(mein_volvo, Auto)                                                 
True                                                                                  
>>>                                                                                   
hugo@weide:~/pythonkurs/Mittwoch$                              
"""
 
###############################################################################
if __name__ == '__main__':
    outer(10)
    try:
      outer("Funktionen ")
    except TypeError:
      print("Es ist ein Fehler aufgetreten")
      # save error_to_file() # siehe Modul traceback
      print("Details siehe Logdatei")