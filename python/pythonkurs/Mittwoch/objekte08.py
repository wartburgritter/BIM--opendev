## hat noch fehler

import random

class Auto:
  """Klasse Auto
  
  Wird allgemeine Funktionen umsetzen
  """
  def __init__(self, kennzeichen='unbekannt'): 
    """Initialisierung des Objektes aka. Konstruktor
    
    Wird beim Erzeugen des Objektes automatisch von Python aufgerufen.
    
    Hier werden ueblicherweise die Attribute mit Default-Werten vorbelegt.
    """
    self.__schildzeichen = kennzeichen
    self.__kmstand = 0
    self.__tank = 0.2
    
  def __del__(self):
    """Destruktor
    
    Wird automatisch von Python aufgerufen, wenn das Objekt geloescht wird
    der macht in dem Fall nichts ausser eine Ausgabe ausgeben. Python loescht
    Objekte automatisch wenn sie nicht mehr benoetigt werden, also hier
    am Programmende. Ausgabe zeigt mir, dass Python die Objekte ordentlich 
    geloescht hat, also den Speicher wieder frei gegeben hat."""
    print("{0} wird verschrottet". format(self.__schildzeichen))
    
  def kmstand_anfang(self, km):
    """Setzt den Kilometerstand auf den angegebenen Wert"""
    self.__kmstand = km

  def set_nummernschild(self, zeichen):
    """Setzt das Nummernschild auf den angegebenen Wert"""
    self.__schildzeichen = zeichen
    
  def get_nummernschild(self):
    """Gibt das Nummernschild zurueck"""
    return self.__schildzeichen
    
  def tankstand_anfang(self, spritmenge):
    """Setzt den initialen Fuellstand des Tanks"""
    self.__tank = spritmenge

  def get_tankstand(self):
    """Gibt den aktuellen Fuellstand des Tanks zurueck"""
    return self.__tank 
    
  def set_tankstand(self, spritmenge):
     """Veraendert den Tankstand um den angegebenen Wert"""
     print("    Sie tanken:", spritmenge, "l")
     
     self.__tank += spritmenge 

  def status(self):
    """Gibt den status des autos aus"""
    print("Auto:", self.__schildzeichen, "KM-Stand:", self.__kmstand, "Tank-Stand:", self.get_tankstand())
    # auf schildzeichen und kmstand wird direkt zugegriffen, io, da innerhalb der klasse
    # auf tankstand wird ueber get-methode zugegriffen (auch moeglich)

  def verbrauch(self):
    """Verbrauch pro 100 km
    
    in aktueller version fix 10 l/km"""
    return 10.00
  
  def fahren(self, strecke):       
    """Erhpoeht den Kilometerstand um den angegebenen Wert
    und senk den Tankstand """
    spritmenge = strecke * self.verbrauch() / 100
    if spritmenge > self.get_tankstand():
      reststrecke = self.get_tankstand() * 100 / self.verbrauch()
      print("Achtung, Tank ist vorher leer, sie koennen noch ", reststrecke, "fahren")
      self.__kmstand += reststrecke
      self.__tank = 0
    else:
      print("    Sie fahren:", strecke, "km und verbrauchen: ", spritmenge, "l")
      self.__kmstand += strecke            # innerhalb der klasse kann direkt auf die attribute zugegriffen werden
      self.__tank -= spritmenge

  def sonntagsausflug(self):
    """Beim sonntagsausflug wird die fahrstrecke zufaellig zwischen 0 und 250 ermittelt"""
    sonntagsrunde = random.randint(1,250)
    spritmenge = sonntagsrunde * self.verbrauch() / 100
    if spritmenge > self.__tank:
      print("Achtung, Heute ist kein Sonntagsausflug moeglich, Tank ist vorher leer")
    else:
      print("Der Sonntagsausflug ist", sonntagsrunde, "km lang und verbraucht: ", spritmenge, "l")
      self.__kmstand += sonntagsrunde
      self.__tank -= spritmenge


class Volvo(Auto):
  """Klasse Volvo erbt alle Attribute und Methoden von Auto"""
  
  def verbrauch(self):
    """Verbrauch pro 100 km
    
    ein Volvo braucht 20 l/km"""
    return 20.00

class Renault(Auto):
  """Klasse Volvo erbt alle Attribute und Methoden von Auto"""
  
  def verbrauch(self):
    """Verbrauch pro 100 km
    
    ein Volvo braucht 20 l/km"""
    return 5.00

###############################################################################
if __name__ == '__main__':
  print()
  print()

 
 
  
  meen_auto = Volvo()                    # neues objekt erzeugen
  zweitcar = Auto()
  drittcar = Auto()
  ersatzauto = Renault()
 
  fuhrpark = [meen_auto, zweitcar, drittcar, ersatzauto]
 
  tankbereich = 55
  fahrbereich = 600
  
  i=1
  while i < 10000:
    #fuhrpark[random.randint(0,2)].
    fuhrpark[random.randint(0,2)].set_tankstand(random.randint(0,tankbereich))
    fuhrpark[random.randint(0,2)].fahren(random.randint(0,fahrbereich))
    # ich koennte die random rausziehen und speichern, dann denn ich das objekt welches tankt
    # oder die funktion set_tankstand gibt das objekt mit dem es aufgerufen wurde zurueck
    # das ist das objekt self, also return (.., self)
    
    i += 1
    
    
  fuhrpark[0].status()
  fuhrpark[1].status()
  fuhrpark[2].status()
  

  