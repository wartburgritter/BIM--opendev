class TypFehler(Exception):
  def __init__(self, wert):
    """Speichert den ausleosenden Wert im Objekt"""
    self.fehlerwert = wert              # fehlerwert ist das Attribut der Klasse TypFehler

  
def inner(value):
  # besser isinstance als type verwenden
  if not isinstance(value, int):
    raise TypFehler(value)     # Wert an Exception-Objekt uebergeben
  print("inner({0})".format(format(value)))

  
def outer(value):
  print("outer({0})".format(value))
  inner(value * 2)

  
 
###############################################################################
if __name__ == '__main__':
    outer(10)
    try:
      outer("Funktionen ")
      """    except TypeError:
      print("Es ist ein Fehler aufgetreten")
      # save error_to_file() # siehe Modul traceback
      print("Details siehe Logdatei")  """
    except TypFehler as exc:     # exc ist der frei gewaehlte name fuer ein exception objekt
      print("Falscher Datentyp")
      print("Ausloesender Wert: {0}".format(exc.fehlerwert))
      
      
      
      
      
      
      
      