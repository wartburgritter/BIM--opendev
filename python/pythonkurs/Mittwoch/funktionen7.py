def mehr(p1, p2, p3=None, p4="test", *vargs, **kwargs):
  """p1 und p2 sind Pflicht-Parameter
     p3 und p4 sind optionale Parameter
  """
  
  print("p1:", p1)
  print("p2:", p2)
  print("p3:", p3)
  print("p4:", p4)
  print("vargs:", vargs)
  #for arg i vargs:
  #    verarbeite_argument(arg)
  print("kwargs:", kwargs)
  print("-" * 50)
  
  
  
def nur_key_word_parameter(**kwargs):
  """alle parameter mit keywordargumenten
  """
  
  print("kwargs:", kwargs)
  print("-" * 50)

 
 
def nur_argumente(*vargs):
  """alle parameter mit keywordargumenten
  """
  
  print("vargs:", vargs)
  print("-" * 50)

  
  
  
#################################################################################  

mehr(1,2)              # p3 und p4 bekommen defaultwerte
mehr(1,2,3)
mehr(1,2,3,4)          # p3 wird mit 3 gesetzt und p4 mit 4

mehr(1,2,False, False, "eins", "zwei")    # p4 soll gesetzt werden, aber p3 nicht


mehr(1,2,False, False, "eins", "zwei", hostname="notebook29", login="nutzer29")   


nur_key_word_parameter(mykeyword='myargument', hallo='welt', computer='weide', zahl=987)
nur_argumente('myargument', 'tisch', 'stuhl', 57)


# wenn ein array mit * voran uebergeben wird, wird es nicht als gesamtes array uebergeben,
# anstatt wird es als einzelne werte uebergeben print array druck dann die einzelnen werte

# analog ** nur dann dictionary mit key und item

