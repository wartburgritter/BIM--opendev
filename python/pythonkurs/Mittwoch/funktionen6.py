def mehr(p1, p2, p3=None, p4="test"):
  """p1 und p2 sind Pflicht-Parameter
     p3 und p4 sind optionale Parameter
  """
  
  print("p1:", p1)
  print("p2:", p2)
  print("p3:", p3)
  print("p4:", p4)
  print("-" * 50)
  
  
  
  

mehr(1,2)              # p3 und p4 bekommen defaultwerte
mehr(1,2,3)
mehr(1,2,3,4)          # p3 wird mit 3 gesetzt und p4 mit 4

mehr(1,2,p4="hugo")    # p4 soll gesetzt werden, aber p3 nicht


# Aufruf mit einem Parameter erzeugt ein Fehler 
# mehr(1)
