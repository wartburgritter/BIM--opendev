class Auto:
  """Klasse Auto
  
  Wird allgemeine Funktionen umsetzen
  """
  
  def kmstand_anfang(self, km):
    """Setzt den Kilometerstand auf den angegebenen Wert"""
    self.__kmstand = km

  def get_kmstand(self):
    """Gibt den aktuellen Kilometerstand zurueck"""
    return self.__kmstand                              # zugriff von aussen ist verhindert

  def set_nummernschild(self, nummer):
    """Setzt das Nummernschild auf den angegebenen Wert"""
    self.__schildzeichen = nummer
    
  def get_nummernschild(self):
    """Gibt das Nummernschild zurueck"""
    return self.__schildzeichen
    
  def tankstand_anfang(self, spritmenge):
    """Setzt den initialen Fuellstand des Tanks"""
    self.tank = spritmenge

  def get_tankstand(self):
    """Gibt den aktuellen Fuellstand des Tanks zurueck"""
    return self.tank  

  def fahren(self, strecke):       # km gilt nur in der methode, koennte auch strecke heissen
    """Erhpoeht den Kilometerstand um den angegebenen Wert"""
    #self.__kmstand = self.kmstand() + km
    self.__kmstand += strecke
    spritmenge = strecke * self.verbrauch() / 100
    self.tank -= spritmenge

  def status(self):
    """Gibt den KM-Stand und den Fuellstand des Tanks aus"""
    print("Auto:", self.get_nummernschild(), "KM-Stand:", self.get_kmstand(), "Tank-Stand:", self.get_tankstand())
  
  def verbrauch(self):
    """Verbrauch pro 100 km
    
    in aktueller version fix 10 l/km"""
    return 10.00
  
  
###############################################################################
if __name__ == '__main__':

  print('-'*80)
  mein_auto = Auto()                    # neues objekt erzeugen
  mein_auto.set_nummernschild('mein_auto')
  mein_auto.kmstand_anfang(32123)       # Funktion vom Objekt aufrufen
  mein_auto.tankstand_anfang(10)
  mein_auto.status()
  mein_auto.fahren(5)
  mein_auto.status()
  mein_auto.fahren(34599)
  mein_auto.status()
 
  
  print('-'*80)
  zweitwagen = Auto()
  zweitwagen.set_nummernschild('zweitwagen')
  zweitwagen.kmstand_anfang(291)
  zweitwagen.tankstand_anfang(30)
  zweitwagen.status()
  zweitwagen.fahren(27)
  zweitwagen.status()
  zweitwagen.fahren(600)

