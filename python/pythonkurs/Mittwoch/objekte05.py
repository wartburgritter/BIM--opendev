class Auto:
  """Klasse Auto
  
  Wird allgemeine Funktionen umsetzen
  """
  
  def kmstand_anfang(self, km):
    """Setzt den Kilometerstand auf den angegebenen Wert"""
    self.__kmstand = km

  def set_nummernschild(self, zeichen):
    """Setzt das Nummernschild auf den angegebenen Wert"""
    self.__schildzeichen = zeichen
    
  def get_nummernschild(self):
    """Gibt das Nummernschild zurueck"""
    return self.__schildzeichen
    
  def tankstand_anfang(self, spritmenge):
    """Setzt den initialen Fuellstand des Tanks"""
    self.__tank = spritmenge

  def get_tankstand(self):
    """Gibt den aktuellen Fuellstand des Tanks zurueck"""
    return self.__tank
    
  def set_tankstand(self, spritmenge):
     """Veraendert den Tankstand um den angegebenen Wert"""
     self.__tank += spritmenge 

  def fahren(self, strecke):       
    """Erhpoeht den Kilometerstand um den angegebenen Wert
    und senk den Tankstand """
    spritmenge = strecke * self.verbrauch() / 100
    if spritmenge > self.__tank:
      print("Achtung, Tank ist vorher leer")
    else:
      self.__kmstand += strecke
      self.__tank -= spritmenge

  def status(self):
    """Gibt den KM-Stand und den Fuellstand des Tanks aus"""
    #print("Auto:", self.__schildzeichen, "KM-Stand:", self.get_kmstand(), "Tank-Stand:", self.get_tankstand())
    #getmethoden!!!!!!!!!!
    print("Auto:", self.__schildzeichen, "KM-Stand:", self.__kmstand, "Tank-Stand:", self.__tank)
  
  def verbrauch(self):
    """Verbrauch pro 100 km
    
    in aktueller version fix 10 l/km"""
    return 10.00
  
  
###############################################################################
if __name__ == '__main__':

  print('-'*80)
  mein_auto = Auto()                    # neues objekt erzeugen
  mein_auto.set_nummernschild('mein_auto')
  mein_auto.kmstand_anfang(32123)       # Funktion vom Objekt aufrufen
  mein_auto.tankstand_anfang(10)
  mein_auto.status()
  mein_auto.fahren(5)
  mein_auto.status()
  mein_auto.fahren(34599)
  mein_auto.status()
  #mein_auto.set_tankstand(3000)
  mein_auto.status()
 
  
  print('-'*80)
  zweitwagen = Auto()
  zweitwagen.set_nummernschild('zweitwagen')
  zweitwagen.kmstand_anfang(291)
  zweitwagen.tankstand_anfang(30)
  zweitwagen.status()
  zweitwagen.fahren(27)
  zweitwagen.status()
  zweitwagen.fahren(600)

  