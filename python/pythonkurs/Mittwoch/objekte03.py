class Auto:
  """Klasse Auto
  
  Wird allgemeine Funktionen umsetzen
  """
  
  def kmstand(self):
    """Gibt den aktuellen Kilometerstand zurueck"""
    return self.__kmstand                              # zugriff von aussen ist verhindert
    
  def kmstand_anfang(self, km):
    """Setzt den Kilometerstand auf den angegebenen Wert"""
    self.__kmstand = km
    
  def fahren(self, km):       # km gilt nur in der methode, koennte auch strecke heissen
    """Erhpoeht den Kilometerstand um den angegebenen Wert"""
    #self.__kmstand = self.kmstand() + km
    self.__kmstand += km
  
  def tankstand(self):
    """Gibt den aktuellen Fuellstand des Tanks zurueck"""
    return self.tank
    
  def tankstand_anfang(self, spritmenge):
    """Setzt den initialen Fuellstand des Tanks"""
    self.tank = spritmenge

  
################################################################################
if __name__ == '__main__':

  mein_auto = Auto()                    # neues objekt erzeugen
  mein_auto.kmstand_anfang(32123)       # Funktion vom Objekt aufrufen
  mein_auto.tankstand_anfang(10)
  mein_auto.fahren(5)
  mein_auto.fahren(34599)

  zweitwagen = Auto()
  zweitwagen.kmstand_anfang(291)
  zweitwagen.tankstand_anfang(30)
  zweitwagen.fahren(27)


  print("KM-Stand Auto:", mein_auto.kmstand())
  print("Tankstand Auto:", mein_auto.tankstand())
  print("KM-Stand Zweitwagen:", zweitwagen.kmstand())
  print("Tankstand Zweitwagen:", zweitwagen.tankstand())



