import random

class Auto:
  """Klasse Auto
  
  Wird allgemeine Funktionen umsetzen
  """
  def __init__(self):
    """Initialisierung des Objektes aka. Konstruktor"""
    self.__schildzeichen = 0
    self.__kmstand = 0
    self.__tank = 0.2
    
  def kmstand_anfang(self, km):
    """Setzt den Kilometerstand auf den angegebenen Wert"""
    self.__kmstand = km

  def set_nummernschild(self, zeichen):
    """Setzt das Nummernschild auf den angegebenen Wert"""
    self.__schildzeichen = zeichen
    
  def get_nummernschild(self):
    """Gibt das Nummernschild zurueck"""
    return self.__schildzeichen
    
  def tankstand_anfang(self, spritmenge):
    """Setzt den initialen Fuellstand des Tanks"""
    self.__tank = spritmenge

  def get_tankstand(self):
    """Gibt den aktuellen Fuellstand des Tanks zurueck"""
    return self.__tank
    
  def set_tankstand(self, spritmenge):
     """Veraendert den Tankstand um den angegebenen Wert"""
     print("    Sie tanken:", spritmenge, "l")
     self.__tank += spritmenge 

  def status_formatiert(self):
    """Gibt den status des autos aus"""
    print("Auto:", self.__schildzeichen, "KM-Stand:", self.__kmstand, "Tank-Stand:", self.get_tankstand())
    # auf schildzeichen und kmstand wird direkt zugegriffen, io, da innerhalb der klasse
    # auf tankstand wird ueber get-methode zugegriffen (auch moeglich)

  def status_formatiert_old(self):
    """ gibt den aktuellen Stand des Autos aus
    """
    print("KM-Stand:", self.__kmstand, ", Tankstand:", self.get_tankstand())
    #format{} formmatiert die Ausgabe, fuegt Parameter an passenden Stellen ein
    # {nnn} ist Platzhalter fuer nnn-ten Parameter
    # {nnn:...} beschreibt das Format
    #           > = rechtsbuendig, < = linksbündig
    #           Zahl nach >/< bedeutet Breite
    #           d = Ganzzahl, f = Fliesskommazahl, s = Zeichenkette
    print("KM-Stand: {0:>8d}, Tankstand: {1:>8.2f}l".format(self.__kmstand, self.get_tankstand()))

  def verbrauch(self):
    """Verbrauch pro 100 km
    
    in aktueller version fix 10 l/km"""
    return 10.00
  
  def fahren(self, strecke):       
    """Erhpoeht den Kilometerstand um den angegebenen Wert
    und senk den Tankstand """
    spritmenge = strecke * self.verbrauch() / 100
    if spritmenge > self.get_tankstand():
      reststrecke = self.get_tankstand() * 100 / self.verbrauch()
      print("Achtung, Tank ist vorher leer, sie koennen noch ", reststrecke, "fahren")
      self.__kmstand += reststrecke
      self.__tank = 0
    else:
      print("    Sie fahren:", strecke, "km und verbrauchen: ", spritmenge, "l")
      self.__kmstand += strecke            # innerhalb der klasse kann direkt auf die attribute zugegriffen werden
      self.__tank -= spritmenge

  def sonntagsausflug(self):
    """Beim sonntagsausflug wird die fahrstrecke zufaellig zwischen 0 und 250 ermittelt"""
    sonntagsrunde = random.randint(1,250)
    spritmenge = sonntagsrunde * self.verbrauch() / 100
    if spritmenge > self.__tank:
      print("Achtung, Heute ist kein Sonntagsausflug moeglich, Tank ist vorher leer")
    else:
      print("Der Sonntagsausflug ist", sonntagsrunde, "km lang und verbraucht: ", spritmenge, "l")
      self.__kmstand += sonntagsrunde
      self.__tank -= spritmenge


class Volvo(Auto):
  """Klasse Volvo erbt alle Attribute und Methoden von Auto"""
  
  def verbrauch(self):
    """Verbrauch pro 100 km
    
    ein Volvo braucht 20 l/km"""
    return 20.00

###############################################################################
if __name__ == '__main__':
  print()
  print()

 
  print('Mein Auto')
  print('-'*40)
  mein_auto = Volvo()                    # neues objekt erzeugen
  mein_auto.set_nummernschild('mein_auto')
  mein_auto.kmstand_anfang(32123)       # Funktion vom Objekt aufrufen
  mein_auto.tankstand_anfang(150)
  mein_auto.status_formatiert()
  mein_auto.fahren(20)
  mein_auto.status_formatiert()
  mein_auto.fahren(39)
  mein_auto.status_formatiert()
  mein_auto.set_tankstand(30)
  mein_auto.status_formatiert()
  mein_auto.sonntagsausflug()
  mein_auto.status_formatiert()
  mein_auto.fahren(39)
  mein_auto.status_formatiert()
  print()
  print()

  print('Zweitwagen')
  print('-'*40)
  zweitwagen = Auto()
  zweitwagen.set_nummernschild('zweitwagen')
  zweitwagen.kmstand_anfang(291)
  zweitwagen.tankstand_anfang(30)
  zweitwagen.status_formatiert()
  zweitwagen.fahren(27)
  zweitwagen.status_formatiert()
  zweitwagen.fahren(60)
  zweitwagen.status_formatiert()
  zweitwagen.set_tankstand(10)
  zweitwagen.status_formatiert()
  zweitwagen.fahren(30)
  zweitwagen.status_formatiert()
  zweitwagen.set_tankstand(1.7)
  zweitwagen.status_formatiert()
  zweitwagen.fahren(400)
  zweitwagen.status_formatiert()
  print()
  print()


  print('Drittwagen')
  print('-'*40)
  drittwagen = Auto()
  drittwagen.status_formatiert()    # erzeugt fehler wenn init methode nicht vorhanden ist, da attribute nicht gesetzt sind
  
  
  