import unittest
from objekte08 import Auto   # bezieht sich auf die dort jetzt vorhandenen bezeichner


# ich koennte mir die test aus alle selbst schreiben, in der klasse und auch die tests



class AutoTests(unittest.TestCase):
  def test_add(self):
    """ test von operatoren in python"""
    self.assertEqual(1 + 1, 2)
    self.assertNotEqual(1 + 1, 3)
    self.assertEqual(1 + -1, 0)
    #self.assertNotEqual(1 + 1, 2)    # test nicht bestanden

  def test_get_tankstand(self):
    """ test von methoden meiner klasse auto"""
    auto = Auto()     # um zu testen muessen wir ein objekt anlegen 
    self.assertEqual(auto.get_tankstand(), 0.2)
    auto.set_tankstand(1)
    self.assertEqual(auto.get_tankstand(), 1.2)
    self.assertNotEqual(auto.get_tankstand(), 5)
    self.assertRaises(TypeError, lambda: auto + "10")
   
  
  def test_index(self):
    """test von einer Exception ValueError in python"""
    self.assertRaises(ValueError, lambda: "test".index('.'))
    # finde mir in test den punkt, da ist keiner also wird eine Exception ausgeloest
    
  def test_indexerror(self):
    with self.assertRaises(IndexError):
      "test"[4]

    
    
##################################################
if __name__ == '__main__':
  unittest.main()
  # finde alle klasse
  # finde alle testklassen, nur die methoden, die mit test beginnen werden aufgerufen
  