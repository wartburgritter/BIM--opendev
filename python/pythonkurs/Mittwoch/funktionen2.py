def inner(value):
  if type(value)  != type(1):    # wenn type von value ungleich int
    raise TypeError
  print("inner({0})".format(format(value)))
  
def outer(value):
  print("outer({0})".format(value))
  inner(value * 2)
  
  
  
###############################################################################
if __name__ == '__main__':
    outer(10)
    try:
      outer("Funktionen ")
    except TypeError:
      print("Es ist ein Fehler aufgetreten")
      # save error_to_file() # siehe Modul traceback
      print("Details siehe Logdatei")