class Auto:
  """Klasse Auto
  
  Wird allgemeine Funktionen umsetzen
  """
  
  def kmstand(self):
    """Gibt den aktuellen Kilometerstand zurueck"""
    return self.__kmstand
    
  def kmstand_anfang(self, km):
    """Setzt den Kilometerstand auf den angegebenen Wert"""
    self.__kmstand = km
    
  def fahren(self, km):       # km gilt nur in der methode, koennte auch strecke heissen
    """Erhpoeht den Kilometerstand um den angegebenen Wert"""
    #self.__kmstand = self.kmstand() + km
    self.__kmstand += km
  
  
  
################################################################################
if __name__ == '__main__':

  mein_auto = Auto()                    # neues objekt erzeugen
  mein_auto.kmstand_anfang(32123)       # Funktion vom Objekt aufrufen
  mein_auto.fahren(5)
  mein_auto.fahren(34599)

  zweitwagen = Auto()
  zweitwagen.kmstand_anfang(291)
  zweitwagen.fahren(27)


  print("KM-Stand Auto:", mein_auto.kmstand())
  print("KM-Stand Zweitwagen:", zweitwagen.kmstand())



