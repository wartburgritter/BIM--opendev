#!/usr/bin/python
# -*- coding: utf-8 -*-

# simple1.py

import sys
from PySide import QtGui

app = QtGui.QApplication(sys.argv)

wid = QtGui.QWidget()
wid.resize(350, 250)
wid.setWindowTitle('MySimple')
wid.show()

sys.exit(app.exec_())
