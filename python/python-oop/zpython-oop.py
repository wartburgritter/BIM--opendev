import sys

mynewpath = '/home/hugo/Documents/projekte--BIM--work/python/python-oop'
sys.path.append(mynewpath)



########################################################
# bank_account1
import bank_account1
reload(bank_account1)

bank_account1.balance
bank_account1.deposit(50)
bank_account1.deposit(50)
bank_account1.deposit(50)

bank_account1.withdraw(200)
bank_account1.balance



########################################################
# bank_account2
import bank_account2

a = bank_account2.make_account()
b = bank_account2.make_account()
bank_account2.deposit(a, 100)
bank_account2.deposit(b, 50)
bank_account2.withdraw(b, 10)
bank_account2.withdraw(a, 10)



########################################################
# bank_account3
import bank_account3

a = bank_account3.BankAccount()
b = bank_account3.BankAccount()
a.deposit(100)
b.deposit(50)
b.withdraw(10)
a.withdraw(10)



########################################################
# bank_account4
import bank_account4

a = bank_account4.BankAccount()
b = bank_account4.MinimumBalanceAccount(0)
a.deposit(100)
a.withdraw(200)
b.deposit(100)
b.withdraw(200)



########################################################
# inheritance_output
import inheritance_output

a = inheritance_output.A()
b = inheritance_output.B()
print a.f(), b.f()  # AB weil classe b ueberschreibt ja def g 
print a.g(), b.g()  # AB



########################################################
# mytest
# hline ist global nicht definiert in drawing_shapes wie hier d()
import mytest

reload(mytest)

a = mytest.A('hello')
getattr(a,'x')
a.x

a.c
a.c()

a.d
a.d()

########################################################
# drawing_shapes
import drawing_shapes

reload(drawing_shapes)

mycan = drawing_shapes.Canvas(10, 10)
mycan.display()
mycan.setpixel(2,5)
mycan.setpixel(2,6)
mycan.setpixel(2,9)
mycan.display()
mycan.getpixel(2,5)
mycan.getpixel(1,1)

mycan.display()
mycan.data

mycan.display()
mycan.clear()
mycan.display()


# Shape ist nur die oberklasse fuer die einzelnen shapes
r1 = drawing_shapes.Rectangle(2,3,4,5)
getattr(r1,'x')
r1.x


mycan2 = drawing_shapes.Canvas(20, 10)
r3 = drawing_shapes.Rectangle(1,1,10,3)
r3.paint(mycan2)
mycan2.display()

mycan2.clear()
sq = drawing_shapes.Square(1,1,5)
sq.paint(mycan2)
mycan2.display()


mycan3 = drawing_shapes.Canvas(20, 10)
r4 = drawing_shapes.Rectangle(1,1,10,3)
sq = drawing_shapes.Square(5,5,5)
cs = drawing_shapes.CompoundShape([r4, sq])
cs.paint(mycan3)
mycan3.display()



########################################################
#


