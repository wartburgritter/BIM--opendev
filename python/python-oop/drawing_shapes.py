class Canvas:

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.data = [[' '] * width for i in range(height)]

    def setpixel(self, col, row):
        self.data[row][col] = '*'

    def getpixel(self, col, row):
        return self.data[row][col]

    def display(self):
        print "\n".join(["".join(row) for row in self.data])

    def clear(self):
        self.data = [[' '] * self.width for i in range(self.height)]




class Shape:

    def paint(self, canvas): pass



class Rectangle(Shape):

    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def hline(self, x, y, w, canvas):
        for i in range(w):
            canvas.setpixel(x+i,y)

    def vline(self, x, y, h, canvas):
        for i in range(h):
            canvas.setpixel(x,y+i)
 

    def paint(self, canvas):
        self.hline(self.x, self.y, self.w, canvas)
        self.hline(self.x, self.y + self.h - 1, self.w, canvas)
        self.vline(self.x, self.y, self.h, canvas)
        self.vline(self.x + self.w - 1, self.y, self.h, canvas)



class Square(Rectangle):

    def __init__(self, x, y, size):
        Rectangle.__init__(self, x, y, size, size)



class CompoundShape(Shape):

    def __init__(self, shapes):
        self.shapes = shapes

    def paint(self, canvas):
        for s in self.shapes:
            s.paint(canvas)



