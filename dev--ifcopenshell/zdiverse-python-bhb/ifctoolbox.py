# aufruf aus python mit 
# ifctoolbox.get_ifc_building_element_type_list(f.by_type("ifcbuildingelement"))
# ifctoolbox.get_ifc_building_element_count(f.by_type("ifcbuildingelement"))
# ifctoolbox.get_ifc_building_element_count(f.by_type("ifcwall"))
# ifctoolbox.get_ifc_building_element_type_count(f.by_type("ifcbuildingelement"))
# ifctoolbox.print_ifc_building_element_type_count(f.by_type("ifcbuildingelement"))



#############################################################################################
def get_ifc_building_element_type_list(fullIfcBuildingElementList):

   allIfcBuildingTypeElementList=[]
   for n in range(len(fullIfcBuildingElementList)):
      allIfcBuildingTypeElementList.append(fullIfcBuildingElementList[n].is_a())

   singleIfcBuildingElementTypeList=[allIfcBuildingTypeElementList[0]]
   for n in range(len(allIfcBuildingTypeElementList)):
      if fullIfcBuildingElementList[n].is_a() in singleIfcBuildingElementTypeList:
         pass
      else:
         singleIfcBuildingElementTypeList.append(fullIfcBuildingElementList[n].is_a())
   
   singleIfcBuildingElementTypeList.sort()
   return singleIfcBuildingElementTypeList

   
#############################################################################################
def get_ifc_building_element_count(fullIfcBuildingElementList):
   return len(fullIfcBuildingElementList)
   
   
#############################################################################################
def get_ifc_building_element_type_count(fullIfcBuildingElementList):

   allIfcBuildingTypeElementList=[]
   for n in range(len(fullIfcBuildingElementList)):
      allIfcBuildingTypeElementList.append(fullIfcBuildingElementList[n].is_a())

   singleIfcBuildingElementTypeList=[allIfcBuildingTypeElementList[0]]
   for n in range(len(allIfcBuildingTypeElementList)):
      if fullIfcBuildingElementList[n].is_a() in singleIfcBuildingElementTypeList:
         pass
      else:
         singleIfcBuildingElementTypeList.append(fullIfcBuildingElementList[n].is_a())
   
   singleIfcBuildingElementTypeList.sort()
   
   ifcBuildingElementTypCount = []
   tmp = ["IfcBuildingElement", len(allIfcBuildingTypeElementList)]
   ifcBuildingElementTypCount.append(tmp)
   for n in range(len(singleIfcBuildingElementTypeList)):
      tmp = [singleIfcBuildingElementTypeList[n], allIfcBuildingTypeElementList.count(singleIfcBuildingElementTypeList[n])]
      ifcBuildingElementTypCount.append(tmp)

   return ifcBuildingElementTypCount   
 
 
#############################################################################################
def print_ifc_building_element_type_count(fullIfcBuildingElementList):
   a = get_ifc_building_element_type_count(fullIfcBuildingElementList)
   for n in range(len(a)):
      print a[n]
 
   
#############################################################################################
print "ifctoolbox.py succesfully loaded"
