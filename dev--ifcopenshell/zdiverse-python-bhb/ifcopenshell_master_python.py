cd /home/hugo/Documents/projekte--ifc/freecad/
python

#############################################################################
#############################################################################
import IfcImport
import ifctoolbox


# hilfe
reload(ifctoolbox)
dir(IfcImport)
dir(IfcImport.Entity)
dir(IfcImport.Entity.is_a)
print IfcImport.Entity.is_a.__doc__
dir(ifctoolbox)

# MEIENE TOOLBOX MUSS UMGESCHRIEBEN WERDEN IN ENTITIY (VARIABLENNAMEN) 
# WEIL DIE ÜBERGEBENE LISTE ENTHAELT DATEN DES TYPS ENTITIY
# entity ist wohl der oberste objekttyp
# LISTE ERSTELLEN
myIfcRootList = f.by_type("ifcroot")


# VOLLSTAENDIGE LISTE DRUCKEN
ifctoolbox.print_ifc_building_element_type_count(f.by_type("ifcroot"))


# LISTENTYP
type(myIfcRootList)
type(myIfcRootList[0])


#IFCKOORDINATEN WERDEN GAR NICHT ANGEZEIGT, AUCH NICHT IFCAXEN USW.


###################################################################################################################
###################################################################################################################


ifctoolbox.get_ifc_building_element_type_list(f.by_type("ifcbuildingelement"))
ifctoolbox.get_ifc_building_element_count(f.by_type("ifcbuildingelement"))
ifctoolbox.get_ifc_building_element_count(f.by_type("ifcwall"))
ifctoolbox.get_ifc_building_element_type_count(f.by_type("ifcbuildingelement"))
ifctoolbox.print_ifc_building_element_type_count(f.by_type("ifcbuildingelement"))
ifctoolbox.print_ifc_building_element_type_count(f.by_type("ifcbuildingelement"))

ifctoolbox.print_ifc_building_element_type_count(f.by_type("ifcroot"))



# Modelle
inFile = 'modelle--bhb/minigebaeude_ohne_bpl.ifc'
inFile = 'modelle--bhb/kappeler.ifc'
inFile = 'modelle--bhb/nak.ifc'
inFile = 'modelle--bhb/lgz-steel.ifc'
inFile = 'modelle--bhb/lgz-treppe.ifc'
inFile = 'modelle--bhb/lgz-treppe--allplan.ifc'
inFile = 'modelle--bhb/aargnet.ifc'
inFile = 'modelle--diverse/Munkerud_hus6_BE.ifc'
inFile = 'modelle--diverse/revit2012_janesville_restaurant.ifc'
inFile = 'modelle--diverse/geometrygym_great_court_roof.ifc'
inFile = 'modelle--ifc_orginale_buero_Extern/stahlbau--mellikon--lgz--ges_halle.ifc'
inFile = 'modelle--import/myhouse_windows.ifc'
inFile = 'modelle--ifc_orginale_inet/Munkerud_hus6_BE.ifc'
inFile = 'minigebaeude_mit_decke_als_bpl.ifc'

path = '/home/hugo/Documents/projekte--ifc/freecad/modelle--ifc/modelle--zbhb--kleine/'
inFile = path + inFile
f = IfcImport.open(inFile)




#############################################################################
## diverse ifcelemente
# strukturierung
# ifcplate --> AllplanProjektName, ifcsite --> Liegenschaft, ifcbuilding --> Gebaeude, ifcbuildingstorey --> Geschoss
f.by_type("ifcproject")
f.by_type("ifcsite")
f.by_type("ifcbuilding")
f.by_type("ifcbuildingstorey")

# ifcbuildingelemente
f.by_type("ifcbuildingelement")
f.by_type("ifcbeam")
f.by_type("ifcplate")
f.by_type("ifcslab")
f.by_type("ifcwall")
f.by_type("ifccolumn")
f.by_type("ifcbuildingelementproxy")

# weiteres
f.by_type("ifcownerhistroy")
f.by_type("ifccartesianpoint")
f.by_type("ifcshaperepresentation")
f.by_type("ifcquantityarea")
f.by_type("ifcquantityvolume")
f.by_type("ifcquantityweight")
f.by_type("ifcquantitylength")


#############################################################################
f.by_type("ifcbuilding")
myEntity = _[0]
myEntity
type(myEntity)
myEntity.is_a()
myEntity.get_argument_count()
myEntity.get_argument('Name')
myEntity.get_argument('GlobalId')
myEntity.get_argument_index('Name')
myEntity.get_argument_index('GlobalId')

myEntity.get_argument_name(1)
myEntity.get_argument_name(2)
for m in range(myEntity.get_argument_count()):
   print myEntity.get_argument_name(m)


myEntity.get_argument(0)
myEntity.get_argument(1)
for m in range(myEntity.get_argument_count()):
   print myEntity.get_argument(m)


# get_argument_type ist wohl noch nicht richtig programmiert 
# evtl BUG !!!!!!!!!
myEntity.get_argument_type(1)
for m in range(myEntity.get_argument_count()):
   print myEntity.get_argument_type(m)


myEntity = f.by_type("ifcbuilding")  geht nicht ist ne liste und kein IfcEntity


###########################################################################################
myIfcRootList = f.by_type("ifcroot")
type(myIfcRootList)
type(myIfcRootList[0])


myBuildingElementList = f.by_type("ifcbuildingelement")



for n in range(len(myBuildingElementList)):
   print myBuildingElementList[n].is_a()


for n in range(len(myBuildingElementList)):
   print myBuildingElementList[n]


for n in range(len(myBuildingElementList)):
   print myBuildingElementList[n].get_argument('Name')


for n in range(len(myBuildingElementList)):
   print unicode(myBuildingElementList[n].get_argument('Name'), 'unicode-escape')


for n in range(len(myBuildingElementList)):
   tmp = myBuildingElementList[n].get_argument('Name')
   print tmp.decode('unicode-escape')


for n in range(len(myBuildingElementList)):
   print myBuildingElementList[n].get_argument('GlobalId')


for n in range(len(myBuildingElementList)):
   print myBuildingElementList[n].get_argument(1)


for n in range(len(myBuildingElementList)):
   print myBuildingElementList[n].get_argument_count()


for n in range(len(myBuildingElementList)):
   print  # leere zeile
   myEntity = myBuildingElementList[n]
   print  myEntity.is_a()
   for m in range(myEntity.get_argument_count()):
       print myEntity.get_argument(m)
 
      
for n in range(len(myBuildingElementList)):
   print  # leere zeile
   myEntity = myBuildingElementList[n]
   print  myEntity.is_a()
   for m in range(myEntity.get_argument_count()):
       print myEntity.get_argument_name(m)



for n in range(len(myIfcRootList)):
   print  # leere zeile
   myEntity = myIfcRootList[n]
   print  myEntity.is_a()
   for m in range(myEntity.get_argument_count()):
       print myEntity.get_argument_name(m)


for n in range(len(myIfcRootList)):
   print  myIfcRootList[n]





########################################################
#Ausgabeumleitung in Datei 
#http://www.python-kurs.eu/sys_modul.php
# funktioniert nicht in freecad

import sys

print("Coming through stdout")

# stdout is saved
save_stdout = sys.stdout

fh = open("test.txt","w")

sys.stdout = fh
print("This line goes to test.txt")


# return to normal:
sys.stdout = save_stdout

fh.close()

#######################################################
save_stdout = sys.stdout
fh = open("test.txt","w")
sys.stdout = fh

sys.stdout = save_stdout
fh.close()


