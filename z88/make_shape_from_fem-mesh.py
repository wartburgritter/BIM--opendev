'''
question, given is a bound of nodes

3D:
How do I only get the surface nodes? --> Mesh --> Shape

2D:
Ho do I get only the 

Neu modellieren ist eine option, aber dann kann ich meist das orginal mesh nicht verwenden, wegen ungenauigkeiten
Besser ist eine shape, die genau aus den meshknoten besteht
siehe Schraubenschluessel, da war bei den fixes genau das problem
'''


# GUI 
# - befehl ausfuehren
# - punkte markieren --> Draft upgrade
# - mit part --> shapebuilder shapes bauen

# alle punkte

selection = FreeCADGui.Selection.getSelection()
if len(selection) == 1 :
    import Draft
    ns = FreeCADGui.Selection.getSelectionEx()[0].Object.FemMesh.Nodes
    for v in ns:
        Draft.makePoint(ns[v])
else:
    print 'Only select one object !'


import Draft
ns = App.ActiveDocument.getObject("b24__z88i1").FemMesh.Nodes
for v in ns:
    Draft.makePoint(ns[v])



#####################################################################
# b1--gabelschluessel, kraefte und fixes
ns = App.ActiveDocument.getObject("b1__z88i1").FemMesh.Nodes
nodes = [ns[11], ns[143], ns[216], ns[220], ns[227], ns[231], ns[238], ns[242], ns[249]]
import Draft
for n in nodes:
    Draft.makePoint(n)


#####################################################################
# b5--tortensegmentplatte, kraefte und fixes
ns = App.ActiveDocument.getObject("b5__z88i1").FemMesh.Nodes
nodes_forces = [ns[1], ns[3], ns[5], ns[7], ns[73], ns[75], ns[77]]
nodes_fixes = [ns[65], ns[66], ns[67], ns[68], ns[69], ns[70], ns[71], ns[72],
               ns[121], ns[122], ns[123], ns[124], ns[125], ns[126]]
import Draft
for n in nodes_fixes:
    Draft.makePoint(n)


#####################################################################
# b24--vierkantrohr, kraefte und fixes
ns = App.ActiveDocument.getObject("b24__z88i1").FemMesh.Nodes
nodes_forces = [ns[1], ns[2], ns[5], ns[6], ns[7], ns[8], ns[9],
                ns[129], ns[131], ns[132], ns[133], ns[134], ns[135],
                ns[236], ns[238], ns[239], ns[240], ns[241], ns[242],
                ns[343], ns[344], ns[345], ns[346], ns[347],
                ns[429], ns[431], ns[437], ns[440], ns[443], ns[446],
                ns[758], ns[762], ns[764], ns[766], ns[769], ns[772],
                ns[1067], ns[1071], ns[1073], ns[1075], ns[1078], ns[1081],
                ns[1376], ns[1378], ns[1380], ns[1383], ns[1386], ns[1390]]
nodes_fixes = [ns[3], ns[4], ns[29], ns[30], ns[31], ns[32], ns[33],
               ns[130], ns[136], ns[137], ns[138], ns[139], ns[140],
               ns[237], ns[243], ns[244], ns[245], ns[246], ns[247],
               ns[348], ns[349], ns[350], ns[351], ns[352],
               ns[434], ns[435], ns[507], ns[509], ns[512], ns[515],
               ns[760], ns[777], ns[780], ns[781], ns[784], ns[787],
               ns[1069], ns[1086], ns[1089], ns[1090], ns[1093], ns[1096],
               ns[1392], ns[1395], ns[1396], ns[1399], ns[1402], ns[1407]]
import Draft
for n in nodes_fixes:
    Draft.makePoint(n)
