"""
  see main loop, for what to do in FreeCAD
"""

def print_objectlist(objectlist):
  """prints the objekt array line by line
   """
  for o in objectlist:
    print(o,'    ' , o.Name,'    ' , o.Label,'    ' ,o.PropertiesList)
  print('')
  print('The objct list has', len(objectlist), 'Objects.')

  

def print_archproxylist(objectlist):
  for o in objectlist:
    if hasattr(o,"Proxy"):         
      if hasattr(o.Proxy,"Type"):         
        print(o,'    ' , o.Name,'    ' , o.Label,'    ' ,o.Proxy.Type)
  print('')

  

def print_archrolelist(objectlist):
  for o in objectlist:
    if hasattr(o,"Role"):         
      print(o,'    ' , o.Name,'    ' , o.Label,'    ' ,o.Role)
  print('')

  
  
def archrolecount(objectlist):
  beam = 0
  column = 0
  foundation = 0
  slab = 0
  wall = 0
  for o in objectlist:
    if hasattr(o,'Role'):         
      if o.Role == 'Beam':
        beam += 1
      elif o.Role == 'Column':
        column += 1
      elif o.Role == 'Foundation':
        foundation += 1
      elif o.Role == 'Slab':
        slab += 1
      elif o.Role == 'Wall':
        wall +=1
  print('Folgende Elemente gefunden:')
  print('Beam', beam)
  print('Column', column)
  print('Foundation', foundation)
  print('Slab', slab)
  print('Wall', wall)
  print('Sum', beam+column+foundation+slab+wall)
  print('')
  

  
def get_structurelist(objectlist):
  structurelist = []
  for o in objectlist:
    if hasattr(o,'Proxy'):
      if hasattr(o.Proxy,'Type'):
        if o.Proxy.Type == 'Structure':
          structurelist.append(o)
  return(structurelist)
      
      
         
def get_walllist(objectlist):         
  walllist = []         
  for o in objectlist:         
    if hasattr(o,'Proxy'):         
      if hasattr(o.Proxy,'Type'):         
        if o.Proxy.Type == 'Wall':         
          #print 'We found a wall!'          
          walllist.append(o)         
  return(walllist)         
         
    

def get_archlist(objectlist):             
  archlist = []             
  for o in objectlist:             
    if hasattr(o,'Proxy'):             
      if hasattr(o.Proxy,'Type'):             
        #if o.Proxy.Type == 'Wall':             
          print 'We found a arch, could be a floor too!'          
          archlist.append(o)      
          print o.Proxy.Type
          print o.PropertiesList
  return(archlist)             



def get_archshapelist(objectlist):             
  archshapelist = []             
  for o in objectlist:             
      if hasattr(o,'Shape'):             
          print 'We found a obj with shape!'          
          archshapelist.append(o)      
          print o.Shape
          print o.PropertiesList
  return(archshapelist)  
  
  
  
def get_archrolecolumnlist(objectlist):
  arl = []
  for o in objectlist:
    if hasattr(o,'Role'):
      if o.Role == 'Column':
        print ('We found a Role: ' + o.Role)
        arl.append(o)
  return(arl)


  
def get_archroleslablist(objectlist):
  arl = []
  for o in objectlist:
    if hasattr(o,'Role'):
      if o.Role == 'Slab':
        print ('We found a Role: ' + o.Role)
        arl.append(o)
  return(arl)



def get_archrolefoundationlist(objectlist):
  arl = []
  for o in objectlist:
    if hasattr(o,'Role'):
      if o.Role == 'Foundation':
        print ('We found a Role: ' + o.Role)
        arl.append(o)
  return(arl)
  
  
  
def get_archifcpropobjectlist(objectlist):
  aifcl = []
  for o in objectlist:
    if hasattr(o,'IfcImportedAttributes'):
      print ('We found a Object which has IfcImportedAttributes! The ifcname is: ' + o.IfcImportedAttributes['IFCName'])
      aifcl.append(o)
  return(aifcl)

  
  
def print_volumen(listofpf):
  """liste die volumina aller shape objekte (pf fuer part feature)
  """
  vol = 0
  for i in range(len(listofpf)):
    print(listofpf[i].Name, '   ', listofpf[i].Shape.Volume)
    vol += listofpf[i].Shape.Volume
  print('')
  print('Volumen is:', vol)


def get_positionvolumen(listofpf):
  """liste die volumina aller shape objekte (pf fuer part feature)
  
  IFCName ist aktull meine Positionsnummer
  """
  for o in listofpf:
    if hasattr(o,'IfcImportedAttributes'):
      pass
    else:
      print ('Object ' + o.Name + ' has no IfcImportedAttributes')
      return

  vol = 0
  lp = [listofpf[0].IfcImportedAttributes['IFCName']]
  print lp
  for o in listofpf:
    print(o.Name + '    ' + o.IfcImportedAttributes['IFCName'] + '    ' + str(o.Shape.Volume))
    vol += o.Shape.Volume
    
    if o.IfcImportedAttributes['IFCName'] in lp:
      pass
    else:
      lp.append(o.IfcImportedAttributes['IFCName'])
  print sorted(lp)
  lpm = [] # list of positionen and mengen
  for p in sorted(lp):
    lpm.append([p,0])
  print lpm
  for o in listofpf:
    for pm in lpm:
      if o.IfcImportedAttributes['IFCName'] == pm[0]:
        #print ('Position ' + o.IfcImportedAttributes['IFCName'] + ' == ' + pm[0])
        pm[1] += o.Shape.Volume
  vol2 = 0
  for pm in lpm:
    print (pm[0] + '  ' + str(pm[1]))
    vol2 += pm[1]
  print('')
  print('Volumensumme ist          :', vol)
  print('Summe Positionsvolumen ist:', vol2)
  return lpm

  
  
def write_listofpositionvolumen(lpm):
  outfile = '/home/hugo/Documents/projekte--ifc/freecad/python--freecad--shape--scripting/holderbank-freecad-pos-mengen.txt'
  wf = open(outfile, 'w')
  for pm in lpm:
    tmps = (pm[0].split('.')[0] + pm[0].split('.')[1])
    wf.write (tmps + ' ' + str(pm[1]) + '\n')
  
################################################################################################  
################################################################################################  
################################################################################################  
if __name__ == '__main__':
  

  # If this file is loaded as module in FreeCAD the following code will not be executed
  # copy the code to the pythonkonsole in FreeCAD
  
  import sys
  sys.path.append('/home/hugo/Documents/projekte--ifc/freecad/python--freecad--shape--scripting/')
  import building_elements as x
  #reload(x)
  
  # oben the model
  
  # lets see what the model has, copy step by step
  objs = FreeCAD.ActiveDocument.Objects
  print(objs)
  x.print_objectlist(objs)        # x weil    import versuch as x
  x.print_archproxylist(objs)     # x weil    import versuch as x
  x.print_archrolelist(objs)      # x weil    import versuch as x
  x.archrolecount(objs)
 
  archifcporpobjectlist = x.get_archifcpropobjectlist(objs)

  # volume of all archelemtents
  x.print_volumen(archifcporpobjectlist)
  
  listofpositionvolumen = x.get_positionvolumen(archifcporpobjectlist)
  print listofpositionvolumen
  x.write_listofpositionvolumen(listofpositionvolumen)
 
  structurelist = x.get_structurelist(objs)
  x.print_objectlist(structurelist)
  
  walllist = x.get_walllist(objs)
  x.print_objectlist(walllist)
   
  archlist = x.get_archlist(objs) 
  x.print_objectlist(archlist) 
  # auch mit groupobjekten wie floors!!!

  archshapelist = x.get_archshapelist(objs) 
  x.print_objectlist(archshapelist) 
  # ohne groupeobjekten!!!
  
  archrolecolumnlist = x.get_archrolecolumnlist(objs)
  x.print_objectlist(archrolecolumnlist)
  
  archroleslablist = x.get_archroleslablist(objs)
  x.print_objectlist(archroleslablist)
  
  archrolefoundationlist = x.get_archrolefoundationlist(objs)
  x.print_objectlist(archrolefoundationlist)
 
  
  
  App.ActiveDocument.IfcBeam.Proxy.Type
  App.ActiveDocument.IfcBeam.Role
  App.ActiveDocument.IfcBeam.Role = Column
   
   