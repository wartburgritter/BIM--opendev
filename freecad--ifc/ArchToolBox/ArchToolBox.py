"""
  see main loop, for what to do in FreeCAD
"""

import FreeCAD

def print_objectlist(objectlist):
  """prints the objekt array line by line
   """
  for o in objectlist:
    print(o,'    ' , o.Name,'    ' , o.Label,'    ' ,o.PropertiesList)
  print('')
  print('The objct list has', len(objectlist), 'Objects.')

  

def print_archproxylist(objectlist):
  for o in objectlist:
    if hasattr(o,"Proxy"):         
      if hasattr(o.Proxy,"Type"):         
        print(o,'    ' , o.Name,'    ' , o.Label,'    ' ,o.Proxy.Type)
  print('')

  

def print_archrolelist(objectlist):
  for o in objectlist:
    if hasattr(o,"Role"):         
      print(o,'    ' , o.Name,'    ' , o.Label,'    ' ,o.Role)
  print('')

  
  
def archrolecount(objectlist):
  beam = 0
  column = 0
  foundation = 0
  slab = 0
  wall = 0
  for o in objectlist:
    if hasattr(o,'Role'):         
      if o.Role == 'Beam':
        beam += 1
      elif o.Role == 'Column':
        column += 1
      elif o.Role == 'Foundation':
        foundation += 1
      elif o.Role == 'Slab':
        slab += 1
      elif o.Role == 'Wall':
        wall +=1
  print('Folgende Elemente gefunden:')
  print('Beam', beam)
  print('Column', column)
  print('Foundation', foundation)
  print('Slab', slab)
  print('Wall', wall)
  print('Sum', beam+column+foundation+slab+wall)
  print('')
  

  
def get_structurelist(objectlist):
  structurelist = []
  for o in objectlist:
    if hasattr(o,'Proxy'):
      if hasattr(o.Proxy,'Type'):
        if o.Proxy.Type == 'Structure':
          structurelist.append(o)
  return(structurelist)
      
      
         
def get_walllist(objectlist):         
  walllist = []         
  for o in objectlist:         
    if hasattr(o,'Proxy'):         
      if hasattr(o.Proxy,'Type'):         
        if o.Proxy.Type == 'Wall':         
          #print 'We found a wall!'          
          walllist.append(o)         
  return(walllist)         
         
    

def get_archlist(objectlist):             
  archlist = []             
  for o in objectlist:             
    if hasattr(o,'Proxy'):             
      if hasattr(o.Proxy,'Type'):             
        #if o.Proxy.Type == 'Wall':             
          print 'We found a arch, could be a floor too!'          
          archlist.append(o)      
          print o.Proxy.Type
          print o.PropertiesList
  return(archlist)             



def get_archshapelist(objectlist):             
  archshapelist = []             
  for o in objectlist:             
      if hasattr(o,'Shape'):             
          print 'We found a obj with shape!'          
          archshapelist.append(o)      
          print o.Shape
          print o.PropertiesList
  return(archshapelist)  
  
  
  
def get_archrolecolumnlist(objectlist):
  arl = []
  for o in objectlist:
    if hasattr(o,'Role'):
      if o.Role == 'Column':
        print ('We found a Role: ' + o.Role)
        arl.append(o)
  return(arl)


  
def get_archroleslablist(objectlist):
  arl = []
  for o in objectlist:
    if hasattr(o,'Role'):
      if o.Role == 'Slab':
        print ('We found a Role: ' + o.Role)
        arl.append(o)
  return(arl)



def get_archrolefoundationlist(objectlist):
  arl = []
  for o in objectlist:
    if hasattr(o,'Role'):
      if o.Role == 'Foundation':
        print ('We found a Role: ' + o.Role)
        arl.append(o)
  return(arl)
  
  
  
def get_archifcpropobjectlist(objectlist):
  aifcl = []
  for o in objectlist:
    if hasattr(o,'IfcImportedAttributes'):
      print ('We found a Object which has IfcImportedAttributes! The ifcname is: ' + o.IfcImportedAttributes['IFCName'])
      aifcl.append(o)
  return(aifcl)

  
  
def print_volumen(listofpf):
  """liste die volumina aller shape objekte (pf fuer part feature)
  """
  vol = 0
  for i in range(len(listofpf)):
    print(listofpf[i].Name, '   ', listofpf[i].Shape.Volume)
    vol += listofpf[i].Shape.Volume
  print('')
  print('Volumen is:', vol)


def get_positionvolumen(listofpf):
  """liste die volumina aller shape objekte (pf fuer part feature)
  
  IFCName ist aktull meine Positionsnummer
  """
  for o in listofpf:
    if hasattr(o,'IfcImportedAttributes'):
      pass
    else:
      print ('Object ' + o.Name + ' has no IfcImportedAttributes')
      return

  vol = 0
  lp = [listofpf[0].IfcImportedAttributes['IFCName']]
  print lp
  for o in listofpf:
    print(o.Name + '    ' + o.IfcImportedAttributes['IFCName'] + '    ' + str(o.Shape.Volume))
    vol += o.Shape.Volume
    
    if o.IfcImportedAttributes['IFCName'] in lp:
      pass
    else:
      lp.append(o.IfcImportedAttributes['IFCName'])
  print sorted(lp)
  lpm = [] # list of positionen and mengen
  for p in sorted(lp):
    lpm.append([p,0])
  print lpm
  for o in listofpf:
    for pm in lpm:
      if o.IfcImportedAttributes['IFCName'] == pm[0]:
        #print ('Position ' + o.IfcImportedAttributes['IFCName'] + ' == ' + pm[0])
        pm[1] += o.Shape.Volume
  vol2 = 0
  for pm in lpm:
    print (pm[0] + '  ' + str(pm[1]))
    vol2 += pm[1]
  print('')
  print('Volumensumme ist          :', vol)
  print('Summe Positionsvolumen ist:', vol2)
  return lpm

  
  
def write_listofpositionvolumen(lpm):
  outfile = '/home/hugo/Documents/projekte--ifc/freecad/python--freecad--shape--scripting/holderbank-freecad-pos-mengen.txt'
  wf = open(outfile, 'w')
  for pm in lpm:
    tmps = (pm[0].split('.')[0] + pm[0].split('.')[1])
    wf.write (tmps + ' ' + str(pm[1]) + '\n')
    
    
    
def get_listofinvalidshapes(listof_f):
  """berechne die volumina der shape objekte (pf fuer part feature)
  
  f fuer Feature, kann Part::Feature oder Feature::Python sein
  nicht direkt eine shape, da .Shape aufgerufen wird
  falls es error gibt ist shape invalid oder keine gueltiger solid
  """
  lois = []
  for f in listof_f:
    #print f.Name
    try:
      vol = f.Shape.Volume*1E-9
      #print f.Shape.Volume*1E-9
    except:
      print ("  error calculating volume of " + f.Name)
      lois.append(f)
  return (lois)



def get_listofvolumequantity(objectlist):
  """Liste der Features, die ihr extern berechnetes Volumen kennen
  
  aktuell wird nur Allplanexport unterstuetzt.
  matti hat auch quantities!!!
  """
  lovq = []
  for o in get_archshapelist(objectlist):
    if hasattr(o,"IfcPropertySet"):
      if 'NetVolume' in o.IfcPropertySet:
        lovq.append(o)
  return (lovq)



def get_listofwrongvolumes(listofvolumequantity):
  """Liste der Volumen, die in FreeCAD von dem importierten Volume abweichen
  
  aktuell wird nur Allplanexport unterstuetzt.
  matti hat auch quantities!!!
  """
  lowv = []
  for f in listofvolumequantity:
        q = f.Shape.Volume*1E-9 / float(f.IfcPropertySet['NetVolume'])
        if q < 0.990 or q > 1.010: 
          print (f.Name, '   ' , q)
          lowv.append(f)
  return (lowv)



def color_markobjectsred(listofredo, listofallo):
  """bestimmte Objekte rot, und alle anderen transparent.
  
  Faerbt die uebergebenen Objekte rot ein. Alle anderen transparent
  Mhh muss ich die anderen mit uebergeben, ja 
  """
  for o in listofallo:
    if o in listofredo:
      o.ViewObject.ShapeColor = (1.0,0.0,0.0)
    else:
      o.ViewObject.Transparency = 75
  
  
def importtest_volumequantity():
  objs = FreeCAD.ActiveDocument.Objects
  listofvolumequantity = get_listofvolumequantity(objs)
  listofinvalidshapes = get_listofinvalidshapes(listofvolumequantity)

  lov = []
  for f in listofvolumequantity:
    if f not in listofinvalidshapes:
      lov.append(f)

  listofwrongvolumes = get_listofwrongvolumes(lov)

  # invalid shapes bleiben a
  # also auch die oberflaechenshapes die keine solids sind
  color_markobjectsred(listofwrongvolumes, listofvolumequantity)

  
################################################################################################  
################################################################################################  
################################################################################################  
if __name__ == '__main__':
  

  # If this file is loaded as module in FreeCAD the following code will not be executed
  # copy the code to the pythonkonsole in FreeCAD
  
  import sys
  sys.path.append('/home/hugo/Documents/projekte--ifc/freecad/python--freecad--ArchToolBox/')
  import ArchToolBox as ATB

  objs = FreeCAD.ActiveDocument.Objects
  
  #reload(ATB)
  
  # open or import the model in FreeCAD
  # copy step by step or use the mode without spaces on line start (ctrl+shift+b) to copy more lines

  
  # lets see what objects the model has, 
  objs = FreeCAD.ActiveDocument.Objects
  print(objs)
  ATB.print_objectlist(objs)        
  ATB.print_archproxylist(objs)    
  ATB.print_archrolelist(objs)      
  ATB.archrolecount(objs)
 
 
  # div
  archifcpropobjectlist = ATB.get_archifcpropobjectlist(objs)

  # volume of all archelemtents
  ATB.print_volumen(archifcpropobjectlist)
  
  listofpositionvolumen = ATB.get_positionvolumen(archifcpropobjectlist)
  print listofpositionvolumen
  ATB.write_listofpositionvolumen(listofpositionvolumen)
 
  
  # lists of arch objects
  structurelist = ATB.get_structurelist(objs)
  ATB.print_objectlist(structurelist)
  
  walllist = ATB.get_walllist(objs)
  ATB.print_objectlist(walllist)
   
  archlist = ATB.get_archlist(objs) 
  ATB.print_objectlist(archlist) 
  # auch mit groupobjekten wie floors!!!

  archshapelist = ATB.get_archshapelist(objs) 
  ATB.print_objectlist(archshapelist) 
  # ohne groupeobjekten!!!
  
  archrolecolumnlist = ATB.get_archrolecolumnlist(objs)
  ATB.print_objectlist(archrolecolumnlist)
  
  archroleslablist = ATB.get_archroleslablist(objs)
  ATB.print_objectlist(archroleslablist)
  
  archrolefoundationlist = ATB.get_archrolefoundationlist(objs)
  ATB.print_objectlist(archrolefoundationlist)
 
  
  
  
  
  
  
  
  
  App.ActiveDocument.IfcBeam.Proxy.Type
  App.ActiveDocument.IfcBeam.Role
  App.ActiveDocument.IfcBeam.Role = Column
   
   