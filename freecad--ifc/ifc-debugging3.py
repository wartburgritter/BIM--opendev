import ifcopenshell
from ifcopenshell import geom
settings = ifcopenshell.geom.settings()
settings.set(settings.USE_BREP_DATA,True)
settings.set(settings.SEW_SHELLS,True)
settings.set(settings.USE_WORLD_COORDS,True)
settings.set(settings.INCLUDE_CURVES,True)     # for stuct

def get_shape(entity):
    import Part
    cr = ifcopenshell.geom.create_shape(settings, entity)
    brep = cr.geometry.brep_data
    shape = Part.Shape()
    shape.importBrepFromString(brep)
    Part.show(shape)



# open file
file_path = '/home/hugo/Documents/projekte--BIM--opendev/freecad--ifc/files_fuer_ifc-debugging3/'
#file_path = 'C:\\Users\\bhb\\Desktop\\'

f1 = ifcopenshell.open(file_path + 'export-struct.ifc')
f2 = ifcopenshell.open(file_path + 'stahlbaudetail-mit-schalenelementen-nureinblech_ohne_fp.ifc')
# check if the file was load, IFCPERSON  should be in any ifc
f1.by_type('ifcperson')
f2.by_type('ifcperson')

'''
products = f.by_type('ifcproduct')
for p in products:
    print p
'''


# AxisVM12 --> stahlbaudetail-mit-schalenelementen-nureinblech_ohne_fp.ifc
f2.by_id(1247)
f2.by_id(1247).Representation
f2.by_id(1246).Representations
f2.by_id(1245).Items
f2.by_id(1241).Bounds
f2.by_id(111).Bound
f2.by_id(110).Polygon
f2.by_id(106).Coordinates



# Allplan2016.1 --> export-struct.ifc
f1.by_id(607)
f1.by_id(607).Representation
f1.by_id(604).Representations
f1.by_id(602).Items
f1.by_id(600).Bounds
f1.by_id(587).Bound
f1.by_id(581).EdgeList
f1.by_id(583).EdgeElement
f1.by_id(356).EdgeStart
f1.by_id(356).EdgeEnd
f1.by_id(356).EdgeGeometry
f1.by_id(151)
f1.by_id(170)
f1.by_id(151)
f1.by_id(360)
f1.by_id(358)


'''
axis
#1247=IfcStructuralSurfaceMember('2Z5SUuNWX5mfyX4fUwgD8p',#6,$,$,$,#1244,#1246,.SHELL.,0.00650000)
#1246=IfcProductDefinitionShape($,$,(#1245))
(#1245=IfcTopologyRepresentation(#53,$,'Undefined',(#1241)))
(#1241=IfcFaceSurface((#111,#117,#123,#129,#135,#141,#147,#153,#159,#165,#171,#177,#183,#189,#195,#201,#207,#213,#219,#225,#231,#237,#243,#249,#255,#261,#267,#273,#279,#285,#291,#297,#303,#309,#315,#321,#327,#333,#339,#345,#351,#357,#363,#369,#375,#381,#387,#393,#399,#405,#411,#417,#423,#429,#435,#441,#447,#453,#459,#465,#471,#477,#483,#489,#495,#501,#507,#513,#519,#525,#531,#537,#543,#549,#555,#561,#567,#573,#579,#585,#591,#597,#603,#609,#615,#621,#627,#633,#639,#645,#651,#657,#663,#669,#675,#681,#687,#693,#699,#705,#711,#717,#723,#729,#735,#741,#747,#753,#759,#765,#771,#777,#783,#789,#795,#801,#807,#813,#819,#825),#1240,.T.),)
(#111=IfcFaceOuterBound(#110,.T.), #117=IfcFaceOuterBound(#116,.T.), #123=IfcFaceOuterBound(#122,.T.), #129=IfcFaceOuterBound(#128,.T.), #135=IfcFaceOuterBound(#134,.T.), #141=IfcFaceOuterBound(#140,.T.), #147=IfcFaceOuterBound(#146,.T.), #153=IfcFaceOuterBound(#152,.T.), #159=IfcFaceOuterBound(#158,.T.), #165=IfcFaceOuterBound(#164,.T.), #171=IfcFaceOuterBound(#170,.T.), #177=IfcFaceOuterBound(#176,.T.), #183=IfcFaceOuterBound(#182,.T.), #189=IfcFaceOuterBound(#188,.T.), #195=IfcFaceOuterBound(#194,.T.), #201=IfcFaceOuterBound(#200,.T.), #207=IfcFaceOuterBound(#206,.T.), #213=IfcFaceOuterBound(#212,.T.), #219=IfcFaceOuterBound(#218,.T.), #225=IfcFaceOuterBound(#224,.T.), #231=IfcFaceOuterBound(#230,.T.), #237=IfcFaceOuterBound(#236,.T.), #243=IfcFaceOuterBound(#242,.T.), #249=IfcFaceOuterBound(#248,.T.), #255=IfcFaceOuterBound(#254,.T.), #261=IfcFaceOuterBound(#260,.T.), #267=IfcFaceOuterBound(#266,.T.), #273=IfcFaceOuterBound(#272,.T.), #279=IfcFaceOuterBound(#278,.T.), #285=IfcFaceOuterBound(#284,.T.), #291=IfcFaceOuterBound(#290,.T.), #297=IfcFaceOuterBound(#296,.T.), #303=IfcFaceOuterBound(#302,.T.), #309=IfcFaceOuterBound(#308,.T.), #315=IfcFaceOuterBound(#314,.T.), #321=IfcFaceOuterBound(#320,.T.), #327=IfcFaceOuterBound(#326,.T.), #333=IfcFaceOuterBound(#332,.T.), #339=IfcFaceOuterBound(#338,.T.), #345=IfcFaceOuterBound(#344,.T.), #351=IfcFaceOuterBound(#350,.T.), #357=IfcFaceOuterBound(#356,.T.), #363=IfcFaceOuterBound(#362,.T.), #369=IfcFaceOuterBound(#368,.T.), #375=IfcFaceOuterBound(#374,.T.), #381=IfcFaceOuterBound(#380,.T.), #387=IfcFaceOuterBound(#386,.T.), #393=IfcFaceOuterBound(#392,.T.), #399=IfcFaceOuterBound(#398,.T.), #405=IfcFaceOuterBound(#404,.T.), #411=IfcFaceOuterBound(#410,.T.), #417=IfcFaceOuterBound(#416,.T.), #423=IfcFaceOuterBound(#422,.T.), #429=IfcFaceOuterBound(#428,.T.), #435=IfcFaceOuterBound(#434,.T.), #441=IfcFaceOuterBound(#440,.T.), #447=IfcFaceOuterBound(#446,.T.), #453=IfcFaceOuterBound(#452,.T.), #459=IfcFaceOuterBound(#458,.T.), #465=IfcFaceOuterBound(#464,.T.), #471=IfcFaceOuterBound(#470,.T.), #477=IfcFaceOuterBound(#476,.T.), #483=IfcFaceOuterBound(#482,.T.), #489=IfcFaceOuterBound(#488,.T.), #495=IfcFaceOuterBound(#494,.T.), #501=IfcFaceOuterBound(#500,.T.), #507=IfcFaceOuterBound(#506,.T.), #513=IfcFaceOuterBound(#512,.T.), #519=IfcFaceOuterBound(#518,.T.), #525=IfcFaceOuterBound(#524,.T.), #531=IfcFaceOuterBound(#530,.T.), #537=IfcFaceOuterBound(#536,.T.), #543=IfcFaceOuterBound(#542,.T.), #549=IfcFaceOuterBound(#548,.T.), #555=IfcFaceOuterBound(#554,.T.), #561=IfcFaceOuterBound(#560,.T.), #567=IfcFaceOuterBound(#566,.T.), #573=IfcFaceOuterBound(#572,.T.), #579=IfcFaceOuterBound(#578,.T.), #585=IfcFaceOuterBound(#584,.T.), #591=IfcFaceOuterBound(#590,.T.), #597=IfcFaceOuterBound(#596,.T.), #603=IfcFaceOuterBound(#602,.T.), #609=IfcFaceOuterBound(#608,.T.), #615=IfcFaceOuterBound(#614,.T.), #621=IfcFaceOuterBound(#620,.T.), #627=IfcFaceOuterBound(#626,.T.), #633=IfcFaceOuterBound(#632,.T.), #639=IfcFaceOuterBound(#638,.T.), #645=IfcFaceOuterBound(#644,.T.), #651=IfcFaceOuterBound(#650,.T.), #657=IfcFaceOuterBound(#656,.T.), #663=IfcFaceOuterBound(#662,.T.), #669=IfcFaceOuterBound(#668,.T.), #675=IfcFaceOuterBound(#674,.T.), #681=IfcFaceOuterBound(#680,.T.), #687=IfcFaceOuterBound(#686,.T.), #693=IfcFaceOuterBound(#692,.T.), #699=IfcFaceOuterBound(#698,.T.), #705=IfcFaceOuterBound(#704,.T.), #711=IfcFaceOuterBound(#710,.T.), #717=IfcFaceOuterBound(#716,.T.), #723=IfcFaceOuterBound(#722,.T.), #729=IfcFaceOuterBound(#728,.T.), #735=IfcFaceOuterBound(#734,.T.), #741=IfcFaceOuterBound(#740,.T.), #747=IfcFaceOuterBound(#746,.T.), #753=IfcFaceOuterBound(#752,.T.), #759=IfcFaceOuterBound(#758,.T.), #765=IfcFaceOuterBound(#764,.T.), #771=IfcFaceOuterBound(#770,.T.), #777=IfcFaceOuterBound(#776,.T.), #783=IfcFaceOuterBound(#782,.T.), #789=IfcFaceOuterBound(#788,.T.), #795=IfcFaceOuterBound(#794,.T.), #801=IfcFaceOuterBound(#800,.T.), #807=IfcFaceOuterBound(#806,.T.), #813=IfcFaceOuterBound(#812,.T.), #819=IfcFaceOuterBound(#818,.T.), #825=IfcFaceOuterBound(#824,.T.))
#110=IfcPolyLoop((#106,#107,#108,#109))
(0.5, 0.5, 3.0)

allplan
#607=IfcStructuralSurfaceMember('2M8_Etipn4MwtLx$9uhpUH',#4,'Surface 1',$,$,#595,#604,.SHELL.,250.0000000000001)
#604=IfcProductDefinitionShape($,$,(#602))
(#602=IfcTopologyRepresentation(#11,$,'Face',(#600)),)
(#600=IfcFaceSurface((#587),#596,.T.),)
(#587=IfcFaceOuterBound(#581,.T.),)
#581=IfcEdgeLoop((#583,#584,#585,#586))
(#583=IfcOrientedEdge(*,*,#356,.T.), #584=IfcOrientedEdge(*,*,#377,.T.), #585=IfcOrientedEdge(*,*,#398,.T.), #586=IfcOrientedEdge(*,*,#419,.T.))
#356=IfcEdgeCurve(#162,#181,#357,.T.)
#162=IfcVertexPoint(#151)
#181=IfcVertexPoint(#170)
#357=IfcLine(#151,#360)
#151=IfcCartesianPoint((5.,5.000000000000002,3.125))
#170=IfcCartesianPoint((-5.,5.000000000000002,3.125))
#151=IfcCartesianPoint((5.,5.000000000000002,3.125))
#360=IfcVector(#358,10.)
#358=IfcDirection((-1.,0.,0.))

'''


get_shape(f2.by_id(1247))
get_shape(f1.by_id(607))




##############################################
# get_attribute_names

get_attribute_names(f1.by_id(356))

# einmal unteres einfuegen um obiges zu verwenden !
def get_attribute_names(entity):
    for a in range(9):
        try:
            print a, ' --> ', entity.attribute_name(a)
        except:
            print a, ' is not defined'


