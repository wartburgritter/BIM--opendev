<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" xml:lang="en-gb" lang="en-gb"><head>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-language" content="en-gb">
<meta http-equiv="imagetoolbar" content="no">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="keywords" content="">
<meta name="description" content="">

<title>FreeCAD forum • View topic - Export all drawings in a folder together per Scripting</title>

<link rel="alternate" type="application/atom+xml" title="Feed - FreeCAD forum" href="http://forum.freecadweb.org/feed.php"><link rel="alternate" type="application/atom+xml" title="Feed - All forums" href="http://forum.freecadweb.org/feed.php?mode=forums"><link rel="alternate" type="application/atom+xml" title="Feed - Forum - Help on using FreeCAD" href="http://forum.freecadweb.org/feed.php?f=22"><link rel="alternate" type="application/atom+xml" title="Feed - Topic - Export all drawings in a folder together per Scripting" href="http://forum.freecadweb.org/feed.php?f=22&amp;t=4419">

<!--
	phpBB style name: prosilver
	Based on style:   prosilver (this is the default phpBB3 style)
	Original author:  Tom Beddard ( http://www.subBlue.com/ )
	Modified by:
-->

<script type="text/javascript">
// <![CDATA[
	var jump_page = 'Enter the page number you wish to go to:';
	var on_page = '1';
	var per_page = '';
	var base_url = '';
	var style_cookie = 'phpBBstyle';
	var style_cookie_settings = '; path=/; domain=forum.freecadweb.org';
	var onload_functions = new Array();
	var onunload_functions = new Array();

	

	/**
	* Find a member
	*/
	function find_username(url)
	{
		popup(url, 760, 570, '_usersearch');
		return false;
	}

	/**
	* New function for handling multiple calls to window.onload and window.unload by pentapenguin
	*/
	window.onload = function()
	{
		for (var i = 0; i < onload_functions.length; i++)
		{
			eval(onload_functions[i]);
		}
	};

	window.onunload = function()
	{
		for (var i = 0; i < onunload_functions.length; i++)
		{
			eval(onunload_functions[i]);
		}
	};

// ]]>
</script>
<script type="text/javascript" src="viewtopic-Dateien/styleswitcher.js"></script>
<script type="text/javascript" src="viewtopic-Dateien/forum_fn.js"></script>

<link href="viewtopic-Dateien/print.css" rel="stylesheet" type="text/css" media="print" title="printonly">
<link href="viewtopic-Dateien/style.css" rel="stylesheet" type="text/css" media="screen, projection">

<link href="viewtopic-Dateien/phpbb.css" rel="stylesheet" type="text/css">

<link href="viewtopic-Dateien/normal.css" rel="stylesheet" type="text/css" title="A">
<link href="viewtopic-Dateien/medium.css" rel="alternate stylesheet" type="text/css" title="A+">
<link href="viewtopic-Dateien/large.css" rel="alternate stylesheet" type="text/css" title="A++">



<link rel="shortcut icon" href="http://www.freecadweb.org/images/favicon.ico">

</head>

<body id="phpbb" class="section-viewtopic ltr">

<div id="wrap">
	<a id="top" name="top" accesskey="t"></a>
	<div id="page-header">
		<div class="headerbar">
			<div class="inner"><span class="corners-top"><span></span></span>

			<div id="site-description">
				<a href="http://forum.freecadweb.org/index.php" title="Board index" id="logo"><img src="viewtopic-Dateien/site_logo.gif" alt="" title="" height="52" width="139"></a>
				<h1>FreeCAD forum</h1>
				<p>The help and development forum of FreeCAD</p>
				<p class="skiplink"><a href="#start_here">Skip to content</a></p>
			</div>

		
			<div id="search-box">
				<form action="./search.php" method="get" id="search">
				<fieldset>
					<input name="keywords" id="keywords" maxlength="128" title="Search for keywords" class="inputbox search" value="Search…" onclick="if(this.value=='Search…')this.value='';" onblur="if(this.value=='')this.value='Search…';" type="text">
					<input class="button2" value="Search" type="submit"><br>
					<a href="http://forum.freecadweb.org/search.php" title="View the advanced search options">Advanced search</a> 
				</fieldset>
				</form>
			</div>
		

			<span class="corners-bottom"><span></span></span></div>
		</div>

		<div class="navbar">
			<div class="inner"><span class="corners-top"><span></span></span>

			<ul class="linklist navlinks">
				<li class="icon-home"><a href="http://forum.freecadweb.org/index.php" accesskey="h">Board index</a>  <strong>‹</strong> <a href="http://forum.freecadweb.org/viewforum.php?f=7">Users</a> <strong>‹</strong> <a href="http://forum.freecadweb.org/viewforum.php?f=3">Help on using FreeCAD</a></li>

				<li class="rightside"><a href="#" onclick="fontsizeup(); return false;" onkeypress="return fontsizeup(event);" class="fontsize" title="Change font size">Change font size</a></li>

				<li class="rightside"><a href="http://forum.freecadweb.org/memberlist.php?mode=email&amp;t=4419" title="E-mail friend" class="sendemail">E-mail friend</a></li><li class="rightside"><a href="http://forum.freecadweb.org/viewtopic.php?f=3&amp;t=4419&amp;view=print" title="Print view" accesskey="p" class="print">Print view</a></li>
			</ul>

			
			<ul class="linklist leftside">
				<li class="icon-ucp">
					<a href="http://forum.freecadweb.org/ucp.php" title="User Control Panel" accesskey="e">User Control Panel</a>
						 (<a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;folder=inbox"><strong>0</strong> new messages</a>) •
					<a href="http://forum.freecadweb.org/search.php?search_id=egosearch">View your posts</a>
					
				</li>
			</ul>
			

			<ul class="linklist rightside">
				<li class="icon-faq"><a href="http://forum.freecadweb.org/faq.php" title="Frequently Asked Questions">FAQ</a></li>
				<li class="icon-members"><a href="http://forum.freecadweb.org/memberlist.php" title="View complete list of members">Members</a></li>
					<li class="icon-logout"><a href="http://forum.freecadweb.org/ucp.php?mode=logout&amp;sid=c64fad04e5b246115bf12896794f379b" title="Logout [ bernd ]" accesskey="x">Logout [ bernd ]</a></li>
				
			</ul>

			<span class="corners-bottom"><span></span></span></div>
		</div>

	</div>

	<a name="start_here"></a>
	<div id="page-body">
		
<h2><a href="http://forum.freecadweb.org/viewtopic.php?f=3&amp;t=4419">Export all drawings in a folder together per Scripting</a></h2>
<!-- NOTE: remove the style="display: none" when you want to have the forum description on the topic body --><div style="display: none !important;">Post here for help on using FreeCAD's graphical user interface (GUI).<br></div>

<div class="topic-actions">

	<div class="buttons">
	
		<div class="reply-icon"><a href="http://forum.freecadweb.org/posting.php?mode=reply&amp;f=3&amp;t=4419" title="Post a reply"><span></span>Post a reply</a></div>
	
	</div>

	
		<div class="search-box">
			<form method="get" id="topic-search" action="./search.php">
			<fieldset>
				<input class="inputbox search tiny" name="keywords" id="search_keywords" size="20" value="Search this topic…" onclick="if(this.value=='Search this topic…')this.value='';" onblur="if(this.value=='')this.value='Search this topic…';" type="text">
				<input class="button2" value="Search" type="submit">
				<input name="t" value="4419" type="hidden">
<input name="sf" value="msgonly" type="hidden">

			</fieldset>
			</form>
		</div>
	
		<div class="pagination">
			6 posts
			 • Page <strong>1</strong> of <strong>1</strong>
		</div>
	

</div>
<div class="clear"></div>


	<div id="p34733" class="post bg2">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="report-icon"><a href="http://forum.freecadweb.org/report.php?f=3&amp;p=34733" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://forum.freecadweb.org/posting.php?mode=quote&amp;f=3&amp;p=34733" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3 class="first"><a href="#p34733">Export all drawings in a folder together per Scripting</a></h3>
			<p class="author"><a href="http://forum.freecadweb.org/viewtopic.php?p=34733#p34733"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2019">armandokengne</a></strong> » Mon Aug 19, 2013 10:26 am </p>

			

			<div class="content">Hello !!! I'm not a FreeCad-Expert, so I need your help. So I have the following questions:<br>-Is
 it possible to export via Scripting (Python command) with freecad all 
drawings (with the same format (STP)) from a odner together to a new 
format (WRL)  and to keep the same original filename (drawingname)?  how
 can i do it? (which commands)<br><br>Thanks!!<br> Mark</div>

			

		</div>

		
			<dl class="postprofile" id="profile34733">
			<dt>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2019">armandokengne</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 13</dd><dd><strong>Joined:</strong> Fri Aug 16, 2013 7:36 pm</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=34733" title="Private message"><span>Private message</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p34740" class="post bg1">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="report-icon"><a href="http://forum.freecadweb.org/report.php?f=3&amp;p=34740" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://forum.freecadweb.org/posting.php?mode=quote&amp;f=3&amp;p=34740" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3><a href="#p34740">Re: Export all drawings in a folder together per Scripting</a></h3>
			<p class="author"><a href="http://forum.freecadweb.org/viewtopic.php?p=34740#p34740"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a></strong> » Mon Aug 19, 2013 12:22 pm </p>

			

			<div class="content">Not tested but something like this should work:<br><dl class="codebox"><dt>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></dt><dd><code>import os, ImportGui<br><br>path="..." # set here your path!!!<br>for i in os.listdir(path):<br>&nbsp; &nbsp; if i.lower().endswith("stp"):<br>&nbsp; &nbsp; &nbsp; &nbsp; fn = os.path.join(path,i)<br>&nbsp; &nbsp; &nbsp; &nbsp; ImportGui.open(fn)<br>&nbsp; &nbsp; &nbsp; &nbsp; Gui.SendMsgToActiveView("ViewFit")<br>&nbsp; &nbsp; &nbsp; &nbsp; wrl = os.path.join(path,fn) + ".wrl"<br>&nbsp; &nbsp; &nbsp; &nbsp; Gui.export(App.ActiveDocument.findObjects("Part::Feature"),wrl)<br>&nbsp; &nbsp; &nbsp; &nbsp; App.closeDocument(App.ActiveDocument.Name)<br></code></dd></dl></div>

			

		</div>

		
			<dl class="postprofile" id="profile34740">
			<dt>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a>
			</dt>

			<dd>Site Admin</dd>

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 5700</dd><dd><strong>Joined:</strong> Thu Feb 19, 2009 10:32 am</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=34740" title="Private message"><span>Private message</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p34744" class="post bg2 online">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="report-icon"><a href="http://forum.freecadweb.org/report.php?f=3&amp;p=34744" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://forum.freecadweb.org/posting.php?mode=quote&amp;f=3&amp;p=34744" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3><a href="#p34744">Re: Export all drawings in a folder together per Scripting</a></h3>
			<p class="author"><a href="http://forum.freecadweb.org/viewtopic.php?p=34744#p34744"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=202" style="color: #0066FF;" class="username-coloured">normandc</a></strong> » Mon Aug 19, 2013 12:32 pm </p>

			

			<div class="content">Hi,<br><br><blockquote><div><cite>armandokengne wrote:</cite>-Is
 it possible to export via Scripting (Python command) with freecad all 
drawings (with the same format (STP)) from a odner together to a new 
format (WRL)</div></blockquote><br>A drawing is 2D. STP is a 3D model format, which WRL is as well. Drawings and 3D models are totally different things.</div>

			<div id="sig34744" class="signature"><a href="http://forum.freecadweb.org/viewtopic.php?f=8&amp;t=1222" class="postlink">Show your FreeCAD projects here!</a> | <a href="https://plus.google.com/u/0/communities/103183769032333474646?cfem=1" class="postlink">Google+ FreeCAD Community</a></div>

		</div>

		
			<dl class="postprofile" id="profile34744">
			<dt>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=202"><img src="viewtopic-Dateien/file.jpeg" alt="User avatar" height="60" width="60"></a><br>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=202" style="color: #0066FF;" class="username-coloured">normandc</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 4520</dd><dd><strong>Joined:</strong> Sat Feb 06, 2010 9:52 pm</dd><dd><strong>Location:</strong> Québec, Canada</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=34744" title="Private message"><span>Private message</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p34753" class="post bg1">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="report-icon"><a href="http://forum.freecadweb.org/report.php?f=3&amp;p=34753" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://forum.freecadweb.org/posting.php?mode=quote&amp;f=3&amp;p=34753" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3><a href="#p34753">Re: Export all drawings in a folder together per Scripting</a></h3>
			<p class="author"><a href="http://forum.freecadweb.org/viewtopic.php?p=34753#p34753"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2019">armandokengne</a></strong> » Mon Aug 19, 2013 1:29 pm </p>

			

			<div class="content">Hi wmayer!! thanks!!<br>the script works, but does not save the wrl-file!!!  <img src="viewtopic-Dateien/icon_neutral.gif" alt=":|" title="Neutral"></div>

			

		</div>

		
			<dl class="postprofile" id="profile34753">
			<dt>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2019">armandokengne</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 13</dd><dd><strong>Joined:</strong> Fri Aug 16, 2013 7:36 pm</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=34753" title="Private message"><span>Private message</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p34755" class="post bg2">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="report-icon"><a href="http://forum.freecadweb.org/report.php?f=3&amp;p=34755" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://forum.freecadweb.org/posting.php?mode=quote&amp;f=3&amp;p=34755" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3><a href="#p34755">Re: Export all drawings in a folder together per Scripting</a></h3>
			<p class="author"><a href="http://forum.freecadweb.org/viewtopic.php?p=34755#p34755"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a></strong> » Mon Aug 19, 2013 1:49 pm </p>

			

			<div class="content"><dl class="codebox"><dt>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></dt><dd><code>import os, ImportGui<br><br>path="..." # set here your path!!!<br>for i in os.listdir(path):<br>&nbsp; &nbsp; if i.lower().endswith("stp"):<br>&nbsp; &nbsp; &nbsp; &nbsp; fn = os.path.join(path,i)<br>&nbsp; &nbsp; &nbsp; &nbsp; ImportGui.open(fn)<br>&nbsp; &nbsp; &nbsp; &nbsp; Gui.SendMsgToActiveView("ViewFit")<br>&nbsp; &nbsp; &nbsp; &nbsp; wrl = os.path.splitext(os.path.join(path,fn))[0] + ".wrl"<br>&nbsp; &nbsp; &nbsp; &nbsp; print wrl<br>&nbsp; &nbsp; &nbsp; &nbsp; Gui.export(App.ActiveDocument.findObjects("Part::Feature"),wrl)<br>&nbsp; &nbsp; &nbsp; &nbsp; App.closeDocument(App.ActiveDocument.Name)<br></code></dd></dl></div>

			

		</div>

		
			<dl class="postprofile" id="profile34755">
			<dt>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a>
			</dt>

			<dd>Site Admin</dd>

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 5700</dd><dd><strong>Joined:</strong> Thu Feb 19, 2009 10:32 am</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=34755" title="Private message"><span>Private message</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p34759" class="post bg1">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="report-icon"><a href="http://forum.freecadweb.org/report.php?f=3&amp;p=34759" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://forum.freecadweb.org/posting.php?mode=quote&amp;f=3&amp;p=34759" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3><a href="#p34759">Re: Export all drawings in a folder together per Scripting</a></h3>
			<p class="author"><a href="http://forum.freecadweb.org/viewtopic.php?p=34759#p34759"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2019">armandokengne</a></strong> » Mon Aug 19, 2013 2:18 pm </p>

			

			<div class="content">hi wmayer!!!! Great!!!!!! <img src="viewtopic-Dateien/icon_e_biggrin.gif" alt=":D" title="Very Happy"> <br>work well!!!<br>thanks!!!!</div>

			

		</div>

		
			<dl class="postprofile" id="profile34759">
			<dt>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2019">armandokengne</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 13</dd><dd><strong>Joined:</strong> Fri Aug 16, 2013 7:36 pm</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=34759" title="Private message"><span>Private message</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<form id="viewtopic" method="post" action="./viewtopic.php?f=3&amp;t=4419">

	<fieldset class="display-options" style="margin-top: 0; ">
		
		<label>Display posts from previous: <select name="st" id="st"><option value="0" selected="selected">All posts</option><option value="1">1 day</option><option value="7">7 days</option><option value="14">2 weeks</option><option value="30">1 month</option><option value="90">3 months</option><option value="180">6 months</option><option value="365">1 year</option></select></label>
		<label>Sort by <select name="sk" id="sk"><option value="a">Author</option><option value="t" selected="selected">Post time</option><option value="s">Subject</option></select></label> <label><select name="sd" id="sd"><option value="a" selected="selected">Ascending</option><option value="d">Descending</option></select> <input name="sort" value="Go" class="button2" type="submit"></label>
		
	</fieldset>

	</form>
	<hr>


<div class="topic-actions">
	<div class="buttons">
	
		<div class="reply-icon"><a href="http://forum.freecadweb.org/posting.php?mode=reply&amp;f=3&amp;t=4419" title="Post a reply"><span></span>Post a reply</a></div>
	
	</div>

	
		<div class="pagination">
			6 posts
			 • Page <strong>1</strong> of <strong>1</strong>
		</div>
	
</div>


	<p></p><p><a href="http://forum.freecadweb.org/viewforum.php?f=3" class="left-box left" accesskey="r">Return to Help on using FreeCAD</a></p>

	<form method="post" id="jumpbox" action="./viewforum.php" onsubmit="if(this.f.value == -1){return false;}">

	
		<fieldset class="jumpbox">
	
			<label for="f" accesskey="j">Jump to:</label>
			<select name="f" id="f" onchange="if(this.options[this.selectedIndex].value != -1){ document.forms['jumpbox'].submit() }">
			
				<option value="-1">Select a forum</option>
			<option value="-1">------------------</option>
				<option value="7">Users</option>
			
				<option value="3" selected="selected">&nbsp; &nbsp;Help on using FreeCAD</option>
			
				<option value="22">&nbsp; &nbsp;Python scripting and macros</option>
			
				<option value="4">&nbsp; &nbsp;Install / Compile</option>
			
				<option value="8">&nbsp; &nbsp;Open discussion</option>
			
				<option value="9">&nbsp; &nbsp;Feature Announcements</option>
			
				<option value="6">Development</option>
			
				<option value="10">&nbsp; &nbsp;Developers corner</option>
			
				<option value="17">&nbsp; &nbsp;Pull Requests</option>
			
				<option value="19">&nbsp; &nbsp;Part Design</option>
			
				<option value="20">&nbsp; &nbsp;Assembly</option>
			
				<option value="18">&nbsp; &nbsp;FEM</option>
			
				<option value="15">&nbsp; &nbsp;CAM</option>
			
				<option value="23">&nbsp; &nbsp;Arch</option>
			
				<option value="21">&nbsp; &nbsp;Wiki</option>
			
				<option value="11">Forums in other languages</option>
			
				<option value="12">&nbsp; &nbsp;Forum français</option>
			
				<option value="13">&nbsp; &nbsp;Forum in Deutsch</option>
			
				<option value="14">&nbsp; &nbsp;Foro en Español</option>
			
				<option value="16">&nbsp; &nbsp;日本語フォーラム</option>
			
			</select>
			<input value="Go" class="button2" type="submit">
		</fieldset>
	</form>


	<h3><a href="http://forum.freecadweb.org/viewonline.php">Who is online</a></h3>
	<p>Users browsing this forum: <a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=202" style="color: #0066FF;" class="username-coloured">normandc</a> and 0 guests</p>
</div>

<div id="page-footer">

	<div class="navbar">
		<div class="inner"><span class="corners-top"><span></span></span>

		<ul class="linklist">
			<li class="icon-home"><a href="http://forum.freecadweb.org/index.php" accesskey="h">Board index</a></li>
				<li class="icon-subscribe"><a href="http://forum.freecadweb.org/viewtopic.php?uid=2069&amp;f=3&amp;t=4419&amp;watch=topic&amp;start=0&amp;hash=6d6a574e" title="Subscribe topic">Subscribe topic</a></li><li class="icon-bookmark"><a href="http://forum.freecadweb.org/viewtopic.php?f=3&amp;t=4419&amp;bookmark=1&amp;hash=6d6a574e" title="Bookmark topic">Bookmark topic</a></li>
			<li class="rightside"><a href="http://forum.freecadweb.org/memberlist.php?mode=leaders">The team</a> • <a href="http://forum.freecadweb.org/ucp.php?mode=delete_cookies">Delete all board cookies</a> • All times are UTC </li>
		</ul>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<div class="copyright">Powered by <a href="https://www.phpbb.com/">phpBB</a>® Forum Software © phpBB Group
		
	</div>
</div>

</div>

<div>
	<a id="bottom" name="bottom" accesskey="z"></a>
	<img src="viewtopic-Dateien/cron.gif" alt="cron" height="1" width="1">
</div>


</body></html>