import FreeCAD, Part, Arch

myDoc = App.newDocument("PropertyMapTest")
box = Part.makeBox(250,250,3000)
body = myDoc.addObject("Part::Feature","body")
body.Shape = box
structure = Arch.makeStructure(body,name="Structure")
myDoc.recompute()
Gui.activeDocument().activeView().viewAxometric()
Gui.SendMsgToActiveView("ViewFit")


structure.addProperty("App::PropertyMap","UserDefined","User","An user-defined dictionary")
myprops = {"custom name":"My Fancy Name","custom length":"0.65"}
structure.UserDefined = myprops

print structure.UserDefined["custom name"]


propertymapname = "PropertySetName"
structure.addProperty("App::PropertyMap",propertymapname,propertymapname,"An user-defined dictionary")
structure.PropertySetName = myprops
print structure.PropertySetName["custom name"]



structure.propertymapname = myprops
print structure.propertymapname["custom name"]


myDoc.recompute()


#App.ActiveDocument.Structure.PropertiesList
#App.ActiveDocument.Structure.UserDefined

#################################################################################################

class Component:
    "The default Arch Component object"
    def __init__(self,obj):
        obj.addProperty("App::PropertyLink","Base","Arch","The base object this component is built upon")
        obj.addProperty("App::PropertyLinkList","Additions","Arch","Other shapes that are appended to this object")
        obj.addProperty("App::PropertyLinkList","Subtractions","Arch","Other shapes that are subtracted from this object")
        obj.addProperty("App::PropertyString","Description","Arch","An optional description for this component")
        obj.addProperty("App::PropertyString","Tag","Arch","An optional tag for this component")
        obj.Proxy = self
        self.Type = "Component"
        self.Subvolume = None

    def __init__(self,obj):
        ArchComponent.Component.__init__(self,obj)
        obj.addProperty("App::PropertyLength","Diameter","Arch","The diameter of the bar")
        obj.addProperty("App::PropertyLength","OffsetStart","Arch","The distance between the border of the beam and the fist bar (concrete cover).")
        obj.addProperty("App::PropertyLength","OffsetEnd","Arch","The distance between the border of the beam and the last bar (concrete cover).")
        obj.addProperty("App::PropertyInteger","Amount","Arch","The amount of bars")
        obj.addProperty("App::PropertyLength","Spacing","Arch","The spacing between the bars")
        obj.addProperty("App::PropertyVector","Direction","Arch","The direction to use to spread the bars. Keep (0,0,0) for automatic direction.")
        obj.addProperty("App::PropertyFloat","Rounding","Arch","The fillet to apply to the angle of the base profile. This value is multiplied by the bar diameter.")
        self.Type = "Rebar"
        obj.setEditorMode("Spacing",1)

class _Structure(ArchComponent.Component):
    "The Structure object"
    def __init__(self,obj):
        ArchComponent.Component.__init__(self,obj)
        obj.addProperty("App::PropertyLink","Tool","Arch",translate("Arch","An optional extrusion path for this element"))
        obj.addProperty("App::PropertyLength","Length","Arch",translate("Arch","The length of this element, if not based on a profile"))
        obj.addProperty("App::PropertyLength","Width","Arch",translate("Arch","The width of this element, if not based on a profile"))
        obj.addProperty("App::PropertyLength","Height","Arch",translate("Arch","The height or extrusion depth of this element. Keep 0 for automatic"))
        obj.addProperty("App::PropertyLinkList","Armatures","Arch",translate("Arch","Armatures contained in this element"))
        obj.addProperty("App::PropertyVector","Normal","Arch",translate("Arch","The normal extrusion direction of this object (keep (0,0,0) for automatic normal)"))
        obj.addProperty("App::PropertyEnumeration","Role","Arch",translate("Arch","The role of this structural element"))
        obj.addProperty("App::PropertyVectorList","Nodes","Arch",translate("Arch","The structural nodes of this element"))
        obj.addProperty("App::PropertyString","Profile","Arch","A description of the standard profile this element is based upon")
        self.Type = "Structure"
        obj.Role = Roles


class _Floor:
    "The Floor object"
    def __init__(self,obj):
        obj.addProperty("App::PropertyLength","Height","Arch",translate("Arch","The height of this floor"))
        obj.addProperty("App::PropertyPlacement","Placement","Arch",translate("Arch","The placement of this group"))
        self.Type = "Floor"
        obj.Proxy = self
        self.Object = obj


class PointsFeature:
    def __init__(self, obj):
        obj.addProperty("App::PropertyColorList","Colors","Points","Ponts").Colors=[]
        obj.Proxy = self


class EasiAccessFront:
   def __init__(self, obj):
      obj.addProperty("App::PropertyFloat","CoverWidth","Parameters","Cover Width").CoverWidth = 400.0
      obj.addProperty("App::PropertyFloat","CoverHeight","Parameters","Cover Height").CoverHeight = 400.0
      obj.Proxy = self
 