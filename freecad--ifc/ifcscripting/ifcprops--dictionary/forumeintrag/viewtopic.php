<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" xml:lang="en-gb" lang="en-gb"><head>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-language" content="en-gb">
<meta http-equiv="imagetoolbar" content="no">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="keywords" content="">
<meta name="description" content="">

<title>FreeCAD forum • View topic - ArchStructure Properties</title>

<link rel="alternate" type="application/atom+xml" title="Feed - FreeCAD forum" href="http://forum.freecadweb.org/feed.php"><link rel="alternate" type="application/atom+xml" title="Feed - All forums" href="http://forum.freecadweb.org/feed.php?mode=forums"><link rel="alternate" type="application/atom+xml" title="Feed - Forum - Arch" href="http://forum.freecadweb.org/feed.php?f=23"><link rel="alternate" type="application/atom+xml" title="Feed - Topic - ArchStructure Properties" href="http://forum.freecadweb.org/feed.php?f=23&amp;t=5197">

<!--
	phpBB style name: prosilver
	Based on style:   prosilver (this is the default phpBB3 style)
	Original author:  Tom Beddard ( http://www.subBlue.com/ )
	Modified by:
-->

<script type="text/javascript">
// <![CDATA[
	var jump_page = 'Enter the page number you wish to go to:';
	var on_page = '1';
	var per_page = '';
	var base_url = '';
	var style_cookie = 'phpBBstyle';
	var style_cookie_settings = '; path=/; domain=forum.freecadweb.org';
	var onload_functions = new Array();
	var onunload_functions = new Array();

	

	/**
	* Find a member
	*/
	function find_username(url)
	{
		popup(url, 760, 570, '_usersearch');
		return false;
	}

	/**
	* New function for handling multiple calls to window.onload and window.unload by pentapenguin
	*/
	window.onload = function()
	{
		for (var i = 0; i < onload_functions.length; i++)
		{
			eval(onload_functions[i]);
		}
	};

	window.onunload = function()
	{
		for (var i = 0; i < onunload_functions.length; i++)
		{
			eval(onunload_functions[i]);
		}
	};

// ]]>
</script>
<script type="text/javascript" src="viewtopic-Dateien/styleswitcher.js"></script>
<script type="text/javascript" src="viewtopic-Dateien/forum_fn.js"></script>

<link href="viewtopic-Dateien/print.css" rel="stylesheet" type="text/css" media="print" title="printonly">
<link href="viewtopic-Dateien/style.css" rel="stylesheet" type="text/css" media="screen, projection">

<link href="viewtopic-Dateien/phpbb.css" rel="stylesheet" type="text/css">

<link href="viewtopic-Dateien/normal.css" rel="stylesheet" type="text/css" title="A">
<link href="viewtopic-Dateien/medium.css" rel="alternate stylesheet" type="text/css" title="A+">
<link href="viewtopic-Dateien/large.css" rel="alternate stylesheet" type="text/css" title="A++">



<link rel="shortcut icon" href="http://www.freecadweb.org/images/favicon.ico">

</head>

<body id="phpbb" class="section-viewtopic ltr">

<div id="wrap">
	<a id="top" name="top" accesskey="t"></a>
	<div id="page-header">
		<div class="headerbar">
			<div class="inner"><span class="corners-top"><span></span></span>

			<div id="site-description">
				<a href="http://forum.freecadweb.org/index.php" title="Board index" id="logo"><img src="viewtopic-Dateien/site_logo.gif" alt="" title="" height="52" width="139"></a>
				<h1>FreeCAD forum</h1>
				<p>The help and development forum of FreeCAD</p>
				<p class="skiplink"><a href="#start_here">Skip to content</a></p>
			</div>

		
			<div id="search-box">
				<form action="./search.php" method="get" id="search">
				<fieldset>
					<input name="keywords" id="keywords" maxlength="128" title="Search for keywords" class="inputbox search" value="Search…" onclick="if(this.value=='Search…')this.value='';" onblur="if(this.value=='')this.value='Search…';" type="text">
					<input class="button2" value="Search" type="submit"><br>
					<a href="http://forum.freecadweb.org/search.php" title="View the advanced search options">Advanced search</a> 
				</fieldset>
				</form>
			</div>
		

			<span class="corners-bottom"><span></span></span></div>
		</div>

		<div class="navbar">
			<div class="inner"><span class="corners-top"><span></span></span>

			<ul class="linklist navlinks">
				<li class="icon-home"><a href="http://forum.freecadweb.org/index.php" accesskey="h">Board index</a>  <strong>‹</strong> <a href="http://forum.freecadweb.org/viewforum.php?f=6">Development</a> <strong>‹</strong> <a href="http://forum.freecadweb.org/viewforum.php?f=23">Arch</a></li>

				<li class="rightside"><a href="#" onclick="fontsizeup(); return false;" onkeypress="return fontsizeup(event);" class="fontsize" title="Change font size">Change font size</a></li>

				<li class="rightside"><a href="http://forum.freecadweb.org/memberlist.php?mode=email&amp;t=5197" title="E-mail friend" class="sendemail">E-mail friend</a></li><li class="rightside"><a href="http://forum.freecadweb.org/viewtopic.php?f=23&amp;t=5197&amp;view=print" title="Print view" accesskey="p" class="print">Print view</a></li>
			</ul>

			
			<ul class="linklist leftside">
				<li class="icon-ucp">
					<a href="http://forum.freecadweb.org/ucp.php" title="User Control Panel" accesskey="e">User Control Panel</a>
						 (<a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;folder=inbox"><strong>0</strong> new messages</a>) •
					<a href="http://forum.freecadweb.org/search.php?search_id=egosearch">View your posts</a>
					
				</li>
			</ul>
			

			<ul class="linklist rightside">
				<li class="icon-faq"><a href="http://forum.freecadweb.org/faq.php" title="Frequently Asked Questions">FAQ</a></li>
				<li class="icon-members"><a href="http://forum.freecadweb.org/memberlist.php" title="View complete list of members">Members</a></li>
					<li class="icon-logout"><a href="http://forum.freecadweb.org/ucp.php?mode=logout&amp;sid=8fdd0efa5fb049cb85318d8ba18395f3" title="Logout [ bernd ]" accesskey="x">Logout [ bernd ]</a></li>
				
			</ul>

			<span class="corners-bottom"><span></span></span></div>
		</div>

	</div>

	<a name="start_here"></a>
	<div id="page-body">
		
<h2><a href="http://forum.freecadweb.org/viewtopic.php?f=23&amp;t=5197">ArchStructure Properties</a></h2>
<!-- NOTE: remove the style="display: none" when you want to have the forum description on the topic body --><div style="display: none !important;">A forum dedicated to the Arch module development<br></div>

<div class="topic-actions">

	<div class="buttons">
	
		<div class="reply-icon"><a href="http://forum.freecadweb.org/posting.php?mode=reply&amp;f=23&amp;t=5197" title="Post a reply"><span></span>Post a reply</a></div>
	
	</div>

	
		<div class="search-box">
			<form method="get" id="topic-search" action="./search.php">
			<fieldset>
				<input class="inputbox search tiny" name="keywords" id="search_keywords" size="20" value="Search this topic…" onclick="if(this.value=='Search this topic…')this.value='';" onblur="if(this.value=='')this.value='Search this topic…';" type="text">
				<input class="button2" value="Search" type="submit">
				<input name="t" value="5197" type="hidden">
<input name="sf" value="msgonly" type="hidden">

			</fieldset>
			</form>
		</div>
	
		<div class="pagination">
			<a href="#unread">First unread post</a> • 4 posts
			 • Page <strong>1</strong> of <strong>1</strong>
		</div>
	

</div>
<div class="clear"></div>


	<div id="p41109" class="post bg2 online">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="edit-icon"><a href="http://forum.freecadweb.org/posting.php?mode=edit&amp;f=23&amp;p=41109" title="Edit post"><span>Edit post</span></a></li><li class="report-icon"><a href="http://forum.freecadweb.org/report.php?f=23&amp;p=41109" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://forum.freecadweb.org/posting.php?mode=quote&amp;f=23&amp;p=41109" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3 class="first"><a href="#p41109">ArchStructure Properties</a></h3>
			<p class="author"><a href="http://forum.freecadweb.org/viewtopic.php?p=41109#p41109"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">bernd</a></strong> » Sun Dec 22, 2013 10:49 am </p>

			

			<div class="content">Yorik, I saw your commit regarding Arch site 
properties. This gave me some ideas I had to play with. After some time I
 came up with the following. I was quite happy because I could define my
 own properties AND most important I could access them using python. <br><div class="inline-attachment">
		<dl class="file">
			<dt class="attach-image"><img src="viewtopic-Dateien/file_002.jpeg" alt="screen.jpg" onclick="viewableArea(this);"></dt>
			
			<dd>screen.jpg (80.56 KiB) Viewed 24 times</dd>
		</dl>
		</div><br><br>But I thought "hey bernd, stop it, it is not flexible to
 put the properties in the class definition of the ArchStructureElement,
 I need some App::PropertyDictionay kind of ... to be flexible". This 
reminds me of a discussion we hat 2 month ago <!-- l --><a class="postlink-local" href="http://forum.freecadweb.org/viewtopic.php?f=8&amp;t=4658&amp;p=36523#p36647">viewtopic.php?f=8&amp;t=4658&amp;p=36523#p36647</a><!-- l -->
 The material stuff ... I have been reading the wiki but the thing is I 
don't know where to start. I could neither find something in the GUI of 
FreeCAD nor at the wiki by using python to define the materials. I'm 
gone missing something. Where do I define some material for my 
ArchElements? Has the material dictionary a App::Property...  so I could
 use it for other properties?<br><br>I have been using FreeCAD just 
about for 3 month now and it seams I get close to the point of doing the
 stuff I have been thinking about 3 month ago  <img src="viewtopic-Dateien/icon_e_biggrin.gif" alt=":D" title="Very Happy"></div>

			

		</div>

		
			<dl class="postprofile" id="profile41109">
			<dt>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">bernd</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 170</dd><dd><strong>Joined:</strong> Sun Sep 08, 2013 8:07 pm</dd><dd><strong>Location:</strong> Zürich, Switzerland</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=41109" title="Private message"><span>Private message</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p41116" class="post bg1">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="report-icon"><a href="http://forum.freecadweb.org/report.php?f=23&amp;p=41116" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://forum.freecadweb.org/posting.php?mode=quote&amp;f=23&amp;p=41116" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3><a href="#p41116">Re: ArchStructure Properties</a></h3>
			<p class="author"><a href="http://forum.freecadweb.org/viewtopic.php?p=41116#p41116"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=68" style="color: #0066FF;" class="username-coloured">yorik</a></strong> » Sun Dec 22, 2013 1:59 pm </p>

			

			<div class="content">Note that the material framework is under way, 
I'm still working on the editor, but there is already a good start in 
the Material module. We'll soon have proper, complete and efficient way 
to deal with materials in freecad. I think that will solve once and for 
all all the requirements for materials for the various modules of 
freecad.<br><br>With the development of the material module, we also gained a dictionary property for freecad: It's the App::PropertyMap<br>It's
 still not used anywhere, but it's there and usable already, and will 
allow for all kinds of user-defined properties. It has no editor yet, 
though.You can only edit it via python at the moment.<br><br>When I saw 
better how the material system will work and also after Jürgen starts 
implementing it in other modules (so I can follow his style and we have a
 more unified system throughout freecad), all Arch objects will gain a 
Material property, and I'll also add a user-defined property, where you 
can fill all that kind of stuff. I guess it'll be useful for IFC in/out 
too...</div>

			<div id="sig41116" class="signature"><a href="http://yorik.uncreated.net/guestblog.php?tag=freecad&amp;complete=3" class="postlink">my freecad blog posts</a></div>

		</div>

		
			<dl class="postprofile" id="profile41116">
			<dt>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=68"><img src="viewtopic-Dateien/file.jpeg" alt="User avatar" height="50" width="50"></a><br>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=68" style="color: #0066FF;" class="username-coloured">yorik</a>
			</dt>

			<dd>Site Admin</dd>

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 3793</dd><dd><strong>Joined:</strong> Tue Feb 17, 2009 9:16 pm</dd><dd><strong>Location:</strong> São Paulo, Brazil</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=41116" title="Private message"><span>Private message</span></a></li><li class="web-icon"><a href="http://yorik.uncreated.net/" title="WWW: http://yorik.uncreated.net"><span>Website</span></a></li><li class="jabber-icon"><a href="http://forum.freecadweb.org/memberlist.php?mode=contact&amp;action=jabber&amp;u=68" onclick="popup(this.href, 550, 320); return false;" title="Jabber"><span>Jabber</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p41118" class="post bg2 online">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="edit-icon"><a href="http://forum.freecadweb.org/posting.php?mode=edit&amp;f=23&amp;p=41118" title="Edit post"><span>Edit post</span></a></li><li class="report-icon"><a href="http://forum.freecadweb.org/report.php?f=23&amp;p=41118" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://forum.freecadweb.org/posting.php?mode=quote&amp;f=23&amp;p=41118" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3><a href="#p41118">Re: ArchStructure Properties</a></h3>
			<p class="author"><a href="http://forum.freecadweb.org/viewtopic.php?p=41118#p41118"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">bernd</a></strong> » Sun Dec 22, 2013 2:04 pm </p>

			

			<div class="content">Ahh, cool thanks again for the informations<br><br><blockquote><div><cite>yorik wrote:</cite>... we also gained a dictionary property for freecad: It's the App::PropertyMap<br>It's
 still not used anywhere, but it's there and usable already, and will 
allow for all kinds of user-defined properties. It has no editor yet, 
though.You can only edit it via python at the moment ...</div></blockquote>
 I would like to play a bit. It seams exactly what I need to use to 
define the user properties from my picture in the first post outside the
 class definition because in the class definition I would define the 
App::PropertyMap. Could you give an small example how to use it?</div>

			

		</div>

		
			<dl class="postprofile" id="profile41118">
			<dt>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">bernd</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 170</dd><dd><strong>Joined:</strong> Sun Sep 08, 2013 8:07 pm</dd><dd><strong>Location:</strong> Zürich, Switzerland</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=41118" title="Private message"><span>Private message</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">
<a id="unread"></a>
	<div id="p41120" class="post bg1 unreadpost">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="report-icon"><a href="http://forum.freecadweb.org/report.php?f=23&amp;p=41120" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://forum.freecadweb.org/posting.php?mode=quote&amp;f=23&amp;p=41120" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3><a href="#p41120">Re: ArchStructure Properties</a></h3>
			<p class="author"><a href="http://forum.freecadweb.org/viewtopic.php?p=41120#p41120"><img src="viewtopic-Dateien/icon_post_target_unread.gif" alt="Unread post" title="Unread post" height="9" width="11"></a>by <strong><a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=68" style="color: #0066FF;" class="username-coloured">yorik</a></strong> » Sun Dec 22, 2013 2:46 pm </p>

			

			<div class="content">Sure, let's say you have a (python-based) object called myobject:<br><dl class="codebox"><dt>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></dt><dd><code>myobject.addProperty("App::PropertyMap","UserDefined","User","An user-defined dictionary")<br>myprops = {"custom name":"My Fancy Name","custom length":"0.65"}<br>myobject.UserDefined = myprops<br>print myobject.UserDefined["custom name"]</code></dd></dl></div>

			<div id="sig41120" class="signature"><a href="http://yorik.uncreated.net/guestblog.php?tag=freecad&amp;complete=3" class="postlink">my freecad blog posts</a></div>

		</div>

		
			<dl class="postprofile" id="profile41120">
			<dt>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=68"><img src="viewtopic-Dateien/file.jpeg" alt="User avatar" height="50" width="50"></a><br>
				<a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=68" style="color: #0066FF;" class="username-coloured">yorik</a>
			</dt>

			<dd>Site Admin</dd>

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 3793</dd><dd><strong>Joined:</strong> Tue Feb 17, 2009 9:16 pm</dd><dd><strong>Location:</strong> São Paulo, Brazil</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=41120" title="Private message"><span>Private message</span></a></li><li class="web-icon"><a href="http://yorik.uncreated.net/" title="WWW: http://yorik.uncreated.net"><span>Website</span></a></li><li class="jabber-icon"><a href="http://forum.freecadweb.org/memberlist.php?mode=contact&amp;action=jabber&amp;u=68" onclick="popup(this.href, 550, 320); return false;" title="Jabber"><span>Jabber</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<form id="viewtopic" method="post" action="./viewtopic.php?f=23&amp;t=5197">

	<fieldset class="display-options" style="margin-top: 0; ">
		
		<label>Display posts from previous: <select name="st" id="st"><option value="0" selected="selected">All posts</option><option value="1">1 day</option><option value="7">7 days</option><option value="14">2 weeks</option><option value="30">1 month</option><option value="90">3 months</option><option value="180">6 months</option><option value="365">1 year</option></select></label>
		<label>Sort by <select name="sk" id="sk"><option value="a">Author</option><option value="t" selected="selected">Post time</option><option value="s">Subject</option></select></label> <label><select name="sd" id="sd"><option value="a" selected="selected">Ascending</option><option value="d">Descending</option></select> <input name="sort" value="Go" class="button2" type="submit"></label>
		
	</fieldset>

	</form>
	<hr>


<div class="topic-actions">
	<div class="buttons">
	
		<div class="reply-icon"><a href="http://forum.freecadweb.org/posting.php?mode=reply&amp;f=23&amp;t=5197" title="Post a reply"><span></span>Post a reply</a></div>
	
	</div>

	
		<div class="pagination">
			4 posts
			 • Page <strong>1</strong> of <strong>1</strong>
		</div>
	
</div>


	<p></p><p><a href="http://forum.freecadweb.org/viewforum.php?f=23" class="left-box left" accesskey="r">Return to Arch</a></p>

	<form method="post" id="jumpbox" action="./viewforum.php" onsubmit="if(this.f.value == -1){return false;}">

	
		<fieldset class="jumpbox">
	
			<label for="f" accesskey="j">Jump to:</label>
			<select name="f" id="f" onchange="if(this.options[this.selectedIndex].value != -1){ document.forms['jumpbox'].submit() }">
			
				<option value="-1">Select a forum</option>
			<option value="-1">------------------</option>
				<option value="7">Users</option>
			
				<option value="3">&nbsp; &nbsp;Help on using FreeCAD</option>
			
				<option value="22">&nbsp; &nbsp;Python scripting and macros</option>
			
				<option value="4">&nbsp; &nbsp;Install / Compile</option>
			
				<option value="8">&nbsp; &nbsp;Open discussion</option>
			
				<option value="9">&nbsp; &nbsp;Feature Announcements</option>
			
				<option value="6">Development</option>
			
				<option value="10">&nbsp; &nbsp;Developers corner</option>
			
				<option value="17">&nbsp; &nbsp;Pull Requests</option>
			
				<option value="19">&nbsp; &nbsp;Part Design</option>
			
				<option value="20">&nbsp; &nbsp;Assembly</option>
			
				<option value="18">&nbsp; &nbsp;FEM</option>
			
				<option value="15">&nbsp; &nbsp;CAM</option>
			
				<option value="23" selected="selected">&nbsp; &nbsp;Arch</option>
			
				<option value="21">&nbsp; &nbsp;Wiki</option>
			
				<option value="11">Forums in other languages</option>
			
				<option value="12">&nbsp; &nbsp;Forum français</option>
			
				<option value="13">&nbsp; &nbsp;Forum in Deutsch</option>
			
				<option value="14">&nbsp; &nbsp;Foro en Español</option>
			
				<option value="16">&nbsp; &nbsp;日本語フォーラム</option>
			
			</select>
			<input value="Go" class="button2" type="submit">
		</fieldset>
	</form>


	<h3><a href="http://forum.freecadweb.org/viewonline.php">Who is online</a></h3>
	<p>Users browsing this forum: <a href="http://forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">bernd</a> and 0 guests</p>
</div>

<div id="page-footer">

	<div class="navbar">
		<div class="inner"><span class="corners-top"><span></span></span>

		<ul class="linklist">
			<li class="icon-home"><a href="http://forum.freecadweb.org/index.php" accesskey="h">Board index</a></li>
				<li class="icon-unsubscribe"><a href="http://forum.freecadweb.org/viewtopic.php?uid=2069&amp;f=23&amp;t=5197&amp;unwatch=topic&amp;start=0&amp;hash=6c752bfd" title="Unsubscribe topic">Unsubscribe topic</a></li><li class="icon-bookmark"><a href="http://forum.freecadweb.org/viewtopic.php?f=23&amp;t=5197&amp;bookmark=1&amp;hash=6c752bfd" title="Bookmark topic">Bookmark topic</a></li>
			<li class="rightside"><a href="http://forum.freecadweb.org/memberlist.php?mode=leaders">The team</a> • <a href="http://forum.freecadweb.org/ucp.php?mode=delete_cookies">Delete all board cookies</a> • All times are UTC </li>
		</ul>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<div class="copyright">Powered by <a href="https://www.phpbb.com/">phpBB</a>® Forum Software © phpBB Group
		
	</div>
</div>

</div>

<div>
	<a id="bottom" name="bottom" accesskey="z"></a>
	
</div>


</body></html>