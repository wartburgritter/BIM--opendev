#***************************************************************************
#*                                                                         *
#*   Copyright (c) 2014                                                    *  
#*   Yorik van Havre <yorik@uncreated.net>                                 *  
#*                                                                         *
#*   This program is free software; you can redistribute it and/or modify  *
#*   it under the terms of the GNU Lesser General Public License (LGPL)    *
#*   as published by the Free Software Foundation; either version 2 of     *
#*   the License, or (at your option) any later version.                   *
#*   for detail see the LICENCE text file.                                 *
#*                                                                         *
#*   This program is distributed in the hope that it will be useful,       *
#*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
#*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
#*   GNU Library General Public License for more details.                  *
#*                                                                         *
#*   You should have received a copy of the GNU Library General Public     *
#*   License along with this program; if not, write to the Free Software   *
#*   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
#*   USA                                                                   *
#*                                                                         *
#***************************************************************************

__title__=   "FreeCAD IFC importer - Enhanced ifcopenshell-only version"
__author__ = "Yorik van Havre"
__url__ =    "http://www.freecadweb.org"

import os,time,tempfile,uuid,FreeCAD,Part,Draft,Arch

if open.__module__ == '__builtin__':
    pyopen = open # because we'll redefine open below

def open(filename,skip=[]):
    "opens an IFC file in a new document"
    #skip = [9006]
    docname = os.path.splitext(os.path.basename(filename))[0]
    doc = FreeCAD.newDocument(docname)
    doc.Label = docname
    doc = insert(filename,doc.Name,skip)
    return doc

def insert(filename,docname,skip=[]):
    "imports the contents of an IFC file"
    
    import ifcopenshell
    print "opening ",filename,"..."
    try:
        doc = FreeCAD.getDocument(docname)
    except:
        doc = FreeCAD.newDocument(docname)
    FreeCAD.ActiveDocument = doc
    
    global ifcfile # keeping global for debugging purposes
    ifcopenshell.clean()
    ifcfile = ifcopenshell.open(filename)
    shape_attributes = ifcopenshell.SEW_SHELLS
    products = ifcfile.by_type("IfcProduct")
    objects = {} # { id:object, ... }

    # products
    for product in products:
        pid = product.id()
        ptype = product.is_a()
        name = product.Name or str(ptype[3:])
        name = "ID" + str(pid) + " " + name
        obj = None
        baseobj = None
        
        if (ptype == "IfcOpeningElement"): continue
        if pid in skip: continue
        
        brep = ifcopenshell.create_shape(product,shape_attributes)
        if brep:
            shape = Part.Shape()
            shape.importBrepFromString(brep)
            if not shape.isNull():
                baseobj = FreeCAD.ActiveDocument.addObject("Part::Feature",name+"_body")
                baseobj.Shape = shape
        if not obj:
            obj = baseobj
        if obj:
            sh = baseobj.Shape.ShapeType if hasattr(baseobj,"Shape") else "None"
            sols = str(baseobj.Shape.Solids) if hasattr(baseobj,"Shape") else ""
            print "creating object ",pid," : ",ptype, " with shape: ",sh," ",sols
            objects[pid] = obj

    FreeCAD.ActiveDocument.recompute()
    
    # cleaning bad shapes
    for obj in objects.values():
        if obj.isDerivedFrom("Part::Feature"):
            if obj.Shape.isNull():
                Arch.rebuildArchShape(obj)
    FreeCAD.ActiveDocument.recompute()
    
    if FreeCAD.GuiUp:
        import FreeCADGui
        FreeCADGui.SendMsgToActiveView("ViewFit")
    return doc


