
# import mittels importIFC
import importIFC
filename = u'C:/test_äöü_test/wall.ifc'
importIFC.open(filename)

# tests mit ifcopenshell
# mit umlaute ohne encoding, geht wenn filename direkt als string eingegeben wird ! 
import ifcopenshell
filename = ('C:/test_äöü_test/wall.ifc')
ifcfile = ifcopenshell.open(filename)
ifcfile.by_type('IfcWall')

## grosse frage wie nur die wall als shape importieren ?!?
# unbedingt fuer debugging speichern --> ins wiki !!!

##########################################################################

# C:\test_äöü_test\wall.ifc  # filename in file explorer
import sys

# returned filename by the gui open method of freecad
print u"C:/test_\xe4\xf6\xfc_test/wall.ifc"                                     

# the encoding done by importIFC
u"C:/test_\xe4\xf6\xfc_test/wall.ifc".encode(sys.getfilesystemencoding())     

# encoding on the windows machine I'm sitting at
u"C:/test_\xe4\xf6\xfc_test/wall.ifc".encode('mbcs')                            

# string sent to ifcopenshell 
print u"C:/test_\xe4\xf6\xfc_test/wall.ifc".encode(sys.getfilesystemencoding()) 

# string which needs to be sent to ifcopenshell on the windows machine I am sitting at !
print u"C:/test_\xe4\xf6\xfc_test/wall.ifc".encode('utf-8') 

# You can find out what is the system encoding that each of them uses:
import sys
print sys.getfilesystemencoding()



###########################################################################################

from ifcopenshell import geom
settings = ifcopenshell.geom.settings()
settings.set(settings.USE_BREP_DATA,True)
settings.set(settings.SEW_SHELLS,True)
settings.set(settings.USE_WORLD_COORDS,True)
 

cr = ifcopenshell.geom.create_shape(settings,77)


