# https://sourceforge.net/p/ifcopenshell/discussion/1782718/thread/1958018c/ 
# http://forum.freecadweb.org/viewtopic.php?f=23&p=96586&sid=08c162636bb9d424e90ee1d301184b7f#p96586


import ifcopenshell
path = '/home/hugo/Documents/projekte--BIM--workopen/freecad--ifc/encoding--decoding/inside_file/'
infile = path + 'my-special-letter-walls.ifc'
f = ifcopenshell.open(infile)
# write file
outfile2 = path + 'my-special-letter-walls--rewrite.ifc'
f.write(outfile2)
# reload file
infile = outfile2
o2 = ifcopenshell.open(infile)


# play a bit
f.by_id(183)
print f.by_id(183).Name.decode("unicode_escape").encode("utf8")
o2.by_id(183)
print o2.by_id(183).Name.decode("unicode_escape").encode("utf8")

f.by_id(346)
print f.by_id(346).Name.decode("unicode_escape").encode("utf8")
o2.by_id(346)
print o2.by_id(346).Name.decode("unicode_escape").encode("utf8")


'''
hugo@weide:~$ python
Python 2.7.9 (default, Mar  1 2015, 12:57:24)
[GCC 4.9.2] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>>
>>> import ifcopenshell
>>> path = '/home/hugo/Documents/projekte--BIM--workopen/freecad--ifc/encoding--decoding/inside_file/'
>>> infile = path + 'my-special-letter-walls.ifc'
>>> f = ifcopenshell.open(infile)
>>> # write file
... outfile2 = path + 'my-special-letter-walls--rewrite.ifc'
>>> f.write(outfile2)
>>> # reload file
... infile = outfile2
>>> o2 = ifcopenshell.open(infile)
>>>
>>>
>>> # play a bit
... f.by_id(183)
#183=IfcBeam('2kaAWBUDj6K9gDsYvGgyVi',#4,'BIG__\u00c4-\u00dc-\u00d6__WALL',$,$,#67,#168,$)
>>> print f.by_id(183).Name.decode("unicode_escape").encode("utf8")
BIG__Ä-Ü-Ö__WALL
>>> o2.by_id(183)
#183=IfcBeam('2kaAWBUDj6K9gDsYvGgyVi',#4,'BIG__\u00c4-\u00dc-\u00d6__WALL',$,$,#67,#168,$)
>>> print o2.by_id(183).Name.decode("unicode_escape").encode("utf8")
BIG__Ä-Ü-Ö__WALL
>>>
>>> f.by_id(346)
#346=IfcBeam('0qy3LZuR9EAAHgTbnFGMjQ',#4,'small__\u00e4-\u00fc-\u00f6__wall',$,$,#252,#342,$)
>>> print f.by_id(346).Name.decode("unicode_escape").encode("utf8")
small__ä-ü-ö__wall
>>> o2.by_id(346)
#346=IfcBeam('0qy3LZuR9EAAHgTbnFGMjQ',#4,'small__\u00e4-\u00fc-\u00f6__wall',$,$,#252,#342,$)
>>> print o2.by_id(346).Name.decode("unicode_escape").encode("utf8")
small__ä-ü-ö__wall
>>>
'''


'''
hugo@weide:~$ diff -w  my-special-letter-walls.ifc my-special-letter-walls--rewrite.ifc
...
104c101
< #183=IFCBEAM('2kaAWBUDj6K9gDsYvGgyVi',#4,'BIG__\X2\00C4\X0\-\X2\00DC\X0\-\X2\00D6\X0\__WALL',$,$,#67,#168,$);
---
> #183=IFCBEAM('2kaAWBUDj6K9gDsYvGgyVi',#4,'BIG__\\u00c4-\\u00dc-\\u00d6__WALL',$,$,#67,#168,$);
110c107
< #211=IFCPROPERTYSINGLEVALUE('Einheit',$,IFCDESCRIPTIVEMEASURE('m\X2\00B3\X0\'),$);
---
> #211=IFCPROPERTYSINGLEVALUE('Einheit',$,IFCDESCRIPTIVEMEASURE('m\\u00b3'),$);
112c109
< #216=IFCPROPERTYSINGLEVALUE('Bezeichnung',$,IFCDESCRIPTIVEMEASURE('BIG__\X2\00C4\X0\-\X2\00DC\X0\-\X2\00D6\X0\__WALL'),$);
---
> #216=IFCPROPERTYSINGLEVALUE('Bezeichnung',$,IFCDESCRIPTIVEMEASURE('BIG__\\u00c4-\\u00dc-\\u00d6__WALL'),$);
180c177
< #346=IFCBEAM('0qy3LZuR9EAAHgTbnFGMjQ',#4,'small__\X2\00E4\X0\-\X2\00FC\X0\-\X2\00F6\X0\__wall',$,$,#252,#342,$);
---
> #346=IFCBEAM('0qy3LZuR9EAAHgTbnFGMjQ',#4,'small__\\u00e4-\\u00fc-\\u00f6__wall',$,$,#252,#342,$);
186c183
< #358=IFCPROPERTYSINGLEVALUE('Einheit',$,IFCDESCRIPTIVEMEASURE('m\X2\00B3\X0\'),$);
---
> #358=IFCPROPERTYSINGLEVALUE('Einheit',$,IFCDESCRIPTIVEMEASURE('m\\u00b3'),$);
188c185
< #360=IFCPROPERTYSINGLEVALUE('Bezeichnung',$,IFCDESCRIPTIVEMEASURE('small__\X2\00E4\X0\-\X2\00FC\X0\-\X2\00F6\X0\__wall'),$);
---
> #360=IFCPROPERTYSINGLEVALUE('Bezeichnung',$,IFCDESCRIPTIVEMEASURE('small__\\u00e4-\\u00fc-\\u00f6__wall'),$);
205d201
<
hugo@weide:~$ 
'''

