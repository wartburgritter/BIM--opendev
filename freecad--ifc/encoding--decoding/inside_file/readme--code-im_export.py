# neu ist in FreeCAD ab Dezember 2015 ohne decode eventuell ?! ist noch nicht in master ...

import ifcopenshell
path = '/home/hugo/Documents/projekte--BIM--opendev/freecad--ifc/encoding--decoding/inside_file/'
infile = path + 'my-special-letter-walls--Allplan_Win7.ifc'
f = ifcopenshell.open(infile)
# import module and load the file needs to be done only once 
#  -->  working part could be done without loeading each time


f.by_id(183)
f.by_id(346)

f.by_id(183).Name
f.by_id(346).Name

# FreeCAD importIFC --> import --> line 400
f.by_id(183).Name.decode("unicode_escape").encode("utf8")
f.by_id(346).Name.decode("unicode_escape").encode("utf8")

print f.by_id(183).Name.decode("unicode_escape").encode("utf8")
print f.by_id(346).Name.decode("unicode_escape").encode("utf8")

type(f.by_id(183).Name)
type(f.by_id(183).Name.decode("unicode_escape").encode("utf8"))


# Zusammenfassung
f.by_id(183).Name
print f.by_id(183).Name

f.by_id(183).Name.decode("unicode_escape").encode("utf8")
print f.by_id(183).Name.decode("unicode_escape").encode("utf8")


# 'BIG__\\u00c4-\\u00dc-\\u00d6__WALL'
# 'BIG__\u00c4-\u00dc-\u00d6__WALL'

# 'BIG__\xc3\x84-\xc3\x9c-\xc3\x96__WALL'
# 'BIG__Ä-Ü-Ö__WALL'


#Allplan_Win7
#183= IFCBEAM('2kaAWBUDj6K9gDsYvGgyVi',#4,'BIG__\X2\00C4\X0\-\X2\00DC\X0\-\X2\00D6\X0\__WALL',$,$,#67,#168,$);
#346= IFCBEAM('0qy3LZuR9EAAHgTbnFGMjQ',#4,'small__\X2\00E4\X0\-\X2\00FC\X0\-\X2\00F6\X0\__wall',$,$,#252,#342,$);

'BIG__\X2\00C4\X0\-\X2\00DC\X0\-\X2\00D6\X0\__WALL'
'small__\X2\00E4\X0\-\X2\00FC\X0\-\X2\00F6\X0\__wall'


# Export --> not changing anything in ifcfile
outfile1 = path + 'outtest--no_change.ifc'
f.write(outfile1)
# reload
infile = outfile1
o1 = ifcopenshell.open(infile)
o1.by_id(183).Name 
o1.by_id(183).Name.decode("unicode_escape").encode("utf8")
print o1.by_id(183).Name.decode("unicode_escape").encode("utf8")
#183=IFCBEAM('2kaAWBUDj6K9gDsYvGgyVi',#4,'BIG__\\u00c4-\\u00dc-\\u00d6__WALL',$,$,#67,#168,$);
#346=IFCBEAM('0qy3LZuR9EAAHgTbnFGMjQ',#4,'small__\\u00e4-\\u00fc-\\u00f6__wall',$,$,#252,#342,$);

# 'BIG__\\u00c4-\\u00dc-\\u00d6__WALL'
# 'small__\\u00e4-\\u00fc-\\u00f6__wall'





#  Export --> changing strings in f !!!
# FreeCAD importIFC --> export --> line 713

# changing special letters in ifcfile
f.by_id(183).Name = u'NEW_BIG__Ä-Ü-Ö__WALL_NEW'.encode("utf8")   # u string utf8 encodiert
f.by_id(183).Name
f.by_id(346).Name = 'new_small__ä-ü-ö__wall_new'                 # std string
f.by_id(346).Name
outfile2 = path + 'outtest--simple_change.ifc'
f.write(outfile2)
# reload
infile = outfile2
o2 = ifcopenshell.open(infile)
o2.by_id(183).Name.decode("unicode_escape").encode("utf8")
print o2.by_id(183).Name.decode("unicode_escape").encode("utf8")
o2.by_id(346).Name.decode("unicode_escape").encode("utf8")
print o2.by_id(346).Name.decode("unicode_escape").encode("utf8")












###################################################################
# file debbuging --> do I really load my infile ?!?
print 'cat  ', infile    # copy in shell for debugging 


# extract the appropriate entities and values from ifcfile

for e in f.by_type('ifcbeam'):
    print e

myid = 183
print f.by_id(myid)

# print f.by_id(myid).attribute_name(0)  # use a for loop for all names !
end = len(f.by_id(myid))
# print end
for nr, an in enumerate(f.by_id(myid)):
    print f.by_id(myid).attribute_name(nr)
    if nr == end-1: break




outfile = path + 'outtest.ifc'
f.write(outfile)



'''
# FreeCAD importIFC --> import --> line 400
            name = product.Name.decode("unicode_escape").encode("utf8")


# FreeCAD importIFC --> export --> line 713
        name = str(obj.Label.encode("utf8"))

'''
