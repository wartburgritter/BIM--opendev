##################################################################################################################################
workaround:

- def methode welche file oeffnet und ifcopenshell encodierungen fuer deutsche umlaute ersetzt
- klein anfangen 
--> grund geruest fuer code und einfach erweiterbar durch die daten welche es zu ersetzen gilt
--> hab ich doch schon mal fuer ArchiCAD gemacht und eine kleine Wortsammluch erstellt, die ich dann gesucht und ersetzt habe
--> check --> nur innerhalb strings ersetzen nicht in den uuids
--> fuer jede zeile des ifcfile suche nach allen ersetzungen

BIG__\X2\00C4\X0\-\X2\00DC\X0\-\X2\00D6\X0\__WALL             allplan
BIG__\\u00c4-\\u00dc-\\u00d6__WALL                            ifcos

small__\X2\00E4\X0\-\X2\00FC\X0\-\X2\00F6\X0\__wall           allplan
small__\\u00e4-\\u00fc-\\u00f6__wall                          ifcos

suchen --> ersetzen
\\u00c4
\X2\00C4\X0\

mhh alles gar nicht so einfach, das ein string mit backslashes gar escaped werden muss, ach wart doch auf Thomas !!!

# schon erstes problem mit suchstring   \X2\00E4\X0\  der will nicht wegen escapesequenzen !
exchanges = {
            'german_small_dotted_a' :  ( ('\\X2\00E4\\X0\\'), ('\\u00e4', '\S\d') ),  #  --> ä
            'german_big_dotted_a'   :  ( (" '\X2\00C4\X0\' "), ('\\u00c4-\\u00dc') )   #  --> Ä
            }
##################################################################################################################################

im aktuellen ifcopenshell 

- win7  direktes schreiben der umlaute ins ifcopenshell

- debian umlaute werden encodiert, aber es gibt unterschiede zwischen innerhalb FreeCAD und ausserhalb FreeCAD in bash shell
- ausserhalb FreeCAD kann ifcopenshell seine eigenen erstellten files lesen und schreiben. Mit FreeCAD code funktioniert das
- innerhalb FreeCAD erzeugt der selbe code andere ergebnisse im ifc files --> siehe file readme--FreeCAD__vs__pythonShell.py

- wird ifc nur gelesen und geschrieben durch ifcopenshell --> externe apps koennen umlaute nicht lesen --> ifcopenshell-solibri--Ä.png




u'Ä'.encode('utf8')
u'Ä'.encode('utf16')


Ä

'\X\C4'        
character code xC4 as 8-bit character code found in ISO 10646 
(first 255 characters - also referred to as "row 0")


'\X2\00C4\X0\' 	
character code xC4 as 16-bit character x00C4 in ISO 10646 (Unicode)


\X2\00C4\X0\
Allplan


\X4\000000C4\X0\
Debian Jessie in FreeCAD, in bash anders !!!



\X2\00C4\X0\
Allplan

-------------------
kleines ä

\S\d
Tekla Sructures -->  aus Tr\S\dger


\S\d
ArchiCAD 12.0 --> aus Gel\S\dnde --> kann Allplan aber glaube nicht lesen !!!


\X\E4
Revit 2009 --> aus Fl\X\E4che

