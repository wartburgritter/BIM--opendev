import ifcopenshell
from ifcopenshell import geom
settings = ifcopenshell.geom.settings()
settings.set(settings.USE_BREP_DATA,True)
settings.set(settings.SEW_SHELLS,True)
settings.set(settings.USE_WORLD_COORDS,True)
settings.set(settings.INCLUDE_CURVES,True)     # for stuct


file_path = '/home/hugo/Desktop/FreeCAD--forum/0112--dlubal--ifc/'
#file_path = 'C:\\Users\\bhb\\Downloads\\'
f = ifcopenshell.open(file_path + 'E1--3-Axis-Struc-Export.ifc')
f.by_type('IfcPerson')


#entity = f.by_id(77)  # is the first one in this file
#entity = f.by_type('IfcWall')[0]  # takes the first found
entity = f.by_type('IfcStructuralCurveMember')[0]  # takes the first found


print entity
cr = ifcopenshell.geom.create_shape(settings, entity)
brep = cr.geometry.brep_data
print brep

import Part
shape = Part.Shape()
shape.importBrepFromString(brep)
Part.show(shape)


