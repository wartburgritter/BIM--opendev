import ifcopenshell
from ifcopenshell import geom
settings = ifcopenshell.geom.settings()
settings.set(settings.USE_BREP_DATA,True)
settings.set(settings.SEW_SHELLS,True)
settings.set(settings.USE_WORLD_COORDS,True)
settings.set(settings.INCLUDE_CURVES,True)     # for stuct

def get_shape(entity):
    import Part
    cr = ifcopenshell.geom.create_shape(settings, entity)
    brep = cr.geometry.brep_data
    shape = Part.Shape()
    shape.importBrepFromString(brep)
    Part.show(shape)

def get_brep(entity):
    cr = ifcopenshell.geom.create_shape(settings, entity)
    brep = cr.geometry.brep_data
    print brep

# open file and check if the file was load by IFCPERSON
file_path = '/home/hugo/Documents/projekte--BIM--opendev/freecad--ifc/files_fuer_ifc-debugging3/'
#file_path = 'C:\\Users\\bhb\\Desktop\\'
f = ifcopenshell.open(file_path + 'export-struct.ifc')
f.by_type('IfcPerson')

'''
products = f.by_type('IfcProduct')
for p in products:
    print p

sm = f.by_type('IfcStructuralSurfaceMember')
for e in sm:
    print e

'''


# Allplan2016.1 --> export-struct.ifc

'''
#656=IfcStructuralSurfaceMember('1Dig4b1a93Eh9$XzYmkCYB',#4,'Surface 2',$,$,#644,#653,.SHELL.,250.)
#653=IfcProductDefinitionShape($,$,(#651))
(#651=IfcTopologyRepresentation(#11,$,'Face',(#649)),)
(#649=IfcFaceSurface((#636),#645,.T.),)
(#636=IfcFaceOuterBound(#630,.T.),)
#630=IfcEdgeLoop((#632,#633,#634,#635))

#632=IfcOrientedEdge(*,*,#440,.T.)
#633=IfcOrientedEdge(*,*,#461,.T.)
#634=IfcOrientedEdge(*,*,#482,.T.)
#635=IfcOrientedEdge(*,*,#503,.T.)

#440=IfcEdgeCurve(#238,#257,#441,.T.)
#461=IfcEdgeCurve(#257,#276,#462,.T.)
#482=IfcEdgeCurve(#276,#295,#483,.T.)
#503=IfcEdgeCurve(#295,#238,#504,.T.)


'''




# slab
f.by_id(607)
f.by_id(607).Representation
f.by_id(604).Representations
f.by_id(602).Items
f.by_id(600).Bounds
f.by_id(587).Bound

# edges
edge_list = f.by_id(581).EdgeList
edge_element = []
for element in edge_list:
    print element
    edge_element.append(element.EdgeElement)

edge_geometry = []
edge_direction = []
for geometry in edge_element:
    print geometry
    edge_geometry.append(geometry.EdgeStart)
    edge_geometry.append(geometry.EdgeEnd)
    edge_geometry.append('')
    edge_direction.append(geometry.EdgeGeometry)

edge_vertexes = []
for vertex in edge_geometry:
    print vertex
    if vertex is not '' and vertex.is_a('IfcVertexPoint'):
        edge_vertexes.append(vertex.VertexGeometry)
    else:
        edge_vertexes.append('')

for v in edge_vertexes:
    print v

dir_vector = []
for direction in edge_direction:
    print direction
    dir_vector.append(direction.Pnt)
    dir_vector.append(direction.Dir)

vec2 = []
for vec in dir_vector:
    print vec
    if vec is not '' and vec.is_a('IfcVector'):
        vec2.append(vec.Orientation)
        vec2.append(vec.Magnitude)

for v in vec2:
    print v



f.by_id(600).FaceSurface
f.by_id(596).Position
f.by_id(597).Location




'''
slab
#607=IfcStructuralSurfaceMember('2M8_Etipn4MwtLx$9uhpUH',#4,'Surface 1',$,$,#595,#604,.SHELL.,250.0000000000001)
#604=IfcProductDefinitionShape($,$,(#602))
(#602=IfcTopologyRepresentation(#11,$,'Face',(#600)),)
(#600=IfcFaceSurface((#587),#596,.T.),)
(#587=IfcFaceOuterBound(#581,.T.),)
#581=IfcEdgeLoop((#583,#584,#585,#586))


#583=IfcOrientedEdge(*,*,#356,.T.)
#584=IfcOrientedEdge(*,*,#377,.T.)
#585=IfcOrientedEdge(*,*,#398,.T.)
#586=IfcOrientedEdge(*,*,#419,.T.)


#356=IfcEdgeCurve(#162,#181,#357,.T.)
#377=IfcEdgeCurve(#181,#200,#378,.T.)
#398=IfcEdgeCurve(#200,#219,#399,.T.)
#419=IfcEdgeCurve(#219,#162,#420,.T.)


#162=IfcVertexPoint(#151)
#181=IfcVertexPoint(#170)

#181=IfcVertexPoint(#170)
#200=IfcVertexPoint(#189)

#200=IfcVertexPoint(#189)
#219=IfcVertexPoint(#208)

#219=IfcVertexPoint(#208)
#162=IfcVertexPoint(#151)


#151=IfcCartesianPoint((5.,5.000000000000002,3.125))
#170=IfcCartesianPoint((-5.,5.000000000000002,3.125))

#170=IfcCartesianPoint((-5.,5.000000000000002,3.125))
#189=IfcCartesianPoint((-5.,-5.000000000000002,3.125))

#189=IfcCartesianPoint((-5.,-5.000000000000002,3.125))
#208=IfcCartesianPoint((5.,-5.000000000000002,3.125))

#208=IfcCartesianPoint((5.,-5.000000000000002,3.125))
#151=IfcCartesianPoint((5.,5.000000000000002,3.125))


#357=IfcLine(#151,#360)
#378=IfcLine(#170,#381)
#399=IfcLine(#189,#402)
#420=IfcLine(#208,#423)


#151=IfcCartesianPoint((5.,5.000000000000002,3.125))
#360=IfcVector(#358,10.)
#170=IfcCartesianPoint((-5.,5.000000000000002,3.125))
#381=IfcVector(#379,10.)
#189=IfcCartesianPoint((-5.,-5.000000000000002,3.125))
#402=IfcVector(#400,10.)
#208=IfcCartesianPoint((5.,-5.000000000000002,3.125))
#423=IfcVector(#421,10.)


#358=IfcDirection((-1.,0.,0.))
10.0
#379=IfcDirection((0.,-1.,0.))
10.0
#400=IfcDirection((1.,0.,0.))
10.0
#421=IfcDirection((0.,1.,0.))
10.0




#596=IfcPlane(#597)
#597=IfcAxis2Placement3D(#598,$,$)
#598=IfcCartesianPoint((0.,0.,0.))


!!!!!!!!!!!FEHLER!!!!!!!!!!!!!!!!!!!!
soll
#598=IfcCartesianPoint((0.,0.,3.125))

'''


# wall
f.by_id(656)
f.by_id(656).Representation
f.by_id(653).Representations
f.by_id(651).Items
f.by_id(649).Bounds
f.by_id(636).Bound


f.by_id(649).FaceSurface
f.by_id(645).Position
f.by_id(646).Location
#647=IfcCartesianPoint((0.,0.,0.))

f.by_id(632)
f.by_id(440)
f.by_id(238)
f.by_id(227)
f.by_id(441)
f.by_id(444)


f.by_id(257)
f.by_id(441)

f.by_id(227)
f.by_id(246)
f.by_id(444)

f.by_id(442)
f.by_id(227)



get_shape(f.by_id(607))
get_shape(f.by_id(656))

get_brep(f.by_id(607))
get_brep(f.by_id(656))




##############################################
# get_attribute_names

get_attribute_names(f.by_id(646))

# einmal unteres einfuegen um obiges zu verwenden !
def get_attribute_names(entity):
    for a in range(9):
        try:
            print a, ' --> ', entity.attribute_name(a)
        except:
            print a, ' is not defined'


