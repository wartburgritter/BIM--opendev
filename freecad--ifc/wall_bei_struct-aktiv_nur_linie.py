# ahh und export testen !!!

'''
wenn curve setting True ist
settings.set(settings.INCLUDE_CURVES,True)
dann werden waende (wohl weil extrusionen) nur als linien importiert
curve setting fuer jedes product einzeln setzen
siehe unten
'''


##########################################################

import ifcopenshell, os, Part

# from importIFC
from ifcopenshell import geom
settings = ifcopenshell.geom.settings()
settings.set(settings.USE_BREP_DATA,True)
settings.set(settings.SEW_SHELLS,True)
settings.set(settings.USE_WORLD_COORDS,True)
settings.set(settings.INCLUDE_CURVES,True)

path = '/home/hugo/Documents/projekte--BIM--opendev/ifcopenshell/zifc/'

#f = ifcopenshell.open(path + 'miniwand.ifc')
#f.by_type('IfcWallStandardCase')  # 219
#p = f.by_id(219)

f = ifcopenshell.open(path + 'minigebaeude_mit_bpl.ifc')
f.by_type('IfcWallStandardCase')  # 526, # 707, # 888
#p = f.by_id(526)
#print p.attribute_name(0)


for p in f.by_type('IfcWallStandardCase'):
    r = p.Representation
    print p, '\n', r
    print p.GlobalId

    if p.GlobalId == '26s21zK75AX83Ot9Dryk30':
        settings.set(settings.INCLUDE_CURVES,False)
    else:
        settings.set(settings.INCLUDE_CURVES,True)

    cr = ifcopenshell.geom.create_shape(settings, p)
    brep = cr.geometry.brep_data
    shape = Part.Shape()
    shape.importBrepFromString(brep)
    Part.show(shape)





