import ifcopenshell

# from importIFC
from ifcopenshell import geom
settings = ifcopenshell.geom.settings()
settings.set(settings.USE_BREP_DATA,True)
settings.set(settings.SEW_SHELLS,True)
settings.set(settings.USE_WORLD_COORDS,True)

# for stuct
settings.set(settings.INCLUDE_CURVES,True)




# open file
f = ifcopenshell.open("C:\\Users\\bhb\\Desktop\\export-struct.ifc")

# check if the file was load, IFCPERSON  should be in any ifc
f.by_type('ifcperson')


products = f.by_type('ifcproduct')
for p in products:
    print p

e = f.by_id(303)
for a in range(9):
    try:
        print a, ' --> ', e.attribute_name(a)
    except:
        print a, ' is not defined'



e.GlobalId
e.OwnerHistory
e.Name
e.Description
e.ObjectType
e.ObjectPlacement
e.Representation
e.AppliedCondition


'''
e = f.by_id(303)
e
for a in range(9):
    try:
        print a, ' --> ', e.attribute_name(a)
    except:
        print a, ' is not defined'

'''
