import ifcopenshell
from ifcopenshell import geom
settings = ifcopenshell.geom.settings()
settings.set(settings.USE_BREP_DATA,True)
settings.set(settings.SEW_SHELLS,True)
settings.set(settings.USE_WORLD_COORDS,True)
settings.set(settings.INCLUDE_CURVES,True)     # for stuct

def get_shape(entity):
    import Part
    cr = ifcopenshell.geom.create_shape(settings, entity)
    brep = cr.geometry.brep_data
    shape = Part.Shape()
    shape.importBrepFromString(brep)
    Part.show(shape)

def get_brep(entity):
    cr = ifcopenshell.geom.create_shape(settings, entity)
    brep = cr.geometry.brep_data
    print brep

# open file and check if the file was load by IFCPERSON
# file_path = '/home/hugo/Documents/projekte--BIM--opendev/freecad--ifc/StructuralAnalysisView/eigene--open/stdbsp/export_allplan/'
file_path = 'C:\\Users\\bhb\\Desktop\\statisches--ifc\\allplan-bug\\'
f = ifcopenshell.open(file_path + 'allplan-export-struct-test.ifc')
f.by_type('IfcPerson')


get_shape(f.by_id(607))

'''

#598=IfcCartesianPoint((0.,0.,0.))
#598=IfcCartesianPoint((0.,0.,3.125))


>>> get_attribute_names(f.by_id(646))
0  -->  Location
1  -->  Axis
2  -->  RefDirection


#645=IfcPlane(#646)
#646=IfcAxis2Placement3D(#647,$,$)
#647=IfcCartesianPoint((0.,0.,0.))


#647=IfcCartesianPoint((0.,0.,0.))
#647=IfcCartesianPoint((0.,-3.875,0.))

es funktioniert noch nicht, da die vectoren in #646

'''