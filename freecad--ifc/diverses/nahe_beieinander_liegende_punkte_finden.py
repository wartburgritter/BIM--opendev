# pendenzen
# unterschied arch shape zu body shape
# 




#print('Die Ausfuehrung meines Programms funktioniert')
#print('Ausfuehrung von funktionen mit versuch.funktionsname')
#print('Alle objekte auflisten mit versuch.list_objects()')

"""
import sys
sys.path.append('/home/hugo/Documents/projekte--ifc/freecad/')
import versuch as x
reload(x)

# beachten bei der arbeitsweiese, das modul wird erst komplett geladen
# und dann ausgefuehrt, schone erkennbar bei einer etwas laengeren
# schleife wenn printanweisungen davor stehen.

# from versuch import *
# importiert alle funktionen von versuch in den namensraum von FreeCAD
# reload ist dann nicht einfach, weil nicht das ganze modul als objekt
imporiert wurde, sondern nur alle funktionen
"""
######################################################################  
######################################################################  
######################################################################  
 
 
def printe_objectlist(objs):
  """prints the objekt array line by line
   """
  for obj in objs:
    print(obj,'    ' , obj.Name,'    ' , obj.Label,'    ' ,obj.PropertiesList)
  print('Im Dokument sind', len(objs), 'Objekte')


def get_partfeaturelist(objectlist, enr):
  """extract all partfeatures from objectlist
  """
  # murks fuer typkontrolle, aber ich weiss nicht wie anders
  # jedes archobjekt hat eine eigene shape, und ein bodyobject
  # welches auch wieder eine shape hat. heisst dass jede shape
  # ist zweimal vorhanden??
  # archobject und bodyobject sind beides part::feature, aber 
  # trotzdem unterschiedlichen typs 
  # erst mit list_objects4 den typ finden, der ein archobject ist
  # und dann den eingeben
  # FORUMEINTRAG FUER UNTERSCHIED, ODER CODE LESEN
  # im mini-haus-mit-massen
  # (objs[0]  ist das partfeature architecture
  # (objs[1]  ist das partfeature shape

  partfeaturelist = []
  for obj in objs:
    if type(obj) == type(objs[enr]):    # (type archobjekt siehe list_objects7)
      partfeaturelist.append(obj)
  print('Es wurden ', len(partfeaturelist), 'Architekturelemente zurueckgegeben.')
  return(partfeaturelist)

  
  
def get_shapevertexlist(listofpartfeature):
  listofshapevertexes = []
  for obj in listofpartfeature:
    listofshapevertexes.append(obj.Shape.Vertexes)  
  print('Es wurden wurden die Vertexe von', len(listofshapevertexes), 'Shapes zurueckgegeben.')
  return(listofshapevertexes)
      
      
      
def get_vertexlist(listofpartfeature):
  listofallvertexes = []
  for obj in listofpartfeature:
    for v in range(len(obj.Shape.Vertexes)):
      listofallvertexes.append(obj.Shape.Vertexes[v])  
      #print(obj.Shape.Vertexes[v].Point)
  print('Es wurden ', len(listofallvertexes), 'Vertexe zurueckgegeben.')
  #print(listofallvertexes)
  return(listofallvertexes)
  
  
  
def get_pointlist(listofpartfeature):
  listofallpoints = []
  for obj in listofpartfeature:
    for v in range(len(obj.Shape.Vertexes)):
      listofallpoints.append(obj.Shape.Vertexes[v].Point)  
  #print(listofallpoints)
  #print(listofpoints[0] - listofpoints[1])
  print('Es wurden ', len(listofpoints), 'Punkte zurueckgegeben.')
  return(listofpoints)

  
  
def make_pointvergleich(pointlist):
  """berechne distanz eines jeden punktes mit jedem anderen
  """
  listofdist = []
  for p in pointlist:
   for q in pointlist:
     #print(p.distanceToPoint(q))
     listofdist.append(p.distanceToPoint(q))
  #print sorted(pointlist)
  for d in sorted(listofdist, reverse=True):
    print(str(d*1000) + ' mm')
  print('Es sind ', len(listofp), 'Punkte vorhanden')
  print('Es sind ', len(listofdist), 'Punktvergleiche berechnet wurden')

  
  
def get_pointvergleich(pointlist):
  """berechne distanz eines jeden punktes mit jedem anderen
  """
  listofdist = []
  for p in pointlist:
   for q in pointlist:
     #print(p.distanceToPoint(q))
     listofdist.append(p.distanceToPoint(q))
  #print sorted(pointlist)
  print('Es wurden', len(listofdist), 'Punktvergleiche zurueckgegeben.')
  return(listofdist)

  
  
def pvergleich_variante(listofp):
  """berechne distanz eines jeden punktes mit jedem anderen, zaehlversion
  """
  listofdist = []
  mygenerator = range(len(listofp))
  #mygenerator = range(10)
  for j in range(len(listofp)):
    #print(p)
    for i in (mygenerator):
      #print(p.distanceToPoint(listofp[i]))
      listofdist.append(listofp[j].distanceToPoint(listofp[i]))
  #print sorted(listofdist)
  for d in sorted(listofdist):
    print(str(d*1000) + ' mm')
  print('Es sind ', len(listofdist), 'Punktvergleiche berechnet wurden')

  
  
def printe_volumen(listofpf):
  """liste die volumina aller shape objekte
  """
  vol = 0
  for i in range(len(listofpf)):
    print(listofpf[i].Name, '   ', listofpf[i].Shape.Volume)
    vol += listofpf[i].Shape.Volume
  print('Volumen ist:', vol)

  
################################################################################################  
################################################################################################  
################################################################################################  
if __name__ == '__main__':
  

  import FreeCAD
  
  # ablauf in pythonkonsole
  objs = FreeCAD.ActiveDocument.Objects
  print(objs)
  x.printe_objectlist(objs)     # x weil    import versuch as x
  # finde ein archshape  im testbeispiel 1
  nr=1
  print(objs[nr], objs[nr].Name, objs[nr].Name, objs[nr].PropertiesList)
  archpfsl  = x.get_partfeaturelist(objs, nr)
  pointsl   = x.get_pointlist(archpfs)
  shapeverl = x.get_shapevertexlist(archpfs)
  vertexesl = x.get_vertexlist(archpfs)
  dists     = x.get_pointvergleich(pointsl)
  x.make_pointvergleich(pointsl)
  x.pvergleich_variante(pointsl)
  x.printe_volumen(archpfs)
  
  
  
  
  
  #pathonbrowser in python
  App.ActiveDocument.IfcSlab.Shape.ShapeType
  App.ActiveDocument.IfcSlab.Shape.Vertexes
  App.ActiveDocument.IfcSlab.Shape.Edges
  App.ActiveDocument.IfcSlab.Shape.Volume
  
  v = App.ActiveDocument.IfcSlab.Shape.Vertexes
  type(v[0].Point)
  v[0].Point
  v[1].Point
  v[0].Point + v[1].Point
  v[0].Point - v[1].Point
  
  
  V1 = Base.Vector(0,10,0)
  V2 = Base.Vector(30,10,0)
  V1 + V2
  
  
  # problem auch die goupobjekte gehoeren zu den objekten
  # dieseh haben keine volumen und keine vertieces

  # List all objects
  App.ActiveDocument=App.getDocument("Unnamed")
  doc = FreeCAD.ActiveDocument

  objs = FreeCAD.ActiveDocument.Objects
  print objs
  print len(FreeCAD.ActiveDocument.Objects)
  
  #doc.removeObject("Ortho") # Clears the designated object

  
  # You can easily explore the topological data structure
  import Part
  b = Part.makeBox(100,100,100)
  b.Wires
  w = b.Wires[0] 
  w
  w.Wires
  w.Vertexes
  Part.show(w)
  w.Edges
  e = w.Edges[0]
  e.Vertexes
  v = e.Vertexes[0]
  v.Point

  
  
  
  
  
