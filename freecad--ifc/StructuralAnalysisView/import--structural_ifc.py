# ifc scripint fuer structural ifc

import ifcopenshell
#ifc_file = ifcopenshell.open("/home/hugo/Documents/projekte--BIM--openwork/freecad--ifc/E1--3-Axis-Struc-Export.ifc")
ifc_file = ifcopenshell.open("/home/hugo/Documents/projekte--BIM--openwork/freecad--fem/IfcStructuralAnalysisView/2D-elemente/E1--3-Axis-Struc-Export.ifc")



# IfcRelAssignsToGroup
for e in ifc_file.by_type("IfcRelAssignsToGroup"):
    print e
    
#1602=IfcRelAssignsToGroup('1FbeFnPSb61x19nBgVR97U',#6,$,$,(#183,#189,#193,#212,#218,#222,#241,#247,#251,#270,#276,#280,#1549,#1560,#1571,#1582,#1593),.PRODUCT.,#1601)
#1635=IfcRelAssignsToGroup('3NA4d4$_n3_flnQw9rmuDl',#6,$,$,(#1599),.NOTDEFINED.,#1594)


ifc_file.by_type("IfcStructuralAnalysisModel")


f = ifc_file
myid = 1602
# print f.by_id(myid).attribute_name(0)  # use a for loop for all names !
end = len(f.by_id(myid))
# print end
for nr, an in enumerate(f.by_id(myid)):
    print f.by_id(myid).attribute_name(nr), ' --> ', f.by_id(myid).attribute_type(nr)
    if nr == end-1: break

'''
GlobalId            -->  STRING
OwnerHistory        -->  ENTITY INSTANCE
Name                -->  STRING
Description         -->  STRING
RelatedObjects      -->  AGGREGATE OF ENTITY INSTANCE
RelatedObjectsType  -->  ENUMERATION
RelatingGroup       -->  ENTITY INSTANCE
'''

groups = {} # { host:[child,...], ... }
for r in ifc_file.by_type("IfcRelAssignsToGroup"):
            groups.setdefault(r.RelatingGroup.id(),[]).extend([e.id() for e in r.RelatedObjects])

groups


#########################################################################################
#1602 = IFCRELASSIGNSTOGROUP ('1FbeFnPSb61x19nBgVR97U', #6, $, $, (#183, #189, #193, #212, #218, #222, #241, #247, #251, #270, #276, #280, #1549, #1560, #1571, #1582, #1593),.PRODUCT., #1601);

# stuetzen connections 4x 2 connections = 8
#183 = IFCSTRUCTURALPOINTCONNECTION ('2NTXd1HUz3cfnqgh2IWeCI', #6, $, $, $, $, #181, #182);
#189 = IFCSTRUCTURALPOINTCONNECTION ('23XadHR5H9rvTVkP9EY_db', #6, $, $, $, $, #187, #188);
#212 = IFCSTRUCTURALPOINTCONNECTION ('0rOEN$Cbr6nRcrzTqPRoya', #6, $, $, $, $, #210, #211);
#218 = IFCSTRUCTURALPOINTCONNECTION ('3oDazJ9$1C5PCSAjSROYHx', #6, $, $, $, $, #216, #217);
#241 = IFCSTRUCTURALPOINTCONNECTION ('1JYlgmZir46hWOuZlecqIE', #6, $, $, $, $, #239, #240);
#247 = IFCSTRUCTURALPOINTCONNECTION ('0$zQNv8Tb5zB2UO28u1bdm', #6, $, $, $, $, #245, #246);
#270 = IFCSTRUCTURALPOINTCONNECTION ('0bDRREAVXEjPBfQkLOQUTQ', #6, $, $, $, $, #268, #269);
#276 = IFCSTRUCTURALPOINTCONNECTION ('2raRpbitL4Fh9ZXKiFfvl0', #6, $, $, $, $, #274, #275);

# plattenconnection 4x 1 connections = 4
#1560 = IFCSTRUCTURALPOINTCONNECTION ('33ZeIot$1EcuavAw51P7Wa', #6, $, $, $, #1554, #1558, #1559);
#1571 = IFCSTRUCTURALPOINTCONNECTION ('0ixFnIrMT84O3eAFjlFKAZ', #6, $, $, $, #1565, #1569, #1570);
#1582 = IFCSTRUCTURALPOINTCONNECTION ('3AQf78GZP6vhWq$9dZW0wh', #6, $, $, $, #1576, #1580, #1581);
#1593 = IFCSTRUCTURALPOINTCONNECTION ('0NliugbKbDIBYDJ4KL1IAe', #6, $, $, $, #1587, #1591, #1592);

# member stuetzen
#193 = IFCSTRUCTURALCURVEMEMBER ('2LTtkBE0DDIA9ag9wJ4biM', #6, $, $, $, $, #192, .RIGID_JOINED_MEMBER.);
#222 = IFCSTRUCTURALCURVEMEMBER ('3cn$VQhDnElvcbxe5VXTnM', #6, $, $, $, $, #221, .RIGID_JOINED_MEMBER.);
#251 = IFCSTRUCTURALCURVEMEMBER ('3QNNwOpMn4JPer_DGENKlO', #6, $, $, $, $, #250, .RIGID_JOINED_MEMBER.);
#280 = IFCSTRUCTURALCURVEMEMBER ('1Deicj2$j1KgLm5iwmZnl3', #6, $, $, $, $, #279, .RIGID_JOINED_MEMBER.);

# member platte
#1549 = IFCSTRUCTURALSURFACEMEMBER ('0rOPDKnX1BIPn5jkwhjQ6l', #6, $, $, $, #1546, #1548, .SHELL., 0.25000000);

#1601 = IFCSTRUCTURALANALYSISMODEL ('0bSzJlwIT5nvwLvhlggsL5', #6, $, $, $, .LOADING_3D., $, $, $);

