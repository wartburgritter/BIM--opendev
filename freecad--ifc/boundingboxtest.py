############################################################################
# messure import time of products loop
# http://stackoverflow.com/questions/1557571/how-to-get-time-of-a-python-program-execution

import time
products_start = time.clock()

products_end = time.clock()
products_time = products_end - products_start
print 'products_time: ', products_time

# oder
products_start = time.clock()
print 'products_time: ', time.clock() - products_start


'''
import als Part::Spape, 
AxixVM Tonnendach Arch modell, vaulted example--arc.ifc
in sekunden
gui minimiert:
ohne bb-code               --> 57, 58
mit bb-code, ohne view fit --> 57, 58
mit  bb-code               --> 92, 96

gui offen:
ohne bb-code               --> 117, 118
mit bb-code, ohne view fit --> 117, 118
mit  bb-code               --> 156, 156
'''

# calculation of bb does not take any more time
# drawing the shapes does take lots of time fit gui is open
# view fit takes time eaven if gui is minimized

# vor allem mit dem wissen, dass mausrad funktioniert lohnt sich das nicht
# mhh dann eher alle 10 ode 50 products view fit, ist aber auch ein hack

############################################################################
bb = App.BoundBox();

objects = App.ActiveDocument.findObjects("Part::Feature")
for obj in objects:
   print obj.Shape.BoundBox
   print bb
   print bb.isInside(obj.Shape.BoundBox)
   bb.add( obj.Shape.BoundBox )
   print bb
   print bb.isInside(obj.Shape.BoundBox)
   print '\n'

print bb


############################################################################
# simple box test
import Part

box1 = Part.makeBox(1,1,1)
box2 = Part.makeBox(10,10,10)
box2.Placement.Base = (-5, -5, -5)
Part.show(box1)
Part.show(box2)
App.ActiveDocument.Shape001.ViewObject.Transparency = 75

bb1 = box1.BoundBox
bb2 = box2.BoundBox

bb1
bb2

bb1.isInside(bb2)
bb2.isInside(bb1)




############################################################################
# ohne verschieben der einen box
import Part
box1 = Part.makeBox(1,1,1)
box2 = Part.makeBox(10,10,10)
Part.show(box1)
Part.show(box2)
App.ActiveDocument.Shape001.ViewObject.Transparency = 75

bb1 = box1.BoundBox
bb2 = box2.BoundBox

bb1
bb2

bb1.isInside(bb2)
bb2.isInside(bb1)

