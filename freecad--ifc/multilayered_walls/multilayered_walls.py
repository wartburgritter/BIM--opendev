
import ifcopenshell
from ifcopenshell import geom
settings = ifcopenshell.geom.settings()
settings.set(settings.USE_BREP_DATA,True)
settings.set(settings.SEW_SHELLS,True)
settings.set(settings.USE_WORLD_COORDS,True)
settings.set(settings.INCLUDE_CURVES,False)     # for stuct
settings.set(settings.APPLY_LAYERSETS,True)     # for multiple layer walls


def get_shape(entity):
    import Part
    cr = ifcopenshell.geom.create_shape(settings, entity)
    brep = cr.geometry.brep_data
    shape = Part.Shape()
    shape.importBrepFromString(brep)
    Part.show(shape)

# open file and check if the file was load by IFCPERSON
file_path = '/home/hugo/Documents/projekte--BIM--opendev/freecad--ifc/multilayered_walls/'
# file_path = 'C:\\Users\\bhb\\Desktop\\statisches--ifc\\allplan-bug\\'
f = ifcopenshell.open(file_path + 'Allplan--mulitlayer_Wall--no_Window.ifc')
f.by_type('IfcPerson')

get_shape(f.by_id(194))
