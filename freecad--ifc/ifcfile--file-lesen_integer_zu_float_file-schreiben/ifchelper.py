
def ifchelp_ifccartesianpoint(l):
  """IFCCARTESIANPOINT
  
  """
  if l.find('IFCCARTESIANPOINT') > 0:
    #print l
    tmp1 = l.split('=')
    #print tmp1[0]
    #print tmp1[1]
    if tmp1[1].startswith('IFCCARTESIANPOINT'):
      #print tmp1[1]
      tmp2 = tmp1[1].lstrip('IFCCARTESIANPOINT((')
      tmp2 = tmp2.rstrip('));')
      #print tmp2
      tmp3 = tmp2.split(',')
      #print tmp3
      tmp3[0] = str(float(tmp3[0])).rstrip('0')
      tmp3[1] = str(float(tmp3[1])).rstrip('0')
      tmp3[2] = str(float(tmp3[2])).rstrip('0')
      #print tmp3
    l = (tmp1[0] + '=IFCCARTESIANPOINT((' + tmp3[0] + ',' + tmp3[1] + ',' + tmp3[2] + '));')
    print l
  return l

    
def ifchelp_ifcdirection(l):
  """IFCDIRECTION
  
  """
  if l.find('IFCDIRECTION') > 0:
    #print l
    tmp1 = l.split('=')
    #print tmp1[0]
    #print tmp1[1]
    if tmp1[1].startswith('IFCDIRECTION'):
      #print tmp1[1]
      tmp2 = tmp1[1].lstrip('IFCDIRECTION((')
      tmp2 = tmp2.rstrip('));')
      #print tmp2
      tmp3 = tmp2.split(',')
      #print tmp3
      tmp3[0] = str(float(tmp3[0])).rstrip('0')
      tmp3[1] = str(float(tmp3[1])).rstrip('0')
      tmp3[2] = str(float(tmp3[2])).rstrip('0')
      #print tmp3
    l = (tmp1[0] + '=IFCDIRECTION((' + tmp3[0] + ',' + tmp3[1] + ',' + tmp3[2] + '));')
    print l
  return l
    
    
def ifchelp_ifcextrudedareasolid(l):
  """IFCEXTRUDEDAREASOLID
  
  """
  if l.find('IFCEXTRUDEDAREASOLID') > 0:
    #print l
    tmp1 = l.split('=')
    #print tmp1[0]
    #print tmp1[1]
    if tmp1[1].startswith('IFCEXTRUDEDAREASOLID'):
      #print tmp1[1]
      tmp2 = tmp1[1].lstrip('IFCEXTRUDEDAREASOLID(')
      tmp2 = tmp2.rstrip(');')
      #print tmp2
      tmp3 = tmp2.split(',')
      #print len(tmp3), tmp3
      tmp3[3] = str(float(tmp3[3])).rstrip('0')
    l = (tmp1[0] + '=IFCEXTRUDEDAREASOLID(' + tmp3[0] + ',' + tmp3[1] + ',' + tmp3[2] + ',' + tmp3[3] + ');')
    print l
  return l


    
def ifchelp_ifcwindow(l):
  """IFCWINDOW
  
  """
  if l.find('IFCWINDOW') > 0:
    #print l
    tmp1 = l.split('=')
    #print tmp1[0]
    #print tmp1[1]
    if tmp1[1].startswith('IFCWINDOW'):
      #print tmp1[1]
      tmp2 = tmp1[1].lstrip('IFCWINDOW(')
      tmp2 = tmp2.rstrip(');')
      #print tmp2
      tmp3 = tmp2.split(',')
      #print len(tmp3), tmp3
      tmp3[8] = str(float(tmp3[8])).rstrip('0')
      tmp3[9] = str(float(tmp3[9])).rstrip('0')
    l = (tmp1[0]+'=IFCWINDOW('+tmp3[0]+','+tmp3[1]+','+tmp3[2]+','+tmp3[3]+','+tmp3[4]+','+tmp3[5]+','+tmp3[6]+','+tmp3[7]+','+tmp3[8]+','+tmp3[9]+');')
    print l
  return l

  
  
def ifchelp_ifcgeometricrepresentationcontext(l):
  """IFCGEOMETRICREPRESENTATIONCONTEXT
  
  """
  if l.find('IFCGEOMETRICREPRESENTATIONCONTEXT') > 0:
    #print l
    tmp1 = l.split('=')
    #print tmp1[0]
    #print tmp1[1]
    if tmp1[1].startswith('IFCGEOMETRICREPRESENTATIONCONTEXT'):
      #print tmp1[1]
      tmp2 = tmp1[1].lstrip('IFCGEOMETRICREPRESENTATIONCONTEXT(')
      tmp2 = tmp2.rstrip(');')
      #print tmp2
      tmp3 = tmp2.split(',')
      #print len(tmp3), tmp3
      #print tmp3[3]
      tmp3[3] = '1.0E-05'
      #print tmp3[3]
    l = (tmp1[0] + '=IFCGEOMETRICREPRESENTATIONCONTEXT(' + tmp3[0] + ',' + tmp3[1] + ',' + tmp3[2] + ',' + tmp3[3] + ',' + tmp3[4] + ',' + tmp3[5] + ');')
    print l
  return l

    
###############################################################################
if __name__ == '__main__':

  infile = 'testhouse-ohne_hor_extrus--integer.ifc'
  #infile = 'testhouse--mit_hor_extrus--integer.ifc'
  #infile = 'exportfreecadtest4.ifc'
  rf = open(infile, 'r')

  outfile = 'exporttesthelp4.ifc'
  outfile = 'test.ifc'
  wf = open(outfile, 'w')

  for l in rf:
    l = l.strip()
    #print l

    l = ifchelp_ifcgeometricrepresentationcontext(l)
    # sets 1.0E-5 !!!!!!!!!!!!!!!!!!!!!!
    
    l = ifchelp_ifccartesianpoint(l)
    l = ifchelp_ifcdirection(l)
    l = ifchelp_ifcextrudedareasolid(l)
    l = ifchelp_ifcwindow(l)
    
    
    # write lines to file
    #print l
    wf.write(l+'\n')
  
  
  rf.close
  wf.close
  
  