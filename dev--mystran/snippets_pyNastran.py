"""
https://pynastran-git.readthedocs.io/en/latest/quick_start/bdf_overview.html
http://www2.me.rochester.edu/courses/ME204/nx_help/index.html#uid:index_QRG:id518516:id507676

"""


# ************************************************************************************************
from platform import system as osis 
if osis() == "Linux":
    bdf_in = "/home/hugo/Documents/projekte--opendev_bim/dev--mystran/plate/plate.bdf"
    bdf_out = "/home/hugo/Documents/projekte--opendev_bim/dev--mystran/plate/plate_pyNastran_frombdf.bdf"
elif osis() == "Windows":
    bdf_in = "C:/Users/BHA/Desktop/plate.bdf"
    bdf_out = "C:/Users/BHA/Desktop/plate_pyNastran_frombdf.bdf"


from pyNastran.bdf.bdf import BDF
model = BDF()
model.read_bdf(bdf_in)
model.write_bdf(bdf_out)


print(model.get_bdf_stats())


# ************************************************************************************************
"""
>>> print(model.get_bdf_stats())
---BDF Statistics---
SOL 101

bdf.load_combinations[2]
  LOAD:    1

bdf.load_combinations[3]
  LOAD:    1

bdf.load_combinations[4]
  LOAD:    1

bdf.load_combinations[5]
  LOAD:    1

bdf.load_combinations[6]
  LOAD:    1

bdf.load_combinations[7]
  LOAD:    1

bdf.loads[1]
  FORCE:   6

bdf.loads[10]
  PLOAD4:  1

bdf.loads[11]
  PLOAD4:  1

bdf.spcadds[2]
  SPCADD:  1

bdf.spcs[1]
  SPC1:    1

bdf.params
  PARAM    : 2

bdf.nodes
  GRID     : 36

bdf.elements
  CQUAD4   : 25

bdf.properties
  PSHELL   : 1

bdf.materials
  MAT1     : 1
"""


model.case_control_deck


for p in model.params:
    model.params[p]

len(model.params)
print(model.params["POST"].get_stats())
print(model.params["PRTMAXIM"].get_stats())


for e in model.elements:
    model.elements[e]

len(model.elements)
print(model.elements[1].get_stats())


for lc in model.load_combinations:
    model.load_combinations[lc]

len(model.load_combinations)
print(model.load_combinations[2][0].get_stats())


for l in model.loads:
    model.loads[l]

len(model.loads)
print(model.loads[1][0].get_stats())
print(model.loads[10][0].get_stats())
print(model.loads[11][0].get_stats())


for m in model.materials:
    model.materials[m]

len(model.materials)
print(model.materials[1].get_stats())


for p in model.properties:
    model.properties[p]

len(model.properties)
print(model.properties[1].get_stats())


for p in model.spcs:
    model.spcs[p]

len(model.spcs)
print(model.spcs[1][0].get_stats())


for p in model.spcadds:
    model.spcadds[p]

len(model.spcadds)
print(model.spcadds[2][0].get_stats())
