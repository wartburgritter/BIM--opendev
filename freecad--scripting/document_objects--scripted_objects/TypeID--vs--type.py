# box per gui ...

App.ActiveDocument.Box.Shape.TypeId
# 'Part::TopoShape'

# vs.

type(App.ActiveDocument.Box.Shape)
#<type 'Part.Solid'>

App.ActiveDocument.Box.TypeId
# 'Part::Box'

# vs.

type(App.ActiveDocument.Box)
# <type 'Part.Feature'>

##############################################
import Part
box = Part.makeBox(1,1,1)
boxobj = App.ActiveDocument.addObject("Part::Feature", "mybox")
boxobj.Shape = box

boxobj.Shape.TypeId
# 'Part::TopoShape'

type(boxobj.Shape)
# <type 'Part.Solid'>

boxobj.TypeId
# 'Part::Feature'

type(boxobj)
# <type 'Part.Feature'>
