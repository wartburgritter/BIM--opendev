#################################################################################################################
# simple scripted object class definition:
class MyFeaturePython:
    def __init__(self, obj):
        "'''Add a custom property to the FeaturePython'''"
        obj.addProperty("App::PropertyBool","MySwitch","Base","ToolTipSwitchMyFeaturePython")
        obj.MySwitch = False
        obj.Proxy = self

    def execute(self, fp):
        "'''Do something when doing a recomputation, this method is mandatory'''"
        FreeCAD.Console.PrintMessage("Recompute MyFeaturePython\n")


class _ViewProviderMyFeaturePython:
    "A View Provider for the MyFeaturePython object"

    def __init__(self, vobj):
        vobj.Proxy = self

    def attach(self, vobj):
        self.standard = coin.SoGroup()
        vobj.addDisplayMode(self.standard,"Standard");

    def getDisplayModes(self,obj):
        "'''Return a list of display modes.'''"
        return ["Standard"]

    def getDefaultDisplayMode(self):
        "'''Return the name of the default display mode. It must be defined in getDisplayModes.'''"
        return "Standard"

    def doubleClicked(self,vobj):
        print 'YEAH, the view provider works'


# create some objects
from pivy import coin
FreeCAD.newDocument()

myobjPart = FreeCAD.ActiveDocument.addObject("Part::FeaturePython","MyObject")
MyFeaturePython(myobjPart)
_ViewProviderMyFeaturePython(myobjPart.ViewObject)

myobjApp = FreeCAD.ActiveDocument.addObject("App::FeaturePython","MyObject")
MyFeaturePython(myobjApp)
_ViewProviderMyFeaturePython(myobjApp.ViewObject)

# recompute
FreeCAD.ActiveDocument.recompute()




#################################################################################################################
#################################################################################################################
# in dem file im FreeCAD source code sollten an einigen stellen eine leerzeile sein, wegen copy pase in FreeCAD console
