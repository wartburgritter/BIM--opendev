#################################################################################################################
# simple scripted object class definition:
class MyFeaturePython:
    def __init__(self, obj):
        obj.addProperty("App::PropertyBool","MySwitch","Base","ToolTipSwitchMyFeaturePython")
        obj.MySwitch = False
        obj.Proxy = self

    def execute(self, fp):
        FreeCAD.Console.PrintMessage("Recompute MyFeaturePython\n")


class _ViewProviderMyFeaturePython:
    def __init__(self, vobj):
        vobj.Proxy = self

    def attach(self, vobj):
        self.ViewObject = vobj
        self.Object = vobj.Object
        self.standard = coin.SoGroup()
        vobj.addDisplayMode(self.standard,"Standard");

    def getDisplayModes(self,obj):
        return ["Standard"]

    def getDefaultDisplayMode(self):
        return "Standard"

    def setEdit(self, vobj, mode = 0):
        taskd = _FemShellThicknessTaskPanel(self.Object)
        taskd.obj = vobj.Object
        #taskd.update()                          # When would this be needed ?
        FreeCADGui.Control.showDialog(taskd)
        return True

    def unsetEdit(self, vobj, mode = 0):
        print 'unset'
        FreeCADGui.Control.closeDialog()
        return

    def doubleClicked(self,vobj):
        self.setEdit(vobj)


class _FemShellThicknessTaskPanel:
    def __init__(self, obj):
        FreeCADGui.Selection.clearSelection()
        self.sel_server = None
        self.obj = obj
        #self.form = FreeCADGui.PySideUic.loadUi(FreeCAD.getHomePath() + "Mod/Fem/MechanicalMaterial.ui")

        self.form = QtGui.QWidget()
        self.form.setObjectName("TaskPanel")
        self.addButton = QtGui.QPushButton(self.form)
        self.addButton.setText("PushButton")

    def accept(self):
        print 'accept and resetEdit'
        FreeCADGui.ActiveDocument.resetEdit()

    def reject(self):
        print 'reject and resetEdit'
        FreeCADGui.ActiveDocument.resetEdit()




# create an objects
from PySide import QtCore, QtGui
from pivy import coin
FreeCAD.newDocument()


myobjApp = FreeCAD.ActiveDocument.addObject("App::FeaturePython","MyObject")
MyFeaturePython(myobjApp)
_ViewProviderMyFeaturePython(myobjApp.ViewObject)

# recompute
FreeCAD.ActiveDocument.recompute()



