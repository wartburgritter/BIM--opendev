# druckt alle supportedTypes aller geladenen module

# es fehlen die PythonObjekte --> siehe Arch, Draft, Fem

App.ActiveDocument.supportedTypes()

import Part
import Drawing
import Mesh
import Fem
# ...

################################################################


for t in sorted(App.ActiveDocument.supportedTypes()):
    print t


################################################################
App.newDocument()
module = 'Fem'
FreeCADGui.doCommand('import ' + module)
for s in sorted(App.ActiveDocument.supportedTypes()):
    if s.startswith(module):
        print s


'''
Fem::Constraint
Fem::ConstraintBearing
Fem::ConstraintFixed
Fem::ConstraintForce
Fem::ConstraintGear
Fem::ConstraintPressure
Fem::ConstraintPulley
Fem::FemAnalysis
Fem::FemAnalysisPython
Fem::FemMeshObject
Fem::FemMeshShapeNetgenObject
Fem::FemMeshShapeObject
Fem::FemResultObject
Fem::FemResultObjectPython
Fem::FemSetElementsObject
Fem::FemSetFacesObject
Fem::FemSetGeometryObject
Fem::FemSetNodesObject
Fem::FemSetObject
'''



################################################################
module = 'Part'
FreeCADGui.doCommand('import ' + module)
for s in sorted(App.ActiveDocument.supportedTypes()):
    if s.startswith(module):
        print s

'''
Part::Boolean
Part::Box
Part::Chamfer
Part::Circle
Part::Common
Part::Compound
Part::Cone
Part::CurveNet
Part::CustomFeature
Part::CustomFeaturePython
Part::Cut
Part::Cylinder
Part::Ellipse
Part::Ellipsoid
Part::Extrusion
Part::Feature
Part::FeatureExt
Part::FeatureGeometrySet
Part::FeaturePython
Part::Fillet
Part::FilletBase
Part::Fuse
Part::Helix
Part::ImportBrep
Part::ImportIges
Part::ImportStep
Part::Line
Part::Loft
Part::Mirroring
Part::MultiCommon
Part::MultiFuse
Part::Offset
Part::Part2DObject
Part::Part2DObjectPython
Part::Plane
Part::Polygon
Part::Primitive
Part::Prism
Part::RegularPolygon
Part::Revolution
Part::RuledSurface
Part::Section
Part::Sphere
Part::Spiral
Part::Spline
Part::Sweep
Part::Thickness
Part::Torus
Part::Vertex
Part::Wedge
'''
