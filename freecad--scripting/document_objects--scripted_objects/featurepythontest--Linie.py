#  src/Mod/TemplatePyMod/FeaturePython.py

class Line:
    def __init__(self, obj):
        ''' Add two point properties '''
        obj.addProperty("App::PropertyBool","MySwitch","Base","ToolTipSwitchMyFeaturePython").MySwitch = False
        obj.Proxy = self

    def execute(self, fp):
        ''' Print a short message when doing a recomputation, this method is mandatory '''
        #fp.Shape = Part.makeLine(fp.p1,fp.p2)
        FreeCAD.Console.PrintMessage("Recompute MyFeaturePython\n")


class ViewProviderLine:
    def __init__(self, obj):
        ''' Set this object to the proxy object of the actual view provider '''
        obj.Proxy = self

    def getDefaultDisplayMode(self):
        ''' Return the name of the default display mode. It must be defined in getDisplayModes. '''
        return "Flat Lines"

def makeLine():
    FreeCAD.newDocument()
    a=FreeCAD.ActiveDocument.addObject("Part::FeaturePython","Line")
    Line(a)
    #ViewProviderLine(a.ViewObject)
    a.ViewObject.Proxy=0 # just set it to something different from None




#######################################
# start FreeCAD
makeLine()
FreeCAD.ActiveDocument.recompute()

