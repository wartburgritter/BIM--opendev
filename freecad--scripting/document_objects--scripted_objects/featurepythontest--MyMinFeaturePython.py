# src/Mod/TemplatePyMod/FeaturePython.py

class MyFeaturePython:
    def __init__(self, obj):
        "'''Add a custom property to the FeaturePython'''"
        obj.addProperty("App::PropertyBool","MySwitch","Base","ToolTipSwitchMyFeaturePython")
        obj.MySwitch = False
        obj.Proxy = self

    def execute(self, fp):
        "'''Do something when doing a recomputation, this method is mandatory'''"
        FreeCAD.Console.PrintMessage("Recompute MyFeaturePython\n")



FreeCAD.newDocument()

myobjPart = FreeCAD.ActiveDocument.addObject("Part::FeaturePython","MyObject")  # ok
MyFeaturePython(myobjPart)
myobjPart.ViewObject.Proxy = 0

myobjApp = FreeCAD.ActiveDocument.addObject("App::FeaturePython","MyObject")    # grey ViewProvider
MyFeaturePython(myobjApp)
myobjApp.ViewObject.Proxy = 0


# recompute
FreeCAD.ActiveDocument.recompute()



# siehe FemElement mit Part::FeaturePython ist icon coloured and edit on double click works !

