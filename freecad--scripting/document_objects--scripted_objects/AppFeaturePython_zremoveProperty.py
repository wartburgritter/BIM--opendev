
class A:

    def __init__(self, obj):
        obj.addProperty("App::PropertyString", "a", "Base", "Prop a" )
        obj.Proxy = self

    def execute(self, obj):
        return


class B(A):

    def __init__(self, obj):
        A.__init__(self, obj)
        obj.a = 'Value class B'
        obj.addProperty("App::PropertyString", "b", "Base", "Prop b" )
        obj.b = 'Value b'
        obj.Proxy = self

    def execute(self, obj):
        return


class C(A):

    def __init__(self, obj):
        A.__init__(self, obj)
        obj.a = 'Value class C'
        obj.addProperty("App::PropertyFloat", "c", "Base", "Prop c" )
        obj.c = 99
        obj.Proxy = self

    def execute(self, obj):
        return



########################################################################################
myobj = FreeCAD.ActiveDocument.addObject("App::FeaturePython", 'MyNewObject')
myobj.PropertiesList      # ['Label', 'Proxy']
B(myobj)
myobj.PropertiesList      # ['Label', 'Proxy', 'a', 'b']
myobj.a        # u'Value class B'
myobj.b        # u'Value b'


myobj.removeProperty('a')
myobj.removeProperty('b')
C(myobj)
myobj.PropertiesList      # ['Label', 'Proxy', 'a', 'c']
myobj.a        # u'Value class C'
myobj.c        # 99.0





########################################################################################
myobj = FreeCAD.ActiveDocument.addObject("App::FeaturePython", 'MyNewObject')
B(myobj)
myobj.PropertiesList    # ['Label', 'Proxy', 'a', 'b']

App.ActiveDocument.removeObject("MyNewObject")
myobj = FreeCAD.ActiveDocument.addObject("App::FeaturePython", 'MyNewObject')
C(myobj)
myobj.PropertiesList    # ['Label', 'Proxy', 'a', 'c']


App.ActiveDocument.removeObject("MyNewObject")

