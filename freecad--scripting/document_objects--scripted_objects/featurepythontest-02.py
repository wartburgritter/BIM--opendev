class _MyFeaturePython:
    def __init__(self, obj):
        "'''Add some custom properties to our box feature'''"
        obj.addProperty("App::PropertyBool","MySwitch","Base","ToolTipSwitchMyFeaturePython")
        obj.MySwitch = False
        obj.Proxy = self

    def execute(self, fp):
        "'''Do something when doing a recomputation, this method is mandatory'''"
        FreeCAD.Console.PrintMessage("Recompute MyFeaturePython\n")



class _ViewProviderMyFeaturePython:
    "A View Provider for the MyFeaturePython object"

    def __init__(self, vobj):
        vobj.Proxy = self

    def attach(self, vobj):
        self.ViewObject = vobj
        self.Object = vobj.Object

    def __getstate__(self):
        return None

    def __setstate__(self, state):
        return None



FreeCAD.newDocument()
myobj = FreeCAD.ActiveDocument.addObject("Part::FeaturePython","MyObject")
#myobj = FreeCAD.ActiveDocument.addObject("App::FeaturePython","MyObject")
# create instanzes of class _MyFeaturePython and _ViewProviderMyFeaturePython
_MyFeaturePython(myobj)
_ViewProviderMyFeaturePython(myobj.ViewObject)
FreeCAD.ActiveDocument.recompute()





