'''
http://www.freecadweb.org/wiki/index.php?title=Code_snippets#Function_resident_with_the_mouse_click_action
http://forum.freecadweb.org/viewtopic.php?t=4280
http://forum.freecadweb.org/viewtopic.php?t=9317

http://forum.freecadweb.org/viewtopic.php?f=22&t=6229

even better is:
class ConstraintSelectionObserver
https://github.com/hamish2014/FreeCAD_assembly2/blob/master/assembly2lib.py
def parseSelection and Instanz ConstraintSelectionObserver
https://github.com/hamish2014/FreeCAD_assembly2/blob/master/axialConstraint.py

'''

'''
in FreeCAD konsole kopieren!!!

import sys
myaddpath = '/home/hugo/Documents/projekte--BIM--work/freecad/scripting/verschiedene/zdev-observer/'
sys.path.append(myaddpath)
import observer01
observer01.tb()

'''

'''Observer

s = observer01.SelObserver()
FreeCADGui.Selection.addObserver(s)


mittels klicken testen ... und dann Obbserver entfernen !!!
FreeCADGui.Selection.removeObserver(s)
'''

'''Selection Gate

Gui.Selection.addSelectionGate(observer01.Gate())

'''



def tb(): # tb == tesbox fuer myObservertest
    import FreeCAD, FreeCADGui
    myDoc = FreeCAD.newDocument('myObservertest')
    box1 = myDoc.addObject("Part::Box","MyBox1")
    box1.Placement.Base = (0,10,0)
    box2 = myDoc.addObject("Part::Box","MyBox2")
    box2.Placement.Base = (0,20,0)
    box3 = myDoc.addObject("Part::Box","MyBox3")
    box3.Placement.Base = (0,30,0)
    box4 = myDoc.addObject("Part::Box","MyBox4")
    box4.Placement.Base = (0,40,0)
    box5 = myDoc.addObject("Part::Box","MyBox5")
    box5.Placement.Base = (0,50,0)
    myDoc.recompute()
    FreeCADGui.ActiveDocument.ActiveView.viewAxometric()
    FreeCADGui.SendMsgToActiveView("ViewFit")



class SelObserver:
    """my first SelectionObserver.
       Reports some information of selected objects in Report View"""

    def addSelection(self,doc,obj,sub,pnt):
        print "addSelection"
        print '  ', doc, ' --> ', type(doc)
        print '  ', obj, ' --> ', type(obj)
        print '  ', sub, ' --> ', type(sub)
        print '  ', pnt, ' --> ', type(pnt)

        import FreeCAD
        added_obj = FreeCAD.getDocument(doc).getObject(obj)
        #added_sub = FreeCAD.getDocument(doc).getObject(obj).???
        print '  ', added_obj.Name, ' --> ', added_obj.Shape.ShapeType, ' --> ', type(added_obj)
        print '  ', added_obj.Shape.ShapeType, ' --> ', type(added_obj)

        print ""

    def removeSelection(self,doc,obj,sub):
        print "removeSelection"
        print ""

    def clearSelection(self,doc): # nicht add ruft clear auf
        print "clearSelection"
        print ""



class Gate:
  def allow(self,doc,obj,sub):
    return (obj.TypeId == 'Part::Box')





