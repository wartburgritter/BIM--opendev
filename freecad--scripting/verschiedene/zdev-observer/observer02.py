'''
in FreeCAD konsole kopieren!!!

import sys
myaddpath = '/home/hugo/Documents/dev/freecad/'
sys.path.append(myaddpath)
import observer02
observer02.tb()


App.closeDocument('myObservertest')
reload(observer02)
observer02.tb()


p = observer02.Protractor()

'''



import FreeCAD, FreeCADGui
import Part
from FreeCAD import Base
import math


def tb(): # tb == tesbox fuer myObservertest
    import FreeCAD, FreeCADGui
    myDoc = FreeCAD.newDocument('myObservertest')
    box = myDoc.addObject("Part::Box","MyBox")
    myDoc.recompute()
    FreeCADGui.ActiveDocument.ActiveView.viewAxometric()
    FreeCADGui.SendMsgToActiveView("ViewFit")



class Protractor:
    """Reports the angle between two edges.
        Select the edges and the result will be shown in Report View"""
    def __init__(self):
        self.firstEdge = None
        FreeCADGui.Selection.addObserver(self)
        FreeCAD.Console.PrintMessage("Select any 2 edges\n")

    def addSelection(self, doc, obj, sub, pos):
        o = FreeCAD.getDocument(doc).getObject(obj)
        if sub != '' and type(o) == Part.Feature:
            e = getattr(o.Shape, sub)
            if e.ShapeType == 'Edge':
                if self.firstEdge is None:
                    self.firstEdge = e
                else:
                    FreeCADGui.Selection.removeObserver(self)
                    d1 = e.Vertexes[0].Point.sub(e.Vertexes[1].Point)
                    d2 = self.firstEdge.Vertexes[0].Point.sub(self.firstEdge.Vertexes[1].Point)
                    angle = d1.getAngle(d2)
                    FreeCAD.Console.PrintMessage("Angle: %f\n" % math.degrees(angle))

if __name__ == "__main__":
    Protractor()
