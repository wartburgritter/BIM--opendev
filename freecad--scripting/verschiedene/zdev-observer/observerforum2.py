'''
import sys
myaddpath = '/home/hugo/Documents/projekte--BIM--work/freecad/scripting/verschiedene/zdev-observer/'
sys.path.append(myaddpath)
import observerforum2
sel_server = observerforum2.SelectionObserver(observerforum2.selectionParser)


'''
'''
reload(observerforum)
sel_server = observerforum.SelectionObserver(observerforum.selectionParser)

'''
'''
or copy code into console

sel_server = SelectionObserver(selectionParser)
'''



import FreeCAD
if FreeCAD.GuiUp:
    import FreeCADGui
    from PySide import QtGui
    from PySide import QtCore



def selectionParser(selection):
    print 'selectionParser: ', selection, '  ', selection.Name
    # lot more things could be parsed including the change of task widgets


class SelectionObserver:
    """SelectionObserver"""
    def __init__(self, parseSelectionFunction):
        self.parseSelectionFunction = parseSelectionFunction 
        FreeCADGui.Selection.addObserver(self)
        FreeCAD.Console.PrintMessage("Select Objects\n")

    def addSelection(self, docName, objName, sub, pos):
        print "addSelection"
        self.added_obj = FreeCAD.getDocument(docName).getObject(objName)
        self.parseSelectionFunction(self.added_obj)

    def clearSelection(self,doc):
        print "clearSelection"
        #FreeCADGui.Selection.removeObserver(self)



