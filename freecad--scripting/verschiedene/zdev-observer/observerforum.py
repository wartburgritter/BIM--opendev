'''
import sys
myaddpath = '/home/hugo/Documents/projekte--BIM--work/freecad/scripting/verschiedene/zdev-observer/'
sys.path.append(myaddpath)
import observerforum
taskd = observerforum._SolidListTaskPanel()
Gui.Control.showDialog(taskd)

'''
'''
reload(observerforum)
taskd = observerforum._SolidListTaskPanel()
Gui.Control.showDialog(taskd)

'''

import FreeCAD
if FreeCAD.GuiUp:
    import FreeCADGui
    from PySide import QtGui
    from PySide import QtCore



class _SolidListTaskPanel:
    '''A simple task panel to demonstrate a list and a selection observer'''
    def __init__(self):
        self.solids = []
        self.uifile = '/home/hugo/Documents/projekte--BIM--work/freecad/scripting/verschiedene/zdev-observer/observerforum.ui'
        self.form = FreeCADGui.PySideUic.loadUi(self.uifile)
        QtCore.QObject.connect(self.form.buttonAddSolids, QtCore.SIGNAL("clicked()"), self.add_solids)

    def accept(self):
        print 'end'
        for s in self.solids:
            print s.Name
        FreeCADGui.ActiveDocument.resetEdit()
        FreeCADGui.Control.closeDialog()

    def reject(self):
        FreeCADGui.ActiveDocument.resetEdit()
        FreeCADGui.Control.closeDialog()

    def add_solids(self):
        selection = FreeCADGui.Selection.getSelection()
        if len(selection) == 1:
            sel = selection[0]
            self.selectionParser(sel)
        elif len(selection) == 0:
            #print "Nothing selected, ok let's start the SelectionObserver"
            # erzeuge nun instanz der klasse SolidSelectionObserver
            #self.sel_server = SolidSelectionObserver(selectionParserGlob) # modulweiter parser
            self.sel_server = SolidSelectionObserver(self.selectionParser)  # parse widget
            #print self.sel_server
        else:
            print 'If the solid is selected befor the buttonclick, multiple solids aren not supported'
            print 'Klick on the button to start the observer --> multiple solid selecting will hopefully work than'

    def selectionParser(self, sel):
        if hasattr(sel,"Shape"):
            if sel.Shape.ShapeType == 'Solid':
                #print 'found solid: ', sel, '  ', sel.Name
                if sel not in self.solids:
                    self.solids.append(sel)
                    self.rebuild_list_solids()
                else:
                    print sel.Name, ' is allready in reference list!'
        else:
            print 'No shape found!'

    def rebuild_list_solids(self):
        self.form.listSolids.clear()
        items = []
        for i in self.solids:
            items.append(i.Name)
        for listItemName in sorted(items):
            listItem = QtGui.QListWidgetItem(listItemName, self.form.listSolids)



class SolidSelectionObserver:
    """SolidSelectionObserver"""
    def __init__(self, parseSelectionFunction):
        self.parseSelectionFunction = parseSelectionFunction 
        FreeCADGui.Selection.addObserver(self)
        # FreeCAD.Console.PrintMessage("SolidSelectionObserver started ...\n")
        FreeCAD.Console.PrintMessage("Select Solids to add them to the list\n")

    def addSelection(self, docName, objName, sub, pos):
        print "addSelection"
        self.added_obj = FreeCAD.getDocument(docName).getObject(objName)
        #print '  ', self.added_obj.Name, ' --> ', self.added_obj.Shape.ShapeType, ' --> ', type(self.added_obj)
        #print '  ',self.added_obj.Shape.ShapeType, ' --> ', type(self.added_obj)

        self.parseSelectionFunction(self.added_obj)

    # remove the selection server bei klick nicht auf ein objName
    # clear wird bei klick ins leere und bei klick auf obj (vor add) aufgerufen
    def clearSelection(self,doc):
        print "clearSelection"
        #FreeCADGui.Selection.removeObserver(self)
        #FreeCAD.Console.PrintMessage("SolidSelectionObserver removed\n")



def selectionParserGlob(selection):
    print 'selectionParser: ', selection, '  ', selection.Name
