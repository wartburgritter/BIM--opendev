#####################################################################
# BEVOR REGULAERE AUSDRUECKE VERWENDET WERDEN ERST
# DIE METHODE s.find(suchstring) verwenden
# die ist meistens ausreichend
# siehe buch seite 75
#####################################################################

test1 = 'Rebar001'
test2 = 'KeineEisen001'
test1.find('Rebar')
test2.find('Rebar')

if test2.find('Rebar')==0:
  print 'found Rebar'
else:
  print 'no Rebar'


# diese paar zeilen sind meine loesung fur die ich 1,5 stunden gebraucht habe  
testliste = ['Rebar001', 'KeineEisen001']
for s in testliste:
  if s.find('Rebar')==0:
    print s, ' found Rebar'
  else:
    print s, ' no Rebar'

 
# diese paar zeilen sind meine loesung fur die ich 2 stunden gebraucht habe  
# finde die rebarobjekte in freecaddateien
rbl = []
objectList = FreeCAD.ActiveDocument.Objects
for obj in objectList:
  print obj.Name, '_________', obj.Label, '_________',  type(obj)
  #print type(obj.Name)
  # wenn der interne Name den String Rebar enthaelt ist es ein Rebar
  # es gibt bestimmt bessere methoden ueber objecttyp, aber ich weiss nicht wie.
  if obj.Name.find('Rebar')==0:
    print ' found Rebar'
  else:
    print ' no Rebar'

 
 


#####################################################################
# REGULAERE AUSDRUECKE
# buch ab seite 415
#####################################################################

#####################################################################
import re
print(re.match(r"Rebar[0-9]*", 'Rebar001'))
print(re.match(r"Rebar[0-9]*", 'KeineEisen001'))

test1 = 'Rebar001'
test2 = 'KeineEisen001'
print (re.match(r"Rebar[0-9]*", test1))
print (re.match(r"Rebar[0-9]*", test2))


#####################################################################
# zugriff auf die gefundenen strings

# buch S436
# die klammern () im regulaeren Ausdruck markieren die gruppe,
# habe ein klammernpaar also eine Gruppe, zwei bsp fuer zugriff
m = re.match(r"(Rebar[0-9]*)", 'Rebar001')
m.expand(r"\g<1>")
m.group(1)


# nachschlagebuch S166
patt = re.compile('(Rebar[0-9]*)')  # die klammern markieren die gruppe ein klammernpaar also eine Gruppe
mobj = patt.match('Rebar001')
mobj.group(1)


#######################################################################
# die untere methode hat den nachteil, dass die variable group 
# nur existiert, wenn der string gefunden wurde
test1 = 'Rebar001'
test2 = 'KeineEisen001'
m = re.match(r"(Rebar)[0-9]*", test2)
if m.group(1):
  print 'found Rebar'
else:
  print 'no Rebar'




#######################################################################
# funktioniert nicht es kommt immer found rebar ???
test1 = 'Rebar001'
test2 = 'KeineEisen001'
if (re.match(r"Rebar[0-9]*", test2)) == 'None':
  print 'kein rebar'
else:
  print 'found rebar'

# obwohl prinzipiell die if-abfrage mit den strings schon funktioniert 

test3 = 'None'
test4 = 'nix'
if test3 == 'None':
  print 'found None'
else:
  print 'kein None'

  
#######################################################
#######################################################
# mit ausgelagerter funktion
def histogram(iterable):
  result = dict()
  for item in iterable:
    result[item] = result.get(item, 0) + 1
  return result

     
print histogram('das ist ein test')
print histogram([1, 2, 3, 2, 4, 6, 1])


# ohne ausglagerter funktion
result = dict()
iterable = 'das ist ein test'
for item in iterable:
  result[item] = result.get(item, 0) + 1

print result



 
