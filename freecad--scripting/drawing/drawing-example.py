import FreeCAD, Part, Drawing


App.newDocument()


# Create three boxes and a cylinder
App.ActiveDocument.addObject("Part::Box","Box")
App.ActiveDocument.Box.Length=100.00
App.ActiveDocument.Box.Width=100.00
App.ActiveDocument.Box.Height=100.00

App.ActiveDocument.addObject("Part::Box","Box1")
App.ActiveDocument.Box1.Length=90.00
App.ActiveDocument.Box1.Width=40.00
App.ActiveDocument.Box1.Height=100.00

App.ActiveDocument.addObject("Part::Box","Box2")
App.ActiveDocument.Box2.Length=20.00
App.ActiveDocument.Box2.Width=85.00
App.ActiveDocument.Box2.Height=100.00

App.ActiveDocument.addObject("Part::Cylinder","Cylinder")
App.ActiveDocument.Cylinder.Radius=80.00
App.ActiveDocument.Cylinder.Height=100.00
App.ActiveDocument.Cylinder.Angle=360.00
# Fuse two boxes and the cylinder
App.ActiveDocument.addObject("Part::Fuse","Fusion")
App.ActiveDocument.Fusion.Base = App.ActiveDocument.Cylinder
App.ActiveDocument.Fusion.Tool = App.ActiveDocument.Box1

App.ActiveDocument.addObject("Part::Fuse","Fusion1")
App.ActiveDocument.Fusion1.Base = App.ActiveDocument.Box2
App.ActiveDocument.Fusion1.Tool = App.ActiveDocument.Fusion
# Cut the fused shapes from the first box
App.ActiveDocument.addObject("Part::Cut","Shape")
App.ActiveDocument.Shape.Base = App.ActiveDocument.Box 
App.ActiveDocument.Shape.Tool = App.ActiveDocument.Fusion1
# Hide all the intermediate shapes 
Gui.ActiveDocument.Box.Visibility=False
Gui.ActiveDocument.Box1.Visibility=False
Gui.ActiveDocument.Box2.Visibility=False
Gui.ActiveDocument.Cylinder.Visibility=False
Gui.ActiveDocument.Fusion.Visibility=False
Gui.ActiveDocument.Fusion1.Visibility=False

App.ActiveDocument.recompute()
Gui.activeDocument().activeView().viewAxometric()
Gui.SendMsgToActiveView("ViewFit")



###########################################################################
# Generate a Drawing
###########################################################################

#Insert a Page object and assign a template
App.ActiveDocument.addObject('Drawing::FeaturePage','Page')
App.ActiveDocument.Page.Template = App.getResourceDir()+'Mod/Drawing/Templates/A3_Landscape.svg'
# recompute to display the page
App.ActiveDocument.recompute()


#Create a view on the "Shape" object, define the position and scale and assign it to a Page
App.ActiveDocument.addObject('Drawing::FeatureViewPart','View')
App.ActiveDocument.View.Source = App.ActiveDocument.Shape
App.ActiveDocument.View.Direction = (0.0,0.0,1.0)
App.ActiveDocument.View.X = 10.0
App.ActiveDocument.View.Y = 10.0
App.ActiveDocument.Page.addObject(App.ActiveDocument.View)

#Create a second view on the same object but this time the view will be rotated by 90 degrees.
App.ActiveDocument.addObject('Drawing::FeatureViewPart','ViewRot')
App.ActiveDocument.ViewRot.Source = App.ActiveDocument.Shape
App.ActiveDocument.ViewRot.Direction = (0.0,0.0,1.0)
App.ActiveDocument.ViewRot.X = 290.0
App.ActiveDocument.ViewRot.Y = 30.0
App.ActiveDocument.ViewRot.Scale = 1.0
App.ActiveDocument.ViewRot.Rotation = 90.0
App.ActiveDocument.Page.addObject(App.ActiveDocument.ViewRot) 

#Create a third view on the same object but with an isometric view direction. The hidden lines are activated too.
App.ActiveDocument.addObject('Drawing::FeatureViewPart','ViewIso')
App.ActiveDocument.ViewIso.Source = App.ActiveDocument.Shape
App.ActiveDocument.ViewIso.Direction = (1.0,1.0,1.0)
App.ActiveDocument.ViewIso.X = 335.0
App.ActiveDocument.ViewIso.Y = 140.0
App.ActiveDocument.ViewIso.ShowHiddenLines = True
App.ActiveDocument.Page.addObject(App.ActiveDocument.ViewIso) 



################################################
#Accessing the bits and pieces, not necessary needed for drawing
#Get the SVG fragment of a single view
ViewSVG = App.ActiveDocument.ViewIso.ViewResult
print ViewSVG                      # prints the svg-content of the view ViewIso on python konsole

#Get the whole result page (it's a file in the document's temporary directory, only read permission)
print "Resulting SVG document: ",App.ActiveDocument.Page.PageResult
file = open(App.ActiveDocument.Page.PageResult,"r")
print "Result page is ",len(file.readlines())," lines long"

#Important: free the file!
del file
del ViewSVG



# ein wenig Sortieren auf dem Blatt ist angebracht, nach recompute wird page auch angepasst
App.ActiveDocument.View.X = 230.0
App.ActiveDocument.View.Y = 30.0
App.ActiveDocument.View.Scale = 1.5

App.ActiveDocument.ViewRot.Direction = (0.0,0.0,1.0)
App.ActiveDocument.ViewRot.X = 40.0
App.ActiveDocument.ViewRot.Y = 90.0
App.ActiveDocument.ViewRot.Scale = 1.0
App.ActiveDocument.ViewRot.Rotation = 325

App.ActiveDocument.ViewIso.Direction = (-1.0,-1.0,-1.0)
App.ActiveDocument.ViewIso.X = 120
App.ActiveDocument.ViewIso.Y = 220

App.ActiveDocument.recompute()




# fuer eigenen Content svg-code direkt in Datei einfuegen
# svg oeffnen und nach ViewSelf suchen, da steht dann genau das untere

App.ActiveDocument.addObject('Drawing::FeatureView','ViewSelf')
App.ActiveDocument.ViewSelf.ViewResult = """<g id="ViewSelf"
  stroke="rgb(0, 0, 0)"
  stroke-width="0.35"
  stroke-linecap="butt"
  stroke-linejoin="miter"
  transform="translate(30,30)"
  fill="#00cc00"
  >
  
  <ellipse cx="100" cy="100" rx="30" ry="15"/>
   </g>"""
App.ActiveDocument.Page.addObject(App.ActiveDocument.ViewSelf)
App.ActiveDocument.recompute()




# ViewSelf anpassen
App.ActiveDocument.ViewSelf.ViewResult = """<g id="ViewSelf"
  stroke="rgb(250, 200, 80)"
  stroke-width="5"
  stroke-linecap="butt"
  stroke-linejoin="miter"
  transform="translate(30,30)"
  fill="#00cc00"
  >
  
  <ellipse cx="200" cy="100" rx="30" ry="15"/>
    </g>"""
App.ActiveDocument.recompute()








###########################################################################

##Insert a Page object and assign a template
#App.ActiveDocument.addObject('Drawing::FeaturePage','Page')
#App.ActiveDocument.Page.Template = App.getResourceDir()+'Mod/Drawing/Templates/A4_Simple.svg'
## recompute to display the page
#App.ActiveDocument.recompute()


##App.ActiveDocument.addObject('Drawing::FeatureView','ViewSelf')
##App.ActiveDocument.ViewSelf.ViewResult = """<g id="ViewSelf"
    ##>
    ##<text
       ##xml:space="preserve"
       ##style="font-size:12px;font-style:normal;font-variant:normal;font-weight:300;font-stretch:condensed;text-align:start;line-height:125%;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;font-family:Comfortaa;-inkscape-font-specification:Comfortaa Light Condensed"
       ##x="25"
       ##y="35"
       ##id="text3411"
       ##sodipodi:linespacing="125%"><tspan
         ##sodipodi:role="line"
         ##id="tspan3413"
         ##x="25"
         ##y="35">hallo dies ist ein test</tspan></text>
    ##</g>"""
##App.ActiveDocument.Page.addObject(App.ActiveDocument.ViewSelf)
##App.ActiveDocument.recompute()



#App.ActiveDocument.addObject('Drawing::FeatureView','ViewSelf')
#App.ActiveDocument.ViewSelf.ViewResult = """<g id="ViewSelf"
    #>
    #<text
       #style="font-size:12px;font-style:normal;font-variant:normal;font-weight:300;font-stretch:condensed;text-align:start;line-height:125%;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;font-family:Comfortaa;-inkscape-font-specification:Comfortaa Light Condensed"
       #x="25"
       #y="35"
       #>hallo dies ist ein test</text>
    #</g>"""
#App.ActiveDocument.Page.addObject(App.ActiveDocument.ViewSelf)
#App.ActiveDocument.recompute()
#ViewSVG = App.ActiveDocument.ViewSelf.ViewResult
#print ViewSVG                      # prints the svg-content of the view ViewIso on python konsole
#del ViewSVG



## aendern stil ist vorhanden, oberes wird kommple ersetzt, daher zum testen gut, braucht page geloescht werden.
#App.ActiveDocument.ViewSelf.ViewResult = """<g id="ViewSelf"
    #>
    #<text
      #style="font-size:12px;font-style:normal;font-variant:normal;font-weight:300;font-stretch:condensed;text-align:start;line-height:125%;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill:#000000;fill-opacity:1;stroke:none;font-family:Comfortaa;-inkscape-font-specification:Comfortaa Light Condensed"
      #x="25"
       #y="35"
       #>hallo dies ist ein test und noch mehr geblubber</text>
    #</g>"""
#App.ActiveDocument.recompute()







###########################################################################
## text mit standardeigenschaften

#App.ActiveDocument.addObject('Drawing::FeaturePage','Page')
#App.ActiveDocument.Page.Template = App.getResourceDir()+'Mod/Drawing/Templates/A4_Simple.svg'
#App.ActiveDocument.recompute()
#App.ActiveDocument.addObject('Drawing::FeatureView','ViewSelf')
#App.ActiveDocument.ViewSelf.ViewResult = """<g id="ViewSelf"
    #>
    #<text
       #x="25"
       #y="35"
       #>hallo text</text>
    #</g>"""
#App.ActiveDocument.Page.addObject(App.ActiveDocument.ViewSelf)
#App.ActiveDocument.recompute()


## text aendern
#App.ActiveDocument.ViewSelf.ViewResult = """<g id="ViewSelf">
     #<text x="15" y="135">Text 1</text>
     #<text x="25" y="155">Text 2</text>
     #<text x="35" y="175">Text 3</text>
  #</g>"""
#App.ActiveDocument.Page.addObject(App.ActiveDocument.ViewSelf)
#App.ActiveDocument.recompute()


## fragt sichnur wie ich den obigen string genau so erstelle??!!






