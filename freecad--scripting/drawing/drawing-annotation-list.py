import FreeCAD, Drawing

myDoc = App.newDocument()

page1 = myDoc.addObject('Drawing::FeaturePage','Page')
App.ActiveDocument.Page.Template = App.getResourceDir()+'Mod/Drawing/Templates/A4_Simple.svg'
myDoc.recompute()    # wenn jetzt schon recompute dann erscheint weisses Blatt

text1 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_1')
myDoc.Page.addObject(myDoc.Text_1)
text2 = myDoc.addObject('Drawing::FeatureViewAnnotation','Text_2')
myDoc.Page.addObject(myDoc.Text_2)

text1.X = 30
text1.Y = 30
text1.Scale = 8
myText1 = ['text1', 'text2', 'text3', 'text4', 'text5', 'text6', 'text7']
text1.Text = (myText1)
text2.X = 30
text2.Y = 40
text2.Scale = 12
myText2 = ['HALLO1', 'HALLO2', 'HALLO3', 'HALLO4', 'HALLO5', 'HALLO6', 'HALLO7']
text2.Text = (myText2)
myDoc.recompute()
