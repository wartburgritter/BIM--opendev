# -*- coding: utf-8 -*-
# liste et sauve tous les objets du document et leurs coordonnées
# list and saves all objects of the document and their coordinates xyz

import Draft, Part, FreeCAD
from PyQt4 import QtGui ,QtCore
from PyQt4.QtGui import *
from PyQt4.QtCore import *

def iso8859(encoder):
    return unicode(encoder).encode('iso-8859-1')
def errorDialog(msg):
    diag = QtGui.QMessageBox(QtGui.QMessageBox.Critical,u"Error Message",msg )
    diag.setWindowModality(QtCore.Qt.ApplicationModal)
    diag.exec_()

path = FreeCAD.ConfigGet("AppHomePath")
#path = FreeCAD.ConfigGet("UserAppData")

SaveName = ""
SaveName = QFileDialog.getSaveFileName(None,QString.fromLocal8Bit("Sauver un fichier Info"),path,"*.txt")
if SaveName == "":
    App.Console.PrintMessage("Procédure abandonnée"+"\n")
    errorDialog(u"Procédure abandonnée")
else:
    App.Console.PrintMessage("Enregistrement de "+SaveName+"\n")
    try:
        f = open(SaveName, 'w') # write
#        f.write(iso8859(u"Info "+"\n"))
        f.write(iso8859(str(App.ActiveDocument.Name)+"\n"))
        for obj in App.ActiveDocument.Objects:
            print obj.Label               # Name object
            f.write(iso8859(str(obj.Label))+"\n")   # Save Name object
            try:
                for j in enumerate(obj.Shape.Edges):
                    try:
                        print "X: ",j[1].Vertexes[0].Point.x," ",
                        print "Y: ",j[1].Vertexes[0].Point.y," ",
                        print "Z: ",j[1].Vertexes[0].Point.z," ",
                        print
                        try:
                            f.write("X ,"+iso8859(str(j[1].Vertexes[0].Point.x)+","))
                        except:
                            None
                        try:
                            f.write("Y ,"+iso8859(str(j[1].Vertexes[0].Point.y)+","))
                        except:
                            None
                        try:
                            f.write("Z ,"+iso8859(str(j[1].Vertexes[0].Point.z)+","))
                        except:
                            None
                        f.write("\n")
                    except:
                        print "None"
                        f.write(iso8859("None\n"))
                print
                f.write(iso8859("\n"))
            except:
                print "None"
                f.write(iso8859("None\n"))
        f.close()
    except:
        App.Console.PrintMessage("Erreur en écriture du fichier "+SaveName+"\n")
        errorDialog(u"Erreur en écriture du fichier "+SaveName)