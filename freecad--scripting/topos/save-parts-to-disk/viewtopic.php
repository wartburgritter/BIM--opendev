<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" xml:lang="en-gb" lang="en-gb"><head>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-language" content="en-gb">
<meta http-equiv="imagetoolbar" content="no">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="keywords" content="">
<meta name="description" content="">

<title>FreeCAD forum • View topic - Finding part given a position</title>

<link rel="alternate" type="application/atom+xml" title="Feed - FreeCAD forum" href="http://forum.freecadweb.org/feed.php"><link rel="alternate" type="application/atom+xml" title="Feed - All forums" href="http://forum.freecadweb.org/feed.php?mode=forums"><link rel="alternate" type="application/atom+xml" title="Feed - Forum - Python scripting and macros" href="http://forum.freecadweb.org/feed.php?f=22"><link rel="alternate" type="application/atom+xml" title="Feed - Topic - Finding part given a position" href="http://forum.freecadweb.org/feed.php?f=22&amp;t=5003">

<!--
	phpBB style name: prosilver
	Based on style:   prosilver (this is the default phpBB3 style)
	Original author:  Tom Beddard ( http://www.subBlue.com/ )
	Modified by:
-->

<script type="text/javascript">
// <![CDATA[
	var jump_page = 'Enter the page number you wish to go to:';
	var on_page = '1';
	var per_page = '';
	var base_url = '';
	var style_cookie = 'phpBBstyle';
	var style_cookie_settings = '; path=/; domain=forum.freecadweb.org';
	var onload_functions = new Array();
	var onunload_functions = new Array();

	

	/**
	* Find a member
	*/
	function find_username(url)
	{
		popup(url, 760, 570, '_usersearch');
		return false;
	}

	/**
	* New function for handling multiple calls to window.onload and window.unload by pentapenguin
	*/
	window.onload = function()
	{
		for (var i = 0; i < onload_functions.length; i++)
		{
			eval(onload_functions[i]);
		}
	};

	window.onunload = function()
	{
		for (var i = 0; i < onunload_functions.length; i++)
		{
			eval(onunload_functions[i]);
		}
	};

// ]]>
</script>
<script type="text/javascript" src="viewtopic-Dateien/styleswitcher.js"></script>
<script type="text/javascript" src="viewtopic-Dateien/forum_fn.js"></script>

<link href="viewtopic-Dateien/print.css" rel="stylesheet" type="text/css" media="print" title="printonly">
<link href="viewtopic-Dateien/style.css" rel="stylesheet" type="text/css" media="screen, projection">

<link href="viewtopic-Dateien/phpbb.css" rel="stylesheet" type="text/css">

<link href="viewtopic-Dateien/normal.css" rel="stylesheet" type="text/css" title="A">
<link href="viewtopic-Dateien/medium.css" rel="alternate stylesheet" type="text/css" title="A+">
<link href="viewtopic-Dateien/large.css" rel="alternate stylesheet" type="text/css" title="A++">



<link rel="shortcut icon" href="http://www.freecadweb.org/images/favicon.ico">

</head>

<body id="phpbb" class="section-viewtopic ltr">

<div id="wrap">
	<a id="top" name="top" accesskey="t"></a>
	<div id="page-header">
		<div class="headerbar">
			<div class="inner"><span class="corners-top"><span></span></span>

			<div id="site-description">
				<a href="http://www.forum.freecadweb.org/index.php" title="Board index" id="logo"><img src="viewtopic-Dateien/site_logo.gif" alt="" title="" height="52" width="139"></a>
				<h1>FreeCAD forum</h1>
				<p>The help and development forum of FreeCAD</p>
				<p class="skiplink"><a href="#start_here">Skip to content</a></p>
			</div>

		
			<div id="search-box">
				<form action="./search.php" method="get" id="search">
				<fieldset>
					<input name="keywords" id="keywords" maxlength="128" title="Search for keywords" class="inputbox search" value="Search…" onclick="if(this.value=='Search…')this.value='';" onblur="if(this.value=='')this.value='Search…';" type="text">
					<input class="button2" value="Search" type="submit"><br>
					<a href="http://www.forum.freecadweb.org/search.php" title="View the advanced search options">Advanced search</a> 
				</fieldset>
				</form>
			</div>
		

			<span class="corners-bottom"><span></span></span></div>
		</div>

		<div class="navbar">
			<div class="inner"><span class="corners-top"><span></span></span>

			<ul class="linklist navlinks">
				<li class="icon-home"><a href="http://www.forum.freecadweb.org/index.php" accesskey="h">Board index</a>  <strong>‹</strong> <a href="http://www.forum.freecadweb.org/viewforum.php?f=7">Users</a> <strong>‹</strong> <a href="http://www.forum.freecadweb.org/viewforum.php?f=22">Python scripting and macros</a></li>

				<li class="rightside"><a href="#" onclick="fontsizeup(); return false;" onkeypress="return fontsizeup(event);" class="fontsize" title="Change font size">Change font size</a></li>

				<li class="rightside"><a href="http://www.forum.freecadweb.org/memberlist.php?mode=email&amp;t=5003" title="E-mail friend" class="sendemail">E-mail friend</a></li><li class="rightside"><a href="http://www.forum.freecadweb.org/viewtopic.php?f=22&amp;t=5003&amp;view=print" title="Print view" accesskey="p" class="print">Print view</a></li>
			</ul>

			
			<ul class="linklist leftside">
				<li class="icon-ucp">
					<a href="http://www.forum.freecadweb.org/ucp.php" title="User Control Panel" accesskey="e">User Control Panel</a>
						 (<a href="http://www.forum.freecadweb.org/ucp.php?i=pm&amp;folder=inbox"><strong>0</strong> new messages</a>) •
					<a href="http://www.forum.freecadweb.org/search.php?search_id=egosearch">View your posts</a>
					
				</li>
			</ul>
			

			<ul class="linklist rightside">
				<li class="icon-faq"><a href="http://www.forum.freecadweb.org/faq.php" title="Frequently Asked Questions">FAQ</a></li>
				<li class="icon-members"><a href="http://www.forum.freecadweb.org/memberlist.php" title="View complete list of members">Members</a></li>
					<li class="icon-logout"><a href="http://www.forum.freecadweb.org/ucp.php?mode=logout&amp;sid=c64fad04e5b246115bf12896794f379b" title="Logout [ bernd ]" accesskey="x">Logout [ bernd ]</a></li>
				
			</ul>

			<span class="corners-bottom"><span></span></span></div>
		</div>

	</div>

	<a name="start_here"></a>
	<div id="page-body">
		
<h2><a href="http://www.forum.freecadweb.org/viewtopic.php?f=22&amp;t=5003">Finding part given a position</a></h2>
<!-- NOTE: remove the style="display: none" when you want to have the forum description on the topic body --><div style="display: none !important;">Need help, or want to share a macro? Post here!<br></div>

<div class="topic-actions">

	<div class="buttons">
	
		<div class="reply-icon"><a href="http://www.forum.freecadweb.org/posting.php?mode=reply&amp;f=22&amp;t=5003" title="Post a reply"><span></span>Post a reply</a></div>
	
	</div>

	
		<div class="search-box">
			<form method="get" id="topic-search" action="./search.php">
			<fieldset>
				<input class="inputbox search tiny" name="keywords" id="search_keywords" size="20" value="Search this topic…" onclick="if(this.value=='Search this topic…')this.value='';" onblur="if(this.value=='')this.value='Search this topic…';" type="text">
				<input class="button2" value="Search" type="submit">
				<input name="t" value="5003" type="hidden">
<input name="sf" value="msgonly" type="hidden">

			</fieldset>
			</form>
		</div>
	
		<div class="pagination">
			<a href="#unread">First unread post</a> • 3 posts
			 • Page <strong>1</strong> of <strong>1</strong>
		</div>
	

</div>
<div class="clear"></div>

<a id="unread"></a>
	<div id="p39171" class="post bg2 unreadpost">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="report-icon"><a href="http://www.forum.freecadweb.org/report.php?f=22&amp;p=39171" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://www.forum.freecadweb.org/posting.php?mode=quote&amp;f=22&amp;p=39171" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3 class="first"><a href="#p39171">Finding part given a position</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=39171#p39171"><img src="viewtopic-Dateien/icon_post_target_unread.gif" alt="Unread post" title="Unread post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2408">ValvetAlien</a></strong> » Thu Nov 21, 2013 8:58 pm </p>

			

			<div class="content">Hello,<br><br>I have the following problem: <br>I
 have a (large) set of positions and have to find if there is a part at 
the given position and if yes which one. How do I do that 
programmatically, i.e. in python?<br><br>Thanks a lot!<br>VA</div>

			

		</div>

		
			<dl class="postprofile" id="profile39171">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2408">ValvetAlien</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 1</dd><dd><strong>Joined:</strong> Thu Nov 21, 2013 8:19 pm</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://www.forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=39171" title="Private message"><span>Private message</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p39182" class="post bg1 unreadpost">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="report-icon"><a href="http://www.forum.freecadweb.org/report.php?f=22&amp;p=39182" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://www.forum.freecadweb.org/posting.php?mode=quote&amp;f=22&amp;p=39182" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3><a href="#p39182">Re: Finding part given a position</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=39182#p39182"><img src="viewtopic-Dateien/icon_post_target_unread.gif" alt="Unread post" title="Unread post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=689" style="color: #00AA00;" class="username-coloured">danielfalck</a></strong> » Fri Nov 22, 2013 1:20 am </p>

			

			<div class="content">You might be able to use the bounding box min/max values and compare to your predetermined coordinates.<br><br>Look at something like:<br><dl class="codebox"><dt>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></dt><dd><code>obj.Shape.BoundBox<br>obj.Shape.BoundBox.XMin<br>obj.Shape.BoundBox.XMax<br>#etc....<br></code></dd></dl></div>

			<div id="sig39182" class="signature">Dan Falck<br><a href="http://opensourcedesigntools.blogspot.com/" class="postlink">http://opensourcedesigntools.blogspot.com/</a><br>Our FreeCAD Book:<br><a href="http://link.packtpub.com/4wHY3D" class="postlink">http://link.packtpub.com/4wHY3D</a></div>

		</div>

		
			<dl class="postprofile" id="profile39182">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=689" style="color: #00AA00;" class="username-coloured">danielfalck</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 166</dd><dd><strong>Joined:</strong> Fri Oct 07, 2011 8:58 pm</dd><dd><strong>Location:</strong> Beaverton,Oregon, USA</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://www.forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=39182" title="Private message"><span>Private message</span></a></li><li class="web-icon"><a href="http://opensourcedesigntools.blogspot.com/" title="WWW: http://opensourcedesigntools.blogspot.com/"><span>Website</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p39299" class="post bg2 unreadpost">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			
				<ul class="profile-icons">
					<li class="report-icon"><a href="http://www.forum.freecadweb.org/report.php?f=22&amp;p=39299" title="Report this post"><span>Report this post</span></a></li><li class="quote-icon"><a href="http://www.forum.freecadweb.org/posting.php?mode=quote&amp;f=22&amp;p=39299" title="Reply with quote"><span>Reply with quote</span></a></li>
				</ul>
			

			<h3><a href="#p39299">Re: Finding part given a position</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=39299#p39299"><img src="viewtopic-Dateien/icon_post_target_unread.gif" alt="Unread post" title="Unread post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=1058">mario52</a></strong> » Mon Nov 25, 2013 9:57 am </p>

			

			<div class="content">Hi<br>This macro saves to disk (a .txt file) all objects in the project with their coordinates<br>You can then retrieve them with a spreadsheet (Open or Free Office or otherwise) and do a search of your data<br><br><dl class="codebox"><dt>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></dt><dd><code># -*- coding: utf-8 -*-<br># liste et sauve tous les objets du document et leurs coordonnées<br># list and saves all objects of the document and their coordinates xyz<br><br>import Draft, Part, FreeCAD<br>from PyQt4 import QtGui ,QtCore<br>from PyQt4.QtGui import *<br>from PyQt4.QtCore import *<br><br>def iso8859(encoder):<br>&nbsp; &nbsp; return unicode(encoder).encode('iso-8859-1')<br>def errorDialog(msg):<br>&nbsp; &nbsp; diag = QtGui.QMessageBox(QtGui.QMessageBox.Critical,u"Error Message",msg )<br>&nbsp; &nbsp; diag.setWindowModality(QtCore.Qt.ApplicationModal)<br>&nbsp; &nbsp; diag.exec_()<br><br>path = FreeCAD.ConfigGet("AppHomePath")<br>#path = FreeCAD.ConfigGet("UserAppData")<br><br>SaveName = ""<br>SaveName = QFileDialog.getSaveFileName(None,QString.fromLocal8Bit("Sauver un fichier Info"),path,"*.txt")<br>if SaveName == "":<br>&nbsp; &nbsp; App.Console.PrintMessage("Procédure abandonnée"+"\n")<br>&nbsp; &nbsp; errorDialog(u"Procédure abandonnée")<br>else:<br>&nbsp; &nbsp; App.Console.PrintMessage("Enregistrement de "+SaveName+"\n")<br>&nbsp; &nbsp; try:<br>&nbsp; &nbsp; &nbsp; &nbsp; f = open(SaveName, 'w') # write<br>#&nbsp; &nbsp; &nbsp; &nbsp; f.write(iso8859(u"Info "+"\n"))<br>&nbsp; &nbsp; &nbsp; &nbsp; f.write(iso8859(str(App.ActiveDocument.Name)+"\n"))<br>&nbsp; &nbsp; &nbsp; &nbsp; for obj in App.ActiveDocument.Objects:<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; print obj.Label&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;# Name object<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; f.write(iso8859(str(obj.Label))+"\n")&nbsp; &nbsp;# Save Name object<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; try:<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; for j in enumerate(obj.Shape.Edges):<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; try:<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; print "X: ",j[1].Vertexes[0].Point.x," ",<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; print "Y: ",j[1].Vertexes[0].Point.y," ",<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; print "Z: ",j[1].Vertexes[0].Point.z," ",<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; print<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; try:<br>&nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; f.write("X 
,"+iso8859(str(j[1].Vertexes[0].Point.x)+","))<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; except:<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; None<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; try:<br>&nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; f.write("Y 
,"+iso8859(str(j[1].Vertexes[0].Point.y)+","))<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; except:<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; None<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; try:<br>&nbsp;
 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
&nbsp; &nbsp; &nbsp; f.write("Z 
,"+iso8859(str(j[1].Vertexes[0].Point.z)+","))<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; except:<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; None<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; f.write("\n")<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; except:<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; print "None"<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; f.write(iso8859("None\n"))<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; print<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; f.write(iso8859("\n"))<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; except:<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; print "None"<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; f.write(iso8859("None\n"))<br>&nbsp; &nbsp; &nbsp; &nbsp; f.close()<br>&nbsp; &nbsp; except:<br>&nbsp; &nbsp; &nbsp; &nbsp; App.Console.PrintMessage("Erreur en écriture du fichier "+SaveName+"\n")<br>&nbsp; &nbsp; &nbsp; &nbsp; errorDialog(u"Erreur en écriture du fichier "+SaveName)<br><br></code></dd></dl><br>mario</div>

			<div id="sig39299" class="signature"><a href="http://www.zimagez.com/" class="postlink"><img src="viewtopic-Dateien/zimagez-80x15-01a.png" alt="Image"></a>  Windows Vista SP2</div>

		</div>

		
			<dl class="postprofile" id="profile39299">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=1058">mario52</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 164</dd><dd><strong>Joined:</strong> Wed May 16, 2012 2:13 pm</dd>
			<dd>
				<ul class="profile-icons">
					<li class="pm-icon"><a href="http://www.forum.freecadweb.org/ucp.php?i=pm&amp;mode=compose&amp;action=quotepost&amp;p=39299" title="Private message"><span>Private message</span></a></li>
				</ul>
			</dd>
		

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<form id="viewtopic" method="post" action="./viewtopic.php?f=22&amp;t=5003">

	<fieldset class="display-options" style="margin-top: 0; ">
		
		<label>Display posts from previous: <select name="st" id="st"><option value="0" selected="selected">All posts</option><option value="1">1 day</option><option value="7">7 days</option><option value="14">2 weeks</option><option value="30">1 month</option><option value="90">3 months</option><option value="180">6 months</option><option value="365">1 year</option></select></label>
		<label>Sort by <select name="sk" id="sk"><option value="a">Author</option><option value="t" selected="selected">Post time</option><option value="s">Subject</option></select></label> <label><select name="sd" id="sd"><option value="a" selected="selected">Ascending</option><option value="d">Descending</option></select> <input name="sort" value="Go" class="button2" type="submit"></label>
		
	</fieldset>

	</form>
	<hr>


<div class="topic-actions">
	<div class="buttons">
	
		<div class="reply-icon"><a href="http://www.forum.freecadweb.org/posting.php?mode=reply&amp;f=22&amp;t=5003" title="Post a reply"><span></span>Post a reply</a></div>
	
	</div>

	
		<div class="pagination">
			3 posts
			 • Page <strong>1</strong> of <strong>1</strong>
		</div>
	
</div>


	<p></p><p><a href="http://www.forum.freecadweb.org/viewforum.php?f=22" class="left-box left" accesskey="r">Return to Python scripting and macros</a></p>

	<form method="post" id="jumpbox" action="./viewforum.php" onsubmit="if(this.f.value == -1){return false;}">

	
		<fieldset class="jumpbox">
	
			<label for="f" accesskey="j">Jump to:</label>
			<select name="f" id="f" onchange="if(this.options[this.selectedIndex].value != -1){ document.forms['jumpbox'].submit() }">
			
				<option value="-1">Select a forum</option>
			<option value="-1">------------------</option>
				<option value="7">Users</option>
			
				<option value="3">&nbsp; &nbsp;Help on using FreeCAD</option>
			
				<option value="22" selected="selected">&nbsp; &nbsp;Python scripting and macros</option>
			
				<option value="4">&nbsp; &nbsp;Install / Compile</option>
			
				<option value="8">&nbsp; &nbsp;Open discussion</option>
			
				<option value="9">&nbsp; &nbsp;Feature Announcements</option>
			
				<option value="6">Development</option>
			
				<option value="10">&nbsp; &nbsp;Developers corner</option>
			
				<option value="17">&nbsp; &nbsp;Pull Requests</option>
			
				<option value="19">&nbsp; &nbsp;Part Design</option>
			
				<option value="20">&nbsp; &nbsp;Assembly</option>
			
				<option value="18">&nbsp; &nbsp;FEM</option>
			
				<option value="15">&nbsp; &nbsp;CAM</option>
			
				<option value="23">&nbsp; &nbsp;Arch</option>
			
				<option value="21">&nbsp; &nbsp;Wiki</option>
			
				<option value="11">Forums in other languages</option>
			
				<option value="12">&nbsp; &nbsp;Forum français</option>
			
				<option value="13">&nbsp; &nbsp;Forum in Deutsch</option>
			
				<option value="14">&nbsp; &nbsp;Foro en Español</option>
			
				<option value="16">&nbsp; &nbsp;日本語フォーラム</option>
			
			</select>
			<input value="Go" class="button2" type="submit">
		</fieldset>
	</form>


	<h3><a href="http://www.forum.freecadweb.org/viewonline.php">Who is online</a></h3>
	<p>Users browsing this forum: <a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">bernd</a> and 0 guests</p>
</div>

<div id="page-footer">

	<div class="navbar">
		<div class="inner"><span class="corners-top"><span></span></span>

		<ul class="linklist">
			<li class="icon-home"><a href="http://www.forum.freecadweb.org/index.php" accesskey="h">Board index</a></li>
				<li class="icon-subscribe"><a href="http://www.forum.freecadweb.org/viewtopic.php?uid=2069&amp;f=22&amp;t=5003&amp;watch=topic&amp;start=0&amp;hash=860eaff8" title="Subscribe topic">Subscribe topic</a></li><li class="icon-bookmark"><a href="http://www.forum.freecadweb.org/viewtopic.php?f=22&amp;t=5003&amp;bookmark=1&amp;hash=860eaff8" title="Bookmark topic">Bookmark topic</a></li>
			<li class="rightside"><a href="http://www.forum.freecadweb.org/memberlist.php?mode=leaders">The team</a> • <a href="http://www.forum.freecadweb.org/ucp.php?mode=delete_cookies">Delete all board cookies</a> • All times are UTC </li>
		</ul>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<div class="copyright">Powered by <a href="https://www.phpbb.com/">phpBB</a>® Forum Software © phpBB Group
		
	</div>
</div>

</div>

<div>
	<a id="bottom" name="bottom" accesskey="z"></a>
	
</div>


</body></html>