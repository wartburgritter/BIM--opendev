#wegen breiten bildschirmen 5 liegende balken 2 m lang und diese mit forschleife erzeugen,
#auch die bewehrung mit forschleife erzeugen wie fuer die stuetzen naechste zeile vorgeschlagen
# 3 Stuetzen uebereinander jeweils eisen in den stuetzen fuer inseide und anschlusseisen fuer trenntest

'''
# siehe forumeintrag
# http://www.forum.freecadweb.org/viewtopic.php?f=13&t=6089&p=48522&hilit=isInside#p48522
!!!!!  
Es kann sein, dass die vertexe inside sind, aber ein teil des zylinde outside 
Der kreis eines zylinders ist mit einem vertex definiert. ich muesste noch einen
hilfspunkt gegenueber dem vertex definieren und kontorllieren
!!!
# wann ?
myDoc.recompute()
'''


import Part


r = 22.56758334191025
#r = 2.256758334191025
#r = 20

myDoc = App.newDocument("isinsidetest")

box1 = myDoc.addObject("Part::Box","box1")
box1.Width = 3000
box1.Height = box1.Length = 250
box1VO = box1.ViewObject
box1VO.Transparency = 75

box2 = myDoc.addObject("Part::Box","box2")
box2.Width = 3000
box2.Height = box2.Length = 250
box2.Placement.Base = (0,3000,0)
box2VO = box2.ViewObject
box2VO.Transparency = 75

box3 = myDoc.addObject("Part::Box","box3")
box3.Width = 3000
box3.Height = box3.Length = 250
box3.Placement.Base = (0,6000,0)
box3VO = box3.ViewObject
box3VO.Transparency = 75

box4 = myDoc.addObject("Part::Box","box4")
box4.Width = 3000
box4.Height = box4.Length = 250
box4.Placement.Base = (0,9000,0)
box4VO = box4.ViewObject
box4VO.Transparency = 75

box5 = myDoc.addObject("Part::Box","box5")
box5.Width = 3000
box5.Height = box5.Length = 250
box5.Placement.Base = (0,12000,0)
box5VO = box5.ViewObject
box5VO.Transparency = 75

box6 = myDoc.addObject("Part::Box","box6")
box6.Width = 3000
box6.Height = box6.Length = 250
box6.Placement.Base = (0,15000,0)
box6VO = box6.ViewObject
box6VO.Transparency = 75

box7 = myDoc.addObject("Part::Box","box7")
box7.Width = 3000
box7.Height = box7.Length = 250
box7.Placement.Base = (0,18000,0)
box7VO = box7.ViewObject
box7VO.Transparency = 75

cyl1 = myDoc.addObject("Part::Cylinder","cyl1")
cyl1.Radius = r
cyl1.Height = 2500
cyl1.Placement.Base = (100,250,100)
cyl1.Placement.Rotation = (0,0.707,0.707,0)
cyl1VO = cyl1.ViewObject
cyl1VO.ShapeColor = (0.40,0.20,0.00)

cyl2 = myDoc.addObject("Part::Cylinder","cyl2")
cyl2.Radius = r
cyl2.Height = 2500
cyl2.Placement.Base = (100,15250,100)
cyl2.Placement.Rotation = (0,0.707,0.707,0)
cyl2VO = cyl2.ViewObject
cyl2VO.ShapeColor = (0.40,0.20,0.00)

cyl3 = myDoc.addObject("Part::Cylinder","cyl3")
cyl3.Radius = r
cyl3.Height = 2500
cyl3.Placement.Base = (150,13750,150)
cyl3.Placement.Rotation = (0,0.707,0.707,0)
cyl3VO = cyl3.ViewObject
cyl3VO.ShapeColor = (0.40,0.20,0.00)

cyl4 = myDoc.addObject("Part::Cylinder","cyl4")
cyl4.Radius = r
cyl4.Height = 2500
cyl4.Placement.Base = (100,9250,100)
cyl4.Placement.Rotation = (0,0.707,0.707,0)
cyl4VO = cyl4.ViewObject
cyl4VO.ShapeColor = (0.40,0.20,0.00)

cyl5 = myDoc.addObject("Part::Cylinder","cyl5")
cyl5.Radius = r
cyl5.Height = 2500
cyl5.Placement.Base = (100,12250,100)
cyl5.Placement.Rotation = (0,0.707,0.707,0)
cyl5VO = cyl5.ViewObject
cyl5VO.ShapeColor = (0.40,0.20,0.00)

cyl6 = myDoc.addObject("Part::Cylinder","cyl6")
cyl6.Radius = r
cyl6.Height = 2500
cyl6.Placement.Base = (50,250,50)
cyl6.Placement.Rotation = (0,0.707,0.707,0)
cyl6VO = cyl6.ViewObject
cyl6VO.ShapeColor = (0.40,0.20,0.00)

cyl7 = myDoc.addObject("Part::Cylinder","cyl7")
cyl7.Radius = r
cyl7.Height = 2500
cyl7.Placement.Base = (150,4750,150)
cyl7.Placement.Rotation = (0,0.707,0.707,0)
cyl7VO = cyl7.ViewObject
cyl7VO.ShapeColor = (0.40,0.20,0.00)

cyl8 = myDoc.addObject("Part::Cylinder","cyl8")
cyl8.Radius = r
cyl8.Height = 2500
cyl8.Placement.Base = (150,10750,150)
cyl8.Placement.Rotation = (0,0.707,0.707,0)
cyl8VO = cyl8.ViewObject
cyl8VO.ShapeColor = (0.40,0.20,0.00)

cyl9 = myDoc.addObject("Part::Cylinder","cyl9")
cyl9.Radius = r
cyl9.Height = 2500
cyl9.Placement.Base = (200,10750,200)
cyl9.Placement.Rotation = (0,0.707,0.707,0)
cyl9VO = cyl9.ViewObject
cyl9VO.ShapeColor = (0.40,0.20,0.00)

cyl10 = myDoc.addObject("Part::Cylinder","cyl10")
cyl10.Radius = r
cyl10.Height = 2500
cyl10.Placement.Base = (50,10750,50)
cyl10.Placement.Rotation = (0,0.707,0.707,0)
cyl10VO = cyl10.ViewObject
cyl10VO.ShapeColor = (0.40,0.20,0.00)

cyl11 = myDoc.addObject("Part::Cylinder","cyl11")
cyl11.Radius = r
cyl11.Height = 2500
cyl11.Placement.Base = (150,250,150)
cyl11.Placement.Rotation = (0,0.707,0.707,0)
cyl11VO = cyl11.ViewObject
cyl11VO.ShapeColor = (0.40,0.20,0.00)

cyl12 = myDoc.addObject("Part::Cylinder","cyl12")
cyl12.Radius = r
cyl12.Height = 2500
cyl12.Placement.Base = (150,18250,150)
cyl12.Placement.Rotation = (0,0.707,0.707,0)
cyl12VO = cyl12.ViewObject
cyl12VO.ShapeColor = (0.40,0.20,0.00)

boxes = [box1, box2, box3, box4, box5, box6, box7]
cylinder = [cyl1, cyl2, cyl3, cyl4, cyl5, cyl6, cyl7, cyl8, cyl9, cyl10, cyl11, cyl12]




#spatialmatrix verschachteltes dictionary mit name (in ifc ueber id) und liste der reinforcements shape
spatialmatrix = {}

#schleife ueber alle cylinder
for c in cylinder:

  #schleife ueber alle boxen mit j in range, da ich mit dem index auch die cyl in spatialmatrix kopiere
  for b in boxes:
    countall = len(c.Shape.Vertexes)
    countin  = 0
    countout = 0

    for i in range(len(c.Shape.Vertexes)):
      #print v
      if b.Shape.isInside(c.Shape.Vertexes[i].Point,0.01,True) == True:
        countin += 1
      else:
        countout += 1
    
    # anstatt ausgabe append in zuordnungsmatrix
    if countin == countall and countout == 0 :
      #print  (c.Name + ' is inside ' + b.Name)
      if b.Name not in spatialmatrix:
        #print 'make entry in dictionary and give an empty list'
        spatialmatrix[b.Name] = []
      #spatialmatrix[b.Name].append(c.Name)       # mhh namen speichern macht keinen sinn, oder ich m
      spatialmatrix[b.Name].append(c.Shape)       # die shape speichern !!!
    elif countout == countall and countin == 0 :
      pass
      #print (c.Name + ' is outside ' + b.Name)
    elif countout != 0 and countin != 0 and countout + countin == countall:
      print (c.Name + ' is inside and outsid ' + b.Name)
      # cyl7 ist in box2 und box3 brauche den teil, der in dem aktuellen box ist
      # boolean operations intersection gibt den teil als kopie zurueck, der in box ist
      # cyltmp ist ein shape, ich habe aber alles PartFeatures, mhh Part.show(cyltmp) erstellt ein ViewObject 
      #cyltmp = b.Shape.common(c.Shape)
      #ich will von dem aktuelle cyl das stueck welches in box ist, also cyltmp als PartFeatures mit Nammen cyl7-1 
      # zu dem dictionary hinzufuegen !!??
      # ach viel besser die shape speichern 
      if b.Name not in spatialmatrix:
        spatialmatrix[b.Name] = []
      spatialmatrix[b.Name].append(b.Shape.common(c.Shape))
    else:
      print 'error something went wrong'


volumelist = {}
#schleife ueber alle cylinder
for c in cylinder:

  #schleife ueber alle boxen mit j in range, da ich mit dem index auch die cyl in spatialmatrix kopiere
  for b in boxes:
    countall = len(c.Shape.Vertexes)
    countin  = 0
    countout = 0

    for i in range(len(c.Shape.Vertexes)):
      #print v
      if b.Shape.isInside(c.Shape.Vertexes[i].Point,0.01,True) == True:
        countin += 1
      else:
        countout += 1
    
    if countin == countall and countout == 0 :     # inside
      #print  (c.Name + ' is inside ' + b.Name)
      if b.Name not in volumelist:
        #print 'initialisiere dictionary eintrag fuer diese box und schreibe volumen des zylinders rein'
        volumelist[b.Name] = c.Shape.Volume
      else:
        volumelist[b.Name] +=  c.Shape.Volume      # da es schon eintrag fuer die box hat addiere volumen des aktuellen zylinders
    elif countout == countall and countin == 0 :   # outside
      pass
      #print (c.Name + ' is outside ' + b.Name)
    elif countout != 0 and countin != 0 and countout + countin == countall:
      print (c.Name + ' is inside and outsid ' + b.Name)   # inside and outside
      if b.Name not in volumelist:
        volumelist[b.Name] = b.Shape.common(c.Shape).Volume
      else:
        volumelist[b.Name] += b.Shape.common(c.Shape).Volume
    else:
      print 'error something went wrong'



for i in sorted(spatialmatrix.items()):
  Vol = 0
  for s in i[1]:
    Vol += s.Volume
  print (i[0] + ' = ' +str(Vol))


for i in sorted(volumelist.items()):
  print i



print 'END' 


# cyl1.Shape.Volume
# 785398.1633974491

# 4E6

# box 1 =  3
# box 2 =  0.5
# box 3 =  0.5
# box 4 =  2.5
# box 5 =  3
# box 6 =  1.5
# box 7 =  1


cyl7.Shape.Volume
shapes = []
tmp = (spatialmatrix['box2'])
shapes.append(tmp[0])
tmp = (spatialmatrix['box3'])
shapes.append(tmp[0])
for s in shapes:
 print s.Volume
 Part.show(s)



# YEP meine Volumenkalkulation stimmt !!!
# beide Varianten, einmal mit shapekopie, einmal mit direkt berechnung des volumens
# nun ne def daraus machen.


# NAMENSZUGRIFF, Erzeugung meiner beispeilobjekte nicht als Part::Feature, sondern als shape

