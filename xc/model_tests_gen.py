# -*- coding: utf-8 -*-

print('\n')

import os
import xc_base
import geom
import xc
import math
from model import predefined_spaces
from model.geometry import grid_model as gm
from model.mesh import finit_el_model as fem
from model.boundary_cond import spring_bound_cond as sprbc
from model.sets import sets_mng as sets
from materials import typical_materials as tm
from actions import loads
from actions import load_cases as lcases
from actions import combinations as cc
from actions.earth_pressure import earth_pressure as ep
from model.geometry import geom_utils as gut
from materials.ehe import EHE_materials
#from materials.sia262 import SIA262_materials

# Default configuration of environment variables.
fullProjPath='/home/hugo/Documents/projekte--BIM--opendev/xc/XCmodels/workingModel/'
execfile(fullProjPath+'env_config.py')



###############################################
#Auxiliary data
 #Geometry
  # red = x
  # green = y
  # blue = z
LbeamX=5    # gesamt x laenge
LbeamY=6    # gesamt y laenge
LcolumnZ=6  # gesamt z hoehe

Wfoot=2.0   # breite fundament

# querschnittsabmessungen
hbeamX=0.5
hbeamY=0.3
hcolumnZ=0.40
wbeamX=0.35
wbeamY=0.5
wcolumnZ=0.40
deckTh=0.20
wallTh=0.5
footTh=0.7


#Materials
concrete=EHE_materials.HA30

# mesh
#eSize= 0.2     #length of elements
#eSize= 0.05     #length of elements
eSize= 5     #length of elements



###############################################
#             *** GEOMETRIC model (points, lines, surfaces) - SETS ***
FEcase= xc.FEProblem()
preprocessor=FEcase.getPreprocessor
prep=preprocessor   #short name
nodes= prep.getNodeHandler
elements= prep.getElementHandler
elements.dimElem= 3
# Problem type
modelSpace= predefined_spaces.StructuralMechanics3D(nodes) #Defines the
# dimension of the space: nodes by three coordinates (x,y,z) and 
# six DOF for each node (Ux,Uy,Uz,thetaX,thetaY,thetaZ)

# coordinates in global X,Y,Z axes for the grid generation
xList=[0,LbeamX/2.0,LbeamX]
yList=[-Wfoot/2.,0,Wfoot/2.,LbeamY]
zList=[0,LcolumnZ/2.0,LcolumnZ]
print(xList)
print(yList)
print(zList)

#auxiliary data
lastXpos=len(xList)-1
lastYpos=len(yList)-1
lastZpos=len(zList)-1

# grid model definition
gridGeom= gm.GridModel(prep,xList,yList,zList)

# Grid geometric entities definition (points, lines, surfaces)
# Points' generation
gridGeom.generatePoints()



# idem for J and K ranges
beamX_rg=gm.IJKRange((0,1,lastZpos),(lastXpos,lastYpos,lastZpos)).extractIncludedIranges(stepJ=1,stepK=1)
beamY_rg=gm.IJKRange((0,1,lastZpos),(lastXpos,lastYpos,lastZpos)).extractIncludedJranges(stepI=2,stepK=1)
columnZ_rg=gm.IJKRange((0,1,0),(lastXpos,lastYpos,lastZpos)).extractIncludedKranges(stepI=2,stepJ=2)+[gm.IJKRange((1,lastYpos,0),(1,lastYpos,1))]
decklv1_rg=gm.IJKRange((0,1,1),(lastXpos,lastYpos,lastZpos)).extractIncludedIJranges(step=2)
decklv2_rg=gm.IJKRange((0,lastYpos-1,lastZpos),(1,lastYpos,lastZpos))
wall_rg=gm.IJKRange((0,1,0),(lastXpos,1,1))
#foot_rg=[gm.IJKRange((0,0,0),(lastXpos,2,0))]
foot_rg=[gut.def_rg_cooLim(XYZLists=(xList,yList,zList),Xcoo=(0,LbeamX),Ycoo=(-Wfoot/2.,Wfoot/2.),Zcoo=(0,0))]
#Lines generation
beamX=gridGeom.genLinMultiRegion(lstIJKRange=beamX_rg,nameSet='beamX')
beamY=gridGeom.genLinMultiRegion(lstIJKRange=beamY_rg,nameSet='beamY')
columnZ=gridGeom.genLinMultiRegion(lstIJKRange=columnZ_rg,nameSet='columnZ')
#Surfaces generation
decklv1=gridGeom.genSurfMultiRegion(lstIJKRange=decklv1_rg,nameSet='decklv1')
decklv2=gridGeom.genSurfOneRegion(ijkRange=decklv2_rg,nameSet='decklv2')
wall=gridGeom.genSurfOneRegion(ijkRange=wall_rg,nameSet='wall')
foot=gridGeom.genSurfMultiRegion(lstIJKRange=foot_rg,nameSet='foot')
decklv1.description='Deck level 1'
decklv1.color=cfg.colors['purple01']
decklv2.description='Deck level 2'
decklv2.color=cfg.colors['blue01']
foot.description='Foundation'
foot.color=cfg.colors['orange01']
wall.description='Wall'
wall.color=cfg.colors['green01']
beamX.description='Beams in X direction'
beamX.color=cfg.colors['blue03']
beamY.description='Beams in Y direction'
beamY.color=cfg.colors['green03']
columnZ.description='Columns'
columnZ.color=cfg.colors['red03']

print('\n')
print(xList)
print(yList)
print(zList)



###############################################
#                         *** MATERIALS *** 
concrProp=tm.MaterialData(name='concrProp',E=concrete.Ecm(),nu=concrete.nuc,rho=concrete.density())

# Isotropic elastic section-material appropiate for plate and shell analysis
deck_mat=tm.DeckMaterialData(name='deck_mat',thickness= deckTh,material=concrProp)
deck_mat.setupElasticSection(preprocessor=prep)   #creates the section-material
wall_mat=tm.DeckMaterialData(name='wall_mat',thickness= wallTh,material=concrProp)
wall_mat.setupElasticSection(preprocessor=prep)   #creates the section-material
foot_mat=tm.DeckMaterialData(name='foot_mat',thickness= footTh,material=concrProp)
foot_mat.setupElasticSection(preprocessor=prep)   #creates the section-material

#Geometric sections
#rectangular sections
from materials.sections import section_properties as sectpr
geomSectBeamX=sectpr.RectangularSection(name='geomSectBeamX',b=wbeamX,h=hbeamX)
geomSectBeamY=sectpr.RectangularSection(name='geomSectBeamY',b=wbeamY,h=hbeamY)
geomSectColumnZ=sectpr.RectangularSection(name='geomSectColumnZ',b=wcolumnZ,h=hcolumnZ)

# Elastic material-section appropiate for 3D beam analysis, including shear
  # deformations.
  # Attributes:
  #   name:         name identifying the section
  #   section:      instance of a class that defines the geometric and
  #                 mechanical characteristiscs
  #                 of a section (e.g: RectangularSection, CircularSection,
  #                 ISection, ...)
  #   material:     instance of a class that defines the elastic modulus,
  #                 shear modulus and mass density of the material

beamX_mat= tm.BeamMaterialData(name= 'beamX_mat', section=geomSectBeamX, material=concrProp)
beamX_mat.setupElasticShear3DSection(preprocessor=prep)
beamY_mat= tm.BeamMaterialData(name= 'beamY_mat', section=geomSectBeamY, material=concrProp)
beamY_mat.setupElasticShear3DSection(preprocessor=prep)
columnZ_mat= tm.BeamMaterialData(name= 'columnZ_mat', section=geomSectColumnZ, material=concrProp)
columnZ_mat.setupElasticShear3DSection(preprocessor=prep)



###############################################
#                         ***FE model - MESH***
# IMPORTANT: it's convenient to generate the mesh of surfaces before meshing the lines,
#            otherwise, sets of shells can take also beam elements touched by them

beamX_mesh=fem.LinSetToMesh(linSet=beamX,matSect=beamX_mat,elemSize=eSize,vDirLAxZ=xc.Vector([0,1,0]),elemType='ElasticBeam3d',dimElemSpace=3,coordTransfType='linear')
beamY_mesh=fem.LinSetToMesh(linSet=beamY,matSect=beamY_mat,elemSize=eSize,vDirLAxZ=xc.Vector([1,0,0]),elemType='ElasticBeam3d',coordTransfType='linear')
columnZ_mesh=fem.LinSetToMesh(linSet=columnZ,matSect=columnZ_mat,elemSize=eSize,vDirLAxZ=xc.Vector([1,0,0]),elemType='ElasticBeam3d',coordTransfType='linear')
decklv1_mesh=fem.SurfSetToMesh(surfSet=decklv1,matSect=deck_mat,elemSize=eSize,elemType='ShellMITC4')
decklv1_mesh.generateMesh(prep)     #mesh the set of surfaces
decklv2_mesh=fem.SurfSetToMesh(surfSet=decklv2,matSect=deck_mat,elemSize=eSize,elemType='ShellMITC4')
decklv2_mesh.generateMesh(prep)     #mesh the set of surfaces
wall_mesh=fem.SurfSetToMesh(surfSet=wall,matSect=wall_mat,elemSize=eSize,elemType='ShellMITC4')
wall_mesh.generateMesh(prep) 
foot_mesh=fem.SurfSetToMesh(surfSet=foot,matSect=foot_mat,elemSize=eSize,elemType='ShellMITC4')
foot_mesh.generateMesh(prep)

fem.multi_mesh(preprocessor=prep,lstMeshSets=[beamX_mesh,beamY_mesh,columnZ_mesh])     #mesh these sets



###############################################
#                         ***output some tests***

'''
print('\ninfotests\n')
print(columnZ)
print(dir(columnZ))
print('\nsome attributes\n')
print(columnZ.getPoints)
print(columnZ.nodes)
print(columnZ.lines)
print(columnZ.bodies)
print('\n')
print(columnZ.getNumNodes)
print(columnZ.getNumElements)
print(columnZ.name)
print(columnZ.description)
print(columnZ.color)

print('\n')
print(dir(columnZ.nodes))
print(dir(columnZ.lines))

print('\n')
print(columnZ.nodes[0])
print(dir(columnZ.nodes[0]))
print(columnZ.nodes[0].get3dCoo)
print(columnZ.nodes[1].get3dCoo)

for n in columnZ.nodes:
    print n.get3dCoo

print('\n')
print(dir(gm))


listOfSurfacesInSetDecks=[s for s in decklv1.getSurfaces]
print(listOfSurfacesInSetDecks)

'''

'''
import os
import sys
sys.path.append(os.path.abspath('/home/hugo/Documents/projekte--BIM--opendev/xc/XCmodels/workingModel/'))
import model_tests as mt

mt.decklv1.getSurfaces[0]


dir(model_tests.gm)
dir(model_tests.gm.geom)
print('\n')
dir(model_tests.gm.xc)
print('\n')
dir(model_tests.gm.xc_base)






'''
