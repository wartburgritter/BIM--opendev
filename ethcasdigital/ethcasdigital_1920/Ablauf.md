### Basics
#### kurs_11_basics.py
+ Spyder läuft wir legen gleich los.
+ Die nächste halbe Stunde ist ein wenig trocken, aber dann wir es wieder spannender.
+ Python ist eine Sciptsprache. Das heisst der Programmiercode kann direkt ausgeführt werden.
+ Das wollen wir sofort testen.
+ `print("Hallo ETHcasDitital!")`
+ Auf das grüne Dreieck klicken. Rechts wird die Ausgabe erstellt.
+ Dies ist eine einfach Ausgabeanweisung.
+ Codedatei speichern unter.
+ Python kann auch rechnen. Folgendes eingeben
+ `print(5 + 3)`
+ `print(5 - 7)`
+ `print(5 * 7)`
+ `print(5 / 7)`
+ Was fällt auf? Alle Anweisungen werden immer wieder ausgeführt, Warum?
+ Wir geben unseren Programmiercode nicht direkt an einer Konsole ein, sonder erstellen eine Pythonscriptfile.
+ Bei klick auf den grünen Button wird das gesamte Script ausgeführt.
+ Somit können wir unseren Programmiercode in eine einfach Textdatei mit der Endung *.py speichern
+ Das wollen wir gerade machen. Dateiname `kurs_11_basic.py`
+ Wenn wir Programmiercode in eine Datei schreiben wollen wir auch eigene Hinweise schreiben, Kommentare.
+ Das machen wir doch gleich.
+ `print(15 ** 2)  # Potenz`
+ Nur einzeilige Prints sind langweilig.
+ Wichtige Eingenschaft von Programmiercode ist, das man beliebig viele Werte und Objekte in Platzhaltern (Containern), den Variablen gespeichert werden können.
+ In Python ist das sehr einfach. Es wird ein Name gewählt und mittels Zuweisungsoperator wird der Wert zugewiesen.
+ `my_var_1 = 5`
+ `my_var_2 = 3`
+ Variablen kann man ausgeben
+ `print(my_var_1)`
+ `print(my_var_2)`
+ Mit Varianblen kann man genauso rechnen wie mit Zahlen
+ `my_summe = my_var_1 + my_var_2`
+ `print(my_summe)`
+ Variablen haben bisher bei uns nur ganzzahlige Werte gehabt. Diesen Typ Variable nennt man in Python `int`
+ Prüfen mit 
+ `print(type(5))`
+ `print(type(my_summe))`
+ Die Welt besteht aber nicht nur aus ganzen Zahlen. Es hat auch Gleitkommazahlen. Der zugehörige Datentyp heisst Float
+ `print(type(5.68))`
+ `print(type(my_var_1 / my_var_2))`
+ Ein weiterer Tatentyp sind Zeichenketten, auch Strings genannt. Diese werden mit Anführungszeichen erstellt.
+ Sowohl:
+ my_string_1 = "Hallo ETHcasDitital!"
+ als auch
+ my_string_2 = 'ich bins'
+ ist möglich
+ `print(type(my_string_1))`
+ Können wir mit Stings rechnen ... ?
+ Natürlich
+ `print(my_string_1 + my_string_2)`
+ Was uns noch fehlt sind boolsche Variablen. Diese können nur zwei Werte annehmen True (wahr) oder False (falsch)
+ `my_bool = True`
+ `print(my_bool)`
+ `print(type(my_bool))`
+ Diese werden wir ganz oft als Rückgabewerte von Abfragen benutzen.
+ Nun wird es noch abstrakter nun kommt noch der Nicht-Typ. In den meisten Programmiersprachen gibt es den.
+ Natürlich auch in Python, einen Datentyp der für nichts steht.
+ `my_nichts = None`
+ `print(my_nichts)`
+ `print(type(my_nichts))`
+ Ein wichtiger Datentyp fehlt noch, die Liste. Die Liste ist ein zusammengesetzter Datentyp.
+ Eine Liste kann zur Gruppierung der verschiedensten Daten verwendet werden.
+ Eine Liste von Werten (Elemente), die durch Kommas getrennt und von eckigen Klammern eingeschlossen werden.
+ Listenelemente müssen nicht alle denselben Typ haben.
+ `my_list = ['ETH', 'Python', 100, 98765]`
+ `print(my_list)`
+ `print(type(my_list))`
+ Zugriff auf die einzelnen Elemente funktioniert über Indexierung mit eckigen Klammern
+ print(my_list[1])`
+ gibt uns das zweite Element aus! Die Indexierung started bei 0!
+ Der Zugriff es sehr flexibel, als kleines Beispiel
+ `print(my_list[:-1])`
+ gibt das letzte Element aus.
+ Auch auf mittlere Elemente lässt sich zugreifen. Das obere Ende des Bereichen wird nicht zurückgegeben.
+ `print(my_list[1:2])`
+ Die Länge von Listen und auch von Variablen kann mit len() abgefragt werden.
+ `print(len(my_list))`
+ Es wird Zeit, dass es interesanter wird, Python kann mehr als nur Zahlen addieren und ausgeben ... 


### Kontrollstrukturen
#### python3 kurs_12_kontrollstruktur_if.py
+ In unserem ersten Miniprogramm wollen wir die if Kontrollstrktur ansehen.
+ Dann geht es los mit richtiger Grafikprogrammierung.
+ Kontrollstrukuren im allgemeinen steuern den Ablauf des Scripts.
+ if, elif und else Fallunterscheiungen
+ Damit können einzelne Codeblöcke nur unter bestimmten Bedingungen ausgeführt werden
+ In Python werden zur Markierung von zusammenhängenden Code-Blöcken Einrückungen
zur Gruppierung und Kenntlichmachung einzelner Code-Blöcke genutzt. Üblicherweise wird für jede Einrückungstiefe
ein Tabulator-Zeichen (entspricht vier Leerzeichen) verwendet.
Das heisst wer wahlahsig ist mischt Tabulatoren und leerzeichen in einer codedatei :-)
+ Die Einrückung ist essentiell wichtig! Daher selber tippen! Dabei ausschliesslich 4 leerzeichen zur Einrückung verwenden.
+ Programm eintippen und ausführen
+ Bei der Untersuchung der einzelnen Bedingungen werden die Werte von Variablen häufig mittls Vergleichsoperatoren überprüft.
+ Das sehen wir in unserem kleinen Programm.
+ else-Zweig oder elif-Zweige sind optional
+ Der elif darf auchmehrmals vorkommen.


### Op-Arts
#### kurs_21_einfachesrechteck_ovar.py
+ Weitere Kontrollstrukturen und Sprachelemente lernen wir am Beispiel. Das macht viel mehr Spass.
+ import svgwrite
    + svgwrite ist ein Modul
    + Module sind Pythonscripte die Funktionen zu unserem Python hinzufügen
    + es gibt hunderte, ja tausende Module
    + Dieses hier fügt Methoden hinzu um svg dateien einfach zu erstellen.
+ from ...
    + einfacher Zugriff auf die mm Methode im Script
    + die mm hätte ich kommplett weglassen sollen, didaktisch unklug

+ Punktoperator zugriff auf Attribute von Objekten, lesend und scheibend.
+ In folgenden Scriptbeispielen auch immer mit parametern spielen


#### kurs_22_einfachesrechteck_var.py
+ wie 21 nur werden Variablen zur Steuerung verwendet


#### kurs_23_vielerechtecke.py
+ Kontrollstruktur for schleife
+ Mit Schleifen koennen Programmierabschnitte wiederholt ausgeführt werden.
+ Python kennt zwei Schleifen. Uns reicht vorerst eine.
+ Die for Schleife
    + for Variable iterierbares Objekt 
    + BSP für iterierbares Obj ist die Liste  [0, 1, 3, 4, 5, 6, 7 ,8 ,9] wenn ich über eine einfache Zählervariable iterieren will.
    + obiges versteht kein Mensch ...
+ bei erster Schleife wirklich die Liste eingeben
+ evtl. die range methode gleich bei listen zeigen und ranges erzeugen, dann kann man hier sofort darauf zugreifen
+ schon range(10) ist zu abstrakt



#### kurs_24_versetzterechtecke.py
+ for anweisung
+ if anweisung
+ Modulooperator, Division mit Rest. Der Rest ist der Rückgabewert. 7 % 2 gibt 1 zurück


#### kurs_23_vielerechtecke.py
+ doppelschleife


### kurs_31_.py
+ with
    + ist eine Möglichkeit Dateien einfach und sicher in Python zu öffnen (Kontextmanager)

+ Ausnahmebehandlung
    + Für die Datenwandlung nach in und float is eine exception die Variant to go in Python daher erklären kurz
    + obwohl das auch niemand verstehen wird
    + Fehlerbehandlung
    + try leitet eine Ausnahmebehandlung ein
    + gibt es eine Ablauffehler im Script, wird das Script **nicht** abgebrochen
    + Stattdessen wird der Fehler abgefangen und der codeblock inhalb exception ausgeführt.
