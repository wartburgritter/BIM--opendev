### Winpython und Spyder
+ install Winpython zero install
    + https://sourceforge.net/projects/winpython/files/WinPython_3.7/3.7.4.1/ 
    + WinPython64-3.7.4.1Zero.exe
    + Das icon Spyder funktioniert nicht, da Spyder noch nicht installiert ist.
+ install Spyder
    + start `WinPython Command Prompt.exe`
    + run `pip install Spyder`
    + Es kommt meldung pip veraltet
    + ugrade pip by `pip install --upgrade pip`
    + Doppelklick auf `Spyder`
+ weitere module
pip install pandas
pip install svgwrite

    
### ifcopenshell
+ Wie FreeCAD unter lib sit-packages die richtige version kopieren
    + Python version muss stimmen und ob 32 oder 64 bit
+ testen
    + siehe test_ifcopenshell.py


### Gooogledocs
+ link zur csv fuer webserverbsp
https://docs.google.com/spreadsheets/d/e/2PACX-1vQmNXr3toOik3vjLMf03HEFSDUbv_ApfbwHAVPMKY_6EbwIzrayHI41ynbRoI4AzdXeKDS7AZGKX3nb/pub?gid=0&single=true&output=csv
+ link zum google drive verzeichnis, so habe ich aber eine schreibrechte auf das dokument, da braucht es link aus whatsapp
https://drive.google.com/drive/folders/1aEx9H-_hfk0ak3_t5sSaLGEuphlYiOIW


### Einführung in Python
+ https://tobiaskohn.ch/files/PythonCheatSheet.pdf
+ https://svgwrite.readthedocs.io/en/master/classes/shapes.html#basic-shapes-examples
+ https://pythonfix.com/code/svgwrite_code_examples/
+ https://www.grund-wissen.de/informatik/python/_downloads/grundkurs-python3.pdf
+ https://py-tutorial-de.readthedocs.io/de/python-3.3/introduction.html


### Faktsheets
+ https://www.cheatography.com/siniansung/cheat-sheets/python-3-deutsch/
+ https://www.cheatography.com/siniansung/cheat-sheets/python-3-deutsch/pdf_bw/
+ https://tobiaskohn.ch/files/PythonCheatSheet.pdf
+ https://perso.limsi.fr/pointal/_media/python:cours:mementopython3-german.pdf
