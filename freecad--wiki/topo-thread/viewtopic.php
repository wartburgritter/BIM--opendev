<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" xml:lang="en-gb" lang="en-gb"><head>

<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-language" content="en-gb">
<meta http-equiv="imagetoolbar" content="no">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="keywords" content="">
<meta name="description" content="">

<title>FreeCAD forum • View topic - Übersetzung von Toplogical Data Scripting</title>

<link rel="alternate" type="application/atom+xml" title="Feed - FreeCAD forum" href="http://forum.freecadweb.org/feed.php"><link rel="alternate" type="application/atom+xml" title="Feed - All forums" href="http://forum.freecadweb.org/feed.php?mode=forums"><link rel="alternate" type="application/atom+xml" title="Feed - Forum - Forum in Deutsch" href="http://forum.freecadweb.org/feed.php?f=13"><link rel="alternate" type="application/atom+xml" title="Feed - Topic - Übersetzung von Toplogical Data Scripting" href="http://forum.freecadweb.org/feed.php?f=13&amp;t=4976">

<!--
	phpBB style name: prosilver
	Based on style:   prosilver (this is the default phpBB3 style)
	Original author:  Tom Beddard ( http://www.subBlue.com/ )
	Modified by:
-->

<script type="text/javascript">
// <![CDATA[
	var jump_page = 'Enter the page number you wish to go to:';
	var on_page = '1';
	var per_page = '10';
	var base_url = './viewtopic.php?f=13&amp;t=4976';
	var style_cookie = 'phpBBstyle';
	var style_cookie_settings = '; path=/; domain=forum.freecadweb.org';
	var onload_functions = new Array();
	var onunload_functions = new Array();

	

	/**
	* Find a member
	*/
	function find_username(url)
	{
		popup(url, 760, 570, '_usersearch');
		return false;
	}

	/**
	* New function for handling multiple calls to window.onload and window.unload by pentapenguin
	*/
	window.onload = function()
	{
		for (var i = 0; i < onload_functions.length; i++)
		{
			eval(onload_functions[i]);
		}
	};

	window.onunload = function()
	{
		for (var i = 0; i < onunload_functions.length; i++)
		{
			eval(onunload_functions[i]);
		}
	};

// ]]>
</script>
<script type="text/javascript" src="viewtopic-Dateien/styleswitcher.js"></script>
<script type="text/javascript" src="viewtopic-Dateien/forum_fn.js"></script>

<link href="viewtopic-Dateien/print.css" rel="stylesheet" type="text/css" media="print" title="printonly">
<link href="viewtopic-Dateien/style.css" rel="stylesheet" type="text/css" media="screen, projection">

<link href="viewtopic-Dateien/phpbb.css" rel="stylesheet" type="text/css">

<link href="viewtopic-Dateien/normal.css" rel="stylesheet" type="text/css" title="A">
<link href="viewtopic-Dateien/medium.css" rel="alternate stylesheet" type="text/css" title="A+">
<link href="viewtopic-Dateien/large.css" rel="alternate stylesheet" type="text/css" title="A++">



<link rel="shortcut icon" href="http://www.freecadweb.org/images/favicon.ico">

</head>

<body id="phpbb" class="section-viewtopic ltr">

<div id="wrap">
	<a id="top" name="top" accesskey="t"></a>
	<div id="page-header">
		<div class="headerbar">
			<div class="inner"><span class="corners-top"><span></span></span>

			<div id="site-description">
				<a href="http://www.forum.freecadweb.org/index.php" title="Board index" id="logo"><img src="viewtopic-Dateien/site_logo.gif" alt="" title="" height="52" width="139"></a>
				<h1>FreeCAD forum</h1>
				<p>The help and development forum of FreeCAD</p>
				<p class="skiplink"><a href="#start_here">Skip to content</a></p>
			</div>

		
			<div id="search-box">
				<form action="./search.php" method="get" id="search">
				<fieldset>
					<input name="keywords" id="keywords" maxlength="128" title="Search for keywords" class="inputbox search" value="Search…" onclick="if(this.value=='Search…')this.value='';" onblur="if(this.value=='')this.value='Search…';" type="text">
					<input class="button2" value="Search" type="submit"><br>
					<a href="http://www.forum.freecadweb.org/search.php" title="View the advanced search options">Advanced search</a> 
				</fieldset>
				</form>
			</div>
		

			<span class="corners-bottom"><span></span></span></div>
		</div>

		<div class="navbar">
			<div class="inner"><span class="corners-top"><span></span></span>

			<ul class="linklist navlinks">
				<li class="icon-home"><a href="http://www.forum.freecadweb.org/index.php" accesskey="h">Board index</a>  <strong>‹</strong> <a href="http://www.forum.freecadweb.org/viewforum.php?f=11">Forums in other languages</a> <strong>‹</strong> <a href="http://www.forum.freecadweb.org/viewforum.php?f=13">Forum in Deutsch</a></li>

				<li class="rightside"><a href="#" onclick="fontsizeup(); return false;" onkeypress="return fontsizeup(event);" class="fontsize" title="Change font size">Change font size</a></li>

				<li class="rightside"><a href="http://www.forum.freecadweb.org/viewtopic.php?f=13&amp;t=4976&amp;view=print" title="Print view" accesskey="p" class="print">Print view</a></li>
			</ul>

			

			<ul class="linklist rightside">
				<li class="icon-faq"><a href="http://www.forum.freecadweb.org/faq.php" title="Frequently Asked Questions">FAQ</a></li>
				<li class="icon-register"><a href="http://www.forum.freecadweb.org/ucp.php?mode=register">Register</a></li>
					<li class="icon-logout"><a href="http://www.forum.freecadweb.org/ucp.php?mode=login" title="Login" accesskey="x">Login</a></li>
				
			</ul>

			<span class="corners-bottom"><span></span></span></div>
		</div>

	</div>

	<a name="start_here"></a>
	<div id="page-body">
		
<h2><a href="http://www.forum.freecadweb.org/viewtopic.php?f=13&amp;t=4976">Übersetzung von Toplogical Data Scripting</a></h2>
<!-- NOTE: remove the style="display: none" when you want to have the forum description on the topic body --><div style="display: none !important;">In diesem Forum Fragen und Diskussionen in Deutsch<br></div>

<div class="topic-actions">

	<div class="buttons">
	
		<div class="reply-icon"><a href="http://www.forum.freecadweb.org/posting.php?mode=reply&amp;f=13&amp;t=4976" title="Post a reply"><span></span>Post a reply</a></div>
	
	</div>

	
		<div class="search-box">
			<form method="get" id="topic-search" action="./search.php">
			<fieldset>
				<input class="inputbox search tiny" name="keywords" id="search_keywords" size="20" value="Search this topic…" onclick="if(this.value=='Search this topic…')this.value='';" onblur="if(this.value=='')this.value='Search this topic…';" type="text">
				<input class="button2" value="Search" type="submit">
				<input name="t" value="4976" type="hidden">
<input name="sf" value="msgonly" type="hidden">

			</fieldset>
			</form>
		</div>
	
		<div class="pagination">
			11 posts
			 • <a href="#" onclick="jumpto(); return false;" title="Click to jump to page…">Page <strong>1</strong> of <strong>2</strong></a> • <span><strong>1</strong><span class="page-sep">, </span><a href="http://www.forum.freecadweb.org/viewtopic.php?f=13&amp;t=4976&amp;start=10">2</a></span>
		</div>
	

</div>
<div class="clear"></div>


	<div id="p38955" class="post bg2">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			

			<h3 class="first"><a href="#p38955">Übersetzung von Toplogical Data Scripting</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=38955#p38955"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">berndhahnebach</a></strong> » Mon Nov 18, 2013 11:35 pm </p>

			

			<div class="content">Hallo Zusammen, <br><br>ich würde mich gerne mal an der Übersetzung der folgenden Wiki - Seite versuchen. <br><br><!-- m --><a class="postlink" href="http://freecadweb.org/wiki/index.php?title=Topological_data_scripting">http://freecadweb.org/wiki/index.php?ti ... _scripting</a><!-- m --><br><br>Warum? Nun weil ich selber mit dem Abschnitt Topologie (<!-- m --><a class="postlink" href="http://freecadweb.org/wiki/index.php?title=Topological_data_scripting#Topology">http://freecadweb.org/wiki/index.php?ti ... g#Topology</a><!-- m -->)
 Probleme bei der Übersetzung habe und einfach nicht alles richtig 
verstehe, und eventuell bin ich ja nicht der Einzige. Das sind aber nun 
mal die Basics von FreeCAD. Da hier einige Cracks auf deutsch (inclusive
 Hautentwickler Werner und Jürgen und beitragende Entwickler) unterwegs 
sind, wollte ich hier kurz um Hilfe bitten. Ich mache einfach einen 
Vorschlag anbei und denke da wird von ganz alleine einiges an Kritik 
aufkommen <img src="viewtopic-Dateien/icon_e_wink.gif" alt=";-)" title="Wink">.
 Ich habe Topologie so übersetzt auch wenn ich trotz Lesen auf Wikipedia
 nichts greifbares mit dem Begriff anfangen kann. In Klammern hab ich 
meine eigenen Kommentare gesetzt. Ich habe um Missverständnisse 
vorzubeugen die englischen topologischen Datentypen belassen. Sollen die
 durch deutsche erstetzt werden? <br><br><span style="font-weight: bold"><span style="text-decoration: underline">Geometrie</span></span><br>Die Geometrischen Objekte sind die Basiskomponenten von allen Topologischen Objekten.<br><br>- <span style="font-weight: bold">Geom</span>	Basisklasse der Geometrischen Objekte.<br><br>- <span style="font-weight: bold">Line</span>	Eine gerade Linie im 3D, definiert durch einen Start- und einen Endpunkt.<br><br>- <span style="font-weight: bold">Kreis</span>	Ein Kreis oder ein Kreissegment definiert durch einen Mittelpunkt und einen Start- und Endpunkt.<br><br>- <span style="font-weight: bold">...…</span>	Bald werden noch einige folgen <img src="viewtopic-Dateien/icon_e_wink.gif" alt=";-)" title="Wink"><br><br><br><span style="text-decoration: underline"><span style="font-weight: bold">Topologie</span></span><br><br>Die folgenden topologischen Datentypen gibt es:<br><br>- <span style="font-weight: bold">Compound</span>	Eine Gruppe jeglichen topologischen Datentyps.<br><br>- <span style="font-weight: bold">Compsolid</span>
	Ein composite solid ist eine Zusammensetzung aus solids die durch ihre 
faces verbunden sind. Es erweitert die Notation (oder Bezeichnung) von 
wire und shell zu solids. (Zusammengesetzter Körper)<br><br>- <span style="font-weight: bold">Solid</span>	Ein solid ist ein räumliches Gebilde begrenzt durch shells. Es ist dreidimensional. (Grundkörper)<br><br>- <span style="font-weight: bold">Shell</span>	Eine Zusammensetzung von faces die durch ihre edges verbunden sind. Ein shell kann offen oder geschlossen sein. (Oberfläche)<br><br>- <span style="font-weight: bold">Face</span>
	Im 2D ist es ein Teil einer Ebene. Im 3D ist es ein Teil einer 
Oberfläche. Seine Geometrie ist begrenzt durch Aussenlinien (seine 
Kontur). Es ist zweidimensional. (Ein Face ist im 3D eben?!)<br><br>- <span style="font-weight: bold">Wire</span>
	Eine Zusammensetzung von edges die durch ihre vertices verbunden sind. 
Es kann  einen offenen oder geschlossenen Umriss haben, je nach dem ob 
die edges untereinander verbunden sind oder nicht. (Im 2D ist es ein 
Polygonzug (Polylinie)). (Geschlossen ist es eine Kontur) (Kann ein Wire
 auch Kreissegmente beinhalten? Ist dann ein B-Spline ein Wire? Dann 
wäre es sicher kein Polygonzug?)<br><br>- <span style="font-weight: bold">Edge</span>
	Ein topologisches Element welches einer Linie (Kante) entspricht. Ein 
edge ist  grundsätzlich begrenzt durch vertices. Es ist eindimensional. 
(Im 2D ist es eine Linie.) (Hat ein Zylinder nicht eine Kande, die ein 
Kreis ist und was ist mit einer Kugel, hat die keine Edges?)<br><br>- <span style="font-weight: bold">Vertex</span>	Ein topologisches Element, welches einem Punkt entspricht. Es hat keine Dimension.<br><br>- <span style="font-weight: bold">Shape</span>	Ein allgemeiner Begriff der alle oben genannten Datentypen umfassen kann.<br><br>gruss bernd</div>

			

		</div>

		
			<dl class="postprofile" id="profile38955">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">berndhahnebach</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 136</dd><dd><strong>Joined:</strong> Sun Sep 08, 2013 8:07 pm</dd><dd><strong>Location:</strong> Zürich, Switzerland</dd>

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p38969" class="post bg1">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			

			<h3><a href="#p38969">Re: Übersetzung von Toplogical Data Scripting</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=38969#p38969"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a></strong> » Tue Nov 19, 2013 9:39 am </p>

			

			<div class="content"><blockquote class="uncited"><div>Kreis Ein Kreis oder ein Kreissegment definiert durch einen Mittelpunkt und einen Start- und Endpunkt.</div></blockquote><br>Eigentlich
 ist die Geometrie immer ein Vollkreis. Ein Kreissegment kann man dann 
erzeugen, wenn man bei der Erzeugung der entspr. Edge nicht den vollen 
Parameterbereich nimmt (hier: [0, 2*PI]).<br><br><blockquote class="uncited"><div>- Solid Ein solid ist ein räumliches Gebilde begrenzt durch shells. Es ist dreidimensional. (Grundkörper)</div></blockquote><br>Vielleicht sollte man noch erwähnen, dass ein Solid vollständig geschlossen ist.<br><br><blockquote class="uncited"><div>(Ein Face ist im 3D eben?!)</div></blockquote><br>Nein!
 Zum Beispiel Mantelfläche eines Zylinders oder Kegels, 
B-Spline-Flächen, usw. Im Original wird 2D und 3D besonders behandelt. 
Natürlich kann im R^2 ein Face nur eine (begrenzte) Ebene beschreiben, 
was denn sonst? Da FreeCAD ohnehin ein rein 3D-CAD-Programm ist, würde 
ich die Unterscheidung zwischen 2D und 3D weglassen.<br><br><blockquote class="uncited"><div>(Im 2D ist es ein Polygonzug (Polylinie)).</div></blockquote><br>Es kann einer sein, muss aber nicht. Also: falsch<br><br><blockquote class="uncited"><div>Kann ein Wire auch Kreissegmente beinhalten?</div></blockquote><br>Ja.<br><br><blockquote class="uncited"><div>Ist dann ein B-Spline ein Wire?</div></blockquote><br>Nein.
 Eine B-Spline-Kurve ist eine Geometrie. Man kann daraus eine Edge 
erzeugen und aus einer Edge wiederum ein Wire (bestehend nur aus dieser 
Edge).<br><br><blockquote class="uncited"><div>Ein topologisches Element welches einer Linie (Kante) entspricht.</div></blockquote><br>Der Begriff "Kurve" ist m.E. angemessener.<br><br><blockquote class="uncited"><div>Hat ein Zylinder nicht eine Kande, die ein Kreis ist</div></blockquote><br>Exakt.<br><br><blockquote class="uncited"><div>und was ist mit einer Kugel, hat die keine Edges?</div></blockquote><br>Es
 gibt wohl CAD-Kerne, die die Fläche einer Vollkugel ohne Edge 
beschreiben können. In OpenCascade muss ein Face immer mindestens eine 
Edge haben. In diesem Beispiel beschreibt die Edge einen Halbkreis.</div>

			

		</div>

		
			<dl class="postprofile" id="profile38969">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a>
			</dt>

			<dd>Site Admin</dd>

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 5654</dd><dd><strong>Joined:</strong> Thu Feb 19, 2009 10:32 am</dd>

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p38999" class="post bg2">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			

			<h3><a href="#p38999">Re: Übersetzung von Toplogical Data Scripting</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=38999#p38999"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">berndhahnebach</a></strong> » Tue Nov 19, 2013 8:34 pm </p>

			

			<div class="content">Vielen vielen Dank Werner für die 
Verbesserungen. Ich blicke etwas mehr durch. Ein zweiter Versuch mit 
weiteren Aussagen. Wo hab ich Falschaussagen eingebaut??<br><br>Ich 
würde die Übersetzung nicht wort für wort vornehmen sondern vor allem 
versuchen den Sinn verständlich zu erläutern. Auf jeden Fall halte ich 
mich an die Abschnittsunterteilungen. Die Topologie würde ich 
logischerweise mit vertex anfangen und mit compound aufhören. Ist ja 
sonst blöd etwas zu definieren, wenn etwas anderes noch gar nicht 
definiert ist. Was meint Ihr?<br><br><br>Neue Aussagen:<br><br>- Ein 
edge kann eine ganz wilde gebogene Kurve sein. Ein Edge hat entweder 
keinen (bsp kreis) vertex oder genau zwei vertexe. Wird ein weiterer 
vertex eingefügt ist es ein wire. <br>- Sobald ein edge ein Knick hat ist es ein wire.<br>- Ein face ist immer begrenzt durch edges (das können doch auch wires sein?). <br>- Sobald ein face ein knick hat ist es ein shell.<br>- Ist die Oberfläche eines Torus ein face? <br>- Alle faces eines Solids bilden seine shell.<br>- Ein solid hat genau eine geschlossene shell, wobei zwei seitenflächen eines würfels bilden ja auch schon ein shell?<br>- Ein Würfel, bei dem ein face fehlt ist kein solid, sodern eine offene shell.<br><br><span style="font-weight: bold"><span style="text-decoration: underline">Geometrie</span></span><br>Die Geometrischen Objekte sind die Basiskomponenten von allen Topologischen Objekten.<br><br>- <span style="font-weight: bold">Geom</span>  Basisklasse der Geometrischen Objekte.<br><br>- <span style="font-weight: bold">Line</span>  Eine gerade Linie im 3D, definiert durch einen Start- und einen Endpunkt.<br><br>- <span style="font-weight: bold">Kreis</span>
  Ein Kreis definiert durch seinen Mittelpunkt. Ein Kreissegment wird 
definiert in dem ein Kreis nur in einem Teil seines Parameterbereich 
([0, 2*PI]) definiert wird.<br><br>- <span style="font-weight: bold">B-Spline</span>  ?<br><br>- <span style="font-weight: bold">...…</span>	Bald werden noch einige folgen <img src="viewtopic-Dateien/icon_e_wink.gif" alt=";-)" title="Wink"><br><br><br><span style="text-decoration: underline"><span style="font-weight: bold">Topologie</span></span><br><br>Die folgenden topologischen Datentypen gibt es:<br><br>- <span style="font-weight: bold">Vertex</span>  Ein topologisches Element, welches einem Punkt entspricht. Es hat keine Dimension.<br><br>- <span style="font-weight: bold">Edge</span>
  Ein topologisches Element welches einer Kurve entspricht. Ein edge ist
 grundsätzlich begrenzt durch vertices. Es ist eindimensional. <br><br>- <span style="font-weight: bold">Wire</span>
	Eine Zusammensetzung von edges die durch ihre vertices verbunden sind. 
Es kann  einen offenen oder geschlossenen Umriss haben, je nach dem ob 
die edges untereinander verbunden sind oder nicht. <br><br>- <span style="font-weight: bold">Face</span>
  Ein topologisches Element welches einer Fläche entspricht. Seine 
Geometrie ist begrenzt durch seine Kontur. Ein face ist ein Teil einer 
Oberfläche. Es ist zweidimensional. Beispiele wären eine Seitenfläche 
eines Würfels oder die Bodenfläche eines Kegels. Die Mantelfläche eines 
Kegels oder Zylinders sind auch faces. Die Oberfläche einer Vollkugel 
ist ein face.<br><br>- <span style="font-weight: bold">Shell</span>   
Eine Zusammensetzung von faces die durch ihre edges verbunden sind. Ein 
shell kann offen oder geschlossen sein. Ein shell ist eine Oberfläche. 
Beispiele wären die Oberfläche eines Würfels oder eines Zylinders.<br><br>- <span style="font-weight: bold">Solid</span>
  Ein solid ist ein räumliches Gebilde begrenzt durch shells. Es ist 
dreidimensional. Ein solid ist ein vollständig geschlossener Körper. 
Beispiele wären ein Zylinder, Würfel oder eine Kugel. <br><br>- <span style="font-weight: bold">Compsolid</span>
  Ein composite solid ist eine Zusammensetzung von solids die durch ihre
 faces verbunden sind. Es erweitert die Notation von wire und shell zu 
solids. Ein solid ist ein zusammengesetzter Körper.<br><br>- <span style="font-weight: bold">Compound</span>  Eine Gruppe jeglichen topologischen Datentyps.<br><br>- <span style="font-weight: bold">Shape</span>  Ein allgemeiner Begriff der alle oben genannten Datentypen umfassen kann.<br><br><br>Was
 hab ich mir da ausgedacht ... Ich muss für heute aufhören, in meinem 
Kopf dehen sich kegelstumpfe und pyramiden herum ... Naja ich brauch das
 aber, ich will Massenberechnungen an Gebäudemodellen machen. Bei meinen
 ersten Versuchen hab ich gemerkt, dass ich genau obige Definitionen die
 Grundlage bilden. Je mehr ich durchblicke um so mehr bin ich davon 
überzeugt, dass FreeCAD mit Python das richtige Werkzeug dafür ist, aber
 umso komplexer wird die Materie. <br><br>gruss bernd</div>

			

		</div>

		
			<dl class="postprofile" id="profile38999">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">berndhahnebach</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 136</dd><dd><strong>Joined:</strong> Sun Sep 08, 2013 8:07 pm</dd><dd><strong>Location:</strong> Zürich, Switzerland</dd>

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p39005" class="post bg1">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			

			<h3><a href="#p39005">Re: Übersetzung von Toplogical Data Scripting</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=39005#p39005"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a></strong> » Tue Nov 19, 2013 11:40 pm </p>

			

			<div class="content"><blockquote class="uncited"><div>Ich würde die 
Übersetzung nicht wort für wort vornehmen sondern vor allem versuchen 
den Sinn verständlich zu erläutern. Auf jeden Fall halte ich mich an die
 Abschnittsunterteilungen. Die Topologie würde ich logischerweise mit 
vertex anfangen und mit compound aufhören. Ist ja sonst blöd etwas zu 
definieren, wenn etwas anderes noch gar nicht definiert ist. Was meint 
Ihr?</div></blockquote><br>Genau das war mir auch aufgefallen.<br><br><blockquote class="uncited"><div>-
 Ein edge kann eine ganz wilde gebogene Kurve sein. Ein Edge hat 
entweder keinen (bsp kreis) vertex oder genau zwei vertexe. Wird ein 
weiterer vertex eingefügt ist es ein wire.</div></blockquote><br>Ich 
denke "hat" passt besser als "ist". Also: "Ein Edge kann eine ganz wilde
 gebogene Kurve haben." Analog zu Face hat eine Edge auch immer 
mindestens einen Vertex, auch ein Vollkreis.<br><br><blockquote class="uncited"><div>- Sobald ein edge ein Knick hat ist es ein wire.</div></blockquote><br>Hm,
 ist eine etwas komische Aussage. Eine Edge kann in diesem Zusammenhang 
keinen Knick haben. Eigentlich ist es ganz einfach. Ein Wire besteht 
immer aus mehreren zusammenhängenden Edges. Fertig!<br><br><blockquote class="uncited"><div>- Ein face ist immer begrenzt durch edges (das können doch auch wires sein?).</div></blockquote><br>So gesehen wird ein Face durch Wires begrenzt (die ja aber aus Edges bestehen).<br><br><blockquote class="uncited"><div>- Ist die Oberfläche eines Torus ein face?</div></blockquote><br>Torus ist eine Geometrie. Aber das entsprechende Shape-Objekt ist sogar ein Solid.<br><br><blockquote class="uncited"><div>- Alle faces eines Solids bilden seine shell.</div></blockquote><br>Ja.<br><br><blockquote class="uncited"><div> Ein solid hat genau eine geschlossene shell, wobei zwei seitenflächen eines würfels bilden ja auch schon ein shell?</div></blockquote><br>Ja
 und Ja. Nur um irgendwelche Missverständnissen vorzubeugen. Zwei 
verbundene Seitenflächen bilden zwar eine Shell, aber noch kein Solid. 
Erst der ganze Würfel bildet ein Solid. <br><br><blockquote class="uncited"><div>- Ein Würfel, bei dem ein face fehlt ist kein solid, sodern eine offene shell.</div></blockquote><br>Wobei dann ist es auch kein Würfel, aber stimmt schon.<br><br><blockquote class="uncited"><div>Die Geometrischen Objekte sind die Basiskomponenten von allen Topologischen Objekten.</div></blockquote><br>Also
 die topologischen Objekte Vertex, Edge und Face entsprechen einer 
Geometrie. Die anderen Objekte Wire, Shell, Solid, Compound sind rein 
topologische Objekte.</div>

			

		</div>

		
			<dl class="postprofile" id="profile39005">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a>
			</dt>

			<dd>Site Admin</dd>

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 5654</dd><dd><strong>Joined:</strong> Thu Feb 19, 2009 10:32 am</dd>

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p39019" class="post bg2">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			

			<h3><a href="#p39019">Re: Übersetzung von Toplogical Data Scripting</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=39019#p39019"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">berndhahnebach</a></strong> » Wed Nov 20, 2013 7:11 am </p>

			

			<div class="content">Guten Morgen Zusammen,<br><br>nochmals vielen Dank Werner, so langsam komme ich den shapes auf die Schliche. <img src="viewtopic-Dateien/icon_e_smile.gif" alt=":-)" title="Smile">
 Der Abschnitt neue Aussagen war nur für mein Verständnis und nicht für 
die Übersetzung gedacht. Wenn mans verstanden hat ist es einfach, weil 
logisch aufeinander aufgebaut. Ich bin heute bis spät abends belegt, und
 hoffe aber morgen das ganze nochmal zu einer dritten Version 
zusammenzubringen, die dann ja evtl. schon brauchbar fürs Wiki ist. <br><br>gruss bernd.</div>

			

		</div>

		
			<dl class="postprofile" id="profile39019">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">berndhahnebach</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 136</dd><dd><strong>Joined:</strong> Sun Sep 08, 2013 8:07 pm</dd><dd><strong>Location:</strong> Zürich, Switzerland</dd>

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p39027" class="post bg1">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			

			<h3><a href="#p39027">Re: Übersetzung von Toplogical Data Scripting</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=39027#p39027"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=765" style="color: #00AA00;" class="username-coloured">shoogen</a></strong> » Wed Nov 20, 2013 12:28 pm </p>

			

			<div class="content"><blockquote><div><cite>wmayer wrote:</cite><blockquote class="uncited"><div>- Sobald ein edge ein Knick hat ist es ein wire.</div></blockquote><br>Hm,
 ist eine etwas komische Aussage. Eine Edge kann in diesem Zusammenhang 
keinen Knick haben. Eigentlich ist es ganz einfach. Ein Wire besteht 
immer aus mehreren zusammenhängenden Edges. Fertig!</div></blockquote><br>Ein Wire kann auch aus einem Edge bestehen.<br>Das Wort <span style="font-weight: bold">Knick</span> würde ich hier ganz vermeiden. Eine BSpline kann an Punkte mit C0 Übergängen haben.<br>Anderseits kann der Übergang von Liniensegment zum Kreissegment die gleiche Tangente haben.</div>

			

		</div>

		
			<dl class="postprofile" id="profile39027">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=765" style="color: #00AA00;" class="username-coloured">shoogen</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 826</dd><dd><strong>Joined:</strong> Thu Dec 01, 2011 5:24 pm</dd>

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p39032" class="post bg2">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			

			<h3><a href="#p39032">Re: Übersetzung von Toplogical Data Scripting</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=39032#p39032"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a></strong> » Wed Nov 20, 2013 1:11 pm </p>

			

			<div class="content"><blockquote class="uncited"><div>Ein Wire kann auch aus einem Edge bestehen.</div></blockquote><br>Richtig, das habe ich oben aber auch schon gesagt. Also: Ein Wire besteht immer aus einer oder mehreren zusammenhängenden Edges.<br><br><blockquote class="uncited"><div>Das Wort Knick würde ich hier ganz vermeiden. Eine BSpline kann an Punkte mit C0 Übergängen haben.</div></blockquote><br>Ja,
 aber worauf Bernd hinaus wollte ist, dass wenn an dem "Knickpunkt" ein 
Vertex ist, kann es nur ein Wire sein. Aber es ist schon richtig, wenn 
eine BSpine-Kurve vom Grad eins ist, dann fällt sie mit ihrem 
Kontrollpolygon zusammen und kann daher als Edge Knicke haben.<br><dl class="codebox"><dt>Code: <a href="#" onclick="selectCode(this); return false;">Select all</a></dt><dd><code>import Part<br>p=[App.Vector(0,0,0),App.Vector(10,0,0),App.Vector(10,10,0),App.Vector(0,10,0)]<br>s=Part.BSplineCurve()<br>s.buildFromPoles(p,False,1)<br>Part.show(s.toShape())<br></code></dd></dl></div>

			

		</div>

		
			<dl class="postprofile" id="profile39032">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a>
			</dt>

			<dd>Site Admin</dd>

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 5654</dd><dd><strong>Joined:</strong> Thu Feb 19, 2009 10:32 am</dd>

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p39054" class="post bg1">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			

			<h3><a href="#p39054">Re: Übersetzung von Toplogical Data Scripting</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=39054#p39054"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">berndhahnebach</a></strong> » Wed Nov 20, 2013 6:19 pm </p>

			

			<div class="content">Nur kurz, genau Werner, du hast das mit dem 
"Knick" und dem vertex vollkommen richtig erkannt. Wird nicht in der 
Übersetzung vorkommen. Danke für den Tip mit den B-Splines. Spline mit 
Ecke,  <img src="viewtopic-Dateien/icon_e_biggrin.gif" alt=":D" title="Very Happy"> Ihr seid einfach so cool.  <img src="viewtopic-Dateien/icon_e_smile.gif" alt=":)" title="Smile"> <br><br>Wenn ein wire aus einem edge bestehen kann, kann sicher auch eine shell auch nur aus einem face bestehen.<br><br>Woher
 weiss ich ob es ein edge oder ein wire ist resp. eine shell oder face? 
Habe gerade ein wenig mit körpern in freecad gespielt. Lässt sich das in
 der gui irgendwo erfahren?<br><br>Genauso wie ein edge der ein 
vollkreis ist, immer ein vertex hat, hat ein face, der eine Mantelfläche
 eines zylinders ist immer ein face mit zwei vertexen. <br><br>(WB 
Draft) Ein regelmässiges Polygon und ein Zweipunktrechteck sind 
demzufolge wire. Eine ellipse ist ein edge. Mehrpunktentwurfsdraht ist 
ja mal ne coole Übersetzung. Glaube so ein Gebilde lässt sich wohl mit 
keiner anderen sprache zusammenbauen  <img src="viewtopic-Dateien/icon_lol.gif" alt=":lol:" title="Laughing">
 . Hab mein FreeCAD sonst immer nur in englisch. Das wort ist so cool, 
da ist selber google vollkommen sprachlos, das muss man erst mal 
schaffen.<br><br>Eins noch, ich hab im deutschen forum ein wenig 
herumgelesen und auch in der kleinen disskusion hier schon gemerkt, die 
begriffe vertex, edge, wire, face, shell, solid, ... bleiben des 
besseren Verständis wegen englisch. Das hab ich bisher so gemacht und 
werde das auch so bei der Übersetzung machen. <br><br>Das wars für heute, hoffentlich morgen mit mehr zeit und mal ein ergebnis. Bernd<br><br>EDIT: Ich vote dafür den Begriff Mehrpunktentwurfsdraht so zu belassen!</div>

			

		</div>

		
			<dl class="postprofile" id="profile39054">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">berndhahnebach</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 136</dd><dd><strong>Joined:</strong> Sun Sep 08, 2013 8:07 pm</dd><dd><strong>Location:</strong> Zürich, Switzerland</dd>

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p39072" class="post bg2">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			

			<h3><a href="#p39072">Re: Übersetzung von Toplogical Data Scripting</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=39072#p39072"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a></strong> » Wed Nov 20, 2013 9:29 pm </p>

			

			<div class="content"><blockquote class="uncited"><div>Woher weiss ich
 ob es ein edge oder ein wire ist resp. eine shell oder face? Habe 
gerade ein wenig mit körpern in freecad gespielt. Lässt sich das in der 
gui irgendwo erfahren?</div></blockquote><br>In der GUI gibt es noch 
keine solche Möglichkeit, jedoch über Python schon. Ein Shape-Objekt hat
 das Attribut "ShapeType", dass dann den entsprechenden Typ 
zurückliefert.</div>

			

		</div>

		
			<dl class="postprofile" id="profile39072">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=69" style="color: #0066FF;" class="username-coloured">wmayer</a>
			</dt>

			<dd>Site Admin</dd>

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 5654</dd><dd><strong>Joined:</strong> Thu Feb 19, 2009 10:32 am</dd>

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<div id="p39164" class="post bg1">
		<div class="inner"><span class="corners-top"><span></span></span>

		<div class="postbody">
			

			<h3><a href="#p39164">Re: Übersetzung von Toplogical Data Scripting</a></h3>
			<p class="author"><a href="http://www.forum.freecadweb.org/viewtopic.php?p=39164#p39164"><img src="viewtopic-Dateien/icon_post_target.gif" alt="Post" title="Post" height="9" width="11"></a>by <strong><a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">berndhahnebach</a></strong> » Thu Nov 21, 2013 6:59 pm </p>

			

			<div class="content"><blockquote><div><cite>wmayer wrote:</cite>... Ein Shape-Objekt hat das Attribut "ShapeType", dass dann den entsprechenden Typ zurückliefert.</div></blockquote>Danke<br><br>Sobald
 ein DWire geschlossen ist, dann ist der shape typ face. Ist das 
allgemein gültig? Gilt das auch für eine shell? Sobald eine Shell ein 
"LuftVolumen" vollständig umschliesst wird es als solid erkannt?</div>

			

		</div>

		
			<dl class="postprofile" id="profile39164">
			<dt>
				<a href="http://www.forum.freecadweb.org/memberlist.php?mode=viewprofile&amp;u=2069">berndhahnebach</a>
			</dt>

			

		<dd>&nbsp;</dd>

		<dd><strong>Posts:</strong> 136</dd><dd><strong>Joined:</strong> Sun Sep 08, 2013 8:07 pm</dd><dd><strong>Location:</strong> Zürich, Switzerland</dd>

		</dl>
	

		<div class="back2top"><a href="#wrap" class="top" title="Top">Top</a></div>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<hr class="divider">

	<form id="viewtopic" method="post" action="./viewtopic.php?f=13&amp;t=4976">

	<fieldset class="display-options" style="margin-top: 0; ">
		<a href="http://www.forum.freecadweb.org/viewtopic.php?f=13&amp;t=4976&amp;start=10" class="right-box right">Next</a>
		<label>Display posts from previous: <select name="st" id="st"><option value="0" selected="selected">All posts</option><option value="1">1 day</option><option value="7">7 days</option><option value="14">2 weeks</option><option value="30">1 month</option><option value="90">3 months</option><option value="180">6 months</option><option value="365">1 year</option></select></label>
		<label>Sort by <select name="sk" id="sk"><option value="a">Author</option><option value="t" selected="selected">Post time</option><option value="s">Subject</option></select></label> <label><select name="sd" id="sd"><option value="a" selected="selected">Ascending</option><option value="d">Descending</option></select> <input name="sort" value="Go" class="button2" type="submit"></label>
		
	</fieldset>

	</form>
	<hr>


<div class="topic-actions">
	<div class="buttons">
	
		<div class="reply-icon"><a href="http://www.forum.freecadweb.org/posting.php?mode=reply&amp;f=13&amp;t=4976" title="Post a reply"><span></span>Post a reply</a></div>
	
	</div>

	
		<div class="pagination">
			11 posts
			 • <a href="#" onclick="jumpto(); return false;" title="Click to jump to page…">Page <strong>1</strong> of <strong>2</strong></a> • <span><strong>1</strong><span class="page-sep">, </span><a href="http://www.forum.freecadweb.org/viewtopic.php?f=13&amp;t=4976&amp;start=10">2</a></span>
		</div>
	
</div>


	<p></p><p><a href="http://www.forum.freecadweb.org/viewforum.php?f=13" class="left-box left" accesskey="r">Return to Forum in Deutsch</a></p>

	<form method="post" id="jumpbox" action="./viewforum.php" onsubmit="if(this.f.value == -1){return false;}">

	
		<fieldset class="jumpbox">
	
			<label for="f" accesskey="j">Jump to:</label>
			<select name="f" id="f" onchange="if(this.options[this.selectedIndex].value != -1){ document.forms['jumpbox'].submit() }">
			
				<option value="-1">Select a forum</option>
			<option value="-1">------------------</option>
				<option value="7">Users</option>
			
				<option value="3">&nbsp; &nbsp;Help on using FreeCAD</option>
			
				<option value="22">&nbsp; &nbsp;Python scripting and macros</option>
			
				<option value="4">&nbsp; &nbsp;Install / Compile</option>
			
				<option value="8">&nbsp; &nbsp;Open discussion</option>
			
				<option value="9">&nbsp; &nbsp;Feature Announcements</option>
			
				<option value="6">Development</option>
			
				<option value="10">&nbsp; &nbsp;Developers corner</option>
			
				<option value="17">&nbsp; &nbsp;Pull Requests</option>
			
				<option value="19">&nbsp; &nbsp;Part Design</option>
			
				<option value="20">&nbsp; &nbsp;Assembly</option>
			
				<option value="18">&nbsp; &nbsp;FEM</option>
			
				<option value="15">&nbsp; &nbsp;CAM</option>
			
				<option value="23">&nbsp; &nbsp;Arch</option>
			
				<option value="21">&nbsp; &nbsp;Wiki</option>
			
				<option value="11">Forums in other languages</option>
			
				<option value="12">&nbsp; &nbsp;Forum français</option>
			
				<option value="13" selected="selected">&nbsp; &nbsp;Forum in Deutsch</option>
			
				<option value="14">&nbsp; &nbsp;Foro en Español</option>
			
				<option value="16">&nbsp; &nbsp;日本語フォーラム</option>
			
			</select>
			<input value="Go" class="button2" type="submit">
		</fieldset>
	</form>


	<h3>Who is online</h3>
	<p>Users browsing this forum: No registered users and 1 guest</p>
</div>

<div id="page-footer">

	<div class="navbar">
		<div class="inner"><span class="corners-top"><span></span></span>

		<ul class="linklist">
			<li class="icon-home"><a href="http://www.forum.freecadweb.org/index.php" accesskey="h">Board index</a></li>
				
			<li class="rightside"><a href="http://www.forum.freecadweb.org/memberlist.php?mode=leaders">The team</a> • <a href="http://www.forum.freecadweb.org/ucp.php?mode=delete_cookies">Delete all board cookies</a> • All times are UTC </li>
		</ul>

		<span class="corners-bottom"><span></span></span></div>
	</div>

	<div class="copyright">Powered by <a href="https://www.phpbb.com/">phpBB</a>® Forum Software © phpBB Group
		
	</div>
</div>

</div>

<div>
	<a id="bottom" name="bottom" accesskey="z"></a>
	
</div>


</body></html>