from FreeCAD import Base
V1 = Base.Vector(0,10,0)
V2 = Base.Vector(30,10,0)
V3 = Base.Vector(30,-10,0)
V4 = Base.Vector(0,-10,0)

VC1 = Base.Vector(-10,0,0)
C1 = Part.Arc(V1,VC1,V4)
# and the second one
VC2 = Base.Vector(40,0,0)
C2 = Part.Arc(V2,VC2,V3)

L1 = Part.Line(V1,V2)
# and the second one
L2 = Part.Line(V3,V4)

S1 = Part.Shape([C1,C2,L1,L2])

W = Part.Wire(S1.Edges)
P = W.extrude(Base.Vector(0,0,10))

Part.show(P)


V1 = Base.Vector(0,10,0)
V2 = Base.Vector(30,10,0)
V3 = Base.Vector(60,5,0)
L3 = Part.Line(V1,V2)
L4 = Part.Line(V2,V3)
S2 = Part.Shape([L1,L2])
Part.show(S2)




F = Part.Face(W)
P2 = F.extrude(Base.Vector(0,0,-10))
P2.translate(Base.Vector(100,0,0))
Part.show(P2)


import Part
plane = Part.makePlane(2,2)
plane.isValid()
Part.show(plane)
plane.Faces
plane.Edges
from FreeCAD import Base
plane.translate(Base.Vector(-4,0,0))

edge1 = Part.makeLine((0,0,0), (10,0,0))
edge2 = Part.makeLine((10,0,0), (10,10,0))
wire1 = Part.Wire([edge1, edge2])
edge3 = Part.makeLine((10,10,0), (5,15,0))
edge4 = Part.makeLine((5,15,0), (0,10,0))
edge5 = Part.makeLine((0,10,0), (0,0,0))
wire2 = Part.Wire([edge3, edge4, edge5])
wire3 = Part.Wire([wire1, wire2])
wire3.Edges


hasattr(wire3, "Edges")

if hasattr(wire3, "Edges"):
  print('found Edges')
 

wire3.Length
wire3.CenterOfMass
wire3.isClosed()
wire2.isClosed()


face = Part.Face(wire3)
face.Area
face.CenterOfMass
face.Length
face.isValid
face.Edges
Part.show(face)


