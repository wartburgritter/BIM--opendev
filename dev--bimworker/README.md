# ************************************************************************************************
# in folgenden Verzeichnissen, in allen
# am besten ein Terminal mit einem Tab je Verzeichnis

git pull

cd /home/hugo/Dokumente/job_fbj/BIMTesterAll/BIMTester/Modellpruefung/
cd /home/hugo/Dokumente/job_fbj/BIMTesterAll/BIMTester/Modellregeln/
cd /home/hugo/Documents/dev/bimtester/jobcode/fbjcode/
cd /home/hugo/.local/share/FreeCAD/Mod/
cd /home/hugo/.local/share/FreeCAD/Mod/bimtester/
cd /home/hugo/Documents/projekte--opendev_bim/dev--bimworker/


# ************************************************************************************************
# BIMWorker
# ein weiteres Terminal zum arbeiten fuer run alles moegliche waehrend coding

cd ~/Documents/dev/bimtester/jobcode/fbjcode/scriptBIMTesting
flake8 --ignore=W503 --max-line-length=150 *.py
flake8 --ignore=W503 --max-line-length=100 *.py

cd ~/Documents/dev/bimtester/jobcode/fbjcode/scriptBIMTesting
autopep8 -i --select=E202 *.py
autopep8 -i --select=E225 *.py
autopep8 -i --select=E226 *.py
autopep8 -i --select=E231 *.py
autopep8 -i --select=E251 *.py
autopep8 -i --select=E261 *.py
autopep8 -i --select=E265 *.py
autopep8 -i --select=E271 *.py
autopep8 -i --select=E302 *.py
autopep8 -i --select=E303 *.py
autopep8 -i --select=E271 *.py
autopep8 -i --select=W291 *.py
autopep8 -i --select=W293 *.py
autopep8 -i --select=W391 *.py
