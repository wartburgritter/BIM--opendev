

#######################################################################################

git clone https://github.com/openscenegraph/OpenSceneGraph -b OpenSceneGraph-3.4   osg34


Packages
libxrandr-dev  libtiff5-dev  libpoppler-glib-dev  librsvg2-dev  libcairo2-dev  libcurl4-gnutls-dev libgtkglext1-dev  libjasper-dev  libgdal-dev  libsdl1.2-dev  
qt5-qmake libqt5widgets5 libqt5opengl5-dev qttools5-dev qtbase5-dev 

libgstreamer1.0-dev  


cmake  \
-DCMAKE_BUILD_TYPE=Debug \
-DCMAKE_PREFIX_PATH=/usr/lib/x86_64-linux-gnu/qt5/bin/qmake \
../osg34/


package gta is not found --> ignore


sudo make install_ld_conf

make -j 4


make install

#######################################################################################

cmake braucht die release libraries von osg, sonst gibt es fehler
wenn die da sind reicht ein
cmake  -DCMAKE_BUILD_TYPE=Debug -DCMAKE_LIBRARY_PATH=/usr/local/lib64 ../ifcplusplus
wenn die nicht da sind wie unten einfach die debug auf die release setzen 

#######################################################################################

apt-get install libopenthreads-dev
better deinstall openscenegraph packages


git clone git://github.com/berndhahnebach/IFCPlusPlus
cd  ifcplusplus

cd ..
mkdir build
cd build

cmake                    \
-DCMAKE_BUILD_TYPE=Debug \
-DOSGDB_LIBRARY=/usr/local/lib64/libosgDBd.so         \
-DOSGFX_LIBRARY=/usr/local/lib64/libosgFXd.so         \
-DOSGGA_LIBRARY=/usr/local/lib64/libosgGAd.so         \
-DOSGSIM_LIBRARY=/usr/local/lib64/libosgSimd.so       \
-DOSGTEXT_LIBRARY=/usr/local/lib64/libosgTextd.so     \
-DOSGUTIL_LIBRARY=/usr/local/lib64/libosgUtild.so     \
-DOSGVIEWER_LIBRARY=/usr/local/lib64/libosgViewerd.so \
-DOSG_LIBRARY=/usr/local/lib64/libosgd.so             \
../ifcplusplus/


make -j 4


sudo make install


# get 210_King_Merged.ifc
wget "http://www.digital210king.org/downloader.php?file=24"
mv "downloader.php?file=24" 10-King-IFC-Merged__2012-02-07.zip
unzip 10-King-IFC-Merged__2012-02-07.zip  # we hopefully get the file:  210_King_Merged.ifc
rm 10-King-IFC-Merged__2012-02-07.zip

# open kings file with viewer
SimpleViewerExampled 210_King_Merged.ifc
