# **************************************************************************
# copy for my FreeCAD dev instance
import sys
sys.path.append('/home/hugo/Documents/dev/freecad/freecadbhb_dev/build/lib')
import FreeCAD


# should output
'''
>>> 
>>> import sys
>>> sys.path.append('/home/hugo/Documents/dev/freecad/freecadbhb_dev/build/lib')
>>> import FreeCAD
FreeCAD 0.19, Libs: 0.19R16653 (Git)
>>> 
>>> 


import Part
Part.makeBox(1,1,1).Volume


>>> 
>>> import Part
>>> Part.makeBox(1,1,1).Volume
0.9999999999999998
>>> 

