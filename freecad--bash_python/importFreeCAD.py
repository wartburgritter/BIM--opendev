# copy code in python terminal

import sys

# add path to FreeCAD.pyd
sys.path.append('C:/0_BHA_privat/progr/FreeCAD_0.19.xxxxx_Py3Qt5/bin')
if system() == "Linux":
    sys.path.append('/home/hugo/Documents/dev/freecad/freecadbhb_dev/build/lib')
elif system() == "Windows":
    sys.path.append('C:/0_BHA_privat/progr/FreeCAD_0.19.xxxxx_Py3Qt5/bin')
else:
    print("Not supported operating system.\n")

import FreeCAD
import Part
round(Part.makeBox(10, 10, 10).Volume, 2)


# https://forum.freecadweb.org/viewtopic.php?f=22&t=30364