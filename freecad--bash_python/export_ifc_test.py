import sys
from platform import system
if system() == "Windows":
    sys.path.append('C:/0_BHA_privat/progr/FreeCAD_0.19.xxxxx_Py3Qt5/bin')
    sys.path.append('C:/0_BHA_privat/progr/FreeCAD_0.19.xxxxx_Py3Qt5/bin/Lib/site-packages')
    ifcfilepath = "C:/column.ifc"
elif system() == "Linux":
    sys.path.append('/home/hugo/Documents/dev/freecad/freecadbhb_dev/build/lib')
    ifcfilepath = "/tmp/column.ifc"
else:
    print('not supported os')


import FreeCAD, Arch, importIFC
FreeCAD.newDocument("mydoc")
col = Arch.makeStructure(None, 400, 400, 2500)
col.Document.recompute()
importIFC.export([col], ifcfilepath)

