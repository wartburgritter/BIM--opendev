#!/bin/bash


# *******************************************
# cd ~ in script here returns home of root = /root
# folder Desktop and Documents do not exist before system setup
sudo rm -rf  build_fc
mkdir build_fc
cd build_fc
base_dir=`pwd`  # without spaces!


# *******************************************
# set up xubuntu desktop system with xfce und lightdm
sudo rm Ubuntu_1804_Bionic--desktop_setup.sh  # needed if file from earlier run exists already
wget https://gitlab.com/wartburgritter/BIM--opendev/raw/master/dev--vagrant/01_system_setups/Ubuntu_1804_Bionic--desktop_setup.sh
sudo bash Ubuntu_1804_Bionic--desktop_setup.sh


# *******************************************
cd $base_dir
# to view directory in browser use: https://github.com/berndhahnebach/FreeCAD_bhb/tree/vagrant/compile
scriptSource=https://raw.githubusercontent.com/berndhahnebach/FreeCAD_bhb/vagrant/compile/
scriptPackages=Linux_Xubuntu_1810_FreeCAD_019_Qt5_Py3.sh
sudo rm $scriptPackages

echo $scriptSource$scriptPackages
wget $scriptSource$scriptPackages
bash $scriptPackages
echo We are done!

# https://raw.githubusercontent.com/berndhahnebach/FreeCAD_bhb/vagrant/compile/Linux_Xubuntu_1810_FreeCAD_019_Qt5_Py3.sh
