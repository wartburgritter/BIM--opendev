#!/bin/bash


# *******************************************
# username on a Buster vagrant box = vagrant
# cd ~ in script here returns home of root = /root
# folder Desktop and Documents do not exist before system setup
sudo rm -rf  build_ifcpp
mkdir build_ifcpp
cd build_ifcpp
base_dir=`pwd`  # without spaces!


# get ifcplusplus, patched version from bernd
cd $base_dir
mkdir ifcpp
cd ifcpp
# git clone https://github.com/berndhahnebach/ifcplusplus/  ifcpp
git clone https://github.com/berndhahnebach/ifcplusplus/ -b busterfix ifcpp


# building ifcplusplus
cd $base_dir
cd ifcpp
mkdir build
cd build
rm -rf *
cmake  -DCMAKE_BUILD_TYPE=Debug  ../ifcpp
make -j2

sudo make install
cd $base_dir
printf '\n\nexport LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH' >> /home/vagrant/.bashrc  # libcarve is not found
