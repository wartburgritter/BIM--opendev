#!/bin/bash


# Released under GPL v2.0
# bernd@bimstatik.org
# Buster--ifcplusplus.sh

# tested on Debian Buster = 10.0
# a Buster Desktopsystem should have been set up


# cd ~  # on a local machine

# sudo rm -rf build_ifcpp
mkdir build_ifcpp
cd build_ifcpp
base_dir=`pwd`
cd $base_dir


# *******************************************
echo START THE IfcPlusPlus JOURNEY !!!
echo ---------------------------------


# *******************************************
# update and upgrade system
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y
sudo apt-get update
sudo apt-get clean


# *******************************************
packages_build="        \
    git                 \
    cmake               \
    g++                 \
    libboost-all-dev    \
    "
sudo apt-get install -y $packages_build

# Qt5
packages_qt5="          \
    qt5-qmake           \
    libqt5widgets5      \
    libqt5opengl5-dev   \
    qttools5-dev        \
    qtbase5-dev         \
    "
sudo apt-get install -y $packages_qt5

# Ifcpp
packages_ifcpp="                \
    libopenscenegraph-3.4-dev   \
    "
sudo apt-get install -y $packages_ifcpp
# libopenscenegraph-3.4-dev is linked against qt5
# the following seams no longer needed
#    libopenthreads-dev          \


# *******************************************
# update system
sudo apt-get update
sudo apt-get clean


# *******************************************
# get ifcplusplus, patched version from bernd
cd $base_dir
mkdir ifcpp
cd ifcpp
git clone https://github.com/berndhahnebach/ifcplusplus/  ifcpp

# building ifcplusplus
cd $base_dir
cd ifcpp
mkdir build
cd build
rm -rf *
cmake  ../ifcpp
# cmake  -DCMAKE_BUILD_TYPE=Debug  ../ifcpp  # debugbuild
make -j2


# install ifcplusplus
sudo make install
# libcarve is not found, we add the library path to .bashrc in user home dir
# to find the lib the following could be used
# sudo find / -type f -name "libcarved.so"
cd $base_dir


# *******************************************
echo -----------------------------
echo END THE IFCPlusPlus JOURNEY !!!


# *******************************************
# get Schependomlaan.ifc
cd $base_dir
wget -O Schependomlaan.ifc "https://raw.githubusercontent.com/openBIMstandards/DataSetSchependomlaan/master/Design%20model%20IFC/IFC%20Schependomlaan.ifc"
SimpleViewerExample $base_dir/Schependomlaan.ifc


# *******************************************
# https://forum.freecadweb.org/viewtopic.php?f=4&t=24115&p=232676#p232676
# export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
# be careful, FreeCAD might not run properly, see
# https://forum.freecadweb.org/viewtopic.php?f=4&t=40201
# ATM IFC++ runs without ... so it is deactivated
# printf '\n\nexport LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH' >> /home/vagrant/.bashrc  # libcarve is not found
