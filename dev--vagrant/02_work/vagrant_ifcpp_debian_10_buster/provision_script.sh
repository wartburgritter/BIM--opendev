#!/bin/bash


# *******************************************
# username on a Buster vagrant box = vagrant
# standard for Bernd, the scripts are executed by user vagrant
# cd ~  


# *******************************************
# set up Buster desktop system with xfce und lightdm
sudo rm Debian_10_Buster--desktop_setup_scipt.sh  # needed if file from earlier run exists already
wget https://gitlab.com/wartburgritter/BIM--opendev/raw/master/dev--vagrant/01_system_setups/Debian_10_Buster--desktop_setup_scipt.sh
sudo bash Debian_10_Buster--desktop_setup_scipt.sh


# *******************************************
cd ~
scriptPackages=Buster--ifcplusplus.sh
sudo rm $scriptPackages

scriptSource=https://gist.githubusercontent.com/berndhahnebach/fb9b3c69c7de0835836cc27db6043e33
scriptRaw=/raw/
echo $scriptSource$scriptRaw$scriptPackages

wget $scriptSource$scriptRaw$scriptPackages
bash $scriptPackages
echo We are done!
