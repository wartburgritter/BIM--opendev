#!/bin/bash

# Released under GPL v2.0
# bernd@bimstatik.org
# Fedora30--ifcplusplus.sh

# tested on Fesora 30
# a Fedora Desktopsystem should have been set up

# ***************************************************************************
# packages for compiling ifcpp
sudo yum -y update
sudo dnf -y install git.x86_64
sudo dnf -y install gcc-c++.x86_64
sudo dnf -y install cmake.x86_64
sudo dnf -y install boost-devel.x86_64
sudo dnf -y install qt5-devel
sudo dnf -y install OpenSceneGraph-devel.x86_64 OpenSceneGraph-qt-devel.x86_64
sudo yum -y update


cd ~  # on a local machine, or if provision is run as user vagrant
# sudo rm -rf build_ifcpp
mkdir build_ifcpp
cd build_ifcpp
git clone https://github.com/berndhahnebach/ifcplusplus/  ifcpp
mkdir build
cd build
cmake  ../ifcpp
## cmake  -DCMAKE_BUILD_TYPE=Debug  ../ifcpp  # debugbuild
make -j2
