#!/bin/bash

# set up the desktop
sudo yum -y update
sudo dnf -y group install "lxqt-desktop-environment" 
sudo yum -y update

# https://docs.fedoraproject.org/en-US/quick-docs/switching-desktop-environments/
# dnf grouplist -v

# Additional to this you have to tell the system 
# start automatically in GUI Environment because for default start in CLI mode:
# vagrant ssh
# sudo systemctl set-default graphical.target
# exit
# vagrant reload


