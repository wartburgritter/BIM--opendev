#!/bin/bash

# Released under GPL v2.0
# bernd@bimstatik.org

# tested on Ubuntu Bionic = 18.04
# username on my vagrant box = ubuntu

# cd ~            # use if start on a local machine or inside a virtual machine
cd /home/ubuntu   # use with a vagrant box, because cd ~ returns  /root

cd Documents
sudo rm -rf build_xc
mkdir build_xc
cd build_xc
base_dir=`pwd`


##########################################################
# update system
sudo apt-get update
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y


##########################################################
# get Dependencies do not forget DoNotAsk on depencies install ... 
wget https://raw.githubusercontent.com/xcfem/xc/master/install/packages_install_ubuntu_bionic.sh
sudo bash packages_install_ubuntu_bionic.sh DoNotAsk
sudo apt-get update
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y


##########################################################
# clone, cmake, install, copy modules, run unit tests
wget https://raw.githubusercontent.com/xcfem/xc/master/install/xc_code_install.sh
bash xc_code_install.sh
