#!/bin/bash


# *******************************************
# set up Buster desktop system with xfce und lightdm
sudo rm Ubuntu_1810_Cosmic--desktop_setup.sh  # needed if file from earlier run exists already
wget https://gitlab.com/wartburgritter/BIM--opendev/raw/master/dev--vagrant/01_system_setups/Ubuntu_1810_Cosmic--desktop_setup.sh
bash Ubuntu_1810_Cosmic--desktop_setup.sh


# *******************************************
# username on this Cosmic vagrant box = vagrant
# thus we explizit use /home/vagrant, because cd ~ returns home of root = /root
cd /home/vagrant


# *******************************************
# to view directory in browser use: https://github.com/berndhahnebach/FreeCAD_bhb/tree/vagrant/compile
scriptSource=https://raw.githubusercontent.com/berndhahnebach/FreeCAD_bhb/vagrant/compile/
scriptPackages=Linux_Ubuntu_1810_Cosmic_FreeCAD_018_Qt5_Py3.sh
sudo rm $scriptPackages

echo $scriptSource$scriptPackages
wget $scriptSource$scriptPackages
bash $scriptPackages
echo We are done!

# https://raw.githubusercontent.com/berndhahnebach/FreeCAD_bhb/vagrant/compile/Linux_Ubuntu_1810_Cosmic_FreeCAD_018_Qt5_Py3.sh
