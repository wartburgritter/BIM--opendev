#!/bin/bash


# Released under GPL v2.0
# bernd@bimstatik.org
# Buster--ifcplusplus.sh

# tested on Debian Buster = 10.0
# a Buster Desktopsystem should have been set up


# cd ~  # on a local machine

# sudo rm -rf build_ifcpp
mkdir build_ifcos
cd build_ifcos
base_dir=`pwd`
cd $base_dir


# *******************************************
echo START THE IfcOpenShell JOURNEY !!!
echo ----------------------------------


# *******************************************
# update and upgrade system
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y
sudo apt-get update
sudo apt-get clean


# *******************************************
packages_build="        \
    git                 \
    cmake               \
    g++                 \
    libboost-all-dev    \
    "
sudo apt-get install -y $packages_build


# *******************************************
# IfcOpenShell
sudo apt-get install -y libicu-dev
sudo apt-get install -y libghc-text-icu-dev
sudo apt-get install -y swig
sudo apt-get install -y python3-all-dev
packages_oce="                      \
    liboce-foundation-dev:amd64     \
    liboce-modeling-dev:amd64       \
    liboce-ocaf-dev:amd64           \
    liboce-visualization-dev:amd64  \
    liboce-ocaf-lite-dev:amd64      \
    " # brauch es evtl. nicht alle
sudo apt-get install -y $packages_oce


# *******************************************
# update system
sudo apt-get update
sudo apt-get clean


# *******************************************
# ifcos
cd $base_dir
git clone https://github.com/IfcOpenShell/IfcOpenShell -b v0.6.0 ifcos

rm -rf build
mkdir build
cd build
cmake \
-DOCC_INCLUDE_DIR=/usr/include/oce \
-DOCC_LIBRARY_DIR=/usr/lib/x86_64-linux-gnu \
-DPYTHON_INCLUDE_DIR=/usr/include/python3.7 \
-DPYTHON_LIBRARY=/usr/lib/python3.7/config-3.7m-x86_64-linux-gnu/libpython3.7.so \
-DCOLLADA_SUPPORT=False \
-DIFCXML_SUPPORT=False \
-DBUILD_IFCPYTHON=True \
-DBUILD_EXAMPLES=True \
../ifcos/cmake

make -j 2
sudo make install
cd $base_dir


# *******************************************
cd $base_dir
rm acad2010_objects.ifc
wget https://raw.githubusercontent.com/IfcOpenShell/files/2abd02c/acad2010_objects.ifc
python3 ./ifcostest.py
