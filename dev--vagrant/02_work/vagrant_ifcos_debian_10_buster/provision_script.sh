#!/bin/bash


# *******************************************
# username on a Buster vagrant box = vagrant
# cd ~ in script here returns home of root = /root
# folder Desktop and Documents do not exist before system setup
sudo rm -rf  build_ifos
mkdir build_ifos
cd build_ifos
base_dir=`pwd`  # without spaces!


# *******************************************
# set up Buster desktop system with xfce und lightdm
sudo rm Debian_10_Buster--desktop_setup_scipt.sh  # needed if file from earlier run exists already
wget https://gitlab.com/wartburgritter/BIM--opendev/raw/master/dev--vagrant/01_system_setups/Debian_10_Buster--desktop_setup_scipt.sh
sudo bash Debian_10_Buster--desktop_setup_scipt.sh


# *******************************************
cd $base_dir
scriptPackages=Buster--ifcopenshell.sh
sudo rm $scriptPackages

scriptSource=https://gist.githubusercontent.com/berndhahnebach/05c5c3b095305dcd636d5d9f9c708516
scriptRaw=/raw/
echo $scriptSource$scriptRaw$scriptPackages

wget $scriptSource$scriptRaw$scriptPackages
bash $scriptPackages
echo We are done!
