#!/bin/bash


# *******************************************
# set up Buster desktop system with xfce und lightdm
sudo rm Debian_10_Buster--desktop_setup_scipt.sh  # needed if file from earlier run exists already
wget https://gitlab.com/wartburgritter/BIM--opendev/raw/master/dev--vagrant/01_system_setups/Debian_10_Buster--desktop_setup_scipt.sh
sudo bash Debian_10_Buster--desktop_setup_scipt.sh


# *******************************************
# username on a Buster vagrant box = vagrant
# thus we explizit use /home/vagrant, because cd ~ returns home of root = /root
cd /home/vagrant


# *******************************************
# to view directory in browser use: https://github.com/berndhahnebach/FreeCAD_bhb/tree/vagrant/compile
scriptSource=https://raw.githubusercontent.com/berndhahnebach/FreeCAD_bhb/vagrant/compile/
scriptPackages=Linux_Debian_10_Buster_FreeCAD_018_Qt4_Py2.sh
sudo rm $scriptPackages

echo $scriptSource$scriptPackages
wget $scriptSource$scriptPackages
bash $scriptPackages
echo We are done!

# https://raw.githubusercontent.com/berndhahnebach/FreeCAD_bhb/vagrant/compile/Linux_Debian_10_Buster_FreeCAD_018_Qt4_Py2.sh
