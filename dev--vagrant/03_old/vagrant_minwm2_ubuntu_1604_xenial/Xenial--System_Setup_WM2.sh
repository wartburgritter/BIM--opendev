#!/bin/bash

sudo apt-get update
sudo apt-get install -y wm2
sudo apt-get install -y lightdm


cat <<EOF >& /tmp/lightdm.conf
[SeatDefaults]
autologin-user=ubuntu
#pam-service=lightdm-autologin
EOF
sudo cp /tmp/lightdm.conf /etc/lightdm/
sudo /etc/init.d/lightdm start &


sudo apt-get install -y nedit    # editor
sudo apt-get install -y spacefm  # file manager


# autologin does not work with WM2, I do not know why !?!