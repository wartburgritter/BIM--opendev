#!/bin/bash

sudo apt-get update

# GUI
sudo apt-get install -y xfce4 xfce4-goodies
# sudo apt-get install -y xubuntu-default-settings  # das gibt es ganz sicher nicht auf jessie :-)
sudo apt-get install -y lightdm
sudo apt-get install -y libcanberra-gtk-module
sudo apt-get install -y libcanberra-gtk3-module libatk-bridge2.0-0 libatk-adaptor

sudo cat /etc/lightdm/lightdm.conf | sed 's/\#autologin-user=/autologin-user=vagrant/' > /tmp/lightdm.conf
sudo cp /tmp/lightdm.conf /etc/lightdm/lightdm.conf
sudo /etc/init.d/lightdm start


# spaeter testen, sind die pakete fuer xenial. auf jessie sind die eventuell anders !!!
# better user experience
# http://stackoverflow.com/questions/18878117/using-vagrant-to-run-virtual-machines-with-desktop-environment?answertab=votes#tab-top
#sudo apt-get install -y virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11
#sudo VBoxClient --clipboard
#sudo VBoxClient --draganddrop
#sudo VBoxClient --display
#sudo VBoxClient --checkhostversion
#sudo VBoxClient --seamless
