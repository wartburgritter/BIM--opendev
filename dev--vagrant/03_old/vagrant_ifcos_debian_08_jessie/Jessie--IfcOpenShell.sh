#!/bin/bash

# Released under GPL v2.0
# bernd@bimstatik.org

# tested on Debian Jessie = 8.0
# username on my vagrant box = vagrant


# *******************************************
# update and upgrade system
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y
sudo apt-get update
sudo apt-get clean


# *******************************************
packages_build="\
    git                 \
    cmake               \
    g++                 \
    libboost-all-dev    \
    "
apt-get install -y $packages_build


# *******************************************
# IfcOpenShell
apt-get install -y libicu-dev
sudo apt-get install -y libghc-text-icu-dev
sudo apt-get install -y swig
sudo apt-get install -y python-all-dev
sudo apt-get install -y liboce-foundation-dev:amd64 liboce-modeling-dev:amd64 liboce-ocaf-dev:amd64 liboce-visualization-dev:amd64 liboce-ocaf-lite-dev:amd64  # brauch ich evtl. auch nitch alle


# *******************************************
# update system
sudo apt-get update
sudo apt-get clean


# *******************************************
# cd ~            # use if start on a local machine or inside a virtual machine
cd /home/vagrant   # use with a vagrant box, because cd ~ returns  /root
cd Documents
sudo rm -rf  ifcos
mkdir ifcos
cd ifcos
git clone https://github.com/IfcOpenShell/IfcOpenShell

#rm -rf build
mkdir build
cd build
cmake ../IfcOpenShell/cmake  -DOCC_INCLUDE_DIR=/usr/include/oce  -DOCC_LIBRARY_DIR=/usr/lib/x86_64-linux-gnu  -DPYTHON_INCLUDE_DIR=/usr/include/python2.7  -DPYTHON_LIBRARY=/usr/lib/python2.7/config-x86_64-linux-gnu/libpython2.7.so  -DUNICODE_SUPPORT=True   -DCOLLADA_SUPPORT=False  -DUSE_IFC4=False  -DBUILD_IFCPYTHON=True  -DBUILD_EXAMPLES=False
make -j 2
sudo make install
