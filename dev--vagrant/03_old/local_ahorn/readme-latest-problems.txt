cd ~/Documents/build_FreeCAD/freecad/build/

cmake ../FreeCAD  -DCMAKE_INSTALL_PREFIX:PATH=/opt/local/FreeCAD-0.17  -DBUILD_FEM_NETGEN=1  -DCMAKE_CXX_FLAGS="-DNETGEN_V5"  -DNETGEN_ROOT=/opt/local/FreeCAD-0.17  -DFREECAD_USE_OCC_VARIANT="Official Version"  -DOCC_INCLUDE_DIR=/opt/local/FreeCAD-0.17/include/opencascade  -DOCC_LIBRARY=/opt/local/FreeCAD-0.17/lib/libTKernel.so
