#!/bin/bash

# Released under GPL v2.0
# bernd@bimstatik.org
# based on script for vagrant box from FreeCAD source, https://github.com/FreeCAD/FreeCAD/blob/master/vagrant/FreeCAD.sh

# tested on Debian Jessie = 8.0
# username on my vagrant box = vagrant

cd ~             # use if start on a local machine or inside a virtual machine
# cd /home/vagrant   # use with a vagrant box, because cd ~ returns  /root

cd Documents
# sudo rm -rf build_FreeCAD
mkdir build_FreeCAD
cd build_FreeCAD
base_dir=`pwd`
echo $base_dir


#  building VTK
cd $base_dir
cd vtk
mkdir build
cd build
cmake ../VTK-7.0.0 -DCMAKE_INSTALL_PREFIX:PATH=/opt/local/FreeCAD-0.17 -DVTK_Group_Rendering:BOOL=OFF -DVTK_Group_StandAlone:BOOL=ON -DVTK_RENDERING_BACKEND=None
make -j 2
sudo make install


# building OCCT
cd $base_dir
cd occt
mkdir build
cd build
cmake ../occt-b007701 -DCMAKE_INSTALL_PREFIX:PATH=/opt/local/FreeCAD-0.17 -DUSE_VTK:BOOL=OFF
make -j 2
sudo make install
sudo rm /opt/local/FreeCAD-0.17/bin/custom.sh  # file gives an error on script rerun, What is the file goot for?


# building Netgen
cd $base_dir
cd netgen
cd Netgen/netgen-5.3.1
./configure --prefix=/opt/local/FreeCAD-0.17 --with-tcl=/usr/lib/tcl8.5 --with-tk=/usr/lib/tk8.5  --enable-occ --with-occ=/opt/local/FreeCAD-0.17 --enable-shared --enable-nglib CXXFLAGS="-DNGLIB_EXPORTS -std=gnu++11"
make -j 2
sudo make install
# copy libsrc, FreeCAD needs it
cd $base_dir
cd netgen
sudo cp -rf Netgen/netgen-5.3.1/libsrc  /opt/local/FreeCAD-0.17/libsrc


# building FreeCAD
cd $base_dir
cd freecad
mkdir build
cd build

# no oce-dev packages, no netgen
# cmake ../FreeCAD -DCMAKE_INSTALL_PREFIX:PATH=/opt/local/FreeCAD-0.17 -DOCC_INCLUDE_DIR=/opt/local/FreeCAD-0.17/include/opencascade

# with oce-dev installed, no netgen
# cmake ../FreeCAD  -DCMAKE_INSTALL_PREFIX:PATH=/opt/local/FreeCAD-0.17  -DFREECAD_USE_OCC_VARIANT="Official Version"  -DOCC_INCLUDE_DIR=/opt/local/FreeCAD-0.17/include/opencascade  -DOCC_LIBRARY=/opt/local/FreeCAD-0.17/lib/libTKernel.so

# no oce-dev packages, with netgen
# cmake ../FreeCAD  -DCMAKE_INSTALL_PREFIX:PATH=/opt/local/FreeCAD-0.17  -DBUILD_FEM_NETGEN=1  -DCMAKE_CXX_FLAGS="-DNETGEN_V5"  -DNETGEN_ROOT=/opt/local/FreeCAD-0.17  -DOCC_INCLUDE_DIR=/opt/local/FreeCAD-0.17/include/opencascade

# with oce-dev installed, with netgen
cmake ../FreeCAD  -DCMAKE_INSTALL_PREFIX:PATH=/opt/local/FreeCAD-0.17  -DBUILD_FEM_NETGEN=1  -DCMAKE_CXX_FLAGS="-DNETGEN_V5"  -DNETGEN_ROOT=/opt/local/FreeCAD-0.17  -DFREECAD_USE_OCC_VARIANT="Official Version"  -DOCC_INCLUDE_DIR=/opt/local/FreeCAD-0.17/include/opencascade  -DOCC_LIBRARY=/opt/local/FreeCAD-0.17/lib/libTKernel.so

make -j 2
sudo make install



# start FreeCAD
# cd ~
# /opt/local/FreeCAD-0.17/bin/FreeCAD
