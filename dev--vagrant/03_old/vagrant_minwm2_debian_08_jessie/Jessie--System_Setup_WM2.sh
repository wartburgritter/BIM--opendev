#!/bin/bash

sudo apt-get update
sudo apt-get install -y wm2
sudo apt-get install -y lightdm

sudo cat /etc/lightdm/lightdm.conf | sed 's/\#autologin-user=/autologin-user=vagrant/' > /tmp/lightdm.conf
sudo cp /tmp/lightdm.conf /etc/lightdm/lightdm.conf
sudo /etc/init.d/lightdm start

sudo apt-get install -y nedit    # editor
sudo apt-get install -y spacefm  # file manager
sudo apt-get install -y dillo    # browser


# autologin will einfach nicht funktionieren mit wm2