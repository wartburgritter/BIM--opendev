#!/bin/bash

# Released under GPL v2.0
# bernd@bimstatik.org

# tested on Debian Buster = 10.0
# username on a Buster vagrant box = vagrant

# cd ~             # use if start on a local computer or inside a virtual machine
cd /home/vagrant   # use with a vagrant box, because cd ~ returns home of root = /root


# *******************************************
# set up Buster desktop system with xfce und lightdm
sudo rm Debian_10_Buster--desktop_setup_scipt.sh  # needed if file from earlier run exists already
wget https://gitlab.com/wartburgritter/BIM--opendev/raw/master/dev--vagrant/01_system_setups/Debian_10_Buster--desktop_setup_scipt.sh
sudo bash Debian_10_Buster--desktop_setup_scipt.sh


# *******************************************
cd Documents  # Documents exists only after desktop setup
#sudo rm -rf build_FC
#sudo rm -rf /opt/local/FreeCAD-0.17
mkdir build_FC
cd build_FC
base_dir=`pwd`
cd $base_dir


# *******************************************
echo START THE FreeCAD JOURNEY !!!
echo -----------------------------


# *******************************************
# update and upgrade system
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y
sudo apt-get update
sudo apt-get clean


# *******************************************
# source to download scripts
# to view directory in browser use: https://github.com/berndhahnebach/FreeCAD_bhb/tree/vagrant/compile
scriptSource=https://raw.githubusercontent.com/berndhahnebach/FreeCAD_bhb/vagrant/compile/


# *******************************************
# get Dependencies from distribution
cd $base_dir
scriptPackages=Linux_packages_Debian_10_Buster_Qt4Py2.sh
sudo rm $scriptPackages
echo $scriptSource$scriptPackages
wget $scriptSource$scriptPackages
bash $scriptPackages


# *******************************************
# update system
sudo apt-get update
sudo apt-get clean


# *******************************************
# get more needed Dependencies, get FreeCAD
cd $base_dir
scriptGetDependencies=Linux_dependencies_1_get_sources.sh
sudo rm $scriptGetDependencies
echo $scriptSource$scriptGetDependencies
wget $scriptSource$scriptGetDependencies
# bash $scriptGetDependencies

cd $base_dir
sudo rm -rf netgen

# *******************************************
# fix for building OCCT 7.2 the "xlocale" bug on Debian Buster (for others the link should not matter)
# https://forum.freecadweb.org/viewtopic.php?t=29523&start=10#p242476
sudo ln -s /usr/include/locale.h /usr/include/xlocale.h
# *******************************************
# compile and install these needed Dependencies
cd $base_dir
scriptCompileInstallDependencies=Linux_dependencies_2_compile_install.sh
sudo rm $scriptCompileInstallDependencies
echo $scriptSource$scriptCompileInstallDependencies
wget $scriptSource$scriptCompileInstallDependencies
# bash $scriptCompileInstallDependencies

cd $base_dir
sudo rm -rf netgen
sudo rm -rf ifcopenshell
sudo rm -rf occt
sudo rm -rf vtk


# *******************************************
# compile and install FreeCAD
cd $base_dir
scriptCompileInstallFreeCAD=Linux_FreeCAD_compile.sh
sudo rm $scriptCompileInstallFreeCAD
echo $scriptSource$scriptCompileInstallFreeCAD
wget $scriptSource$scriptCompileInstallFreeCAD
# bash $scriptCompileInstallFreeCAD


# *******************************************
# run FreeCAD unit test
#FreeCADCmd --run-test 0  # all
#FreeCADCmd --run-test "TestFem"


# *******************************************
echo -----------------------------
echo END THE FreeCAD JOURNEY !!!


# *******************************************
# get back to bas dir
cd $base_dir


# *******************************************
# delete build_FC (all git clone and all build dirs)
#cd $base_dir
#cd ..
#sudo rm -rf build_FC
