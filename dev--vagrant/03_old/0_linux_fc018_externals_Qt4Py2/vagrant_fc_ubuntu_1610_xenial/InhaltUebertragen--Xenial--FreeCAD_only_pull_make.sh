#!/bin/bash

# Ubuntu Xenial --> users name is ubuntu

# get freecad
cd /home/ubuntu/Documents/build_FreeCAD/freecad

# pull changes
cd FreeCAD
git pull


# make FreeCAD
cd /home/ubuntu/Documents/build_FreeCAD/freecad
# rm -rf build
# mkdir build
cd build

# cmake ../FreeCAD  -DCMAKE_INSTALL_PREFIX:PATH=/opt/local/FreeCAD-0.17  -DBUILD_FEM_NETGEN=1  -DCMAKE_CXX_FLAGS="-DNETGEN_V5"  -DNETGEN_ROOT=/opt/local/FreeCAD-0.17  -DFREECAD_USE_OCC_VARIANT="Official Version"  -DOCC_INCLUDE_DIR=/opt/local/FreeCAD-0.17/include/opencascade  -DOCC_LIBRARY=/opt/local/FreeCAD-0.17/lib/libTKernel.so
# ohne netgen, wichtig erst build directory cleanen wenn cmake netgen versucht wurde !, besser manuell in der virtuellen machine mit   rm -rf *
cmake ../FreeCAD  -DCMAKE_INSTALL_PREFIX:PATH=/opt/local/FreeCAD-0.17  -DFREECAD_USE_OCC_VARIANT="Official Version"  -DOCC_INCLUDE_DIR=/opt/local/FreeCAD-0.17/include/opencascade  -DOCC_LIBRARY=/opt/local/FreeCAD-0.17/lib/libTKernel.so

make -j 2
sudo make install

