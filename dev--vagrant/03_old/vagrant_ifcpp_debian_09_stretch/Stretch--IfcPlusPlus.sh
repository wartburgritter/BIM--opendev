#!/bin/bash


# *******************************************
# username on a Stretch vagrant box = vagrant
# cd ~ in script here returns home of root = /root
# folder Desktop and Documents do not exist before system setup
sudo rm -rf  build_ifcpp
mkdir build_ifcpp
cd build_ifcpp
base_dir=`pwd`  # without spaces!


# *******************************************
# set up Stretch desktop system with xfce und lightdm
sudo rm Debian_09_Stretch--desktop_setup_scipt.sh  # needed if file from earlier run exists already
wget https://gitlab.com/wartburgritter/BIM--opendev/raw/master/dev--vagrant/system_setups/Debian_09_Stretch--desktop_setup_scipt.sh
sudo bash Debian_09_Stretch--desktop_setup_scipt.sh


# *******************************************
cd $base_dir
# to view directory in browser use: https://gist.github.com/berndhahnebach/e4d57634fe7eebbae537bf3da1b1b95a
scriptSource=https://gist.githubusercontent.com/berndhahnebach/e4d57634fe7eebbae537bf3da1b1b95a/raw/24e3fe6f0a644952c7cb73b4dde4cf407734721d/
scriptPackages=Stretch--ifcplusplus.sh
sudo rm $scriptPackages

echo $scriptSource$scriptPackages
wget $scriptSource$scriptPackages
bash $scriptPackages
echo We are done!

# https://gist.githubusercontent.com/berndhahnebach/e4d57634fe7eebbae537bf3da1b1b95a/raw/24e3fe6f0a644952c7cb73b4dde4cf407734721d/Stretch--ifcplusplus.sh
