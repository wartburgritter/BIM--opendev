#!/bin/bash


# *******************************************
# username on a Stretch vagrant box = vagrant
# cd ~ in script here returns home of root = /root
# folder Desktop and Documents do not exist before system setup
sudo rm -rf  build_ifcpp
mkdir build_ifcpp
cd build_ifcpp
base_dir=`pwd`  # without spaces!


# *******************************************
# system setup
sudo rm Debian_09_Stretch--desktop_setup_scipt.sh  # needed if file from earlier run exists already
wget https://gitlab.com/wartburgritter/BIM--opendev/raw/master/dev--vagrant/system_setups/Debian_09_Stretch--desktop_setup_scipt.sh
sudo bash Debian_09_Stretch--desktop_setup_scipt.sh


# *******************************************
cd $base_dir


# *******************************************
# update and upgrade system
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y
sudo apt-get update
sudo apt-get clean


# *******************************************
packages_build="\
    git                 \
    cmake               \
    g++                 \
    libboost-all-dev    \
    "
sudo apt-get install -y $packages_build

# Openscenegraph
packages_osg="\
    libxrandr-dev       \
    libtiff5-dev        \
    libpoppler-glib-dev \
    librsvg2-dev        \
    libcairo2-dev       \
    libcurl4-gnutls-dev \
    libgtkglext1-dev    \
    libgdal-dev         \
    libsdl1.2-dev       \
    libgstreamer1.0-dev \
    "
sudo apt-get install -y $packages_osg

# Qt5
packages_qt5="\
    qt5-qmake           \
    libqt5widgets5      \
    libqt5opengl5-dev   \
    qttools5-dev        \
    qtbase5-dev         \
    "
sudo apt-get install -y $packages_qt5

# Ifcpp
packages_ifcpp="libopenthreads-dev "
sudo apt-get install -y $packages_ifcpp


# *******************************************
# update system
sudo apt-get update
sudo apt-get clean


# *******************************************
# get osg version 3.4
cd $base_dir
mkdir osg
cd osg
git clone https://github.com/openscenegraph/OpenSceneGraph -b OpenSceneGraph-3.4   osg34

# building osg
cd $base_dir
cd osg
mkdir build
cd build
rm -rf *
cmake -DCMAKE_BUILD_TYPE=Release  ../osg34
# cmake -DCMAKE_BUILD_TYPE=Debug  ../osg34
sudo make install_ld_conf  # needed to find the libs, see cmake run of osg
make -j 4
sudo make install_ld_conf  # needed to find the libs, see cmake run of osg
sudo make install
sudo make install_ld_conf  # needed to find the libs, see cmake run of osg


# *******************************************
# get ifcplusplus, patched version from bernd
cd $base_dir
mkdir ifcpp
cd ifcpp
git clone https://github.com/berndhahnebach/ifcplusplus/  ifcplusplus

# building ifcplusplus
cd $base_dir
cd ifcpp
mkdir build
cd build
rm -rf *
cmake -DCMAKE_LIBRARY_PATH=/usr/local/lib64 ../ifcplusplus
# cmake  -DCMAKE_BUILD_TYPE=Debug -DCMAKE_LIBRARY_PATH=/usr/local/lib64 ../ifcplusplus  # debugbuild
make -j 4

sudo make install
cd $base_dir
echo 'export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH' >> /home/vagrant/.bashrc  # libcarve is not found


# *******************************************
# get 210_King_Merged.ifc
# in Mai 2018 the site was offline
# cd $base_dir
# wget "http://www.digital210king.org/downloader.php?file=24"
# mv "downloader.php?file=24" 10-King-IFC-Merged__2012-02-07.zip
# unzip 10-King-IFC-Merged__2012-02-07.zip  # we hopefully get the file:  210_King_Merged.ifc
# rm 10-King-IFC-Merged__2012-02-07.zip

# open kings file with viewer, does not work through vagrant
# SimpleViewerExampled 210_King_Merged.ifc
# SimpleViewerExampled $base_dir/ifcplusplus/IfcOpenHouse.ifc

# https://forum.freecadweb.org/viewtopic.php?f=4&t=24115&p=232676#p232676
# export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
