#!/bin/bash

# Released under GPL v2.0
# bernd@bimstatik.org

# tested on Debian Stretch
# username on my Stretch vagrant box = vagrant

# cd ~             # use if start on a local computer or inside a virtual machine
cd /home/vagrant   # use with a vagrant box, because cd ~ returns home of root = /root

cd Documents
cd build_ifcpp
base_dir=`pwd`



### INFO mit osg 3.4 sollte es auch ohne osg zu uebersetzen funktionieren !!!!


# get osg version 3.4
cd $base_dir
mkdir osg
cd osg
git clone https://github.com/openscenegraph/OpenSceneGraph -b OpenSceneGraph-3.4   osg34


# building osg
cd $base_dir
cd osg
mkdir build
cd build
rm -rf *
cmake -DCMAKE_BUILD_TYPE=Release  ../osg34
# cmake -DCMAKE_BUILD_TYPE=Debug  ../osg34
sudo make install_ld_conf  # needed to find the libs, see cmake run of osg
make -j 4
sudo make install_ld_conf  # needed to find the libs, see cmake run of osg
sudo make install
sudo make install_ld_conf  # needed to find the libs, see cmake run of osg


# get ifcplusplus, patched version from bernd
cd $base_dir
mkdir ifcpp
cd ifcpp
git clone https://github.com/berndhahnebach/ifcplusplus/  ifcplusplus


# building ifcplusplus
cd $base_dir
cd ifcpp
mkdir build
cd build
rm -rf *
cmake  -DCMAKE_BUILD_TYPE=Debug -DCMAKE_LIBRARY_PATH=/usr/local/lib64 ../ifcplusplus
make -j 4

sudo make install

