#!/bin/bash

# Released under GPL v2.0
# bernd@bimstatik.org

# tested on Debian Stretch
# username on my Stretch vagrant box = vagrant

# cd ~             # use if start on a local computer or inside a virtual machine
cd /home/vagrant   # use with a vagrant box, because cd ~ returns home of root = /root

cd Documents
sudo rm -rf build_xc
mkdir build_xc
cd build_xc
base_dir=`pwd`


##########################################################
# update system
sudo apt-get update
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y


##########################################################
# add nonfree to sources.list
cat /etc/apt/sources.list | sed 's/main/main non-free/' > /tmp/sources.list
# TODO we may have multiple non-free in our sources.list after a few runs of the scipt ...
sudo cp /tmp/sources.list  /etc/apt/sources.list
sudo apt-get update


##########################################################
# get Dependencies do not forget DoNotAsk on depencies install ... 
wget https://raw.githubusercontent.com/xcfem/xc/master/install/packages_install_debian_stretch.sh
sudo bash packages_install_debian_stretch.sh DoNotAsk
sudo apt-get update
sudo apt-get dist-upgrade -y
sudo apt-get autoremove -y


##########################################################
# clone, cmake, install, copy modules, run unit tests
wget https://raw.githubusercontent.com/xcfem/xc/master/install/xc_code_install.sh
bash xc_code_install.sh
