#!/bin/bash

sudo apt-get update
sudo apt-get install -y xfce4 xfce4-goodies
sudo apt-get install -y xubuntu-default-settings
sudo apt-get install -y lightdm
sudo apt-get install -y libcanberra-gtk-module
sudo apt-get install -y libcanberra-gtk3-module libatk-bridge2.0-0 libatk-adaptor

sudo apt-get install -y overlay-scrollbar-gtk2 unity-gtk-module-common unity-gtk2-module  # ubuntu only


cat <<EOF >& /tmp/lightdm.conf
[SeatDefaults]
user-session=xfce
autologin-session=xfce
autologin-user=ubuntu
autologin-user-timeout=0
greeter-session=xfce
pam-service=lightdm-autologin
EOF
sudo cp /tmp/lightdm.conf /etc/lightdm/
sudo rm /etc/lightdm/lightdm-gtk-greeter.conf
sudo /etc/init.d/lightdm start &


# better user experience
# http://stackoverflow.com/questions/18878117/using-vagrant-to-run-virtual-machines-with-desktop-environment?answertab=votes#tab-top
sudo apt-get install -y virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11
sudo VBoxClient --clipboard
sudo VBoxClient --draganddrop
sudo VBoxClient --display
sudo VBoxClient --checkhostversion
sudo VBoxClient --seamless


# default settup anklicken nicht empty panel
# shut down mit gui in boxbox
# wieder vagrant up
# nochmal vagrant provision

# aber in vejmaries orginal automatischer reboot ?!